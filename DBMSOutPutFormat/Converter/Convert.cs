﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBMS.Formatter.Convert
{
    public static class Convert
    {

        public static String ConvertToExcellPQLFormat(System.String rowString)
        {
           String[] lines = rowString.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            String resString = String.Empty;


            Int32 count = 0;
            for (System.Int32 i = 0; i < lines.Length; ++i)
                if (lines[i] != String.Empty)
                    count++;

            String[] resLines = new String[count];

            for (System.Int32 i = 0; i < lines.Length; ++i)
                if (lines[i] != String.Empty)
                    resLines[i] = lines[i];


            String columnNames = resLines[1];
            columnNames = columnNames.Replace("|", ",");
              String rowName = String.Empty;
            String data = String.Empty;

            for(System.Int32 i = 2; i < resLines.Length; ++i)
                data += (resLines[i]).Replace("|", ",");


            Int32 countRow = 0;
            for (System.Int32 i = 2; i < resLines.Length; ++i, ++countRow)
                rowName += countRow.ToString() + ",";



            for (System.Int32 i = columnNames.Length - 1; i >= 0; --i)
                if (columnNames[i] == ',')
                {
                    columnNames = columnNames.Remove(i, 1);
                    break;
                }


            for (System.Int32 i = rowName.Length - 1; i >= 0; --i)
                if (rowName[i] == ',')
                {
                    rowName = rowName.Remove(i, 1);
                    break;
                }


            for (System.Int32 i = data.Length-1; i >= 0; --i)
                if (data[i] == ',')
                {
                    data = data.Remove(i, 1);
                    break;
                }

            data = data.Trim();
            columnNames = columnNames.Trim();
            rowName = rowName.Trim();

            resString = (columnNames) + ":" + (rowName) + ":" + (data);

            return resString;



        }

    }
}
