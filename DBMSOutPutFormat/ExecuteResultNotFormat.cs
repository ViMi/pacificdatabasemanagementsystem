﻿using DBMS.Core.InfoTypes;
using DBMS.Core.Interprater;
using DBMS.Core.IO;
using DBMS.Core.Table;
using System;
using DBMS.Core.TableCollections;
using DBMS.Core.Helpers;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using DBMS.Core.Helpers.Tests;

namespace DBMS.Formatter
{
    public sealed class ExecuteResultNotFormat
        : IExecuteResult
    {


        #region Documentaiton
#if DocTrue
        static ExecuteResultNotFormat() => Documentation.AddHistoryToSection(
                  PQLDocGlobal.OutPutFormatters,
            PQLDocGlobal.OutPutTypes,
            "ExecuteResultTable",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "IExecuteResult"

            );
#endif
        #endregion




        public ITransactionMethodResult _CurrentTI { set; get; }

        private System.String _ExecuteResult = System.String.Empty;

        public void Reset() => _ExecuteResult = String.Empty;

        public System.Object Clone() => new ExecuteResultNotFormat();

        public System.String ExecuteResult { 
            set {   _ExecuteResult = value;}
            get {return _ExecuteResult;} 
        }

        public void Append(string value) { }


        public string GetExecuteResult() => this.ExecuteResult;

        public void SetExecuteResult(string executeResult) => this.ExecuteResult = executeResult;

        public void Append(object obj)
        {
            ITransactionMethodResult res = obj as ITransactionMethodResult;
            _CurrentTI = res;

            if (res.IsSuccess && !DBMSIO.VIEW_TABLE)
            {
                _ExecuteResult += res.ResultString + Environment.NewLine;

            //    String header = String.Empty;
              //  String resStr = String.Empty;

              //  ITable col = res.AddedInfo as ITable;

                /*
                if (col != null)
                {


                    #region Max Column Size Calculation
                    Dictionary<String, Int32> maxColl = new Dictionary<String, Int32>();

                    for (System.Int32 i = 0; i < col.TableRow.Count; ++i) {
                        TableRow tr = col.TableRow.ElementAt(i).Value;

                        for (System.Int32 j = 0; j < tr.RowCells.Count; ++j) {

                            TableCell tc = tr.RowCells.ElementAt(j);
                            if (maxColl.ContainsKey(tc.Alias))
                            {
                                    Int32 size = 0;
                                    maxColl.TryGetValue(tc.Alias, out size);

                                    if (size < tc.ColumnData.Length || size < tc.Alias.Length) 
                                        maxColl.Remove(tc.Alias);
                                    else if (tc.ColumnData.Length > tc.Alias.Length)
                                         maxColl.Add(tc.Alias, tc.ColumnData.Length);
                            }
                            else maxColl.Add(tc.Alias, tc.Alias.Length);

                        }
                    }
                    #endregion



                    #region Header Calculation
                    String headStr = String.Empty;
                    if (col.TableRow.Count == 0)
                    {
                        _ExecuteResult += header + resStr;
                        return;
                    }
                    TableRow tr2 = col.TableRow.ElementAt(0).Value;


                    foreach (TableCell cell in tr2.RowCells)
                    {
                        headStr += cell.Alias;

                        Int32 size;
                        maxColl.TryGetValue(cell.Alias, out size);

                        for (System.Int32 i = headStr.Length; i <= size; ++i)
                            headStr += " ";

                        headStr += " | ";

                        header += headStr;
                        headStr = String.Empty;
                    }

                    header += Environment.NewLine;


                    #endregion





                    #region Body Calculation
                    String tmpData = String.Empty;

                    foreach (TableRow row in col.TableRow.Values)
                    {
                        foreach (TableCell cell in row.RowCells)
                        {
                            

                            if (cell?.ColumnData != null)
                                tmpData += cell.ColumnData.ToString();
                            else
                                tmpData += "NULL";



                            Int32 size;
                            maxColl.TryGetValue(cell.Alias, out size);

                            for (System.Int32 i = tmpData.Length; i <= size; ++i)
                                tmpData += " ";

                            tmpData += " | ";

                            resStr += tmpData;
                            tmpData = String.Empty;
                        }
                        

                        resStr += Environment.NewLine;
                    }
                    #endregion


                    _ExecuteResult += header + resStr;

                    _ExecuteResult += Environment.NewLine + "Passed Time :" + TestTimer.GetResultTime().ToString() + Environment.NewLine; 
                }
                */

            }
            else if(!DBMSIO.VIEW_TABLE)
                this.ExecuteResult += res.ResultString;
            

        }

        public object GetExecuteResultObject() { throw new System.NotImplementedException();}

        public void SetExecuteResult(object executeResult) { throw new System.NotImplementedException();}
    }
}
