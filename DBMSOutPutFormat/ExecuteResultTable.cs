﻿using System;
using DBMS.Core.Helpers;


using DBMS.Core.Interprater;
using DBMS.Core.InfoTypes;


namespace DBMS.Formatter
{
    /// <summary>
    /// This class contains logic about formating und execution Parse Result Objects
    /// </summary>
    public sealed class ExecuteResultTable 
        : IExecuteResult
    {


        #region Documentation
#if DocTrue
        static ExecuteResultTable()
        {
            Documentation.AddHistoryToSection(
                  PQLDocGlobal.OutPutFormatters,
            PQLDocGlobal.OutPutTypes,
            "ExecuteResultTable",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "IExecuteResult");


            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreGlobalInterfaces,
            "IExecuteResult",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined);

        }
#endif
        #endregion


        private System.String _ExecuteResult = System.String.Empty;
        public ITransactionMethodResult _CurrentTI { set; get; }

        public void Reset() => ExecuteResult = String.Empty;

        /// <summary>
        /// This property contains formatted string.
        /// This property can not be NULL
        /// </summary>
    //    [SQL.Types.Attributes.Nullable(false)]
        public System.String ExecuteResult { 
             set  {  _ExecuteResult = value; }
            get {  return _ExecuteResult;  } 
        
        }

        public void Append(string value) {  throw new NotImplementedException();}

        public void Append(object obj) { throw new NotImplementedException();}

        public object Clone(){  throw new NotImplementedException();}

        public string GetExecuteResult() { throw new NotImplementedException(); }

        public object GetExecuteResultObject(){  throw new NotImplementedException();}

        public void SetExecuteResult(string executeResult){ throw new NotImplementedException(); }

        public void SetExecuteResult(object executeResult) {  throw new NotImplementedException();}
    }
}
