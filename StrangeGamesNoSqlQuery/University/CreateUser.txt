Use.DataBase("University");

Grant.ToUser("TestUser").Right("Select-StudentView-[VIEW]");
Grant.ToUser("TestUser").Right("Update-StudentView-[VIEW]");
Grant.ToUser("TestUser").Right("Insert-StudentView-[VIEW]");
Grant.ToUser("TestUser").Right("Delete-StudentView-[VIEW]");

Grant.ToUser("TestUser").Right("Create-Procedure-[OBJ]");
Grant.ToUser("TestUser").Right("Drop-Procedure-[OBJ]");

Grant.ToUser("TestUser").Right("Execute-TestProcedure-[PROCEDURE]");