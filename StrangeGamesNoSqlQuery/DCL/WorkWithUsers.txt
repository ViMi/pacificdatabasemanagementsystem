Use.DataBase("ProjectInformation");

Grant.ToUser("TestUser").Rights("Select-State|Select-Tag");
Revoke.ToUser("TestUser").Rights("Select-State|Select-Tag");
Deny.ToUser("TestUser").Rights("Select-State|Select-Tag");