﻿using DBMS.Core.Helpers;




namespace DBMS.Redactor.Project
{
    public class AppFileContext
    {


#if DocTrue
        static AppFileContext() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.Redactor,
            PQLDocGlobal.RedactorIO,
            "AppFileContext",
            PQLDocGlobal.TypeClassDTO,
            PQLDocGlobal.DescriptionUndefined

            );
#endif


        public FileContext fileContext;
        public System.String Name { set; get; }


        public AppFileContext(FileContext file, System.String name)
        {
            this.fileContext = file;
            this.Name = name;
        }

    }
}
