﻿using DBMS.Core.Helpers;



namespace DBMS.Redactor.Project
{
    public class FileContext
    {

#if DocTrue
        static FileContext() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.Redactor,
            PQLDocGlobal.RedactorIO,
            "FileContext",
            PQLDocGlobal.TypeClassDTO,
            PQLDocGlobal.DescriptionUndefined

            );
#endif

        public System.String pathToFile { set; get; }
        public System.String FileName { set; get; }
        public System.String FileContent { set; get; }

        public FileContext()
        {

        }


        public FileContext(System.String fileContent, System.String path, System.String fileName)
        {
            this.FileContent = fileContent;
            this.pathToFile = path;
            this.FileName = fileName;
        }

    }

}
