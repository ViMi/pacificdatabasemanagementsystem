﻿using System.Collections.Generic;
using DBMS.Core.Helpers;



namespace DBMS.Redactor.Project
{
    public class RedactorContexts
    {

#if DocTrue
        static RedactorContexts() => Documentation.AddHistoryToSection(
             PQLDocGlobal.Redactor,
            PQLDocGlobal.RedactorIO,
            "RedactorContexts",
            PQLDocGlobal.TypeClassDTO,
            PQLDocGlobal.DescriptionUndefined


            );
#endif



        public List<FileContext> context = new List<FileContext>(10);
        public FileContext CurrentContext { set; get; }
    }
}
