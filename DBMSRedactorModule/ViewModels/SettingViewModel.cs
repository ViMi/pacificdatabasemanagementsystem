﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;


using DBMS.Redactor.Configuration.MinoreConfig;
using DBMS.Redactor.Configuration.InformationRes;
using DBMS.Integration.Windows;
using DBMS.Integration;
using DBMS.Integration.MicrosoftOffice.Excell;
using DBMS.Redactor.Project;
using Test.ViewModels.NotifyProperties;

using DBMS.Formatter;


namespace DBMS.Redactor.ViewModels
{

    /// <summary>
    /// This class present Setting window view model
    /// </summary>
    public class SettingViewModel 
        :NotifyEditorProperty
    {



        internal String _PrintText { set; get; } = String.Empty;
     
        public SettingViewModel()
        {


        ColumnWidth = "5000";
        ColumnGap =  "32" ;
        UndoLimit =  "100";
        PageHeight = "2250";
        PagePadding ="14";
        FontWeight = FontConfigurationInfoRes.TextFontWeight[0];
        FontStyle = FontConfigurationInfoRes.TextFontStyle[0];
        FontStretch = FontConfigurationInfoRes.TextFontStretch[0];
        SelectedTextAligmet = FontConfigurationInfoRes.TextAligment[0];
        LineHeight = "2";
        PageWidth = "500";
        SelectedTextFamily = (System.Windows.Media.FontFamily)Fonts.SystemFontFamilies.ElementAt(0);
        SelectedFontSize = (System.Double)FontConfigurationInfoRes.FontSizes[0];
        SelectedColors = FontConfigurationInfoRes.BrushesNames[1];
        BackgroundColor =FontConfigurationInfoRes.BrushesNames[1];
        PrinterIndex = 0;
        Configuration = new ConfigurationData();

        FormatterIndex = 0;



        }



        #region Variable Definition
        public ConfigurationData Configuration { set { this.configuration = value; OnPropertyChanged(); } get { return this.configuration; } }
        public ConfigurationData configuration;


        public System.String ColumnWidth { set { this.columnWidth = value; OnPropertyChanged(); } get { return this.columnWidth; } }
        public System.String columnWidth;

        public System.String ColumnGap { set { this.columnGap = value; OnPropertyChanged(); } get { return this.columnGap; } }
        public System.String columnGap;

        public System.String UndoLimit { set { this.undoLimit = value; OnPropertyChanged(); } get { return this.undoLimit; } }
        public System.String undoLimit;


        public System.String PageHeight { set { this.pageHeight = value; OnPropertyChanged(); } get { return this.pageHeight; } }
        public System.String pageHeight;


        public System.String PagePadding { set { this.pagePadding = value; OnPropertyChanged(); } get { return this.pagePadding; } }
        public System.String pagePadding;


        public System.String FontWeight { set { this.fontWeight = value; OnPropertyChanged(); } get { return this.fontWeight; } }
        public System.String fontWeight;


        public System.String FontStyle { set { this.fontStyle = value; OnPropertyChanged(); } get { return this.fontStyle; } }
        public System.String fontStyle;



        public System.String FontStretch { set { this.fontStretch = value; OnPropertyChanged(); } get { return this.fontStretch; } }
        public System.String fontStretch;


        public System.String SelectedTextAligmet { set { this.selectedTextAligmet = value; OnPropertyChanged(); } get { return this.selectedTextAligmet; } }
        public System.String selectedTextAligmet;


        public System.String LineHeight { set { this.lineHeight = value; OnPropertyChanged(); } get { return this.lineHeight; } }
        public System.String lineHeight;


        public System.String PageWidth { set { this.pageWidth = value; OnPropertyChanged(); } get { return this.pageWidth; } }
        public System.String pageWidth;



        public System.Windows.Media.FontFamily SelectedTextFamily { set { this.selectedTextFamily = value; OnPropertyChanged(); } get { return this.selectedTextFamily; } }
        public System.Windows.Media.FontFamily selectedTextFamily;


        public System.Double SelectedFontSize { set { this.selectedFontSize = value; OnPropertyChanged(); } get { return this.selectedFontSize; } }
        public System.Double selectedFontSize;


        public System.String SelectedColors { set { this.selectedColors = value; OnPropertyChanged(); } get { return this.selectedColors; } }
        public System.String selectedColors;


        public System.String BackgroundColor { set { this.backgroundColor = value; OnPropertyChanged(); } get { return this.backgroundColor; } }
        public System.String backgroundColor;


        public System.Byte PrinterIndex { set { this.printerIndex = value; OnPropertyChanged(); } get { return this.printerIndex; } }
        public System.Byte printerIndex;



        public System.Byte FormatterIndex { set { this.formatterIndex = value; OnPropertyChanged(); } get { return this.formatterIndex; } }
        public System.Byte formatterIndex;

        #endregion


        private Command textSettingSaveCommand;
        /// <summary>
        /// This property present Save Command
        /// </summary>
        public Command TextSettingSaveCommand
        {
            get
            {
                return textSettingSaveCommand ??
                    (textSettingSaveCommand = new Command(
                        
                        obj => {



                            ConfigurationData config = new ConfigurationData();

                            FontConfig fontConfig = new FontConfig();
                            fontConfig.ColumnGap = Double.Parse(this.ColumnGap);
                            fontConfig.ColumnWidth = Double.Parse(this.ColumnWidth);
                            fontConfig.LineHeight = Double.Parse(this.LineHeight);
                            fontConfig.PageHeight = Double.Parse(this.PageHeight);

                            Thickness thickness = new Thickness(Double.Parse(this.PagePadding));
                            fontConfig.PagePadding = thickness;

                            fontConfig.PageWidth = Double.Parse(this.PageWidth);
                            fontConfig.FontSize = this.SelectedFontSize;

                            Type type = typeof(FontStretches);
                            fontConfig.FontStrech = (System.Windows.FontStretch)type.GetProperty(this.FontStretch).GetValue(type, null);

                            type = typeof(System.Windows.FontStyles);
                            fontConfig.FontStyle = (System.Windows.FontStyle)(type.GetProperty(this.FontStyle)).GetValue(type, null);

                            type = typeof(System.Windows.Media.Colors);      
                            fontConfig.FontColor = new SolidColorBrush((System.Windows.Media.Color)type.GetProperty(this.SelectedColors).GetValue(type, null));



                            fontConfig.FontFamily = this.SelectedTextFamily;

                            config.FontConfig = fontConfig;


                            RedactorConfig redactorConfig = new RedactorConfig();

                            redactorConfig.AllowDrop = false;
                            redactorConfig.UndoLimit = System.Int16.Parse(this.UndoLimit);


                            type = typeof(System.Windows.Media.Colors);
                            redactorConfig.BackGroundBrush = new SolidColorBrush((System.Windows.Media.Color)type.GetProperty(this.BackgroundColor).GetValue(type));

                            config.RedactorConfig = redactorConfig;


                            Configuration = config;
  
                        }


                        ));
            }



        }


        /// <summary>
        /// This property contains General configuration command
        /// </summary>
        public Command GeneralCommand { set; get; }


        /// <summary>
        /// This propert present translation configuration command
        /// </summary>
        public Command TranslationCommand { set; get; }



        private Command printerCommand;
        /// <summary>
        /// This property present printer command
        /// </summary>
        public Command PrintCommand { 
            
            get
            {


                return printerCommand ??
                   (printerCommand = new Command(

                       obj => {

                           if (this.PrinterIndex == 1)
                               this._PrintText = Formatter.Convert.Convert.ConvertToExcellPQLFormat(this._PrintText);

                           if (((RedactorContexts)obj).CurrentContext != null)
                               this._Printers[this.PrinterIndex].Print(this._PrintText);
                           else
                               System.Windows.MessageBox.Show("No file in current context");
                           


                       }


                       ));


            }
        
        
        }






        private IPrinter[] _Printers = new IPrinter[2]
        {
            new WindowsPrinter(),
            new InputToExcell()
        };



        



        public System.String[] PrinterTypes
        {
            get
            {
                return (new System.String[2] { "System Printed", "Microsoft Word Excell Output"});
            }
        }






        public string[] FormatterTypes
        {
            get
            { 
                return (new string[1] { "Table Formatted OutPut" });
            }
        }




    }
}
