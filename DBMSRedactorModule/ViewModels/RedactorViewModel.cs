﻿using System;
using System.Windows;

using DBMS.Redactor.MDIStrategy;
using DBMS.Redactor.Project;
using DBMS.Core.Interprater;
using DBMS.Redactor.FileSystem;
using DBMS.Integration.JSON;
using DBMS.Core.IO;
using DBMS.PQL;
using DBMS.Formatter;
using DBMS.Core;
using Test.ViewModels.NotifyProperties;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.Journal;

using DBMS.Core.Helpers.Tests;

namespace DBMS.Redactor.ViewModels
{

    /// <summary>
    /// This class contains logic of redactor view model
    /// </summary>
    public class RedactorViewModel 
        : NotifyEditorProperty
    {

        /// <summary>
        /// This property contain setting window view model
        /// </summary>
        /// 

        #region Variable Declaration
        public SettingViewModel SettingViewModel { set; get; }

        public AuthorizationViewModel AuthorizationViewModel { set; get; }
        public RedactorContexts RedactorContextsCollection { set; get; }

        public RedactorTree _hieracrhy { set; get; }
        public MDIMaintein MDI { set; get; }




        public Visibility workAreaVisibility;
        public Visibility WorkAreaVisibility { set { this.workAreaVisibility = value; OnPropertyChanged(); } get { return this.workAreaVisibility; } }
        public String projectName;
        public String ProjectName { set { this.projectName = value; OnPropertyChanged(); } get { return this.projectName; } }
        public String userDescription;
        public String UserDescription { set { this.userDescription = value; OnPropertyChanged(); } get { return this.userDescription; } }
        public System.Windows.Controls.TreeViewItem[] navigationItems;
        public System.Windows.Controls.TreeViewItem[] NavigationItems { set { this.navigationItems = value; OnPropertyChanged(); } get { return this.navigationItems; } }
        public String consoleString;
        public String ConsoleString { set { this.consoleString = value; OnPropertyChanged(); } get { return this.consoleString; } }
        public String workAreaContent;
        public String WorkAreaContent { set { this.workAreaContent = value; OnPropertyChanged(); } get { return this.workAreaContent; } }
        
        public System.Boolean IsSaved = false;




        private System.String _currentDataBaseName;
        public System.String CurrentDataBaseName {set { this._currentDataBaseName = value; OnPropertyChanged();} get{return this._currentDataBaseName;}}


        System.String initRes = String.Empty;
        #endregion


        /// <summary>
        /// This constructor provade start initialization of Redactor view model
        /// </summary>
        public RedactorViewModel()
        {


            DBMSIO.DBMSConfig = new DBMSConfiguration();
          
            JSON json = new JSON();
            

            DBMSIO.IOIntegrationModule = json;
            
              initRes= DBMSIO.ServerCheck();


            DBMSIO.Logger = new StandartLogger(
                DBMSIO.DBMSConfig.BaseDir
            + DBMSIO.DBMSConfig.BaseSecurityDir
            + DBMSIO.DBMSConfig.PathToLog
            + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
            + DBMSIO.DBMSConfig.StandartLogFileName
            + DBMSIO.DBMSConfig.StandartLogFileExtension
            );


            DBMSIO.ServerJournal = new StandartJournal(
                     DBMSIO.DBMSConfig.BaseDir
            + DBMSIO.DBMSConfig.BaseSecurityDir
            + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
            + DBMSIO.DBMSConfig.BaseJournalDir
            + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
            + DBMSIO.DBMSConfig.BaseJournalFileName
            + DBMSIO.DBMSConfig.BaseJournalFileExtension
                );

            DBMSIO.RegisterNewEvent(DBMSStandartEventNames.ClearConsole, ClearConsoleString);

            CurrentDataBaseName = System.String.Empty;
            WorkAreaVisibility = (Visibility.Collapsed);
            ProjectName = "Projecct DBMS (Prototype)";
            UserDescription = "Group: PI-61";
            ConsoleString = "ConsoleString";
            WorkAreaContent = "WorkAreaContent";

            this._hieracrhy = new RedactorTree("[Server]", this);
            this.SettingViewModel = new SettingViewModel();
            this.RedactorContextsCollection = new RedactorContexts();
            this.AuthorizationViewModel = new AuthorizationViewModel();
   
        }


        public System.String GetServerInitCode() => initRes;



        public IExecuteResult[] ResultFormatter = new IExecuteResult[1]
        {
          new  ExecuteResultNotFormat()
        };




        private void ClearConsoleString()
        {
            ConsoleString = String.Empty;
            ResultFormatter[this.SettingViewModel.FormatterIndex].Reset();

        }





        private Command startTranslate; 
        /// <summary>
        /// This property present start tranlation command
        /// </summary>
        public Command StartTranslate
        {

            get
            {
                return startTranslate ??
                   (startTranslate = new Command(

                       obj =>
                       {

                           if (AuthorizationViewModel.IsAuthorized)
                           {

                               DBMSIO.IOIntegrationModule = new JSON();

                               DBMSIO.SetCurrentLogin(AuthorizationViewModel.User);
                               DBMSIO.SetGlobalExecuteStrategy(new NoSQLExecuter(new NoSQlExecuteSecureStrategy(), ResultFormatter[this.SettingViewModel.FormatterIndex]));
                               DBMSIO.SetGlobalParseConfiguration(new NoSQLParseModule(new NoSQLLanguageSecureStrategy(WorkAreaContent), WorkAreaContent));
                               ConsoleString = String.Empty;
                               ConsoleString = DBMSIO.Interpret(new NoSQLSecureStrategy());
                               this.SettingViewModel._PrintText = consoleString;

                           }
                           else
                               System.Windows.MessageBox.Show(ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.UNAUTHORIZED_ERROR]);
                       }


                       ));
            }

        }




        private Command openFile;
        /// <summary>
        /// This property present start tranlation command
        /// </summary>
        public Command OpenFile
        {

            get
            {
                return openFile ??(openFile = new Command(obj => this.MDI.AddNewMDIButton(IO.OpenFile()) )); 
            }

        }




        private Command closeFile;
        /// <summary>
        /// This property present start tranlation command
        /// </summary>
        public Command CloseFile
        {

            get
            {
                return closeFile ?? (closeFile = new Command(obj =>  this.MDI.CloseCurrentFile()));
            }

        }





        private Command saveFile;
        /// <summary>
        /// This property present start tranlation command
        /// </summary>
        public Command SaveFile
        {

            get
            {
                return saveFile ??
                   (saveFile = new Command( obj => this.MDI.SaveObserver(this.IsSaved) ));
            }

        }


        private Command createNew;
        /// <summary>
        /// This property present start tranlation command
        /// </summary>
        public Command CreateNew
        {

            get
            {
                return createNew ??
                   (createNew = new Command(

                       obj => {
                           IO.CreateNew((System.String)obj);
                           this.MDI.AddNewMDIButton
                           (IO.OpenFileByName
                           (Environment.CurrentDirectory 
                           + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                           + (System.String)obj + ".txt"));
                       }


                       ));
            }

        }







        private Command refreshDBInfo;
        /// <summary>
        /// This property present start tranlation command
        /// </summary>
        public Command RefreshDBInfo
        {

            get
            {
                return refreshDBInfo ??
                   (refreshDBInfo = new Command(

                       obj => {

                           
                           if (AuthorizationViewModel.IsAuthorized)
                             NavigationItems =
                               _hieracrhy.TryGetUserDBOMetaInformation
                               (AuthorizationViewModel.User);
                           else
                               System.Windows.MessageBox.Show(ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.UNAUTHORIZED_ERROR]);
                           
                       }


                       ));
            }

        }








        private Command visitDocumentationSite;
        /// <summary>
        /// This property present start tranlation command
        /// </summary>
        public Command VisitDocumentationSite
        {

            get
            {
                return visitDocumentationSite ??
                   (visitDocumentationSite = new Command( obj => DBMS.Integration.Community.VisitHelpPacificDBMSSite())); 
            }

        }







        private Command visitComunitySite;
        /// <summary>
        /// This property present start tranlation command
        /// </summary>
        public Command VisitComunitySite
        {

            get
            {
                return visitComunitySite ??
                   (visitComunitySite = new Command( obj => DBMS.Integration.Community.VisitPacificDBMSSite() ));
            }

        }


    }
}
