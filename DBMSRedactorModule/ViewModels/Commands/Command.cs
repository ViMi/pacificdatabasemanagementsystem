﻿using System;
using System.Windows.Input;
using DBMS.Core.Helpers;



namespace DBMS.Redactor.ViewModels
{

    /// <summary>
    /// This class contains Command which use in Pacific DBMS
    /// </summary>
    public sealed class Command 
        : ICommand
    {


#if DocTrue
        static Command()
        {
            Documentation.AddHistoryToSection(
                                        PQLDocGlobal.Redactor,
            PQLDocGlobal.RedactorIO,
            "Command",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "ICommand"

                );


            Documentation.AddHistoryToSection(PQLDocGlobal.Redactor,
            PQLDocGlobal.RedactorIO,
            "ICommand",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined
);
        }
#endif


        private Action<System.Object> _Execute;
        private Func<System.Object, System.Boolean> _CanExecute;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="execute"></param>
        /// <param name="canExecute"></param>
        public Command(Action<System.Object> execute, Func<System.Object, System.Boolean> canExecute = null)
        {
            _Execute = execute;
            _CanExecute = canExecute;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public bool CanExecute(System.Object parameter)=> _CanExecute == null || _CanExecute(parameter);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(System.Object parameter) => _Execute(parameter);

    }

}
