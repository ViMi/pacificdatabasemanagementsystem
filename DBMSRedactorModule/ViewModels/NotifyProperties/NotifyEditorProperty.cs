﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using DBMS.Core.Helpers;



namespace Test.ViewModels.NotifyProperties
{
    public class NotifyEditorProperty 
        : INotifyPropertyChanged
    {


        #region Documentation
#if DocTrue
        static NotifyEditorProperty()
        {
            Documentation.AddHistoryToSection(

                                        PQLDocGlobal.Redactor,
            PQLDocGlobal.RedactorIO,
            "NotifyEditorProperty",
            PQLDocGlobal.TypeClass,
            PQLDocGlobal.DescriptionUndefined,
            "INotifyPropertyChanged"

                );
        }
#endif
        #endregion


        /// <summary>
        /// This property notifice change in observable property
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;


        /// <summary>
        /// This method inform all observers when observe property changes
        /// </summary>
        /// <param name="prop"></param>
        public void OnPropertyChanged([CallerMemberName] System.String prop = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));





    }
}
