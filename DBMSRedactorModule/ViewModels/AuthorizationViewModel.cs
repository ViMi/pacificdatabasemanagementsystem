﻿using DBMS.Core;
using System;
using MessageBox = System.Windows.MessageBox;
using Test.ViewModels.NotifyProperties;
using DBMS.Core.IO;
using DBMS.Core.Helpers;

using DBMS.Core.ErrorControlSystem.Exeption;

namespace DBMS.Redactor.ViewModels
{
     public sealed class AuthorizationViewModel 
        : NotifyEditorProperty
    {



        #region Documentaiton
#if DocTrue
        static AuthorizationViewModel() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.Redactor,
            PQLDocGlobal.RedactorIO,
            "AuthorizationViewModel",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "NotifyEditorProperty"

            );
#endif
        #endregion


        public AuthorizationViewModel()
        {
           User = null;
           Login = System.String.Empty;
           Password = System.String.Empty;
           this.IsAuthorized = false;
        }


        public void Reset()
        {
            User = null;
            Login = null;
            Password = null;
        }


        public ILogin user;
        public ILogin User { set { this.user = value; OnPropertyChanged(); } get { return this.user; } }
        public System.String login = String.Empty;
        public System.String Login { set { this.login = value; OnPropertyChanged(); } get { return this.login; } }
        public System.String password = String.Empty;
        public System.String Password { set { this.password = value; OnPropertyChanged(); } get { return this.password; } }





        public System.Boolean IsAuthorized { set; get; }

        private Command _getLogin;

        /// <summary>
        /// This property present start tranlation command
        /// </summary>
        public Command GetLogin
        {

            get
            {
                return this._getLogin ??
                   (this._getLogin = new Command(

                       obj => {

                           try
                           {
                               if (this.Password == null)
                                   this.Password = String.Empty;

                               this.User = DBMSIO.GetLoginWithPassword(this.login, this.Password);
                               DBMSIO.ReadUserByLogin(this.User);
                               DBMSIO.SetCurrentLogin(this.User);
                               this.IsAuthorized = true;

  
                           }
                           catch (DBExeption e)
                           {
                               MessageBox.Show(e.ServerMessageError + Environment.NewLine);
                               this.IsAuthorized = false;
                           }
                           
                       }


                       ));
            }

        }

    }
}
