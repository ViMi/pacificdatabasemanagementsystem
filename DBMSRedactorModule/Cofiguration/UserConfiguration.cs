﻿using DBMS.Core.Helpers;



namespace DBMS.Redactor.Configuration.MinoreConfig
{
    public class UserConfiguration
    {

#if DocTrue
        static UserConfiguration() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.Redactor,
            PQLDocGlobal.RedactorIO,
            "UserConfiguration",
            PQLDocGlobal.TypeClassDTO,
            PQLDocGlobal.DescriptionUndefined
            );
#endif


        public string StyleConfig { set; get; }
        public string Localization { set; get; }

    }
}
