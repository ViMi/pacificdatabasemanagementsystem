﻿using DBMS.Core.Helpers;



namespace DBMS.Redactor.Configuration.MinoreConfig
{
    public class ConfigurationData 
    {

#if DocTrue
        static ConfigurationData() => Documentation.AddHistoryToSection(
              PQLDocGlobal.Redactor,
            PQLDocGlobal.RedactorIO,
            "ConfigurationData (Aggregat)",
            PQLDocGlobal.TypeClassDTO,
            PQLDocGlobal.DescriptionUndefined

            );
#endif

        public UserConfiguration UserConfiguration { set; get; }
        public FontConfig FontConfig { set; get; }
        public RedactorConfig  RedactorConfig { set; get; }
    }
}
