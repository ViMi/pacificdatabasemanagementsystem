﻿using DBMS.Core.Helpers;




namespace DBMS.Redactor.Configuration.MinoreConfig
{
    public class RedactorConfig
    {

#if DocTrue
        static RedactorConfig() => Documentation.AddHistoryToSection(
              PQLDocGlobal.Redactor,
            PQLDocGlobal.RedactorIO,
            "RedactorConfig",
            PQLDocGlobal.TypeClassDTO,
            PQLDocGlobal.DescriptionUndefined
            );
#endif

        public System.Int16 UndoLimit { set; get; }
        public System.Boolean AllowDrop { set; get; }
        public System.Windows.Media.SolidColorBrush SelectionBrush { set; get; }
        public System.Windows.Media.SolidColorBrush BackGroundBrush { set; get; }
        public FontConfig FontConfiguration { set; get; }

    }
}
