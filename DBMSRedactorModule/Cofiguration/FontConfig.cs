﻿using System.Windows;
using System.Windows.Media;
using DBMS.Core.Helpers;




namespace DBMS.Redactor.Configuration.MinoreConfig
{
    public class FontConfig
    {


#if DocTrue
        static FontConfig() => Documentation.AddHistoryToSection(
              PQLDocGlobal.Redactor,
            PQLDocGlobal.RedactorIO,
            "FontConfig",
            PQLDocGlobal.TypeClassDTO,
            PQLDocGlobal.DescriptionUndefined
            );
#endif



        public System.Double ColumnGap { set; get; }
        public System.Double ColumnWidth { set; get; }
        public TextAlignment TextAlignment { set; get; }

        public System.Double PageWidth { set; get; }
        public System.Double PageHeight { set; get; }
        public System.Double MinPageWidth { set; get; }
        public System.Double MaxPageWidth { set; get; }

        public System.Double MinPageHeight { set; get; }
        public System.Double MaxPageHeight { set; get; }

        public System.Double LineHeight { set; get; }
        public System.Double FontSize { set; get; }


        public Thickness PagePadding { set; get; }
        public FontWeight FontWeight { set; get; }
        public FontStyle FontStyle { set; get; }
        public FontStretch FontStrech { set; get; }
        public FontFamily FontFamily { set; get; }


        public System.Windows.Media.SolidColorBrush FontColor { set; get; }

    }
}
