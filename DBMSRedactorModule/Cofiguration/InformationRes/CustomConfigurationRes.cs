﻿using DBMS.Core.Helpers;



namespace DBMS.Redactor.Configuration.InformationRes
{
    public static class CustomConfigurationRes
    {

#if DocTrue
        static CustomConfigurationRes() => Documentation.AddHistoryToSection(
              PQLDocGlobal.Redactor,
            PQLDocGlobal.RedactorIO,
            "CustomConfigurationRes",
            PQLDocGlobal.TypeStaticClass,
            PQLDocGlobal.DescriptionUndefined
            );
#endif

        public static System.String[] StyleConfig
        {
            get { return (new System.String[2] { "StandartMMGThem", "StandartMMGThem_Dark" }); }

        }

        public static System.String[] Localization
        {
            get { return (new System.String[2] { "EN-en", "RU-ru" }); }

        }


    }
}
