﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;
using DBMS.Core.Helpers;



namespace DBMS.Redactor.Configuration.InformationRes
{
    public static class FontConfigurationInfoRes
    {


#if DocTrue
        static FontConfigurationInfoRes() => Documentation.AddHistoryToSection(
              PQLDocGlobal.Redactor,
            PQLDocGlobal.RedactorIO,
            "FontConfigurationInfoRes",
            PQLDocGlobal.TypeStaticClass,
            PQLDocGlobal.DescriptionUndefined

            );
#endif

        public static System.Double[] FontSizes
        {
            get
            {
                return (new System.Double[]    {
                                            3.0, 4.0, 5.0, 6.0, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0,
                                            9.5, 10.0, 10.5, 11.0, 11.5, 12.0, 12.5,13.0,13.5,
                                            14.0, 15.0,16.0, 17.0, 18.0, 19.0, 20.0, 22.0, 24.0,
                                            26.0, 28.0, 30.0,32.0, 34.0, 36.0, 38.0, 40.0, 44.0,
                                            48.0, 52.0, 56.0, 60.0, 64.0, 68.0, 72.0, 76.0,80.0,
                                            88.0, 96.0, 104.0, 112.0, 120.0, 128.0, 136.0, 144.0
                                       });
            }
        }


        public static System.String[] BrushesNames
        {

            get
            {



                HashSet<System.String> colorFilterSet = new HashSet<System.String>
                {
                    "R", "G", "B", "A", "Name", "IsKnownColor", "Transparent",

                };


                PropertyInfo[] fields = (typeof(System.Windows.Media.Colors).GetProperties());

                System.Int32 size = fields.Length - colorFilterSet.Count;

                System.String[] str = new System.String[size];



                for(System.Int32  i = 0; i < size; ++i)
                {

                    if (!colorFilterSet.Contains(fields[i].Name))
                    {
                        str[i] = fields[i].Name;
                    }
                }

             

                return str;
            }
        }


        


        public static System.String[] TextAligment
        {
            get
            {
                return Enum.GetNames((typeof(TextAlignment)));
            }
        }



        public static System.String[] TextFontStyle
        {
            get
            {
                return (new string[3] { "Normal", "Italic", "Obliqua" });
            }
        }




        public static System.String[] TextFontWeight
        {

            get
            {

                return (new System.String[5] { "Bold", "Normal", "SemiBold", "Black", "Ultra Bold" });

            }

        }




        public static System.String[] TextFontStretch
        {
            get
            {

                return (new System.String[10]
                {"Normal","Medium","Condensed","Expanded", "ExtraCondensed","ExtraExpanded", "SemiCondensed", "SemiExpanded", "UltraCondensed", "UltraExpanded" });
            }
        }




    }
}
