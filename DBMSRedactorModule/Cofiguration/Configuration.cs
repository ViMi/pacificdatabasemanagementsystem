﻿using DBMS.Redactor.Configuration.MinoreConfig;
using System.IO;
using System.Text.Json;
using DBMS.Core.Helpers;



namespace DBMS.Redactor.Configuration
{
    public static class Configuration
    {

#if DocTrue
        static Configuration() => Documentation.AddHistoryToSection(
              PQLDocGlobal.Redactor,
            PQLDocGlobal.RedactorIO,
            "Configuration",
            PQLDocGlobal.TypeStaticClass,
            PQLDocGlobal.DescriptionUndefined

            );
#endif



        public static ConfigurationData ReadUserConfiguration()
        {
            ConfigurationData config;
            using (StreamReader sr = new StreamReader("..\\..\\Resources\\userConfiguration.json"))
            {
               
                config = JsonSerializer.Deserialize<ConfigurationData>(sr.ReadToEnd());
                
            }

            return config;
        }



        public static void SaveConfiguration(ConfigurationData config)
        {
            using (FileStream fs = new FileStream("..\\..\\Resources\\userConfiguration.json", FileMode.OpenOrCreate))
            {
                System.String serializaConfig = JsonSerializer.Serialize<ConfigurationData>(config);
                System.Byte[] byteConfig = System.Text.Encoding.UTF8.GetBytes(serializaConfig);
                fs.Write(byteConfig, 0, byteConfig.Length);
            }

        }

    }
}
