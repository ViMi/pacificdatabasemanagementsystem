﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using DBMS.Core;
using DBMS.Core.IO;
using System.Text.Json;
using System.Runtime.CompilerServices;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using System.IO;
using DBMS.Core.MetaInfoTables;
using System.Windows;
using DBMS.Redactor.ViewModels;
using DBMS.Core.UsersFromRoles;
using DMBS.Core.Secure.UserRights.RightTable;


using DBMS.Core.Secure.UserRights;
using DBMS.Core.ErrorControlSystem.Exeption;

namespace DBMS.Redactor
{
    public sealed class RedactorTree
    {




        public System.String startItemHeader = System.String.Empty;
        public RedactorViewModel VM;
        public RedactorTree(String startString, RedactorViewModel redactor)
        {
            this.startItemHeader = startString;
            VM = redactor;

        }



        //Replace to the Redactor Module
        public System.Windows.Controls.TreeViewItem[] TryGetUserDBOMetaInformation
            (ILogin login)
        {

            System.Boolean accesRule = login != null;

            if (accesRule)
            {

                System.Windows.Controls.TreeViewItem[] treeNodes = new System.Windows.Controls.TreeViewItem[1]
                { new System.Windows.Controls.TreeViewItem()};

                treeNodes[0].Header = startItemHeader;


                IMetaInfo info = new TableMetaInfo();
                info.ChildrenList = DBMSIO.ServerMetaInformation.ChildrenList;
                info.Parent = DBMSIO.ServerMetaInformation.Parent;
                info.Type = DBMSIO.ServerMetaInformation.Type;
                info.Owner = DBMSIO.ServerMetaInformation.Owner;
                info.NodeName = DBMSIO.ServerMetaInformation.NodeName;
                
                treeNodes[0].ItemsSource = CreateNode(DBMSIO.DBMSConfig.BaseDir, info, login.LoginRights);
                  
                return treeNodes;
            }
            else
                return null;

            
        }





        private System.Windows.Controls.TreeViewItem[] 
            CreateNode(System.String path, IMetaInfo meta, Dictionary<System.String, NodeType> loginSchems)
        {

            System.String parent = meta.Parent;



            if (loginSchems.TryGetValue(parent, out _))
            {


                System.String[] items
                    = meta.ChildrenList.Trim().Split(DBMSIO.DBMSConfig.StandartSeparator);

                System.Windows.Controls.TreeViewItem[] node 
                    = new System.Windows.Controls.TreeViewItem[items.Length];

                for (System.Int32 i = 0; i < items.Length; ++i)
                {
                    if (items[i] == (DBMSIO.DBMSConfig.StandartSeparator.ToString()) || items[i] == String.Empty)
                        continue;

                    node[i] = new System.Windows.Controls.TreeViewItem();
                    node[i].Header = items[i];


                    if (File.Exists
                        (path
                        + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                        + items[i]
                        + DBMSIO.DBMSConfig.FullMetaFileName))
                    {

                        IMetaInfo info = DBMSIO.ReadMetaInfo
                            (path
                            + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                            + items[i]
                            + DBMSIO.DBMSConfig.StandartMetaFileName
                            + DBMSIO.DBMSConfig.StandartMetaFileExtension);

        
                            if (info.Type < NodeType.DATABASE 
                            && info.ChildrenList != System.String.Empty && CheckUserRightView(info))
                            {
                                node[i].ItemsSource =
                                    CreateNode(path
                                    + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                                    + items[i],
                                      info,
                                      loginSchems);
                            }

                            else if (info.Type > NodeType.DATABASE && CheckUserRightView(info))
                            {
                                IUserToken user;
                            if (
                                info.Type != NodeType.LOGIN 
                                && info.Type != NodeType.BACKUP 
                                && DBMSIO.GetUserToken(info.Parent, out user)
                                && CheckUserRightView(info)
                                )
                            {
                                if (user.IsUserWorkWith(items[i]))
                                {
                                    node[i].Tag = DBMSIO.ReadMetaInfo(
                                        path
                                        + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                                        + items[i]
                                        + DBMSIO.DBMSConfig.StandartMetaFileName
                                        + DBMSIO.DBMSConfig.StandartMetaFileExtension);
                                    node[i].Selected += SelectItem;
                                } 
                            }
                            }
                            else if (info.Type == NodeType.DATABASE && CheckUserRightView(info))
                            {
                                node[i].Tag = DBMSIO.ReadMetaInfo(
                                  path
                                  + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                                  + items[i]
                                  + DBMSIO.DBMSConfig.StandartMetaFileName
                                  + DBMSIO.DBMSConfig.StandartMetaFileExtension);
                                node[i].Selected += SelectItem;
                                node[i].ItemsSource =
                                   CreateNode(path
                                   + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                                   + items[i],
                                     info,
                                     loginSchems);
                            }
                        
                        

                    }

                }

                return node;
            }

            return null;

        }




        static String currentDB = String.Empty;
        static String parrentDir = String.Empty;
        public Boolean CheckUserRightView(IMetaInfo info)
        {



            if (info.Type == NodeType.DATABASE)
            {

                if (!DBMSIO.GetCurrentLogin().LoginRights.ContainsKey(info.NodeName))
                    return false;
                
                else
                {
                    currentDB = info.NodeName;
                    return true;
                }

            }


            if (
                info.Type == NodeType.ROOT_DIRECTORY 
                || info.Type == NodeType.ROOT 
                || info.Type == NodeType.SYSTEM_DIRECTORY 
                || info.Type == NodeType.SECURITY_DIRECTORY)
            {



                if (info.NodeName == DBMSIO.DBMSConfig.BaseDataBaseDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty) ||
                    info.NodeName == DBMSIO.DBMSConfig.BaseSecurityDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty)||
                    info.NodeName == DBMSIO.DBMSConfig.BaseLoginDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty) || 
                    info.NodeName == DBMSIO.DBMSConfig.PathToLog.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty) || 
                    info.NodeName == DBMSIO.DBMSConfig.BaseBackUpDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty))
                {
                    if(info.NodeName == DBMSIO.DBMSConfig.BaseLoginDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty))
                        if(DBMSIO.GetCurrentLogin().LoginRights.ContainsKey(Rbac.ResolveName(NodeType.LOGIN)))
                                return true;

                    if (info.NodeName == DBMSIO.DBMSConfig.PathToLog.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty))
                        if (DBMSIO.GetCurrentLogin().LoginRights.ContainsKey(Rbac.ResolveName(NodeType.LOG_REPORT)))
                            return true;

                    if (info.NodeName == DBMSIO.DBMSConfig.BaseBackUpDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty))
                        if (DBMSIO.GetCurrentLogin().LoginRights.ContainsKey(Rbac.ResolveName(NodeType.BACKUP)))
                            return true;

                    if (info.NodeName == DBMSIO.DBMSConfig.BaseSecurityDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty))
                        if (DBMSIO.GetCurrentLogin().LoginRights.ContainsKey(Rbac.ResolveName(NodeType.SECURITY_DIRECTORY)))
                            return true;


                    if (info.NodeName == DBMSIO.DBMSConfig.BaseDataBaseDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty))
                        if (DBMSIO.GetCurrentLogin().LoginRights.ContainsKey(Rbac.ResolveName(NodeType.DATABASE)))
                            return true;



                    if (DBMSIO.GetCurrentLogin().LoginRights.ContainsKey(DBMSIO.DBMSConfig.BaseSecurityDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty)))
                        return true;
                    
                   

                    return false;
                   

                }


                if (
                    info.NodeName == DBMSIO.DBMSConfig.DataBasesUsersDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty) || 
                    info.NodeName == DBMSIO.DBMSConfig.DataBasesProcedureDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty) || 
                    info.NodeName == DBMSIO.DBMSConfig.DataBaseTableDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty) ||
                    info.NodeName == DBMSIO.DBMSConfig.TriggersBaseDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty) || 
                    info.NodeName == DBMSIO.DBMSConfig.DataBaseViewDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty))
                {




                    if (info.NodeName == DBMSIO.DBMSConfig.DataBasesUsersDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty))
                        if (DBMSIO.GetCurrentLogin().LoginRights.ContainsKey(Rbac.ResolveName(NodeType.USER)))
                            return true;

                    if (info.NodeName == DBMSIO.DBMSConfig.DataBasesProcedureDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty))
                        if (DBMSIO.GetCurrentLogin().LoginRights.ContainsKey(Rbac.ResolveName(NodeType.PROCEDURE)))
                            return true;

                    if (info.NodeName == DBMSIO.DBMSConfig.DataBaseTableDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty))
                        if (DBMSIO.GetCurrentLogin().LoginRights.ContainsKey(Rbac.ResolveName(NodeType.TABLE)))
                            return true;

                    


                    if (info.NodeName == DBMSIO.DBMSConfig.TriggersBaseDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty))
                        if (DBMSIO.GetCurrentLogin().LoginRights.ContainsKey(Rbac.ResolveName(NodeType.TRIGGER)))
                            return true;


                    if (info.NodeName == DBMSIO.DBMSConfig.DataBaseViewDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty))
                        if (DBMSIO.GetCurrentLogin().LoginRights.ContainsKey(Rbac.ResolveName(NodeType.VIEW)))
                            return true;







                    IUserToken user;
                    DBMSIO.GetUserToken(currentDB, out user);


                    AllowedCommandWithDB userRights = user.UserRights;
                    Dictionary<String, AllowedCommand> allowedCommands = userRights.AllowedCommandOn;
                    System.Boolean flag = false;


                    //Check db existing 
                    AllowedCommand allowedOnDB;
                    if (!allowedCommands.TryGetValue(currentDB, out allowedOnDB))
                        throw new TypeError();


                    //Check all rights in table
                    CommandRight cr;


                    if (allowedOnDB.AllowCommand == null)
                        return false;

                    if (allowedOnDB.AllowCommand.ContainsKey(Rbac.ALL_COMPATIBILE_OPERATION_ALLOWED))
                        if (allowedOnDB.AllowCommand.TryGetValue(Rbac.ALL_COMPATIBILE_OPERATION_ALLOWED, out cr))
                            if (cr.CommandRights.ContainsKey(Rbac.GLOBAL_ALL_OBJECT_RIGHT_TOKEN))
                                if (cr.CommandRights.TryGetValue(Rbac.GLOBAL_ALL_OBJECT_RIGHT_TOKEN, out flag))
                                    return true;





                    foreach (var userRight in allowedOnDB?.AllowCommand)
                        if (info.Type != NodeType.SYSTEM_DIRECTORY && userRight.Key == Rbac.ResolveName(info.Type))
                            return true;
                    


                }



                if (info.NodeName == DBMSIO.DBMSConfig.BaseDataBaseDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty)
                    && DBMSIO.GetCurrentLogin().LoginRights.ContainsKey(info.NodeName))
                    return true;

                if (info.NodeName == DBMSIO.DBMSConfig.BaseSecurityDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty)
                     && DBMSIO.GetCurrentLogin().LoginRights.ContainsKey(info.NodeName))
                    return true;
            }



         

         

            return false;

        }



        public void SelectItem(object sender, RoutedEventArgs handler) 
        {
            System.Windows.Controls.TreeViewItem node = (System.Windows.Controls.TreeViewItem)sender;
            IMetaInfo info = (IMetaInfo)node.Tag;

            ResolveNodeType(info);

        }




        private void ResolveNodeType(IMetaInfo info)
        {
            switch (info.Type)
            {
                case NodeType.DATABASE: { this.VM.CurrentDataBaseName = GetDataBaseInfo(info); } break;
                case NodeType.TABLE: { this.VM.WorkAreaContent = GetTableInfo(info); } break;
                case NodeType.LOGIN: { this.VM.WorkAreaContent = GetLoginInfo(info); } break;
                case NodeType.BACKUP: { this.VM.WorkAreaContent = GetBackUpInfo(info); } break;
                case NodeType.USER: { this.VM.WorkAreaContent =  GetUserInfo(info); }break;
                case NodeType.VIEW: { this.VM.WorkAreaContent =  GetViewInfo(info); } break;
                case NodeType.PROCEDURE: { this.VM.WorkAreaContent =  GetProcedureInfo(info); }break;
   //             case NodeType.TRIGGER: { this.VM.WorkAreaContent = Get}
                default: { Console.WriteLine("None"); }break;
            }

        }




        


        private System.String GetDataBaseInfo(IMetaInfo info) => info.NodeName;




        private System.String GetLoginInfo(IMetaInfo info)
        {

            ILogin user = DBMSIO.GetLoginWithPassword(info.NodeName, info.NodeAccessPassword._Password);

            return (
                 "Login: " + user.GetUserLogin() + Environment.NewLine
                 + "Owne resources: " + user.ChildrenList + Environment.NewLine
                 + "Parent Section: " + user.Parent
                  );


        }



        private System.String GetBackUpInfo(IMetaInfo info)
        {
            return null;
        }



        private System.String GetUserInfo(IMetaInfo info) 
        {

            DataBaseIO dataBase = new DataBaseIO(info.Parent, info.Owner);
            ILogin user = DBMSIO.GetLogin(info.NodeName);

                return (
                      "Login: " + user.GetUserLogin() + Environment.NewLine
        
                     + "Owne resources: " + user.ChildrenList + Environment.NewLine
                     + "Parent Section: " + user.Parent
                      );

            
        }



        private System.String GetProcedureInfo(IMetaInfo info)
        {
            DataBaseIO dataBase = new DataBaseIO(info.Parent, info.Owner);
            return (
                dataBase.IsProcedureExist(info.NodeName) ? 
                dataBase.ReadStoredProcedureMMIL(info.NodeName).GetListiongIL() : 
               throw new Exception());
            
        }


        private System.String GetTableInfo(IMetaInfo info)
        {

            DataBaseIO dataBase = new DataBaseIO(info.Parent, info.Owner);

            System.String result =
                                    "[Name] " + info.NodeName + Environment.NewLine +
                                    "[Owner] " + info.Owner + Environment.NewLine +
                                    "[Parent] " + info.Parent + Environment.NewLine;

                System.String[] rowsMeta = info.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator);

                for (System.Int32 i = 0; i < rowsMeta.Length; ++i)
                {
                    result += rowsMeta[i] + Environment.NewLine;
                }


                return result;

        }


        private System.String GetViewInfo(IMetaInfo info)
        {

           
                System.String result =
                                    "[Name] " + info.NodeName + Environment.NewLine +
                                    "[Owner] " + info.Owner + Environment.NewLine +
                                    "[Parent] " + info.Parent + Environment.NewLine;

                System.String[] metaInfo = info.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator);

                for (System.Int32 i = 0; i < metaInfo.Length; ++i)
                {
                    result += metaInfo[i] + Environment.NewLine;
                }

                return result;
 
        }




    }
}
