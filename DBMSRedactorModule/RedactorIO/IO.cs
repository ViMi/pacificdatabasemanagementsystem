﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

using System.Windows.Documents;
using MessageBox = System.Windows.MessageBox;
using SaveFileDialog = Microsoft.Win32.OpenFileDialog;

using DBMS.Redactor.Project;
using DBMS.Core.Helpers;




namespace DBMS.Redactor.FileSystem
{
    public static class IO
    {

#if DocTrue
        static IO() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.Redactor,
            PQLDocGlobal.RedactorIO,
            "IO",
            PQLDocGlobal.TypeStaticClass,
            PQLDocGlobal.DescriptionUndefined


            );
#endif



        public static void SaveAll(List<FileContext> contexts)
        {
            for (System.Int32 i = 0; i < contexts.Count; ++i)
            {
                using (FileStream file = new FileStream(contexts.ElementAt(i).pathToFile, FileMode.OpenOrCreate))
                {
                    System.Byte[] bytes = System.Text.Encoding.Default.GetBytes(contexts.ElementAt(i).FileContent);
                    file.Write(bytes, 0, bytes.Length);

                }
            }

        }

        public static void Save(TextRange t, FileContext context)
        {

            if (context != null)
            {

                using (FileStream file = new FileStream(context.pathToFile, FileMode.OpenOrCreate))
                {
                    t.Save(file, System.Windows.DataFormats.Text);
                }

            }

        }



        public static void Save(FileContext context)
        {

            if (context != null)
            {

                using (FileStream file = new FileStream(context.pathToFile, FileMode.OpenOrCreate))
                {
                    System.Byte[] stringBytes = Encoding.UTF8.GetBytes(context.FileContent);
                    file.Write(stringBytes,0,stringBytes.Length);
            
                }

            }

        }



        public static void SaveAs(TextRange t, FileContext context)
        {

            SaveFileDialog savefile = new SaveFileDialog();
            // set a default file name
            savefile.FileName = "unknown.doc";
            // set filters - this can be done in properties as well
            savefile.Filter = "Document files (*.rtf)|*.rtf";
            if (savefile.ShowDialog() == true)
            {
              //  TextRange t = new TextRange(this._workArea.Document.ContentStart, this._workArea.Document.ContentEnd);

                using (FileStream file = new FileStream(context.FileName, FileMode.Create))
                {
                    t.Save(file, System.Windows.DataFormats.Text);
                }
            }
         
            
            //IsSaved = true;

        }

        public static FileContext OpenFile()
        {

            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Filter = "Document files (*.txt)|*.txt";
            var result = dlg.ShowDialog();

            if (result.Value)
            {

               return new FileContext(_OpenByName(dlg.FileName), dlg.FileName, _GetFileName(dlg.FileName)) ;
            

            }

            return null;
        }





        public static FileContext OpenFileByName(System.String pathToFile)
        {
                return new FileContext(_OpenByName(pathToFile), pathToFile, _GetFileName(pathToFile));

        }



        public static System.String _GetFileName(System.String pathToFile)
        {
            System.Int16 length = (System.Int16)pathToFile.Split('\\').Length;
            System.String name = pathToFile.Split('\\')[length - 1].Split('.')[0];

            return name;
        }



        public static void CreateNew(System.String fileName)
        {
            System.String path = Environment.CurrentDirectory + "\\" + fileName + ".txt";
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                System.Byte[] bytes = new System.Byte[0];
                fs.Write(bytes, 0, 0);
            }
        }



        public static System.String _OpenByName(System.String path)
        {
            return System.IO.File.ReadAllText(path);
        }


    }
}
