﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using DBMS.Redactor.Project;


using MessageBox = System.Windows.MessageBox;

using DBMS.Redactor.FileSystem;
using DBMS.Redactor.ViewModels;
using DBMS.Core.Helpers;



namespace DBMS.Redactor.MDIStrategy
{

    public sealed class MDIMaintein
    {



        #region Variable Declaration
        public UIElementCollection collection { get; }
         public Style MDIButtonStyle { get; }
         public ControlTemplate MDITemplate { get; }


         public Style MDICloseBtnStyle { get; }
         public ControlTemplate MDICloseBtnTemlate { get; }


         public RedactorViewModel RedactorView { get; }

        #endregion



#if DocTrue
        static MDIMaintein() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.Redactor,
            PQLDocGlobal.RedactorIO,
            "MDIMaintein",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined

            );
#endif

        public MDIMaintein(UIElementCollection relativeCollection)
        {
            this.collection = relativeCollection;
        }


        public MDIMaintein(UIElementCollection relativeCollection, Style mdiMainbtnStl, Style mdiCloseBtnStl):this(relativeCollection)
        {
            this.MDIButtonStyle = mdiMainbtnStl;
            this.MDICloseBtnStyle = mdiCloseBtnStl;
        }



        public MDIMaintein(UIElementCollection relativeCollection, Style mdiMainBtnStl, Style mdiCloseBtnStl, ControlTemplate mdiMainBtnTmp, ControlTemplate mdiCloseBtnTemplate)
            :this(relativeCollection, mdiMainBtnStl, mdiCloseBtnStl)
        {
            
            
            this.MDITemplate = mdiMainBtnTmp;
            this.MDICloseBtnTemlate = mdiCloseBtnTemplate;
        }


        public MDIMaintein(UIElementCollection relativeCollection, Style mdiMainBtnStl, Style mdiCloseBtnStl, ControlTemplate mdiMainBtnTmp, ControlTemplate mdiCloseBtnTemplate, RedactorViewModel viewModel)
            : this(relativeCollection, mdiMainBtnStl, mdiCloseBtnStl, mdiMainBtnTmp, mdiCloseBtnTemplate)
        {
            this.RedactorView = viewModel;
        }





        public void AddNewMDIButton(FileContext context)
        {

            try
            {
                if (context == null)
                    return;

                this.ButtonMDITemplate(context.FileName, out StackPanel stack);

                this.collection.Add(stack);
                RedactorView.RedactorContextsCollection.context.Add(context);
                RedactorView.RedactorContextsCollection.CurrentContext = context;


                if (stack == null)
                {
                    MessageBox.Show("Error of open file process");
                    return;

                }


                if (this.RedactorView.WorkAreaVisibility == Visibility.Collapsed)
                {
                    this.RedactorView.WorkAreaVisibility = Visibility.Visible;
                }

                this.RedactorView.WorkAreaContent = context.FileContent;
            } catch (Exception e)
            {
                throw e;
            }



        }


        private StackPanel ButtonMDITemplate(System.String name, out StackPanel stack)
        {


            try
            {

                stack = new StackPanel
                {
                    Orientation = System.Windows.Controls.Orientation.Horizontal,
                    Name = name + "StackPanel"

                };


                System.Windows.Controls.Button Btn = new System.Windows.Controls.Button
                {
                    Content = name,
                    Style = MDIButtonStyle,
                    Template = MDITemplate
                };

                Btn.Click += ChangeContext;


                System.Windows.Controls.CheckBox checkBox = new System.Windows.Controls.CheckBox
                {

                    Content = "",
                    Style = MDICloseBtnStyle,
                    Template = MDICloseBtnTemlate,
                    Name = name


                };
                checkBox.Checked += CloseMDIFile;

                stack.Children.Add(Btn);
                stack.Children.Add(checkBox);

                return stack;
            }

            catch
            {
               
                return (stack = null);
            }
        }




        public void CloseMDIFile(object sender, RoutedEventArgs e)
        {
            try
            {

                System.String name = ((System.Windows.Controls.CheckBox)sender).Name + "StackPanel";
                System.String elemName = ((System.Windows.Controls.CheckBox)sender).Name;

                foreach (var i in this.collection)
                {

                    if (i.GetType().Name == "StackPanel")
                    {
                        if (((StackPanel)i).Name == name)
                        {
                            this.collection.Remove((UIElement)i);
                            RedactorView.RedactorContextsCollection.context.Remove
                                (RedactorView.RedactorContextsCollection.context.Find((a) => a.FileName == elemName ? true : false));


                            if (RedactorView.RedactorContextsCollection.context.Count > 0)
                            {
                                RedactorView.RedactorContextsCollection.CurrentContext = RedactorView.RedactorContextsCollection.context.Last();
                                RefreshDocument();
                            }
                            else
                            {
                                RedactorView.WorkAreaVisibility = Visibility.Collapsed;
                            }

                            break;
                        }
                    }
                }
            } catch (Exception ex)
            {
                throw ex;
            }
        }






        public void RefreshDocument()
        {


            this.RedactorView.WorkAreaContent = RedactorView.RedactorContextsCollection.CurrentContext.FileContent;

        }





        public void _workArea_TextChanged(object sender, TextChangedEventArgs e)
        {

            FlowDocument fd = ((System.Windows.Controls.RichTextBox)sender).Document;
            TextRange t = new TextRange(fd.ContentStart, fd.ContentEnd);

            if (RedactorView.RedactorContextsCollection.CurrentContext != null)
                RedactorView.RedactorContextsCollection.CurrentContext.FileContent = t.Text;
        }



        public void ChangeContext(object sender, RoutedEventArgs e)
        {
            System.String name = (System.String)((System.Windows.Controls.Button)sender).Content;
            FileContext fileContext =
                (RedactorView.RedactorContextsCollection.context.Find((a) => a.FileName == name ? true : false));
            if (fileContext != null)
            {

                RedactorView.RedactorContextsCollection.CurrentContext = fileContext;
                this.RedactorView.WorkAreaContent = fileContext.FileContent;

            }
        }





        public void CloseCurrentFile()
        {

            RedactorView.RedactorContextsCollection.context.
                RemoveAll((a) => a.FileName == RedactorView.RedactorContextsCollection.CurrentContext.FileName ? true : false);

            if (RedactorView.RedactorContextsCollection.context.Count > 0)
            {
                RedactorView.RedactorContextsCollection.CurrentContext = RedactorView.RedactorContextsCollection.context.Last();
                this.RefreshDocument();
            }
            else
            {
                RedactorView.WorkAreaVisibility = Visibility.Collapsed;
            }
        }


        public void SaveObserver(System.Boolean IsSaved)
        {
            if (RedactorView.IsSaved == false)
                if (MessageBox.Show("Do you want save changes ?", "Message", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {

                    IO.Save(RedactorView.RedactorContextsCollection.CurrentContext);
                }

        }



        public void HideSplitChk_Click(System.Object sender, RoutedEventArgs e)
        {
            this.RedactorView.RedactorContextsCollection.context.RemoveAll((a) => true);
            this.collection.RemoveRange(0, this.collection.Count);
            this.RedactorView.RedactorContextsCollection.CurrentContext = new FileContext(System.String.Empty, System.String.Empty, System.String.Empty);
            this.RefreshDocument();
            this.RedactorView.WorkAreaVisibility = Visibility.Collapsed;
            ((System.Windows.Controls.CheckBox)sender).IsChecked = false;
        }




    }



}
