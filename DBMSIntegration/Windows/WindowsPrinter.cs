﻿using System.Drawing;
using System.Drawing.Printing;
using DBMS.Core.Helpers;



namespace DBMS.Integration.Windows
{
    public sealed class WindowsPrinter
        : IPrinter
    {

        #region Documentation
#if DocTrue
        static WindowsPrinter()
        {
            Documentation.AddHistoryToSection(
                                  PQLDocGlobal.IntegrationSystems,
            PQLDocGlobal.IntegrationWindows,
            "WindowsPrinter",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "IPrinter"
                );


            Documentation.AddHistoryToSection(

                                  PQLDocGlobal.IntegrationSystems,
            PQLDocGlobal.IntegrationWindows,
            "IPrinter",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined
                );
        }

#endif
        #endregion



        /// <summary>
        /// This property contains print text
        /// </summary>
        public string PrintText { set; get; } = System.String.Empty;

        public System.String FontFamily { set; get; } = "Arial";
        public System.Int32 FontSize { set; get; } = 14;

        void Printing(System.Object sender, PrintPageEventArgs e)
            => e.Graphics.DrawString(this.PrintText, new Font(FontFamily, FontSize), System.Drawing.Brushes.Black, 0, 0);



        /// <summary>
        /// This method initialize print proces
        /// </summary>
        /// <param name="printContent"></param>
        public void Print(System.String printContent)
        {

            this.PrintText = printContent;

            PrintDocument printDocument = new PrintDocument();
            printDocument.PrintPage += Printing;
            System.Windows.Forms.PrintDialog printDialog = new System.Windows.Forms.PrintDialog();

            printDialog.Document = printDocument;

            if (printDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                printDialog.Document.Print();
        }





    }
}
