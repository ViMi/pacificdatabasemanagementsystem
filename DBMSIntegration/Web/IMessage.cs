﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBMS.Integration.Web
{


    public enum WebQuestion 
        : System.Byte
    {
        HEAR_ME  = 0,
        CORRECT_SUM = 1,
        SEND_MESSAGE = 2,
        GET_MESSAGE = 3
    }


    public enum WebAnswer 
        : System.Byte
    {
        YES = 0,
        NO = 1
    }

    public interface IMessage
    {

        System.String Message { set; get; }
        WebQuestion Question { set; get; }
        WebAnswer Answer { set; get; }

    }
}
