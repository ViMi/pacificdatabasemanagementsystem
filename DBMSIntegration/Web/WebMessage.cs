﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBMS.Core.Helpers;



namespace DBMS.Integration.Web
{
    public class WebMessage
        : IMessage
    {

        #region Documentaiton
#if DocTrue
        static WebMessage() { }
#endif
        #endregion

        public System.String Message { set; get; }
        public WebQuestion Question { set; get; }
        public WebAnswer Answer { set; get; }
    }
}
