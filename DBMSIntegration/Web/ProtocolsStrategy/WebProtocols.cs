﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using DBMS.Integration.Web;


namespace DBMS.Integration.Web
{


    [ServiceContract]
    public interface IWebProtocols
    {

        [OperationContract]
        void Connect();

        
        
        [OperationContract]
        void DisConnect();



        [OperationContract]
        void SendMessage();



        [OperationContract]
        IMessage GetMessage();


    }
}
