﻿
using System.ServiceModel;

using DBMS.Core.Helpers;


namespace DBMS.Integration.Web.ProtocolsStrategy
{

    
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public sealed class PSXP
        : IWebProtocols
    {

        #region Documentation
#if DocTrue
        static PSXP() { }
#endif
        #endregion


        public void Connect()
        {

        }


        public void DisConnect()
        {

        }



        public void SendMessage()
        {

        }



    
        public IMessage GetMessage()
        {

            return null;
        }



    }
}
