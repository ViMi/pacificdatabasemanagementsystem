﻿using System;
using System.Collections.Generic;

using System.ServiceModel;
using System.Net;

using DBMS.Integration.Web;
using DBMS.Integration.Web.ProtocolsStrategy;
using DBMS.Core.ErrorControlSystem;

using System.ServiceModel.Description;
using System.ServiceModel.Discovery;
using DBMS.Core.Helpers;



namespace DBMS.Integration.Web
{
    public static class Web
    {
        #region Documentaiton
#if DocTrue
        static Web() { }
#endif
        #endregion


        static private ServiceHost _Host;

        static private Uri _TcpUri;
        static private EndpointAddress _Address;
        static private BasicHttpBinding _Binping;
        static private ChannelFactory<IWebProtocols> _Factory;
        static private IWebProtocols _Service;

        /// <summary>
        /// This method receved ipv4 of desctination endpoint
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public static System.String OpenChanelWith(IPAddress ip)
        {

            String message = String.Empty;

            System.String name = Dns.GetHostName();
            IPAddress[] addresses = Dns.GetHostAddresses(name);

               System.String baseAddress = $"http://{ip}:9000/IWebProtocols";

               Type contract = typeof(IWebProtocols);
               Type implementation = typeof(PSXP);

               System.String address = baseAddress + implementation.Name;

               BasicHttpBinding binding = new BasicHttpBinding();

               _Host = new ServiceHost(implementation, new Uri[] { new Uri(address) });
               _Host.AddServiceEndpoint(contract, binding, address);

               ServiceMetadataBehavior sb = new ServiceMetadataBehavior();

               sb.HttpGetEnabled = true;
               _Host.Description.Behaviors.Add(sb);

               _Host.AddServiceEndpoint(typeof(IMetadataExchange), MetadataExchangeBindings.CreateMexHttpBinding(), "mex");

               _Host.AddServiceEndpoint(new UdpDiscoveryEndpoint());
               _Host.Description.Behaviors.Add(new ServiceDiscoveryBehavior());

               _Host.AddServiceEndpoint(typeof(IWebProtocols), new BasicHttpBinding(), baseAddress);

               _Host.Open();

                message = ErrorMessagesCore.MessageDictionary[(System.Byte)ErrorCodes.HOST_OPENED];
                    
            return message;

        }



        public static System.String ConnectWith(System.String uri)
        {
            _TcpUri = new Uri(uri);
            _Address = new EndpointAddress(_TcpUri);
            _Binping = new BasicHttpBinding();

            _Factory = new ChannelFactory<IWebProtocols>(_Binping, _Address);
            _Service = _Factory.CreateChannel();

            return "CHANNEL CREATED";
        }



        public static List<String> GetServicesAddress()
        {
            List<String> responseList = new List<String>();

            try
            {
                UdpDiscoveryEndpoint endpoint = new UdpDiscoveryEndpoint();
                DiscoveryClient dc = new DiscoveryClient(endpoint);

                FindCriteria criteria = new FindCriteria(typeof(IWebProtocols));
                criteria.Duration = new TimeSpan(0, 0, 1);

                FindResponse response = dc.Find(criteria);

                foreach(var item in response.Endpoints)
                    responseList.Add(item.Address.ToString());
                
            } catch
            {
                return null;
            }


            return responseList;
        }


        public static void CloseHost() => _Host.Close();
        public static void IsDataSync() => _Service.Connect();
        public static void SyncData() => _Service.SendMessage();
        public static void RunService() => _Service.Connect();
        public static void CloseService() => _Service.DisConnect();
        public static void GetResponseService() => _Service.GetMessage();

    }
}
