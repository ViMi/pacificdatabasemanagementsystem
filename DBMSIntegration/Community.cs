﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBMS.Core.Helpers;



namespace DBMS.Integration
{
     public static class Community
    {

        #region Documentation
#if DocTrue
        static Community() => Documentation.AddHistoryToSection(
                  PQLDocGlobal.IntegrationSystems,
            PQLDocGlobal.IntegrationCommunity,
            "Community",
            PQLDocGlobal.TypeStaticClass,
            PQLDocGlobal.DescriptionUndefined
            );
#endif
        #endregion


        public static void VisitCommunitySite() { }

        public static void VisitPacificDBMSSite() => System.Diagnostics.Process.Start("http://pacific/");

        public static void VisitHelpPacificDBMSSite() => System.Diagnostics.Process.Start("http://pacific/");



    }
}
