﻿using System;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using DBMS.Core.Helpers;



namespace DBMS.Integration.MicrosoftOffice.Excell
{
    public sealed class InputToExcell 
        : IPrinter
    {

        #region Documentation
#if DocTrue
        static InputToExcell() { }

#endif
        #endregion


        #region Variable Declaration

        private Excel.Application _App = null;
        private Excel.Workbook _Workbook = null;
        private Excel._Worksheet _Worksheet = null;

#pragma warning disable CS0414
        private Excel.Range _WorkSheetRange = null;
#pragma warning restore CS0414


        private System.String[] _ColumnNames = null;
        private System.String[] _RowNames = null;
        private System.String[] _Values = null;
        #endregion


        private void CreateDoc()
        {

            System.Int32 columnCount = this._ColumnNames.Length;
            System.Int32 rowCount = this?._RowNames?.Length ?? (System.Int32)Math.Ceiling(((System.Decimal)(_Values.Length / columnCount)));

            System.Int32 ro = (System.Int32)Math.Ceiling(((System.Decimal)(this._Values.Length / columnCount)));


            _App = new Excel.Application();

            _Workbook = _App.Workbooks.Add(1);


            _Worksheet = (Excel.Worksheet)_Workbook.Sheets[1];
            Excel.Range range = _Worksheet.Range["A1", Missing.Value];
            range = range.Resize[rowCount+1, columnCount+1];

            System.Object[,] ValueString = new System.String[rowCount+1, columnCount+1];


            for(System.Int32 i = 0; i < columnCount; ++i)
                ValueString[0, i + 1] = this._ColumnNames[i];




            for (System.Int32 i = 0; i < rowCount; ++i)
            {
                ValueString[i + 1, 0] = this._RowNames == null && _RowNames.Length < i ? "___" : this._RowNames[i];
                for (System.Int32 j = 0; j < columnCount; ++j)
                    ValueString[i + 1, j + 1] = this._Values[(i * columnCount) + j];
                
            }
            


           range.Value = ValueString;
           _App.Visible = true;


        }








        /// <summary>
        /// This method parse string to crete rows und columns names 
        /// String format "1_columnName,2_columnName,...,n_columnName:1_rowName,2_rowName,....,n_rowName:1_value,....,i*j_value"
        /// </summary>
        /// <param name="printContent"></param>
        public void Print(System.String printContent)
        {

                if (printContent.Split(':').Length >= 3)
                {
                    this._ColumnNames = printContent.Split(':')[0].Split(',', (Char)StringSplitOptions.RemoveEmptyEntries);
                    this._RowNames = printContent.Split(':')[1].Split(',', (Char)StringSplitOptions.RemoveEmptyEntries);
                    this._Values = printContent.Split(':')[2].Split(',', (Char)StringSplitOptions.RemoveEmptyEntries);
                }
                else if (printContent.Split(':').Length == 2)
                {
                    this._ColumnNames = printContent.Split(':')[0].Split(',', (Char)StringSplitOptions.RemoveEmptyEntries);
                    this._Values = printContent.Split(':')[1].Split(',', (Char)StringSplitOptions.RemoveEmptyEntries);
                }
                else
                {
                    throw new Exception();
                }
                this.CreateDoc();

            
        }
    }
}
