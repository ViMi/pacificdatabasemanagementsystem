﻿using DBMS.Core;
using DBMS.Core.MetaInfoTables;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using DBMS.Core.IO;
using DBMS.Core.User;
using DBMS.Core.InfoTypes;
using DBMS.Integration.JSON.SQL;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.TableCollections;
using DBMS.Core.Roles;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.UsersFromRoles;
using DBMS.Core.Secure;
using DBMS.Core.Helpers;

using DBMS.Core.Interprater;
using DBMS.Core.Table;



namespace DBMS.Integration.JSON
{

    internal sealed class JsonMethodResult
        : ITransactionMethodResult
    {

        #region Documentation
#if DocTrue
        static JsonMethodResult()
        {

        }

#endif
        #endregion


        public JsonMethodResult(System.Boolean status, System.String resultStr)
        {
            this.IsSuccess = status;
            this.ResultString = resultStr;
        }

        public Boolean IsSuccess { set; get; }
        public Object AddedInfo { set; get; }
        public System.String ResultString { get; set; }
    }



    internal sealed class JsonMethodInfo
        : ITransactionInfo
    {


        public System.String UserName { get; set; }
        public System.String DataBaseName { get; set; }
        public System.String Parameters { get; set; }
        public System.Object AddedInfoFragmentTwo { get; set; }
        public System.Object AddedInfo { get; set; }
    }





    internal sealed class JsonTransactionInfo
        : ITransactionInfo
    {

        #region Documentation
#if DocTrue
        static JsonTransactionInfo() { }
#endif

        #endregion


        public String UserName { get; set; }
        public String DataBaseName { get; set; }
        public String Parameters { get; set; }
        public Object AddedInfoFragmentTwo { get; set; }
        public Object AddedInfo { get; set; }

        public JsonTransactionInfo(String userName, String dbName, String parameters)
        {
            this.UserName = userName;
            this.DataBaseName = dbName;
            this.Parameters = parameters;
        }

        public JsonTransactionInfo()
        {

        }
    }



    public sealed class JSON
        : IOIntegrateDBMS
    {

        #region Documentation
#if DocTrue
        static JSON()
        {

        }
#endif
        #endregion


        #region Variable Declaration 
        private static JSONDataDeclarationLanguage _DDL { set; get; } = new JSONDataDeclarationLanguage();
        private static JSONDataControlLanguage _DCL { set; get; } = new JSONDataControlLanguage();
        private static JSONDataManipulationLanguage _DML { set; get; } = new JSONDataManipulationLanguage();
        private static JSONExtensionComands _ExtensionCommand { set; get; } = new JSONExtensionComands();
        private static JSONAdministrationExtension _AdminExtensio { set; get; } = new JSONAdministrationExtension();
        private static JSONDataControlLanguageExtension _DCLExtension { set; get; } = new JSONDataControlLanguageExtension();

        internal static ServerRules RulesManager { set; get; } = new ServerRules();
        #endregion


        private System.Boolean ResolveSaveMode(IOSaveMode mode, System.String path)
        {
            if (File.Exists(path))
                return mode == IOSaveMode.REWRITE ? true : false;
            
            return true;

        }



        #region Read Save  Methods
        /// <summary>
        ///Save user token
        /// </summary>
        /// <param name="path"></param>
        /// <param name="user"></param>
        /// <param name="saveMode"></param>
        public void Save(System.String path, IUserToken user, IOSaveMode saveMode = IOSaveMode.REWRITE)
        {
            path = path + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + user.NodeName + DBMSIO.DBMSConfig.StandartMetaFileExtension;
            if (ResolveSaveMode(saveMode, path))
            {
                using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
                {
                    String serializaConfig =
                        JsonSerializer.Serialize<IUserToken>(user);
                    Byte[] byteConfig =
                        System.Text.Encoding.UTF8.GetBytes(serializaConfig);

                    fs.Write(byteConfig, 0, byteConfig.Length);
                }
            }

        }

        public void Read(System.String fileName) {}

        public void Read(System.String fileName, System.String path) {  }
        #endregion



        #region Restore Server Methods
        public void RestoreServer()
        {


            Directory.CreateDirectory(
                    DBMSIO.DBMSConfig.BaseDir
                );

            Directory.CreateDirectory(
                    DBMSIO.DBMSConfig.FullPathToSecureRoot
                );

            Directory.CreateDirectory(
                    DBMSIO.DBMSConfig.FullPathToLoginRoot
                );

            Directory.CreateDirectory(
                    DBMSIO.DBMSConfig.FullPathToBackUp
                );

            Directory.CreateDirectory(
                    DBMSIO.DBMSConfig.BaseDir + DBMSIO.DBMSConfig.BaseDataBaseDir
                );

            Directory.CreateDirectory(
                   DBMSIO.DBMSConfig.FullPathToSecureRoot + DBMSIO.DBMSConfig.PathToLog
                );


            Directory.CreateDirectory(
          DBMSIO.DBMSConfig.FullPathToSecureRoot + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + DBMSIO.DBMSConfig.BaseJournalDir
       );



            IMetaInfo loginInfo = new DataBaseMetaInfo();
            loginInfo.NodeName = DBMSIO.DBMSConfig.BaseLoginDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty);
            loginInfo.Owner = DBMSIO.DBMSConfig.ServerName;
            loginInfo.Parent = DBMSIO.DBMSConfig.ServerName;
            loginInfo.ChildrenList =
                DBMSIO.DBMSConfig.SimpleDBCreator
                + DBMSIO.DBMSConfig.StandartSeparator
                + DBMSIO.DBMSConfig.SimpleSecureAdmin
                + DBMSIO.DBMSConfig.StandartSeparator
                + DBMSIO.DBMSConfig.SimpleSysAdminName;

            loginInfo.Type = NodeType.SYSTEM_DIRECTORY;
            _DDL.CreateFileWithMetaFile<IMetaInfo>(DBMSIO.DBMSConfig.FullPathToTheLoginMetaFile, loginInfo);


            System.String serverChilds =
                DBMSIO.DBMSConfig.BaseDataBaseDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty)
                + DBMSIO.DBMSConfig.StandartSeparator
                + DBMSIO.DBMSConfig.BaseSecurityDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty);

            ServerMetaInfo serverInfo = new ServerMetaInfo();
            serverInfo.NodeName = DBMSIO.DBMSConfig.ServerName;
            serverInfo.ServerName = DBMSIO.DBMSConfig.ServerName;
            serverInfo.CurrentSeenDate = DateTime.Now.ToLongDateString();
            serverInfo.LastSeenDate = DateTime.Now.ToLongDateString();
            serverInfo.ServerType = ServerType.LOCAL;

            serverInfo.Owner = Environment.MachineName;
            serverInfo.ServerOwner = Environment.MachineName;
            serverInfo.Parent = DBMSIO.DBMSConfig.ServerName;
            serverInfo.ChildrenList = serverChilds;
            serverInfo.Type = NodeType.ROOT_DIRECTORY;
            _DDL.CreateFileWithMetaFile<IServerMetaInfo>(DBMSIO.DBMSConfig.BaseDir + DBMSIO.DBMSConfig.FullMetaFileName, serverInfo);


            System.String secureChild =
                 DBMSIO.DBMSConfig.BaseBackUpDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty)
                 + DBMSIO.DBMSConfig.StandartSeparator
                 + DBMSIO.DBMSConfig.PathToLog.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty)
                 + DBMSIO.DBMSConfig.StandartSeparator
                 + DBMSIO.DBMSConfig.BaseLoginDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty)
                 + DBMSIO.DBMSConfig.StandartSeparator
                 + DBMSIO.DBMSConfig.BaseJournalDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty)
                ;

            IMetaInfo secureInfo = new DataBaseMetaInfo();
            secureInfo.NodeName = DBMSIO.DBMSConfig.BaseSecurityDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty);
            secureInfo.Owner = DBMSIO.DBMSConfig.ServerName;
            secureInfo.Parent = DBMSIO.DBMSConfig.ServerName;
            secureInfo.Type = NodeType.SYSTEM_DIRECTORY;
            secureInfo.ChildrenList = secureChild;
            _DDL.CreateFileWithMetaFile<IMetaInfo>(DBMSIO.DBMSConfig.FullPathToTheSecurityMetaFile, secureInfo);

            IMetaInfo backUpInfo = new DataBaseMetaInfo();
            backUpInfo.NodeName = DBMSIO.DBMSConfig.BaseBackUpDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty);
            backUpInfo.Owner = DBMSIO.DBMSConfig.ServerName;
            backUpInfo.Parent = DBMSIO.DBMSConfig.ServerName;
            backUpInfo.Type = NodeType.SYSTEM_DIRECTORY;
            backUpInfo.ChildrenList = String.Empty;
            _DDL.CreateFileWithMetaFile<IMetaInfo>(DBMSIO.DBMSConfig.FullPathToTheBackUpMetaFile, backUpInfo);

            IMetaInfo logInfo = new DataBaseMetaInfo();
            logInfo.NodeName = DBMSIO.DBMSConfig.PathToLog.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty);
            logInfo.Owner = DBMSIO.DBMSConfig.ServerName;
            logInfo.Parent = DBMSIO.DBMSConfig.BaseSecurityDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty);
            logInfo.Type = NodeType.SYSTEM_DIRECTORY;
            logInfo.ChildrenList = String.Empty;
            _DDL.CreateFileWithMetaFile<IMetaInfo>(DBMSIO.DBMSConfig.FullPathToSecureRoot 
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.PathToLog 
                + DBMSIO.DBMSConfig.FullMetaFileName, logInfo);


            IMetaInfo DataBaseInfo = new DataBaseMetaInfo();
            DataBaseInfo.NodeName = DBMSIO.DBMSConfig.BaseDataBaseDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty);
            DataBaseInfo.Owner = DBMSIO.DBMSConfig.ServerName;
            DataBaseInfo.Parent = DBMSIO.DBMSConfig.ServerName;
            DataBaseInfo.ChildrenList = String.Empty;
            DataBaseInfo.Type = NodeType.SYSTEM_DIRECTORY;
            _DDL.CreateFileWithMetaFile<IMetaInfo>(DBMSIO.DBMSConfig.BaseDir 
                + DBMSIO.DBMSConfig.BaseDataBaseDir 
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator 
                + DBMSIO.DBMSConfig.FullMetaFileName, DataBaseInfo);





            //CREATE ROLES  (LOGINS)

            ILogin dbCreator = new DataBaseCreator();
            ILogin sysAdmin = new SystemAdministrator();
            ILogin secureAdmin = new SecurityAdministrator();


            Directory.CreateDirectory(
                    DBMSIO.DBMSConfig.FullPathToTheAdmin
                );

            Directory.CreateDirectory(
                    DBMSIO.DBMSConfig.FullPathToTheDBCreator
                );

            Directory.CreateDirectory(
                    DBMSIO.DBMSConfig.FullPathToTheSecureAdmin
                );

            _DDL.CreateFileWithMetaFile<ILogin>(DBMSIO.DBMSConfig.FullPathToTheDBCreator + DBMSIO.DBMSConfig.FullMetaFileName, dbCreator);
            _DDL.CreateFileWithMetaFile<ILogin>(DBMSIO.DBMSConfig.FullPathToTheAdmin + DBMSIO.DBMSConfig.FullMetaFileName, sysAdmin);
            _DDL.CreateFileWithMetaFile<ILogin>(DBMSIO.DBMSConfig.FullPathToTheSecureAdmin + DBMSIO.DBMSConfig.FullMetaFileName, secureAdmin);



        }

        #endregion


        #region SerializeXxx Methods

        public String Serialize<T>(T obj) => JsonSerializer.Serialize<T>(obj);

        #endregion



        #region RemoveXxx Methods
        internal void RemoveSchemaInLogin(System.String loginName, System.String schemaName, NodeType schemaType)
        {
            if (IsLoginExist(loginName))
            {
                ILogin login = GetLogin(loginName);
                if (login.LoginRights.ContainsKey(schemaName))
                {
                    login.LoginRights.Remove(schemaName);
                    if (schemaType == NodeType.DATABASE)
                        login.ChildrenList.Replace(schemaName, String.Empty);

                    this.ReWriteLogin<ILogin>(DBMSIO.DBMSConfig.GetPathToLogin(loginName), login);
                }
            }
        }


        internal void RemoveSchemaInAllLogin(System.String schemaName, NodeType schemaType)
        {
            List<String> allLoginNames = GetNamesOfAllLoginsOnServer();

            foreach (String name in allLoginNames)
                if (name.Trim() != String.Empty)
                    RemoveSchemaInLogin(name, schemaName, schemaType);
        }


        internal void RemoveObjectFromAllUsers(System.String right, System.String objName)
        {
            List<String> allDBNames = GetAllDataBaseNames();

            foreach(String DBname in allDBNames) 
                if(DBname.Trim() != String.Empty)
                {
                    IDataBaseIO db = new DataBaseIO(DBname, String.Empty);
                    List<String> dbUsers = db.GetUserName();
                    foreach (String userName in dbUsers)
                        if(userName.Trim() != String.Empty)
                        db.RemoveRightFromUser(userName, right, objName);
                }


        }

        #endregion


        #region RewriteXxx Methods
        public void ReWriteTableMetaInfo(ITableInfo info, System.String path)
        {
            using (StreamWriter sw = new StreamWriter(path))
            {
                String str = JsonSerializer.Serialize(info);
                sw.Write(str);

            }
        }

        private void ReWriteLogin<T>(System.String path, T login)
        {
            using(FileStream fs = new FileStream(path, FileMode.Truncate))
            {
                System.String str = JsonSerializer.Serialize<T>(login);
                System.Byte[] arr = Encoding.UTF8.GetBytes(str);
                fs.Write(arr, 0, arr.Length);
            }

        }

        public void ReWriteUserTokenByName(System.String wayToUseRoot, System.String name, IUserToken ut)
        {
            using(StreamWriter sw = new StreamWriter(wayToUseRoot
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + name
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.FullMetaFileName))
            {
                String str = JsonSerializer.Serialize<IUserToken>(ut);
                sw.Write(str);
            }


        }

        public Boolean ReWriteMetaInfo(IMetaInfo info, System.String path)
        {
            if (File.Exists(path))
                File.Delete(path);

            using (
                FileStream fs = new FileStream(path, FileMode.Create)
                )
            {
                String serializaConfig = JsonSerializer.Serialize<IMetaInfo>(info);
                Byte[] byteConfig = System.Text.Encoding.UTF8.GetBytes(serializaConfig);
                fs.Write(byteConfig, 0, byteConfig.Length);
            }

            return true;
        }


        #endregion


        #region IsXxxMethods
        public System.Boolean IsDataBaseExist(System.String dataBaseName) => this.GetAllDataBaseNames().Contains(dataBaseName);
     
        public System.Boolean IsServerExist(System.String pathToServer)
        => Directory.Exists(pathToServer);
     
        public System.Boolean IsLoginExist(System.String login)
        {

            IMetaInfo userMeta = GetMetaInfoByPath(
              DBMSIO.DBMSConfig.FullPathToLoginRoot
              + DBMSIO.DBMSConfig.FullMetaFileName);

            System.String[] users
                = userMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator);

            for (System.Int16 i = 0; i < users.Length; ++i)
                if (users[i] == login)
                    return true;

            return false;
        }

        #endregion


        #region AppendXxx Methods
        internal void AppendSchemaInLogin(System.String loginName, System.String schemaName, NodeType schemaType)
        {
            if (IsLoginExist(loginName))
            {
                ILogin login = GetLogin(loginName);
                if (!login.LoginRights.ContainsKey(schemaName))
                {
                    login.LoginRights.Add(schemaName, schemaType);
                    if (schemaType == NodeType.DATABASE)
                        login.ChildrenList += DBMSIO.DBMSConfig.StandartSeparator + schemaName;


                    this.ReWriteLogin<ILogin>(DBMSIO.DBMSConfig.GetPathToLogin(loginName), login);
                }

            }
        }

        public void AppedSchemaToLogin(System.String loginName, System.String shemName, NodeType type)
        {
            if (!IsLoginExist(loginName))
                return;

            ILogin login = GetLogin(loginName);

            if (login.LoginRights.TryGetValue(shemName, out _))
                return;

            login.LoginRights.Add(shemName, type);

            ReWriteLogin(
                DBMSIO.DBMSConfig.FullPathToLoginRoot
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + loginName
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.FullMetaFileName,
                login
                );

        }

        public Boolean AppendLogin(String login)
        {

            System.String path = DBMSIO.DBMSConfig.FullPathToLoginRoot
           + DBMSIO.DBMSConfig.FullMetaFileName;


            IMetaInfo logins = GetMetaInfoByPath(path);

            logins.ChildrenList += DBMSIO.DBMSConfig.StandartSeparator + login;

            ReWriteMetaInfo(logins, path);

            return true;
        }

        public Boolean AppendDataBase(String dataBase)
        {
            System.String path = DBMSIO.DBMSConfig.FullPathToDataBaseMetaFile;

            IMetaInfo dbs = GetMetaInfoByPath(path);

            dbs.ChildrenList += DBMSIO.DBMSConfig.StandartSeparator + dataBase;

            ReWriteMetaInfo(dbs, path);

            return true;
        }

        internal void AppendRightToUser
     (ref IUserToken user, System.String command, System.String objectOn, NodeType type, System.Boolean rightFlag)
        {


            Dictionary<String, AllowedCommand> comm = user.UserRights.AllowedCommandOn;

            if (comm.ContainsKey(DBMSIO.GetCurrentDataBase().GetDataBaseName()))
            {
                AllowedCommand ac;
                comm.TryGetValue(DBMSIO.GetCurrentDataBase().GetDataBaseName(), out ac);

                if (ac.AllowCommand.ContainsKey(command))
                {
                    CommandRight cr;
                    if (ac.AllowCommand.TryGetValue(command, out cr))
                    {
                        if (cr.CommandRights.ContainsKey(objectOn))
                            cr.CommandRights.Remove(objectOn);


                        cr.CommandRights.Add(objectOn, rightFlag);
                    }
                }
                else
                    ac.AllowCommand.Add(command, new CommandRight(new Dictionary<string, bool>() { [objectOn] = rightFlag }));


            }

            DBMSIO.GetCurrentDataBase().ReWriteUserTokenByName(user.NodeName, user);


        }

        #endregion


        #region SetXxx methods

        public void SetNewCurrentDate(String currentDate, IServerMetaInfo serverMeta)
        {
            serverMeta.CurrentSeenDate = DateTime.Now.ToLongDateString();
            _DDL.RewriteObject<IServerMetaInfo>(DBMSIO.DBMSConfig.BaseDir + DBMSIO.DBMSConfig.FullMetaFileName, serverMeta);
        }

        public void SetNewPrevSeenDate(String lastSeenDate, IServerMetaInfo serverMeta)
        {
            serverMeta.LastSeenDate = lastSeenDate;
            _DDL.RewriteObject<IServerMetaInfo>(DBMSIO.DBMSConfig.BaseDir + DBMSIO.DBMSConfig.FullMetaFileName, serverMeta);
        }

        public void SetNewServerName(String serverName, IServerMetaInfo serverMeta)
        {
            serverMeta.ServerName = serverName;
            serverMeta.NodeName = serverName;
            _DDL.RewriteObject<IServerMetaInfo>(DBMSIO.DBMSConfig.BaseDir + DBMSIO.DBMSConfig.FullMetaFileName, serverMeta);
        }

        public void SetNewServerPassword(Password password, IServerMetaInfo serverMeta)
        {
            serverMeta.NodeAccessPassword = password;
            _DDL.RewriteObject<IServerMetaInfo>(DBMSIO.DBMSConfig.BaseDir + DBMSIO.DBMSConfig.FullMetaFileName, serverMeta);
        }

        public void SetNewServerType(ServerType type, IServerMetaInfo serverMeta)
        {
            serverMeta.ServerType = type;
            _DDL.RewriteObject<IServerMetaInfo>(DBMSIO.DBMSConfig.BaseDir + DBMSIO.DBMSConfig.FullMetaFileName, serverMeta);

        }

        #endregion


        #region ReadXxx Methods
        public T ReadObject<T>(String way)
        {
            T obj = default;

            using (StreamReader sr = new StreamReader(way))
                obj = JsonSerializer.Deserialize<T>(sr.ReadToEnd());


            return obj;

        }

        public IUserToken ReadUserTokenByName(System.String wayToUserRoot, System.String userName)
        {
            IUserToken token;
            using (StreamReader sr = new StreamReader(wayToUserRoot
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + userName
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.FullMetaFileName))
            {
                token = JsonSerializer.Deserialize<User>(sr.ReadToEnd());
            }

            return token;
        }

        public ITableInfo ReadTableMetaInfo(System.String path)
        {
            ITableInfo tableInfo = new TableRecordMetaInfo();

            using (StreamReader sr = new StreamReader(path))
                tableInfo =
                    JsonSerializer.Deserialize<TableRecordMetaInfo>(sr.ReadToEnd());


            return tableInfo;
        }

        public Dictionary<System.String, IUserToken> ReadUserByLogin(ILogin login)
        {
            Dictionary<System.String, IUserToken> dict = new Dictionary<String, IUserToken>(1);
            if (login == null)
                throw new LoginNotFound();

            Dictionary<System.String, NodeType> loginRight = login.LoginRights;
            System.Int32 length = loginRight.Keys.Count;

            List<IUserToken> tokens = new List<IUserToken>(0);
            System.String path = System.String.Empty;

            for (System.Int32 i = 0; i < length; ++i)
            {
                System.String Key = loginRight.Keys.ElementAt(i);

                NodeType type = loginRight.Values.ElementAt(i);

                if (type == NodeType.DATABASE)
                {
                    path =
                        DBMSIO.DBMSConfig.BaseDir
                        + DBMSIO.DBMSConfig.DataBasesStandartDir
                        + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                        + Key
                        + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                        + DBMSIO.DBMSConfig.DataBasesUsersDir;


                    tokens
                           = ReadUserToken(path, login);

                    foreach (IUserToken item in tokens)
                        dict.Add(Key, item);
                    


                }


            }

            return dict;

        }

        public List<IUserToken> ReadUserToken(System.String path, ILogin login)
        {
            IMetaInfo meta
                = GetMetaInfoByPath(path + DBMSIO.DBMSConfig.FullMetaFileName);

            IUserToken user;
            System.String[] usersList
                = meta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator);

            System.String tmpString = path;
            List<IUserToken> tokens = new List<IUserToken>(1);

            for (System.Int32 i = 0; i < usersList.Length; ++i)
            {
                path =
                      tmpString
                    + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                    + usersList[i]
                    + DBMSIO.DBMSConfig.FullMetaFileName;

                using (StreamReader sr = new StreamReader(path))
                    user = JsonSerializer.Deserialize<User>(sr.ReadToEnd());


                if (user.Owner == login.NodeName)
                    tokens.Add(user);

            }


            return tokens;

        }


        #endregion



        #region GetXxx Methods
        public ITableCollection GetReadedTableData(ITransactionInfo info) => SelectT.GetTableStorageByName(info);

        public List<String> GetSchemasList()
        {
            ILogin login = GetLogin(DBMSIO.DBMSConfig.SimpleSysAdminName);
            List<String> schemas = new List<String>();

            foreach (var item in login.LoginRights)
                schemas.Add(item.Key);

            return schemas;

        }

        public Dictionary<String, NodeType> GEtSchemasDict()
        {
            ILogin login = GetLogin(DBMSIO.DBMSConfig.SimpleSysAdminName);
            return login.LoginRights;
        }

        public List<String> GetSchemasListWithType()
        {
            ILogin login = GetLogin(DBMSIO.DBMSConfig.SimpleSysAdminName);
            List<String> schemas = new List<String>();

            foreach (var item in login.LoginRights)
                schemas.Add(item.Key + DBMSIO.DBMSConfig.StandartSeparator + item.Value);


            return schemas;
        }

        public IMetaInfo GetUserMetaInfo(System.String userName)
        {


            //UserNotExist
            throw new Exception();
        }

        public Int32 GetCountOfDataBasesOnServer()
        {
            Int32 Count = 0;
            foreach (String item in this.GetAllDataBaseNames())
                if (item != String.Empty)
                {
                    Count++;
                }


            return Count;
        }

        public Int32 GetCountOfAllViewOnServer()
        {
            List<String> db = this.GetAllDataBaseNames();

            Int32 count = 0;

            foreach (String item in db)
                if (item != String.Empty)
                {
                    IDataBaseIO dbIO
                        = new DataBaseIO(item, DBMSIO.GetCurrentLogin().NodeName);
                    count += dbIO.GetCountOfViews();
                }



            return count;
        }

        public Int32 GetCountOfAllProceduresOnServer()
        {
            List<String> db = this.GetAllDataBaseNames();

            Int32 count = 0;

            foreach (String item in db)
                if (item != String.Empty)
                {
                    IDataBaseIO dbIO
                        = new DataBaseIO(item, DBMSIO.GetCurrentLogin().NodeName);
                    count += dbIO.GetCountOfProcedures();
                }



            return count;
        }

        public Int32 GetCountOfAllTablesOnServer()
        {
            List<String> db = this.GetAllDataBaseNames();

            Int32 count = 0;

            foreach (String item in db)
                if (item != String.Empty)
                {
                    IDataBaseIO dbIO
                        = new DataBaseIO(item, DBMSIO.GetCurrentLogin().NodeName);
                    count += dbIO.GetCountOfTables();
                }



            return count;
        }

        public Int32 GetCountOfAllUsersOnServer()
        {
            List<String> db = this.GetAllDataBaseNames();

            Int32 count = 0;

            foreach (String item in db)
                if (item != String.Empty)
                {
                    IDataBaseIO dbIO
                        = new DataBaseIO(item, DBMSIO.GetCurrentLogin().NodeName);
                    count += dbIO.GetCountOfUser();
                }



            return count;
        }

        public List<String> GetAllDataBaseNames()
        {

            IMetaInfo table;
            using (StreamReader sr = new StreamReader
                (DBMSIO.DBMSConfig.BaseDir + DBMSIO.DBMSConfig.DataBasesStandartDir + DBMSIO.DBMSConfig.FullMetaFileName))
                table = JsonSerializer.Deserialize<TableMetaInfo>(sr.ReadToEnd());

            System.String[] db
                = table.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator);


            return new List<String>(db);

        }

        public List<String> GetNamesOfAllTablesOnServer()
        {
            List<String> list = new List<String>();
            List<String> db = this.GetAllDataBaseNames();

            foreach (String item in db)
                if (item != String.Empty)
                {
                    IDataBaseIO dbIO = new DataBaseIO(item, DBMSIO.GetCurrentLogin().NodeName);
                    list.AddRange(dbIO.GetTableNames());
                }


            return list;
        }

        public List<String> GetNamesOfAllProceduresOnServer()
        {
            List<String> list = new List<String>();
            List<String> db = this.GetAllDataBaseNames();

            foreach (String item in db)
                if (item != String.Empty)
                {
                    IDataBaseIO dbIO = new DataBaseIO(item, DBMSIO.GetCurrentLogin().NodeName);
                    list.AddRange(dbIO.GetProceduresName());
                }


            return list;
        }

        public List<String> GetNamesOfAllViewOnServer()
        {
            List<String> list = new List<String>();
            List<String> db = this.GetAllDataBaseNames();

            foreach (String item in db)
                if (item != String.Empty)
                {
                    IDataBaseIO dbIO = new DataBaseIO(item, DBMSIO.GetCurrentLogin().NodeName);
                    list.AddRange(dbIO.GetViewsNames());
                }


            return list;
        }

        public IServerMetaInfo GetServerInfo(System.String pathToServerRoot)
        {

            ServerMetaInfo server;
            using (StreamReader sr = new StreamReader(pathToServerRoot))
                server
                    = JsonSerializer.Deserialize<ServerMetaInfo>(sr.ReadToEnd());

            return server;

        }

        public List<String> GetNamesOfAllUserOnServer()
        {
            List<String> list = new List<String>();
            List<String> db = this.GetAllDataBaseNames();

            foreach (String item in db)
                if (item != String.Empty)
                {
                    IDataBaseIO dbIO = new DataBaseIO(item, DBMSIO.GetCurrentLogin().NodeName);
                    list.AddRange(dbIO.GetUserName());
                }


            return list;
        }

        public ILogin GetLogin(System.String loginName)
        {
            if (IsLoginExist(loginName))
            {
                Login login;
                using (StreamReader sr = new StreamReader(DBMSIO.DBMSConfig.FullPathToLoginRoot
                    + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                    + loginName
                    + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                    + DBMSIO.DBMSConfig.FullMetaFileName))
                {

                    login =
                        JsonSerializer.Deserialize<Login>(sr.ReadToEnd());

                }


                return login;
            }


            throw new LoginNotFound();
        }

        public IServerMetaInfo GetServerMetaInfo()
        {
            IServerMetaInfo serverMeta;

            using (StreamReader sr = new StreamReader(DBMSIO.DBMSConfig.BaseDir + DBMSIO.DBMSConfig.FullMetaFileName))
                serverMeta = JsonSerializer.Deserialize<ServerMetaInfo>(sr.ReadToEnd());


            return serverMeta;
        }

        public IMetaInfo GetMetaInfoByPath(System.String path)
        {
            TableMetaInfo info;
            using (StreamReader sr = new StreamReader(path))
                info
                    = JsonSerializer.Deserialize<TableMetaInfo>(sr.ReadToEnd());



            return info;

        }

        public List<String> GetNamesOfAllLoginsOnServer()
        {
            IMetaInfo info = GetMetaInfoByPath(
                DBMSIO.DBMSConfig.FullPathToLoginRoot
              + DBMSIO.DBMSConfig.FullMetaFileName);

            List<String> str = new List<string>(info.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator));

            return str;
        }

        public Int32 GetCountOfLoginOnServer()
        {

            IMetaInfo userMeta = GetMetaInfoByPath(
                                 DBMSIO.DBMSConfig.FullPathToLoginRoot
                               + DBMSIO.DBMSConfig.FullMetaFileName);

            System.String[] users
                = userMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator);


            return users.Length;
        }


        #endregion


        #region TruncateXxx Methods

        public void TruncateSchemaFromLogin(System.String loginName, System.String shemName)
        {

            if (!IsLoginExist(loginName))
                return;

            ILogin login = GetLogin(loginName);

            if (!login.LoginRights.TryGetValue(shemName, out _))
                return;

            login.LoginRights.Remove(shemName);

            ReWriteLogin(
                DBMSIO.DBMSConfig.FullPathToLoginRoot
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + loginName
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.FullMetaFileName,
                login
                );
        }

        public void TruncateSchemaFromAllLogins(String schemaName)
        {

            List<String> loginsName = DBMSIO.GetNamesOfAllLoginsOnServer();

            foreach (String loginName in loginsName)
            {
                if (loginName == String.Empty)
                    continue;


                if (DBMSIO.IsLoginExist(loginName))
                {
                    ILogin login = GetLogin(loginName);

                    if (!login.LoginRights.TryGetValue(schemaName, out _))
                        return;

                    login.LoginRights.Remove(schemaName);

                    ReWriteLogin(
                        DBMSIO.DBMSConfig.FullPathToLoginRoot
                        + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                        + loginName
                        + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                        + DBMSIO.DBMSConfig.FullMetaFileName,
                        login
                        );
                }
            }


        }

        internal void TruncateRightFromUser(ref IUserToken user, System.String command, System.String objectOn, NodeType type)
        {
            objectOn = Rbac.ResolveName(type) + objectOn;

            Dictionary<String, AllowedCommand> comm = user.UserRights.AllowedCommandOn;

            if (comm.ContainsKey(DBMSIO.GetCurrentDataBase().GetDataBaseName()))
            {
                AllowedCommand ac;
                comm.TryGetValue(DBMSIO.GetCurrentDataBase().GetDataBaseName(), out ac);

                if (ac.AllowCommand.ContainsKey(command))
                {
                    CommandRight cr;
                    if (ac.AllowCommand.TryGetValue(command, out cr))
                        if (cr.CommandRights.ContainsKey(objectOn))
                            cr.CommandRights.Remove(objectOn);

                }

            }

            DBMSIO.GetCurrentDataBase().ReWriteUserTokenByName(user.NodeName, user);

        }

        public System.Boolean TruncateDataBase(System.String info)
        {
            IMetaInfo table
                = GetMetaInfoByPath(DBMSIO.DBMSConfig.FullPathToDataBaseMetaFile);

            table.ChildrenList = table.ChildrenList.Replace(info, String.Empty);
            table.ChildrenList = table.ChildrenList.Trim();
            ReWriteMetaInfo(table, DBMSIO.DBMSConfig.FullPathToDataBaseMetaFile);

            return true;
        }

        public System.Boolean TruncateLogin(System.String login)
        {

            IMetaInfo table
                = GetMetaInfoByPath(DBMSIO.DBMSConfig.FullPathToTheLoginMetaFile);

            table.ChildrenList = table.ChildrenList.Replace(login, String.Empty);
            table.ChildrenList = table.ChildrenList.Trim();

            ReWriteMetaInfo(table, DBMSIO.DBMSConfig.FullPathToTheLoginMetaFile);

            return true;

        }

        #endregion



        #region Execute Commands Method

        public ITransactionMethodResult ExecuteCommand(System.String command, ITransactionInfo info){ throw new NotImplementedException();}


        public ITransactionMethodResult ExecuteCommand(DBCommand command, ITransactionInfo info)
        {


            switch (command) {

                case DBCommand.CREATE_DATABASE:
                case DBCommand.CREATE_LOGIN:
                case DBCommand.CREATE_PROCEDURE:
                case DBCommand.CREATE_TABLE:
                case DBCommand.CREATE_TRIGGER:
                case DBCommand.CREATE_USER:
                case DBCommand.CREATE_VIEW:

                case DBCommand.ALTER_DATABASE:
                case DBCommand.ALTER_LOGIN:
                case DBCommand.ALTER_PROCEDURE:
                case DBCommand.ALTER_TABLE:
                case DBCommand.ALTER_TRIGGER:
                case DBCommand.ALTER_USER:
                case DBCommand.ALTER_VIEW:

                case DBCommand.DROP_DATABASE:
                case DBCommand.DROP_LOGIN:
                case DBCommand.DROP_PROCEDURE:
                case DBCommand.DROP_TABLE:
                case DBCommand.DROP_TRIGGER:
                case DBCommand.DROP_USER:
                case DBCommand.DROP_VIEW: return _DDL.ExecuteCommand(command, info);

                case DBCommand.BACKUP:
                case DBCommand.BACKUP_WITH_LOG:
                case DBCommand.EXECUTE:
                case DBCommand.USE:
                case DBCommand.MIGRATE: return _ExtensionCommand.ExecuteCommand(command, info);

                case DBCommand.SELECT:
                case DBCommand.INSERT:
                case DBCommand.DELETE:
                case DBCommand.UPDATE: return _DML.ExecuteCommand(command, info);

                case DBCommand.REVOKE_USER:
                case DBCommand.DENY_USER:
                case DBCommand.DENY_LOGIN:
                case DBCommand.REVOKE_LOGIN:
                case DBCommand.GRANT_LOGIN:
                case DBCommand.GRANT_USER: return _DCL.ExecuteCommand(command, info);

                case DBCommand.GET_VIEW_TEXT:
                case DBCommand.GET_TABLE_INFO:
                case DBCommand.GET_TABLE_COLUMNS_DATAS:
                case DBCommand.GET_TABLE_COLUMNS_CONSTRAINTS:
                case DBCommand.GET_STORED_PROCEDURE_TEXT:
                case DBCommand.GET_SERVER_TYPE:
                case DBCommand.GET_SERVER_NAME:
                case DBCommand.GET_PREVIOUS_START_DATE:
                case DBCommand.GET_LOG_FILE_CONTENT:
                case DBCommand.GET_LAST_START_DATE:
                case DBCommand.GET_CURRENT_LOGIN_INFO:
                case DBCommand.GET_CURRENT_DATABASE_INFO:
                case DBCommand.GET_COUNT_OF_VIEW_ON_SERVER:
                case DBCommand.GET_COUNT_OF_VIEW_ON_CURRENT_DATABASE:
                case DBCommand.GET_COUNT_OF_USER_ON_SERVER:
                case DBCommand.GET_COUNT_OF_USER_ON_CURRENT_DATABASE:
                case DBCommand.GET_COUNT_OF_TABLE_ON_SERVER:
                case DBCommand.GET_COUNT_OF_TABLES_ON_CURRENT_DATABASE:
                case DBCommand.GET_COUNT_OF_PROCEDURES_ON_SERVER:
                case DBCommand.GET_COUNT_OF_PROCEDURES_ON_CURRENT_DATABASE:
                case DBCommand.GET_COUNT_OF_LOGIN_ON_SERVER:
                case DBCommand.GET_ALL_VIEW_NAME_ON_SERVER:
                case DBCommand.GET_ALL_VIEW_NAME_ON_CURRENT_DATABASE:
                case DBCommand.GET_ALL_TABLE_NAME_ON_SERVER:
                case DBCommand.GET_ALL_TABLE_NAME_ON_CURRENT_DATABASE:
                case DBCommand.GET_ALL_PROCEDURES_NAME_ON_SERVER:
                case DBCommand.GET_ALL_PROCEDURES_NAME_ON_CURRENT_DATABASE:
                case DBCommand.SET_SERVER_NAME:
                case DBCommand.SET_SERVER_PASSWORD:
                case DBCommand.GET_TRIGGER_TEXT:
                case DBCommand.RESTORE_SERVER:
                case DBCommand.GET_SERVER_CONFIG:
                case DBCommand.SET_STANDART_MODE:
                case DBCommand.SET_UNSAFE_MODE:
                case DBCommand.SET_LNG_UNSAFE_MODE:
                case DBCommand.SET_SERVER_TYPE: return _AdminExtensio.ExecuteCommand(command, info);

                case DBCommand.CHANGE_PASSWORD_TO_USER:
                case DBCommand.CHANGE_PASSWORD_TO_LOGIN: return _DCLExtension.ExecuteCommand(command, info);


                default: { return new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.UNKNOWN_COMMAND]); }
            }


         
        }

        #endregion

    }

}
