﻿using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using System;
using DBMS.Core.MMIL;
using DBMS.Core.Table;
using System.Collections.Generic;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Helpers;
using System.Linq;
using DBMS.Core.TableCollections;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.MMIL.ILConfiguration;
using DBMS.Integration.JSON.SQL.StorageCollection;
using DBMS.Core;
using DBMS.Core.UsersFromRoles;


namespace DBMS.Integration.JSON.SQL
{
    internal sealed class JSONDataManipulationLanguage
    {

        #region Documentation
#if DocTrue
        static JSONDataManipulationLanguage() { }
#endif
        #endregion


        public ITransactionMethodResult ExecuteCommand(DBCommand command, ITransactionInfo info) => command switch
        {
            DBCommand.SELECT => Select(info),
            DBCommand.DELETE => Delete(info),
            DBCommand.UPDATE => Update(info),
            DBCommand.INSERT => Insert(info),

            _ => new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.UNKNOWN_COMMAND])
        };


        #region Commands
        private ITransactionMethodResult Select(ITransactionInfo info)
        {

            try
            {
                MMILDataManipulationContext mmil = info.AddedInfo as MMILDataManipulationContext;

                SelectedTable selectedTable = new SelectedTable(info.AddedInfo as MMILDataManipulationContext);



                ITable res = selectedTable.Select().CrossJoin().Union().Where().GetResult();

                if (mmil.GetContextValue(MMILDataManipulationConfigure.MaxDeclarationCommand) != null) return selectedTable.Max();
                if (mmil.GetContextValue(MMILDataManipulationConfigure.MinDeclarationCommand) != null) return selectedTable.Min();
                if (mmil.GetContextValue(MMILDataManipulationConfigure.SumDeclarationCommand) != null) return selectedTable.Sum();
                if (mmil.GetContextValue(MMILDataManipulationConfigure.AvgDeclarationCommand) != null) return selectedTable.Avg();
                if (mmil.GetContextValue(MMILDataManipulationConfigure.CountDeclarationCommand) != null) return selectedTable.Count();

                if (mmil.GetContextValue(MMILDataManipulationConfigure.GetDomainDeclarationCommand) != null) return selectedTable.GetDomainInfo();
                if (mmil.GetContextValue(MMILDataManipulationConfigure.GetColumnTypesDeclarationCommand) != null) return selectedTable.GetCollumnTypes();
                if (mmil.GetContextValue(MMILDataManipulationConfigure.DistinctDeclarationCommand) != null) res = res.Distinct();
                if (mmil.GetContextValue(MMILDataManipulationConfigure.ReverseDeclarationCommand) != null) res = res.Reverse();
                if (mmil.GetContextValue(MMILDataManipulationConfigure.OrderByDescDeclarationCommand) != null) res = res.OrderByDesc(mmil.GetContextValue(MMILDataManipulationConfigure.OrderByDescDeclarationCommand));
                if (mmil.GetContextValue(MMILDataManipulationConfigure.OrderByAscDeclarationCommand) != null) res = res.OrderByAsc(mmil.GetContextValue(MMILDataManipulationConfigure.OrderByAscDeclarationCommand));

  


                return new JsonMethodResult(true,
                    "Query return " + res.TableRow.Count +
                    " records from table" + info.Parameters
                    + " in DataBase " + info.DataBaseName
                    + "Query inspied by user " + info.UserName)
                { AddedInfo = res };
            } catch (Exception e)
            {
                DBExeption dbEx = e as DBExeption;
                if (dbEx != null)
                    return new JsonMethodResult(false, dbEx.ServerMessageError);
                else return new JsonMethodResult(false, e.Message);
            }
        }

        private ITransactionMethodResult Insert(ITransactionInfo info)
        {
            ITransactionMethodResult result = null; 
            try
            {
                MMILDataManipulationContext mmil = info.AddedInfo as MMILDataManipulationContext;
                List<String> valuesList = info.AddedInfoFragmentTwo as List<String>;

                SelectedTable inserted = new SelectedTable(mmil, valuesList, info.Parameters);

                inserted.Select();
                result = inserted.Insert();

                if (result.IsSuccess)
                    result.ResultString += $"{info.DataBaseName} Query inspired by user {info.UserName}";
                else return result;

                inserted.Save();
            }
            catch (Exception e)
            {
                DBExeption dbEx = e as DBExeption;
                if (dbEx != null)
                    return new JsonMethodResult(false, dbEx.ServerMessageError);
                else return new JsonMethodResult(false, e.Message);
            }
            return result;
        }

        private ITransactionMethodResult Update (ITransactionInfo info)
        {

            ITransactionMethodResult result = null;

            try
            {
                SelectedTable update = new SelectedTable(info.AddedInfo as MMILDataManipulationContext);
                update.Select().Where().GetResult();
                result = update.Update();

                if (result.IsSuccess)
                    result.ResultString += "  query inspired by user " + info.UserName;
                else return result;

                update.Save();

            }
            catch (Exception e)
            {
                DBExeption dbEx = e as DBExeption;
                if (dbEx != null)
                    return new JsonMethodResult(false, dbEx.ServerMessageError);
                else return new JsonMethodResult(false, e.Message);
            }

            return result;
           
        }

        private ITransactionMethodResult Delete(ITransactionInfo info)
        {
            ITransactionMethodResult result = null;
            try
            {
                SelectedTable deleted = new SelectedTable(info.AddedInfo as MMILDataManipulationContext);
                deleted.Select().Where();
                result = deleted.Delete();

                if (result.IsSuccess)
                    result.ResultString += " query inspured by user " + info.UserName;
                else return result;

                deleted.Save();

            }
            catch (Exception e)
            {
                DBExeption dbEx = e as DBExeption;
                if (dbEx != null)
                    return new JsonMethodResult(false, dbEx.ServerMessageError);
                else return new JsonMethodResult(false, e.Message);
            }

            return result;
           
        }
        #endregion


    }
}
