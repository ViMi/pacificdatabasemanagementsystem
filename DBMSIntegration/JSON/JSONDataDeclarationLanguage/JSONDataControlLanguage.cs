﻿using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using DBMS.Core;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.Helpers;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.User;

namespace DBMS.Integration.JSON.SQL
{
    internal sealed class JSONDataControlLanguage
    {

        #region Documentation
#if DocTrue
        static JSONDataControlLanguage() { }
#endif
        #endregion


        public ITransactionMethodResult ExecuteCommand(DBCommand command, ITransactionInfo info) => command switch
        {
            DBCommand.DENY_USER => DenyForUser(info),
            DBCommand.REVOKE_USER => RevokeForUser(info),
            DBCommand.GRANT_USER => GrantedForUser(info),

            DBCommand.DENY_LOGIN => DenyForLogin(info),
            DBCommand.REVOKE_LOGIN => RevokeForLogin(info),
            DBCommand.GRANT_LOGIN => GrantedForLogin(info),

            _ => new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.UNKNOWN_COMMAND])
        };


        #region Deny Methods
        private ITransactionMethodResult DenyForUser(ITransactionInfo info)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                ITransactionMethodResult res;
                res = JSON.RulesManager.Invoke((Byte)ServerRulesID.USER_EXIST, info);
                if (!res.IsSuccess) return res;

                if (DBMSIO.GetCurrentDataBase().GetUserByLoginName(DBMSIO.GetCurrentLogin().NodeName).NodeName == info.Parameters)
                    return new JsonMethodResult(false, "YOU CAN NOT CHANGE SELF\n ");
                #endregion
            }



            IUserToken token = DBMSIO.GetCurrentDataBase().GetUserByName(info.Parameters);




            AllowedCommand controlMap = info.AddedInfo as AllowedCommand;
            AllowedCommand all;

            if (
            !token.UserRights
                .AllowedCommandOn
                .TryGetValue(DBMSIO.GetCurrentDataBase().GetDataBaseName(),
                out all))
                return new JsonMethodResult
                    (false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SHEMA_NOT_EXIST] + " IN USER LIST");


            if (all.AllowCommand == null)
                all.AllowCommand = new Dictionary<String, CommandRight>();

            foreach (var item in controlMap.AllowCommand)
            {
                if (!all.AllowCommand.ContainsKey(item.Key))
                {
                    for (System.Int32 i = 0; i < item.Value.CommandRights.Count; ++i)
                    {
                        item.Value.CommandRights.Remove(item.Value.CommandRights.ElementAt(i).Key);
                        item.Value.CommandRights.Add(item.Value.CommandRights.ElementAt(i).Key, false);
                    }

                }
                else
                {
                    CommandRight right;
                    all.AllowCommand.TryGetValue(item.Key, out right);

                    foreach(var rightItem in right.CommandRights)
                    {
                        for (System.Int32 i = 0; i < item.Value.CommandRights.Count; ++i)
                        {
                            if (rightItem.Key == item.Value.CommandRights.ElementAt(i).Key)
                            {
                                item.Value.CommandRights.Remove(item.Value.CommandRights.ElementAt(i).Key);
                                item.Value.CommandRights.Add(item.Value.CommandRights.ElementAt(i).Key, true);
                            }
                        }
                    }

                         
                }
            }




            DBMSIO.ReWriteUserTokenByname(info.Parameters, token);



            return new JsonMethodResult(true, "User With name " + info.Parameters +
                " Granted rights" + " From user with login " + DBMSIO.GetCurrentLogin().NodeName + " in DataBase " + DBMSIO.GetCurrentDataBase().GetDataBaseName());
        }

        private ITransactionMethodResult DenyForLogin(ITransactionInfo info)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                ITransactionMethodResult res;
                res = JSON.RulesManager.Invoke((Byte)ServerRulesID.LOGIN_EXIST, info);
                if (!res.IsSuccess) return res;
                if (DBMSIO.GetCurrentLogin().NodeName == info.Parameters)
                    return new JsonMethodResult(false, "YOU CAN NOT CHANGE SELF\n");
                #endregion
            }

            ILogin login = this.ReadRightsFile<Login>(DBMSIO.DBMSConfig.FullPathToLoginRoot
                   + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                   + info.Parameters
                   + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                   + DBMSIO.DBMSConfig.FullMetaFileName);

            Dictionary<String, NodeType> controlMap =
                info.AddedInfo as Dictionary<String, NodeType>;

           
            for(System.Int32 i = 0; i < controlMap.Count; ++i)
                if(login.LoginRights.ContainsKey(controlMap.ElementAt(i).Key))
                    login.LoginRights.Remove(controlMap.ElementAt(i).Key);

           



            this.WriteRightsFile<ILogin>(DBMSIO.DBMSConfig.FullPathToLoginRoot
                    + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                    + info.Parameters
                    + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                    + DBMSIO.DBMSConfig.FullMetaFileName, login);




            return new JsonMethodResult(true, "Login With name " + login.NodeName + " Deny rights" +
               " From user with login " + DBMSIO.GetCurrentLogin().NodeName);
        }
        #endregion


        #region Revoke Methods
        private ITransactionMethodResult RevokeForLogin(ITransactionInfo info)
        {

            DenyForLogin(info);


            return new JsonMethodResult(true, "Login With name " + info.Parameters + " Granted rights" +
               " From user with login " + DBMSIO.GetCurrentLogin().NodeName);
        }

        private ITransactionMethodResult RevokeForUser(ITransactionInfo info)
        {

            DenyForUser(info);

            return new JsonMethodResult(true, "User With name " + info.Parameters +
                " Revoke rights" + " From user with login " + DBMSIO.GetCurrentLogin().NodeName + " in DataBase " + DBMSIO.GetCurrentDataBase().GetDataBaseName());
        }
     
        #endregion


        #region Granted Methods
        private ITransactionMethodResult GrantedForUser(ITransactionInfo info)
        {

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleChecck
                ITransactionMethodResult res;
                res = JSON.RulesManager.Invoke((Byte)ServerRulesID.USER_EXIST, info);
                if (!res.IsSuccess) return res;

                if (DBMSIO.GetCurrentDataBase().GetUserByLoginName(DBMSIO.GetCurrentLogin().NodeName).NodeName == info.Parameters)
                    return new JsonMethodResult(false, "YOU CAN NOT CHANGE SELF\n");
                #endregion
            }

            IUserToken token = DBMSIO.GetCurrentDataBase().GetUserByName(info.Parameters);

            AllowedCommand controlMap = info.AddedInfo as AllowedCommand;
            AllowedCommand all;


            if (
            !token.UserRights
                .AllowedCommandOn
                .TryGetValue(DBMSIO.GetCurrentDataBase().GetDataBaseName(),
                out all))
                return new JsonMethodResult
                    (false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SHEMA_NOT_EXIST] + " IN USER LIST");

            if (all.AllowCommand == null)
                all.AllowCommand = new Dictionary<String, CommandRight>();

            foreach (var item in controlMap.AllowCommand)
            {

                if (!all.AllowCommand.ContainsKey(item.Key))
                {
                    System.Int32 size = item.Value.CommandRights.Count;

                    for (System.Int32 i = 0; i <size; ++i)
                    {
                        String key = item.Value.CommandRights.ElementAt(i).Key;

                       item.Value.CommandRights.Remove(key);
                       item.Value.CommandRights.Add(key, true);    
                    }

                    all.AllowCommand.Add(item.Key, item.Value);
                    

                }
                else
                {
                    CommandRight right;
                    all.AllowCommand.TryGetValue(item.Key, out right);

                    foreach (var rightItem in right.CommandRights)
                    {
                        System.Int32 size = item.Value.CommandRights.Count;

                        for (System.Int32 i = 0; i < size; ++i)
                        {
                            String key = item.Value.CommandRights.ElementAt(i).Key;

                            if (rightItem.Key == key)
                            {
                                item.Value.CommandRights.Remove(key);
                                item.Value.CommandRights.Add(key, true);
                            }
                        }


                    }


                }
            }

            DBMSIO.ReWriteUserTokenByname(info.Parameters, token);
      

            return new JsonMethodResult(true, "User With name " + info.Parameters +
                " Granted rights" + " From user with login " + DBMSIO.GetCurrentLogin().NodeName + " in DataBase " + DBMSIO.GetCurrentDataBase().GetDataBaseName());
        }

        private ITransactionMethodResult GrantedForLogin(ITransactionInfo info)
        {

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {

                #region RuleCheck
                ITransactionMethodResult res;
                res = JSON.RulesManager.Invoke((Byte)ServerRulesID.LOGIN_EXIST, info);
                if (!res.IsSuccess) return res;
                if (DBMSIO.GetCurrentLogin().NodeName == info.Parameters)
                    return new JsonMethodResult(false, "YOU CAN NOT CHANGE SELF\n");
                #endregion
            }


            ILogin login = this.ReadRightsFile<Login>(DBMSIO.DBMSConfig.FullPathToLoginRoot
                    + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                    + info.Parameters
                    + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                    + DBMSIO.DBMSConfig.FullMetaFileName);

            Dictionary<String, NodeType> controlMap =
                        info.AddedInfo as Dictionary<String, NodeType>;


            for (System.Int32 i = 0; i < controlMap.Count; ++i)
                if (!login.LoginRights.ContainsKey(controlMap.ElementAt(i).Key))
                    login.LoginRights.Add(controlMap.ElementAt(i).Key, controlMap.ElementAt(i).Value);
            



            this.WriteRightsFile<ILogin>(DBMSIO.DBMSConfig.FullPathToLoginRoot
                    + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                    + info.Parameters
                    + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                    + DBMSIO.DBMSConfig.FullMetaFileName, login);


            return new JsonMethodResult(true, "Login With name " + login.NodeName + " Granted rights" +
               " From user with login " + DBMSIO.GetCurrentLogin().NodeName);
        }

        #endregion





        #region File System Methods
        private void WriteRightsFile<T>(System.String path, T token)
        {
            try
            {
                using (FileStream fs = new FileStream(path, FileMode.Truncate))
                {
                    String json = JsonSerializer.Serialize<T>(token);

                    System.Byte[] arr
                        = Encoding.UTF8.GetBytes(json);
                    fs.Write(arr, 0, arr.Length);
                }

            }
            catch (Exception ex)
            {
                if (ex.IsFileLocked())
                    throw new FileLockedError();
                else
                    throw ex;
                
            }

        }

        private T ReadRightsFile<T>(System.String path)
        {
            T token = default;
            try
            {
                using (StreamReader fs = new StreamReader(path))
                    token = JsonSerializer.Deserialize<T>(fs.ReadToEnd());
                
            }
            catch (Exception ex)
            {

                if (ex.IsFileLocked())
                    throw new FileLockedError();
                else
                    throw ex;
                
            }

            return token;

        }
        #endregion




    }
}
