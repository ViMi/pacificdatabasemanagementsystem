﻿using System;
using System.Collections.Generic;
using DBMS.Core.TableCollections;
using DBMS.Core.IO;
using DBMS.Core.Table;
using DBMS.Core;
using System.IO;
using System.Text.Json;
using DBMS.Core.Secure.UserRights;
using System.Linq;
using DBMS.Core.InfoTypes;
using DBMS.Core.MMIL;
using DBMS.Core.MMIL.ILConfiguration;
using DBMS.Core.Helpers;


namespace DBMS.Integration.JSON.SQL.StorageCollection
{


    internal enum StorageDataStep
        :Int16
    {
        LOW_STEP = 250,
        MEDIUM_STEP = 500,
        BIG_STEP = 1000,
    }



    internal class IndexedTableCollection
        : ITableCollection
    {


        #region Documentation
#if DocTrue
        static IndexedTableCollection() { }
#endif
        #endregion


        #region Variable Definition
        public System.String _DataBaseName { get; }
        public System.String _DataBaseOwner { get; }
        public System.String _WayToDB { get; }
        public System.String _TableName { get; }
        public ITableInfo _TableInfo { set; get; }
        public System.Int32 Length { set; get; }
        public Table _TableCollection { get; set; }

        private StorageDataStep Step { set; get; } = StorageDataStep.LOW_STEP;
        private List<Table> _tableList { set; get; }

        #endregion


        public IndexedTableCollection(
            System.String dataBaseName,
            System.String dataBaseOwner,
            System.String tableName)
        {
            this._DataBaseName = dataBaseName;
            this._DataBaseOwner = dataBaseOwner;
            this._WayToDB =
                DBMSIO.DBMSConfig.GetDataBaseRootWay(dataBaseOwner, dataBaseName);
            this._TableName = tableName;
            this._tableList = new List<Table>();
            this.ReadMetaInfo();
        }



        public ITableInfo GetMetaInfoTable() => this._TableInfo;
        public string GetWayToStored() => this._WayToDB
                 + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                 + DBMSIO.DBMSConfig.DataBaseTableDir
                 + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                 + this._TableName
                 + DBMSIO.DBMSConfig.OSDirectoryWaySeparator;


        public ITableCollection ReadMetaInfo()
        {
            this._TableInfo ??= DBMSIO.GetCurrentDataBase().GetTableMetaByName(this._TableName);

            return this;

        }

        public ITableCollection ReadStoredTable()
        {
            String wayToTableFile
                = GetWayToStored();

            String[] names 
                = this.GetFileNameArray();

            for(Int32 i = 0; i < names.Length; ++i)
            {
                String way = wayToTableFile + names[i];


                if (!File.Exists(way))
                    this.IntiWithEmpty(way);


                using (StreamReader sr = new StreamReader(way))
                {
                    String json = sr.ReadToEnd();
                    this._tableList.Add(
                        JsonSerializer.Deserialize<Table>(json)
                        );

                }

            }


            this._TableCollection = new Table();
            this._TableCollection.TableRow = new Dictionary<string, TableRow>();

            for(Int32  i = 0; i < this._tableList.Count; ++i)
            {
                for(Int32 j = 0; j <  this._tableList[i].TableRow.Count; ++j)
                {
                    String key = this._tableList[i].TableRow.ElementAt(j).Key;
                    TableRow row = this._tableList[i].TableRow.ElementAt(j).Value;

                    this._TableCollection.TableRow.Add(key, row);
                }

            }


            return this;

        }

        public ITableCollection StoreCurrentTable()
        {
            String wayToTableFile
                = GetWayToStored();

            String[] names 
                = this.GetFileNameArray();


            for (Int32 i = 0; i < names.Length; ++i)
            {
                String way = wayToTableFile + names[i];
                Table table = new Table();
                table.TableRow = new Dictionary<string, TableRow>();

                for(Int32 j = (int)Step*i; j < (int)Step*(i+1); ++j)
                {
                    if (j < this._TableCollection.TableRow.Count)
                    {
                        String key = this._TableCollection.TableRow.ElementAt(j).Key;
                        TableRow row = this._TableCollection.TableRow.ElementAt(j).Value;
                        table.TableRow.Add(key, row);
                    }

                }

                if (!File.Exists(way))
                    IntiWithEmpty(way);
                

                using (StreamWriter sw = new StreamWriter(way))
                {
                    String arr =
                        JsonSerializer.Serialize<ITable>(table);

                    sw.Write(arr);
                }
            }



            return this;
        }



        private void IntiWithEmpty(String wayToTableFile)
        {
            HeapTableCollection collection =
                new HeapTableCollection(this._DataBaseName, this._DataBaseOwner, this._TableName);


            collection._TableCollection = new DBMS.Core.Table.Table();
            collection._TableCollection.TableRow = new Dictionary<String, DBMS.Core.Table.TableRow>();

            this.ReadMetaInfo();
            ITableInfo info = _TableInfo;

            using (FileStream sw = new FileStream(wayToTableFile, FileMode.Create))
            {
                String json = JsonSerializer.Serialize(collection._TableCollection);
                Byte[] arr = System.Text.Encoding.UTF8.GetBytes(json);
                sw.Write(arr, 0, arr.Length);
            }


            this._TableInfo.FileCount = this._TableInfo.FileCount + 1;
            DBMSIO.GetCurrentDataBase().ReWriteMetaTableInfoByTableName(this._TableName);
        }



        private String[] GetFileNameArray()
        {

            Int32 files = 0;
            if (this._TableInfo.RowCount != 0)
             files = (Int32)((Double)this.Step % (Double)this._TableInfo.RowCount);

            if (files == 0) files = 1;
            _TableInfo.FileCount = files;


            DBMSIO.GetCurrentDataBase().ReWriteMetaTableInfoByTableName(this._TableName);

            String[] fNames = new string[files];

            for (Int32 i = 0; i < files; ++i)
                fNames[i] =
                    this._TableName
                    + i.ToString()
                    + DBMSIO.DBMSConfig.StandartMetaFileExtension;
            


            return fNames;
        }



        internal Dictionary<String, ITableCollection> GetTableStorageFromTablesList(String[] fromSelected)
        {


            Dictionary<String, ITableCollection> selected = new Dictionary<string, ITableCollection>();

            for (Int32 i = 0; i < fromSelected.Length; ++i)
            {
                ITransactionInfo ti_
                    = new JsonTransactionInfo(DBMSIO.GetCurrentDataBase().GetDataBaseName(),
                    DBMSIO.GetCurrentLogin().NodeName, fromSelected[i]);

                ITableCollection coll = GetTableStorageByName(ti_);


                selected.Add(fromSelected[i], coll);
            }


            return selected;
        }



        internal ITableCollection GetTableStorageByName(ITransactionInfo info)
        {

            ITableInfo ti =
                DBMSIO.GetCurrentDataBase().GetTableMetaByName(info.Parameters);
            ITableCollection selected = default;


            if (ti.IsIndexed)
                selected = new IndexedTableCollection(info.DataBaseName, info.UserName, info.Parameters);
            
            else
                selected = new HeapTableCollection(info.DataBaseName, info.UserName, info.Parameters);
            

            selected.ReadStoredTable();
            return selected;
        }





    }
}
