﻿using System;
using System.Collections.Generic;
using DBMS.Core;
using DBMS.Core.Table;
using DBMS.Core.TableCollections;
using DBMS.Core.IO;
using System.IO;
using System.Text.Json;
using DBMS.Core.Secure.UserRights;


namespace DBMS.Integration.JSON.SQL.StorageCollection
{
    internal class HeapTableCollection
        : ITableCollection
    {

        #region Documentation
#if DocTrue
        static HeapTableCollection() { }

#endif
        #endregion



        #region Variable Definition
        public DBMS.Core.Table.Table _TableCollection { set; get; }
        public ITableInfo _TableInfo { set; get; }
        public System.String _DataBaseName { get; }
        public System.String _DataBaseOwner { get; }
        public System.String _WayToDB { get; }
        public System.String _TableName { set; get; }
        public System.Int32 Length { set; get; }
        #endregion


        public HeapTableCollection(
            System.String dataBaseName, 
            System.String dataBaseOwner,
            System.String tableName)
        {
            this._DataBaseName = dataBaseName;
            this._DataBaseOwner = dataBaseOwner;
            this._WayToDB = 
                DBMSIO.DBMSConfig.GetDataBaseRootWay(dataBaseOwner, dataBaseName);

            this._TableName = tableName;
            this._TableCollection = new DBMS.Core.Table.Table();
            this.ReadMetaInfo();
        }


        public string GetWayToStored() => this._WayToDB
                 + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                 + DBMSIO.DBMSConfig.DataBaseTableDir
                 + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                 + this._TableName
                 + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                 + this._TableName
                 + DBMSIO.DBMSConfig.StandartMetaFileExtension;

        public ITableCollection ReadMetaInfo()
        {
            this._TableInfo = this._TableInfo == null ?
           DBMSIO.GetCurrentDataBase().GetTableMetaByName(this._TableName)
           : this._TableInfo;

            return this;
        }
        

        public ITableCollection ReadStoredTable()
        {
            String wayToTableFile
                = GetWayToStored();

            if (!File.Exists(wayToTableFile))
                this.IntiWithEmpty(wayToTableFile);
            

            using (StreamReader sr = new StreamReader(wayToTableFile))
            {
                String json = sr.ReadToEnd();
                this._TableCollection 
                    = JsonSerializer.Deserialize<DBMS.Core.Table.Table>(json);
            }

            return this;

        }



        public ITableInfo GetMetaInfoTable() => this._TableInfo;

        private void IntiWithEmpty(String wayToTableFile)
        {
            HeapTableCollection collection =
                new HeapTableCollection(this._DataBaseName, this._DataBaseOwner, this._TableName);


            collection._TableCollection = new DBMS.Core.Table.Table();
            collection._TableCollection.TableRow = new Dictionary<string, DBMS.Core.Table.TableRow>();

            this.ReadMetaInfo();
            ITableInfo info = _TableInfo;
        
            using (FileStream sw = new FileStream(wayToTableFile, FileMode.Create))
            {
                String json = JsonSerializer.Serialize(collection._TableCollection);
                Byte[] arr = System.Text.Encoding.UTF8.GetBytes(json);
                sw.Write(arr, 0, arr.Length);
            }

            this._TableInfo.FileCount = 1;
            IDataBaseIO db = DBMSIO.GetCurrentDataBase();
            db.ReWriteMetaTableInfoByTableName(this._TableName);
        }

        public ITableCollection StoreCurrentTable()
        {
            String wayToTableFile 
                = GetWayToStored();

            using(StreamWriter sw = new StreamWriter(wayToTableFile))
            {

                System.String arr
                    = JsonSerializer.Serialize<Table>(this._TableCollection);
                sw.Write(arr);

            }

            return this;
        }




    }
}
