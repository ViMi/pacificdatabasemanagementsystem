﻿using System;
using System.Collections.Generic;
using DBMS.Core;
using DBMS.Core.Table;
using DBMS.Core.TableCollections;
using DBMS.Core.IO;
using System.IO;
using System.Text.Json;
using DBMS.Core.Secure.UserRights;



namespace DBMS.Integration.JSON.SQL.StorageCollection
{
    internal sealed class VirtualTableCollection
                : ITableCollection
    {

        #region Documentaiton
#if DocTrue
        static VirtualTableCollection() { }
#endif
        #endregion


        #region Varialbe Definition
        public DBMS.Core.Table.Table _TableCollection { set; get; }
        public ITableInfo _TableInfo { set; get; }
        public System.String _DataBaseName { get; }
        public System.String _DataBaseOwner { get; }
        public System.String _WayToDB { get; }
        public System.String _TableName { set; get; }
        public System.Int32 Length { set; get; }
        public static System.String VIRTUAL_OBJECT { get; } = "VIRTUAL_OBJECT";
        #endregion




        public VirtualTableCollection(
            System.String dataBaseName,
            System.String dataBaseOwner,
            System.String tableName,
            ITable table)
        {
            this._DataBaseName = dataBaseName;
            this._DataBaseOwner = dataBaseOwner;
            this._WayToDB =
                DBMSIO.DBMSConfig.GetDataBaseRootWay(dataBaseOwner, dataBaseName);

            this._TableName = tableName;
            this._TableCollection = table as Table;
            this.ReadMetaInfo();
        }


        public String GetWayToStored() => VIRTUAL_OBJECT;
        public ITableCollection ReadMetaInfo(){ return this; }
        public ITableCollection ReadStoredTable() {  return this; }
        public ITableInfo GetMetaInfoTable() => this._TableInfo;
        public ITableCollection StoreCurrentTable() {  return this;}


    }
}
