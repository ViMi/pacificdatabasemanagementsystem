﻿using System;
using System.Collections.Generic;
using DBMS.Core.Helpers.RulesControl;
using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using DBMS.Core.Table;
using System.Text.RegularExpressions;
using DBMS.PQL.Grammar;

namespace DBMS.Integration.JSON.SQL.Specification
{
    internal sealed class LikeSpecification
        : IStatement
    {


        public LikeSpecification() { }
        public LikeSpecification(
            String key,
            String insertedValue,
            String keyMain,
            PacificDataBaseDataType dataType,
            ref Dictionary<System.String, String> consItem,
            ref TableCell tc)
        {
            _Key = key;
            _InsertedItem = insertedValue;
            _KeyMain = keyMain;
            _DataType = dataType;
            _ConsItem = consItem;
            _tc = tc;
        }


        public IStatement SetValue(
            String key,
            String insertedValue,
            String keyMain,
            PacificDataBaseDataType dataType,
            ref Dictionary<System.String, String> consItem,
            ref TableCell tc)
        {
            _Key = key;
            _InsertedItem = insertedValue;
            _KeyMain = keyMain;
            _DataType = dataType;
            _ConsItem = consItem;
            _tc = tc;

            return this;
        }


        #region Variable Definition
        private String _Key;
        private TableCell _tc;
        private String _InsertedItem;
        private PacificDataBaseDataType _DataType;
        private String _KeyMain;
        private Dictionary<System.String, String> _ConsItem;
        #endregion

        public int StatementID { get; set; }
        public string StatementName { get; set; }
        public Statement StatementDefinition { get; set; } 

        public ITransactionMethodResult Invoke(ITransactionInfo ti)
        {


            if (_Key == PQLGrammar.LIKE)
            {

                System.String str;
                _ConsItem.TryGetValue(_Key, out str);
                str = str.Trim(PQLGrammar.ConstantLiteralStringBracetChar).Trim();
                Regex regex = new Regex(str.ToString());


                String tmp = _InsertedItem.Trim().Trim(PQLGrammar.ConstantLiteralStringBracetChar);

                MatchCollection matches = regex.Matches(tmp);
                System.String[] parameters = new System.String[matches.Count];
                if (matches.Count != 1)
                    return new JsonMethodResult
                        (false, $"Attribute with name {_KeyMain} constraint error (Must be Like {str})");

                _tc = TableCreator.CreateTableCell(alias: _KeyMain, columnData: _InsertedItem, columnType: _DataType) as TableCell;


                return new JsonMethodResult(true, String.Empty) { AddedInfo = _tc };
           }


            return new JsonMethodResult(true, String.Empty) { AddedInfo = null };


        }
    }
}
