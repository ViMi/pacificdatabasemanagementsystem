﻿using System;
using System.Collections.Generic;
using DBMS.Core.Helpers.RulesControl;
using DBMS.Core.InfoTypes;
using DBMS.PQL.Grammar;
using DBMS.Core.IO;
using DBMS.Core.Table;



namespace DBMS.Integration.JSON.SQL.Specification
{
    internal sealed class FKReferences
        : IStatement
    {




        public FKReferences() { }
        public FKReferences(
            String key, 
            String insertedValue, 
            String keyMain, 
            PacificDataBaseDataType dataType, 
            ref Dictionary<System.String, String> consItem,
            ref TableCell tc)
        {
            _Key = key;
            _InsertedItem = insertedValue;
            _KeyMain = keyMain;
            _DataType = dataType;
            _ConsItem = consItem;
            _tc = tc;
        }


        public IStatement SetValue(
            String key,
            String insertedValue,
            String keyMain,
            PacificDataBaseDataType dataType,
            ref Dictionary<System.String, String> consItem,
            ref TableCell tc)
        {
            _Key = key;
            _InsertedItem = insertedValue;
            _KeyMain = keyMain;
            _DataType = dataType;
            _ConsItem = consItem;
            _tc = tc;

            return this;
        }



        #region Value Definition
        private String _Key;
        private String _InsertedItem;
        private PacificDataBaseDataType _DataType;
        private String _KeyMain;
        private Dictionary<System.String, String> _ConsItem;
        private TableCell _tc;
        #endregion

        public int StatementID { get; set; }
        public string StatementName { get; set; }
        public Statement StatementDefinition { get; set; }


        public ITransactionMethodResult Invoke(ITransactionInfo ti)
        {


            if (_Key == PQLGrammar.FOREIGN_KEY_REFERENCES)
            {

                System.String value;
                _ConsItem.TryGetValue(_Key, out value);
                String[] values = (value as String).Split(PQLGrammar.LeftParameterBracetInner);

                if (DBMSIO.GetCurrentDataBase().IsTableExist(values[0]))
                {



                    if (!DBMSIO.GetCurrentDataBase().
                        IsColumnWithNameExsitInTableWithName
                        (values[1].Replace(PQLGrammar.RightParameterBracetInner.ToString(), String.Empty), values[0]))
                        return new JsonMethodResult(false
                     , $"FOREIGN_REFERENCED_TABLE " +
                     $"{values[0]} do not have column with name ${values[1].Replace(PQLGrammar.RightParameterBracetInner.ToString(), String.Empty)}");



                    if (!DBMSIO.GetCurrentDataBase()
                        .IsValueExistIntColumnWithNameInTableWithName
                        (values[1].Replace(PQLGrammar.RightParameterBracetInner.ToString(), String.Empty), values[0], _InsertedItem))
                        return new JsonMethodResult(false
                         , $"FOREIGN_REFERENCED_TABLE " +
                         $"{value} do not have a PRIMARY_KEY contraint and can not be " +
                         $"indexed because value not exist");


                    _tc = TableCreator.CreateTableCell(alias: _KeyMain, columnData: _InsertedItem, columnType: _DataType) as TableCell;

               

                    return new JsonMethodResult(true, String.Empty) { AddedInfo=_tc };
                }



                return new JsonMethodResult(false, $"TABLE NOT FOUND {values[0]}") { AddedInfo = _tc };

            }


            return new JsonMethodResult(true, String.Empty) { AddedInfo = _tc };





        }
    }
}
