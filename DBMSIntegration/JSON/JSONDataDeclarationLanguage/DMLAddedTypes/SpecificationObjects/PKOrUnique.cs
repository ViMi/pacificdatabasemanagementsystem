﻿using System;
using System.Collections.Generic;
using DBMS.Core.Helpers.RulesControl;
using DBMS.Core.InfoTypes;
using DBMS.Core.Table;
using DBMS.PQL.Grammar;

namespace DBMS.Integration.JSON.SQL.Specification
{
    internal sealed class PKOrUnique
        : IStatement
    {



        public PKOrUnique(String key,
            String keMain,
            ref ITable operateTable,
            String insertedItem,
            ref TableCell tc, 
            PacificDataBaseDataType dataType,
            ref Dictionary<System.String, PacificDataBaseDataType> type )
        {
            _Key = key;
            _KeyMain = keMain;
            _OperateTable = operateTable;
            _InsertedItem = insertedItem;
            _tc = tc;
            _DataType = dataType;
            _TypesConstr = type;

        }


        public PKOrUnique() { }


        #region Value Declaration
        private String _Key;
        private String _KeyMain;
        private ITable _OperateTable;
        private String _InsertedItem;
        private TableCell _tc;
        private PacificDataBaseDataType _DataType;
        private Dictionary<System.String, PacificDataBaseDataType> _TypesConstr;
        #endregion


        public IStatement SetValues(
            ref Dictionary<System.String, PacificDataBaseDataType> typeConstr,
            String key, 
            String keMain, 
            ref ITable operateTable,
            String insertedItem,
            ref TableCell tc, 
            PacificDataBaseDataType dataType)
        {
            _Key = key;
            _KeyMain = keMain;
            _OperateTable = operateTable;
            _InsertedItem = insertedItem;
            _tc = tc;
            _DataType = dataType;
            _TypesConstr = typeConstr;

            return this;

        }

        public int StatementID { get; set; }
        public string StatementName { get; set; }
        public Statement StatementDefinition { get; set; }

        public ITransactionMethodResult Invoke(ITransactionInfo ti) 
        {


            if (_Key == PQLGrammar.PRIMARY_KEY || _Key == PQLGrammar.UNIQUE)
            {

                PacificDataBaseDataType pc;
                _TypesConstr.TryGetValue(_KeyMain, out pc);

                if (pc == PacificDataBaseDataType.NUMBER && Int32.TryParse(_InsertedItem, out _))
                {

                    if (_OperateTable.TableRow.Count > 0)
                    {
                        foreach (TableRow tr in _OperateTable.TableRow.Values)
                            if ((tr.RowCells.Find(x => x.ColumnData.ToString() == _InsertedItem)) != default)
                                return new JsonMethodResult(false,
                                    $"Table alrayde Contains that value {_InsertedItem}" +
                                    $" with PRIMARY_KEY or UNIQUE Constraint in Collemg {_KeyMain}");



                        _tc = TableCreator.CreateTableCell(alias: _KeyMain, columnData: _InsertedItem,  columnType: _DataType) as TableCell;



                        return new JsonMethodResult(true, String.Empty) { AddedInfo = _tc};

                    }


                    _tc = TableCreator.CreateTableCell(alias: _KeyMain, columnData: _InsertedItem,  columnType: _DataType) as TableCell;




                    return new JsonMethodResult(true, String.Empty) { AddedInfo = _tc};


                }
                else if (pc == PacificDataBaseDataType.STRING)
                {


                    if (_OperateTable.TableRow.Count > 0)
                    {
                        
                        foreach(TableRow tr in _OperateTable.TableRow.Values)
                            if ((tr.RowCells.Find(x => x.ColumnData.ToString() == _InsertedItem)) != default)
                            return new JsonMethodResult(false,
                                $"Table alrayde Contains that value {_InsertedItem}" +
                                $" with PRIMARY_KEY or UNIQUE Constraint in Collemg {_KeyMain}");


                        _tc = TableCreator.CreateTableCell(alias: _KeyMain, columnData: _InsertedItem, columnType: _DataType) as TableCell;


                  
                        return new JsonMethodResult(true, String.Empty) { AddedInfo = _tc };
                  
                    }



                    _tc = TableCreator.CreateTableCell(alias: _KeyMain, columnData: _InsertedItem,  columnType: _DataType) as TableCell;



                    return new JsonMethodResult(true, String.Empty) { AddedInfo = _tc};
                 
                }
                else
                    return new JsonMethodResult (false, $"Type Error in column {_KeyMain} ");
                


            }


            return new JsonMethodResult(true, String.Empty) { AddedInfo = null };


    }

}
}
