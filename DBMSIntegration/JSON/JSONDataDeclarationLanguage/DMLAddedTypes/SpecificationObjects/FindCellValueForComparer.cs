﻿using System;
using DBMS.Core.Helpers.RulesControl;
using DBMS.Core.InfoTypes;
using DBMS.Core.Table;


namespace DBMS.Integration.JSON.SQL.Specification
{
    internal sealed class FindCellValueForComparer
        : IStatement
    {


        public FindCellValueForComparer(Boolean frs, Boolean sec, String name, Char separator,ref TableRow operateRow)
        {
            _First = frs;
            _Second = sec;
            _Separator = separator;
            _OperateRow = operateRow;
            _Name = name;

        }



        private Boolean _First;
        private Boolean _Second;
        private Char _Separator;
        private TableRow _OperateRow;
        private String _Name;
        public int StatementID { get; set; }
        public string StatementName { get; set; }
        public Statement StatementDefinition { get; set; }

        public ITransactionMethodResult Invoke(ITransactionInfo ti)
        {
            TableCell value = new TableCell();

            if (_First)    
                if (_OperateRow.RowCells.Exists((x) => x.Alias == _Name))
                    value = _OperateRow.RowCells.Find((x) => x.Alias == _Name);
                else if (_OperateRow.RowCells.Exists((x) => x.Alias == _Name.Split(_Separator)[1]))
                    value = _OperateRow.RowCells.Find((x) => x.Alias == _Name.Split(_Separator)[1]);

            return new JsonMethodResult(true, String.Empty) { AddedInfo = value };
        }

    }
}
