﻿using System;
using DBMS.Core.Helpers.RulesControl;
using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using DBMS.Core.ErrorControlSystem.Exeption;

namespace DBMS.Integration.JSON.SQL.Specification
{
    internal sealed class TableStatement
        : IStatement
    {

        public TableStatement(Boolean fist, Boolean sec, String name)
        {
            _First = fist;
            _Second = sec;
            _TableName = name;
        }

        private Boolean _First { set; get; }
        private Boolean _Second { set; get; }
        private String _TableName { set; get; }
        public int StatementID { get; set; }
        public string StatementName { get; set; }
        public Statement StatementDefinition { get; set; }

        public ITransactionMethodResult Invoke(ITransactionInfo ti)
        {
            if (!_First)
                if (!_Second)
                    if (DBMSIO.GetTriggerTmpTableTypeByName(_TableName) == StaticContextTablesID.DELETED_TABLE
                       || DBMSIO.GetTriggerTmpTableTypeByName(_TableName) == StaticContextTablesID.UPDATED_TABLE)
                        return null;
                    else if (!DBMSIO.GetCurrentDataBase().IsTableExist(_TableName))
                        throw new TableNotExist();

            return null;
        }
    }
}
