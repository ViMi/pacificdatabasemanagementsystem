﻿using System;
using DBMS.Core.Helpers.RulesControl;
using DBMS.Core.InfoTypes;
using DBMS.PQL.Grammar;


namespace DBMS.Integration.JSON.SQL.Specification
{
    internal sealed class NotNullOrPK
        : IStatement
    {


        public NotNullOrPK(String key, String insertedItem, String keyMain)
        {
            _Key = key;
            _InsertedItem = insertedItem;
            _KeyMain = keyMain;
        }


        public NotNullOrPK()
        {

        }


        public IStatement SetValues(String key, String insertedItem, String keyMain)
        {
            _Key = key;
            _InsertedItem = insertedItem;
            _KeyMain = keyMain;

            return this;
        }


        private String _Key;
        private String _InsertedItem;
        private String _KeyMain;

        public int StatementID { get; set; }
        public string StatementName { get; set; }
        public Statement StatementDefinition { get; set; }

        public ITransactionMethodResult Invoke(ITransactionInfo ti) 
        {
            String chekc = _InsertedItem.Trim(PQLGrammar.ConstantLiteralStringBracetChar).Trim();
            if ((_Key == PQLGrammar.NOT_NULL || _Key == PQLGrammar.PRIMARY_KEY)  && chekc == String.Empty)
                return new JsonMethodResult (false, $"Attribute with name {_KeyMain} constraint error (MustBeNotNull)");
            


            return new JsonMethodResult(true, String.Empty);

        }
    }
}
