﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBMS.Core.Helpers;
using DBMS.Core.Helpers.RulesControl;
using DBMS.Core.InfoTypes;
using DBMS.PQL.Grammar;
using DBMS.Core.Table;
using DBMS.Core.IO;
using DBMS.Core.MMIL;

namespace DBMS.Integration.JSON.SQL.Specification
{
    internal sealed class IncludeObject
       : IStatement
    {
        public Int32 StatementID { get; set; }
        public String StatementName { get; set; }
        public Statement StatementDefinition { get; set; }



        public IStatement SetValue(
    String key,
    String insertedValue,
    String keyMain,
    PacificDataBaseDataType dataType,
    ref Dictionary<System.String, String> consItem,
    ref TableCell tc)
        {
            _Key = key;
            _InsertedItem = insertedValue;
            _KeyMain = keyMain;
            _DataType = dataType;
            _ConsItem = consItem;
            _tc = tc;

            return this;
        }


        #region Variable Declaration
        private String _Key;
        private String _InsertedItem;
        private PacificDataBaseDataType _DataType;
        private String _KeyMain;
        private Dictionary<System.String, String> _ConsItem;
        private TableCell _tc;
        #endregion

        public ITransactionMethodResult Invoke(ITransactionInfo ti)
        {

            if (_Key == PQLGrammar.INCLUDE_FOREIGN_TABLE_BY_KEY && _DataType == PacificDataBaseDataType.INCLUDED_TABLE)
            {

                System.String value;
                _ConsItem.TryGetValue(_Key, out value);
                String[] values = (value as String).Split(PQLGrammar.LeftParameterBracetInner);

                if (DBMSIO.GetCurrentDataBase().IsTableExist(values[0]))
                {



                    if (!DBMSIO.GetCurrentDataBase().
                        IsColumnWithNameExsitInTableWithName
                        (values[1].Replace(PQLGrammar.RightParameterBracetInner.ToString(), String.Empty), values[0]))
                        return new JsonMethodResult(false
                     , $"INCLUDE_FOREIGN_REFERENCED_TABLE " +
                     $"{values[0]} do not have column with name ${values[1].Replace(PQLGrammar.RightParameterBracetInner.ToString(), String.Empty)}");



                    if (!DBMSIO.GetCurrentDataBase()
                        .IsValueExistIntColumnWithNameInTableWithName
                        (values[1].Replace(PQLGrammar.RightParameterBracetInner.ToString(), String.Empty), values[0], _InsertedItem))
                        return new JsonMethodResult(false
                         , $"INCLUDE_FOREIGN_REFERENCED_TABLE " +
                         $"{value} do not have a PRIMARY_KEY or ALTERNATE_KEY contraint and can not be " +
                         $"indexed because value not exist");


                    MMILDataManipulationContext mmil = new MMILDataManipulationContext();
                    mmil.InitExecutionContext(PQLGrammar.INSERT_COMMAND);
                    mmil.SetTable(values[0].Trim());
                    mmil.SetTo(values[0].Trim());
                    mmil.SetFrom(values[0].Trim());
                    mmil.EndExecuteContext();
                    SelectedTable select = new SelectedTable(mmil);

                    TableRow row = null;
                    if (select.Select().GetResult().TableRow.ContainsKey(value))
                        select.Select().GetResult().TableRow.TryGetValue(value, out row);
                    else
                        return new JsonMethodResult(false, $"TABLE NOT CONTAIN PRIMARY KEY WITH VALUE {value}");

                    JSON json = new JSON();
                    String jsonContent = json.Serialize<TableRow>(row);

                    _tc = TableCreator.CreateTableCell(alias: _KeyMain, columnData: jsonContent, columnType: _DataType) as TableCell;



                    return new JsonMethodResult(true, String.Empty) { AddedInfo = _tc };
                }



                return new JsonMethodResult(false, $"TABLE NOT FOUND {values[0]}") { AddedInfo = _tc };

            }


            return new JsonMethodResult(true, String.Empty) { AddedInfo = _tc };




        }
    }
}
