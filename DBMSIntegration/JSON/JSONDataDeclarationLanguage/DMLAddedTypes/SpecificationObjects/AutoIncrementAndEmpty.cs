﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBMS.Core.Helpers.RulesControl;
using DBMS.Core.InfoTypes;
using DBMS.PQL.Grammar;
using DBMS.Core.IO;
using DBMS.Core.Table;



namespace DBMS.Integration.JSON.SQL.Specification
{
    internal sealed class AutoIncrementAndEmpty
        : IStatement
    {


        public AutoIncrementAndEmpty() { }


        public AutoIncrementAndEmpty(
            String key,
            String insertedItem,
            String keyMain,
            ref ITable operateTablem,
            ref TableCell tc,
            PacificDataBaseDataType dataType)
        {
            _Key = key;
            _InsertedItem = insertedItem;
            _KeyMain = keyMain;
            _OperateTable = operateTablem;
            _tc = tc;
            _DataType = dataType;
        }


        public IStatement SetValue(
            String key,
            String insertedItem,
            String keyMain,
            ref ITable operateTablem,
            ref TableCell tc,
            PacificDataBaseDataType dataType)
        {
            _Key = key;
            _InsertedItem = insertedItem;
            _KeyMain = keyMain;
            _OperateTable = operateTablem;
            _tc = tc;
            _DataType = dataType;

            return this;
        }



        #region Values Declaration
        private String _Key;
        private String _InsertedItem;
        private String _KeyMain;
        private ITable _OperateTable;
        private TableCell _tc;
        private PacificDataBaseDataType _DataType;
        #endregion


        public int StatementID { get; set; }
        public string StatementName { get; set; }
        public Statement StatementDefinition { get; set; }



        public ITransactionMethodResult Invoke(ITransactionInfo ti)
        {

            String checkValue = _InsertedItem.Trim(PQLGrammar.ConstantLiteralStringBracetChar).Trim();

            if (_Key == PQLGrammar.AUTO_INCREMENT && checkValue == String.Empty)
            {

                if (_OperateTable.TableRow.Count > 0)
                {
                    TableCell tcs =
                        _OperateTable
                        .TableRow.ElementAt(_OperateTable.TableRow.Count - 1).Value
                        .RowCells.Find(x => x.Alias == _KeyMain);

                    if (tcs.ColumnData == null)
                        tcs.ColumnData = 0.ToString();

                    _tc = TableCreator.CreateTableCell(alias: _KeyMain, columnData: (Int32.Parse(tcs.ColumnData) + 1).ToString(), columnType: tcs.DataType) as TableCell;

                    return new JsonMethodResult(true, String.Empty) { AddedInfo = _tc };
                }
                else
                    _tc = TableCreator.CreateTableCell(alias: _KeyMain, columnData: 0.ToString(), columnType: _DataType) as TableCell;

                    return new JsonMethodResult(true, String.Empty) { AddedInfo = _tc };


            }



            return new JsonMethodResult(true, String.Empty) { AddedInfo = null };



        }
    }
}
