﻿using System;
using DBMS.Core.Helpers.RulesControl;
using DBMS.Core.InfoTypes;
using DBMS.PQL.Grammar;


namespace DBMS.Integration.JSON.SQL.Specification
{
    internal sealed class AutoIncrementAndNotEmpty
        : IStatement

    {

        public AutoIncrementAndNotEmpty() { }

        public AutoIncrementAndNotEmpty(String key, String insertedValue, String keyMain)
        {
            _Key = key;
            _InsertedValue = insertedValue;
            _KeyMain = keyMain;
        }


        public IStatement SetValue(String key, String insertedValue, String keyMain)
        {
            _Key = key;
            _InsertedValue = insertedValue;
            _KeyMain = keyMain;

            return this;
        }


        #region Variable Declaration
        private String _Key;
        private String _InsertedValue;
        private String _KeyMain;
        #endregion

        public int StatementID { get; set; }
        public string StatementName { get; set; }
        public Statement StatementDefinition { get; set; }
        public ITransactionMethodResult Invoke(ITransactionInfo ti)
        {

            String checkValue = _InsertedValue.Trim(PQLGrammar.ConstantLiteralStringBracetChar).Trim();

            if (_Key == PQLGrammar.AUTO_INCREMENT &&  checkValue != String.Empty)
                return new JsonMethodResult (false, $"Attribute constrained like AUTHO_INCEREMETN {_KeyMain} and not maintain object values");

            return new JsonMethodResult(true, String.Empty) { AddedInfo = null };


        }
    }
}
