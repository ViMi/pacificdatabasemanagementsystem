﻿using System;
using System.Collections.Generic;
using DBMS.Core.Helpers.RulesControl;
using DBMS.Core.InfoTypes;
using DBMS.Core.Table;
using DBMS.PQL.Grammar;

namespace DBMS.Integration.JSON.SQL.Specification
{
    internal sealed class DefaultValue
        : IStatement
    {


        public DefaultValue() { }

        public DefaultValue(
            String key,
            String insertedValue,
            String keyMain,
            ref TableCell tc,
            ref Dictionary<System.String, String> consItem,
            PacificDataBaseDataType dataType)
        {
            _Key = key;
            _InsertedItem = insertedValue;
            _KeyMain = keyMain;
            _tc = tc;
            _ConsItem = consItem;
            _DataType = dataType;

        }


        public IStatement SetValue(
            String key,
            String insertedValue,
            String keyMain,
            ref TableCell tc,
            ref Dictionary<System.String, String> consItem,
            PacificDataBaseDataType dataType)
        {
            _Key = key;
            _InsertedItem = insertedValue;
            _KeyMain = keyMain;
            _tc = tc;
            _ConsItem = consItem;
            _DataType = dataType;

            return this;
        }


        #region Varible Declaration
        private String _Key;
        private String _InsertedItem;
        private String _KeyMain;
        private TableCell _tc;
        private Dictionary<System.String, String> _ConsItem;
        private PacificDataBaseDataType _DataType;
        #endregion

        public int StatementID { get; set; }
        public string StatementName { get; set; }
        public Statement StatementDefinition { get; set; }


        public ITransactionMethodResult Invoke(ITransactionInfo ti)
        {
            String Check = _InsertedItem.Trim(PQLGrammar.ConstantLiteralStringBracetChar).Trim();
            if (_Key == PQLGrammar.DEFAULT && Check == String.Empty)
            {
                System.String obj;
                _ConsItem.TryGetValue(_Key, out obj);
                _tc = TableCreator.CreateTableCell(alias: _KeyMain, columnData: obj.ToString(), columnType: _DataType) as TableCell;
                return new JsonMethodResult(true, String.Empty) { AddedInfo = _tc };

            }



            return new JsonMethodResult(true, String.Empty) { AddedInfo = null };



        }
    }
}
