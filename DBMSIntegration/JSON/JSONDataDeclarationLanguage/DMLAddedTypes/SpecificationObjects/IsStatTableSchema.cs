﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DBMS.Core.Helpers;
using DBMS.Core.Helpers.RulesControl;
using DBMS.Core.InfoTypes;

namespace DBMS.Integration.JSON.SQL.Specification
{
    internal sealed class IsStatTableSchema
        : IStatement
    {
        public int StatementID { get; set; }
        public string StatementName { get; set; }
        public Statement StatementDefinition { get; set; } = (ITransactionInfo ti) => {

            if ((ti.Parameters.Split(ti.DataBaseName[0]).Length > 1) && (!ti.Parameters.StartsWith(ti.UserName)))
                return new JsonMethodResult(true, String.Empty);

            return new JsonMethodResult(false, String.Empty);

        };

        public ITransactionMethodResult Invoke(ITransactionInfo ti) => this.StatementDefinition.Invoke(ti);
    }
}
