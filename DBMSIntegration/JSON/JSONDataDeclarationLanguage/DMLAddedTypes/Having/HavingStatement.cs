﻿using System;
using System.Collections.Generic;
using DBMS.Core.Table;
using DBMS.Core.Helpers;



namespace DBMS.Integration.JSON.SQL
{


    internal enum HavingStrategy : Byte
    {
        MAX = 0,
        MIN = 1,
        COUNT = 2,
        AVG = 3,
    }


    internal sealed class HavingStatement
    {


        #region Documentation
#if DocTrue
        static HavingStatement() { }
#endif
        #endregion




        private Dictionary<HavingStrategy, IFunctor> coll = new Dictionary<HavingStrategy, IFunctor>()
        {
            [HavingStrategy.AVG] = new Avg(),
            [HavingStrategy.COUNT] = new Count(),
            [HavingStrategy.MAX] = new Max(),
            [HavingStrategy.MIN] = new Min(),

        };

        private PacificDataBaseDataType _DataType = default;
        private ITable _FilteredTable = default;
        private HavingStrategy _Functor = default;

        public HavingStatement(String functorName, PacificDataBaseDataType compareType, ref ITable filteredTable)
        {
            this._DataType = compareType;
            this._FilteredTable = filteredTable;
            this._Functor = ResolveTypeByName(functorName);
        }



        public JsonMethodResult Execute(String field)
            => this.coll[this._Functor].Execute(ref this._FilteredTable, this._DataType, field);



        private HavingStrategy ResolveTypeByName(String name) => name switch
        {
            "AVG" => HavingStrategy.AVG,
            "MIN" => HavingStrategy.MIN,
            "MAX" => HavingStrategy.MAX,
            "COUNT" => HavingStrategy.COUNT,
            _ => throw new Exception()
        };


    }
}
