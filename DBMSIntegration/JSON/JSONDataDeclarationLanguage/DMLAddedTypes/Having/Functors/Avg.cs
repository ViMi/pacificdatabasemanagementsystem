﻿using DBMS.Core.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.Helpers;



namespace DBMS.Integration.JSON.SQL
{
    internal sealed class Avg
        : IFunctor
    {

        #region Documentation
#if DocTrue
        static Avg() { }

#endif
        #endregion


        public JsonMethodResult Execute(ref ITable filteredTable, PacificDataBaseDataType type, String field)
        {

         
            if (filteredTable.TableRow.Count == 0 || field == null)
                return new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SELECT_COUNT_ITEMS_ERROR]);


            List<TableCell> allValuesOf = new List<TableCell>();
            foreach (TableRow tr in filteredTable.TableRow.Values)
                foreach (TableCell tc in tr.RowCells)
                    if(tc.Alias == field)
                        allValuesOf.Add(tc);


            String avgResult = "0";
            TableCell value = new TableCell() { DataType = PacificDataBaseDataType.STRING };
            if (allValuesOf != null)
            {

                value = filteredTable.TableRow.ElementAt(0).Value.RowCells.ElementAt(0);
                type = value.DataType;
                avgResult = type == PacificDataBaseDataType.NUMBER ? allValuesOf.Average(x => Int32.Parse(x.ColumnData)).ToString() : allValuesOf.Average(x => x.ColumnData.Length).ToString();
            }

            return new JsonMethodResult(false, $"Average value {avgResult} type of cell {value.DataType} [if type STRING returned average  str length]");

        }

    }
}
