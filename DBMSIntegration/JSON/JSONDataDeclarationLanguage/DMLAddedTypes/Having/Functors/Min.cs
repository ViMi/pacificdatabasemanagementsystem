﻿using DBMS.Core.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using DBMS.Core.ErrorControlSystem;

namespace DBMS.Integration.JSON.SQL
{
    internal class Min 
        : IFunctor
    {


        #region Documentation
#if DocTrue
        static Min() { }
#endif
        #endregion


        public JsonMethodResult Execute(ref ITable filteredTable, PacificDataBaseDataType type, String field) {

            if (filteredTable.TableRow.Count == 0 || field == null)
                return new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SELECT_COUNT_ITEMS_ERROR]);


            TableCell value = filteredTable.TableRow.ElementAt(0).Value.RowCells.ElementAt(0);
            type = value.DataType;

            List<TableCell> allValuesOf = new List<TableCell>();
            foreach (TableRow tr in filteredTable.TableRow.Values)
                foreach (TableCell tc in tr.RowCells)
                    if(tc.Alias == field)
                    allValuesOf.Add(tc);

            String minRes = "0";
            if (allValuesOf.Count != 0)
               minRes = type == PacificDataBaseDataType.NUMBER ? allValuesOf.Min(x => Int32.Parse(x.ColumnData)).ToString() : allValuesOf.Min(x => x.ColumnData.Length).ToString();




            return new JsonMethodResult(false,  $"Minimum value {minRes} type of cell {value.DataType} [if type STRING returned min str length]");

        }

    }
}
