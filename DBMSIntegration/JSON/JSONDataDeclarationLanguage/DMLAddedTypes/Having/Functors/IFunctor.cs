﻿using DBMS.Core.Table;


namespace DBMS.Integration.JSON.SQL
{
    internal interface IFunctor
    {
        JsonMethodResult Execute(ref ITable filteredTable, PacificDataBaseDataType type, System.String field);

    }
}
