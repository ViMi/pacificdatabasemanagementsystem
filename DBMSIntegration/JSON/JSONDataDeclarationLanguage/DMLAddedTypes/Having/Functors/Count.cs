﻿using DBMS.Core.Table;
using DBMS.Core.Helpers;



namespace DBMS.Integration.JSON.SQL
{
    internal sealed class Count
        : IFunctor
    {

        #region Documentation
#if DocTrue
        static Count() { }
#endif
        #endregion



        public JsonMethodResult Execute(ref ITable filteredTable, PacificDataBaseDataType type, System.String field)
            => new JsonMethodResult(true, "Table Contains " + filteredTable.TableRow.Count.ToString() + " Rows");

    }
}
