﻿using System;
using System.Collections.Generic;
using DBMS.Core.IO;
using DBMS.Core.Table;
using DBMS.Core.MMIL;
using DBMS.Core.MMIL.ILConfiguration;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.InfoTypes;
using DBMS.Core.Helpers;
using System.Linq;

using DBMS.Core;
using DBMS.Core.Secure.UserRights;

namespace DBMS.Integration.JSON.SQL
{
    internal sealed class DeleteT
        : IDeleteStrategy
    {

        #region Documentation
#if DocTrue
        static DeleteT() { }
#endif
        #endregion


        #region Variable Declaration
        ITable _OperateTable { set; get; }
        ITable _CommonSet { set; get; }
        String _TableName;

        MMILDataManipulationContext _MMIL { set; get; }
        #endregion

        public DeleteT(ref ITable operateTable, ITable commonSet, String tableName, MMILDataManipulationContext mmil = null)
        {
            this._OperateTable = operateTable;
            _CommonSet = commonSet;
            _MMIL = mmil;
            _TableName = tableName;
        }




        public ITransactionMethodResult Delete()
        {

            if (_OperateTable == null)
                return new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.ERROR_OF_FILTERING_TABLE]);


            Dictionary<String, TableRow> rows = _OperateTable.TableRow;

            System.String destinationTableName = _MMIL.GetContextValue(MMILDataManipulationConfigure.FromDeclarationCommad);
            System.String selectedRow = _MMIL.GetContextValue(MMILDataManipulationConfigure.RowsDeclarationCommand);
            System.Int32 CountOfDeleted = 0;


            if (destinationTableName.Split(DBMSIO.DBMSConfig.StandartSeparator).Length != 1)
                return new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.MULTIPLY_DELETE_TABLE_ERROR]);

            ITableInfo ti = DBMSIO.GetCurrentDataBase().GetTableMetaByName(destinationTableName);

            List<String> headKeys = new List<String>();

            for (System.Int32 i = 0; i < _OperateTable.TableRow.Count; ++i)
            {

                foreach (var tmp in _CommonSet.TableRow)
                    if (tmp.Value.RowCells.SequenceEqual(_OperateTable.TableRow.ElementAt(i).Value.RowCells))
                        headKeys.Add(tmp.Key);




                foreach (var tr in headKeys)
                {
                    if (_CommonSet.TableRow.ContainsKey(tr))
                    {
                        TableRow tmpTr;
                        _CommonSet.TableRow.TryGetValue(tr, out tmpTr);

                        if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
                        {
                            if (DBMSIO.GetCurrentDataBase().IsExistForeignConstraintToTable(_TableName))
                                foreach (TableCell tc in tmpTr.RowCells)
                                    if (DBMSIO.GetCurrentDataBase().IsExistForeignConstraintToCellWithValue(tc, _TableName))
                                        return new JsonMethodResult(false, $"FOREIGN_KEY_ERROR table {_TableName} cell {tc.Alias} value {tc.ColumnData}");
                        }

                        _CommonSet.TableRow.Remove(tr);
                        CountOfDeleted++;
                    }

                }

            }




            if (DBMSIO.GetCurrentDataBase().IsTableHasDeleteTrigger(destinationTableName))
            {
                DBMSIO.SetNewTmpTableToTriggerDS(StaticContextTablesID.DELETED_TABLE, _OperateTable);
                DBMSIO.GetCurrentDataBase().ExecuteTriggerOnTable(destinationTableName, Core.NodeType.DELETE_TRIGGER);
                DBMSIO.ReseTriggerGloalTmpTable();
            }
            ti.RowCount -= CountOfDeleted;

            return new JsonMethodResult(true, $"Query delete { CountOfDeleted } records from table with name {destinationTableName} " +
                $"from dataBase {DBMSIO.GetCurrentDataBase().GetDataBaseName()}")
            { AddedInfo = ti};

        }
    }
}
