﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBMS.Core.Table;
using DBMS.Core.ErrorControlSystem;

namespace DBMS.Integration.JSON.SQL
{
    public interface IOrderBy
    {

        ITable Order(ref ITable orderedTable);

    }
}
