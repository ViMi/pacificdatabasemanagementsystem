﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBMS.Core.Table;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.Helpers;




namespace DBMS.Integration.JSON.SQL
{
    internal sealed class Asc
        :IOrderBy
    {

        #region Documentation
#if DocTrue
        static Asc() { }
#endif
        #endregion



        public ITable Order(ref ITable orderedTable) {


            List<TableRow> trColl = new List<TableRow>();
            List<String> keys = new List<string>();

            for(System.Int32 i = 0; i < orderedTable.TableRow.Count; ++i)
            {
                trColl.Add(orderedTable.TableRow.ElementAt(i).Value);
                keys.Add(orderedTable.TableRow.ElementAt(i).Key);
            }

            trColl.OrderBy(x => x.RowCells);
            keys.OrderBy(x => x);

            trColl.OrderByDescending(x => x.RowCells);
            keys.OrderByDescending(x => x);
            orderedTable.TableRow.Values.OrderBy(x => x.RowCells);
            return orderedTable;
        }

    }
}
