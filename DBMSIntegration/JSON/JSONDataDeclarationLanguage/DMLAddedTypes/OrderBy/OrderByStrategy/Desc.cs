﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBMS.Core.Table;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.Helpers;



namespace DBMS.Integration.JSON.SQL
{
   internal sealed class Desc 
        : IOrderBy
    {

        #region Documentation
#if DocTrue
        static Desc() { }
#endif
        #endregion



        public ITable Order(ref ITable orderedTable) {
            orderedTable.TableRow.Values.OrderByDescending(x => x.RowCells);
            return orderedTable;
        }

    }
}
