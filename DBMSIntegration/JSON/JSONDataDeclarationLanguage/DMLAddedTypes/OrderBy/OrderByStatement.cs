﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBMS.Core.Table;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Helpers;



namespace DBMS.Integration.JSON.SQL
{

    internal enum OrderByTypes : Byte
    {
        DESC = 0,
        ASC = 1,
    }

    internal sealed class OrderByStatement
    {

        #region Documentation
#if DocTrue
        static OrderByStatement() {  }
#endif
        #endregion


        private OrderByTypes _Type = default;
        private ITable _FilteredTable = default;

        private Dictionary<OrderByTypes, IOrderBy> coll = new Dictionary<OrderByTypes, IOrderBy>()
        {
            [OrderByTypes.ASC] = new Asc(),
            [OrderByTypes.DESC] = new Desc()

        };

        public OrderByStatement(String orderByType, ref ITable filteredTable)
        {
            this._Type = ResolveOrderByType(orderByType);
            this._FilteredTable = filteredTable;
        }



        public JsonMethodResult Order()
            => (new JsonMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE])
            { AddedInfo = this.coll[this._Type].Order(ref this._FilteredTable) });
        


        private OrderByTypes ResolveOrderByType(String name) => name switch
        {
            "DESC" => OrderByTypes.DESC,
            "ASC" => OrderByTypes.ASC,
            _ => throw new UnknownOrderStrategy()
        };


    }
}
