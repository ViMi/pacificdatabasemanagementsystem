﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBMS.Core.IO;
using DBMS.Core.Table;
using DBMS.Core.MMIL.ILConfiguration;
using DBMS.Core.MMIL;
using DBMS.Core.InfoTypes;
using DBMS.Core.Helpers;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.TableCollections;
using DBMS.Integration.JSON.SQL.StorageCollection;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.UsersFromRoles;

namespace DBMS.Integration.JSON.SQL
{
    internal sealed class CrosJoin
        : IJoin
    {

        private System.String _JoinedTable = String.Empty;

        public CrosJoin(String joinedTable)
        {
            _JoinedTable = joinedTable;
        }


        public System.Boolean On()
        {
            return false;
        }





        public ITable Joined(JoinType joinType, Dictionary<String, ITableCollection> selectedCollections)
        {
            String[] joinTableNames = _JoinedTable.Split(DBMSIO.DBMSConfig.StandartSeparator);
          
            List<ITable> joinedTableCollection = new List<ITable>();

            foreach (ITableCollection tabcleColl in selectedCollections.Values)
            {

                ITable tableJoin = null;
                if (!joinTableNames.Contains(tabcleColl.GetMetaInfoTable().TableName))
                    tableJoin = tabcleColl._TableCollection;


                foreach (String joineTable in joinTableNames)
                {
                    if (joineTable.Trim() == String.Empty)
                        continue;

                    if (!selectedCollections.ContainsKey(joineTable))
                        throw new Exception("Error of Joine");

                    ITableCollection joinTable;
                    selectedCollections.TryGetValue(joineTable, out joinTable);
                    ITable joinWith = joinTable._TableCollection;


                    if (tableJoin == null)
                        break;

                    joinedTableCollection.Add(_Join(selectedCollections, ref joinWith, ref tableJoin));
                }

            }

         
            ITable resTable = new Table();
            resTable.TableRow = new Dictionary<String, TableRow>();

                for (Int32 j = 0; j < joinedTableCollection.Count; ++j)
                {
                    ITable table = joinedTableCollection.ElementAt(j);

                   for(Int32 k = 0; k < table.TableRow.Count; ++k)
                    {
                        var tr = table.TableRow.ElementAt(k);
                    

                        if (resTable.TableRow.ContainsKey(tr.Key))
                        {
                            TableRow resRow;
                            resTable.TableRow.TryGetValue(tr.Key, out resRow);

                        foreach (TableCell tc in tr.Value.RowCells)
                            if (!resRow.RowCells.Exists(x => x.Alias == tc.Alias))
                                resRow.RowCells.Add(tc);


                        } else
                        {
                            resTable.TableRow.Add(tr.Key, tr.Value.Clone() as TableRow);
                        }

                    }


                }



            return resTable;
        }



        private ITable _Join(Dictionary<String, ITableCollection> selectedCollections,  ref ITable joinWith, ref ITable tableJoin)
        {
            ITable resTable = new Table();
            resTable.TableRow = new Dictionary<String, TableRow>();


            foreach (TableRow trJoined in joinWith.TableRow.Values)
            {

                foreach (TableRow tr in tableJoin.TableRow.Values)
                {
                    TableRow tmpRow = new TableRow();
                    tmpRow.RowCells = new List<TableCell>();

                    tmpRow.RowCells.AddRange(tr.RowCells);
                    tmpRow.RowCells.AddRange(trJoined.RowCells);

                    resTable.TableRow.Add(resTable.TableRow.Count.ToString(), tmpRow);
                }


            }


            return resTable;


        }
    }
}

