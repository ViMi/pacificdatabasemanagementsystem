﻿using DBMS.Core.Table;
using System.Collections.Generic;
using DBMS.Core.TableCollections;

namespace DBMS.Integration.JSON.SQL
{
    public interface IJoin
    {

        System.Boolean On();
        ITable Joined(JoinType joinType, Dictionary<System.String, ITableCollection> selectedCollections);

    }
}
