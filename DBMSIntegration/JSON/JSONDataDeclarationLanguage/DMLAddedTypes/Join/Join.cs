﻿using DBMS.Core.Table;
using System.Collections.Generic;
using DBMS.Core.TableCollections;


namespace DBMS.Integration.JSON.SQL
{

    public enum JoinType 
        : System.Byte
    {
        RIGHT_JOIN = 0,
        LEFT_JOIN = 1,
        CROSS_JOIN = 2,
        FULL_JOIN = 3
    };


    public sealed class Join 
        : IJoin
    {

        private System.String _JoinTable = System.String.Empty;


        #region Variable Declaration
        private IJoin _CrossJoinStrategy;

        #endregion



        public Join(System.String joinTableName)
        {
            this._JoinTable = joinTableName;
            _CrossJoinStrategy = new CrosJoin(joinTableName);
        }


        public System.Boolean On()
        {
            return false;
        }


        public ITable Joined(JoinType joinType, Dictionary<System.String, ITableCollection> selectedCollections) => joinType switch
        {
            JoinType.CROSS_JOIN => _CrossJoinStrategy.Joined(joinType, selectedCollections),

            _ => throw new System.Exception()

        };

    }
}
