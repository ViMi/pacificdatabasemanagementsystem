﻿using DBMS.Core.InfoTypes;

namespace DBMS.Integration.JSON.SQL
{
    internal interface IInsertStrategy
    {
        ITransactionMethodResult Insert();
    }
}
