﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBMS.Core.IO;
using DBMS.Core.Table;
using System.Text.RegularExpressions;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.MMIL;
using DBMS.Core.MMIL.ILConfiguration;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.InfoTypes;
using DBMS.Core.Helpers;
using DBMS.Integration.JSON.SQL.Specification;
using DBMS.PQL.Grammar;
using DBMS.Core;


namespace DBMS.Integration.JSON.SQL
{
    internal sealed class InsertT
        : IInsertStrategy
    {

        #region Documentation
#if DocTrue
        static InsertT() { }
#endif
        #endregion



        #region Variabe Definition
        private ITable _OperateTable;
        private MMILDataManipulationContext _MMIL { set; get; }
        private List<String> valuesList { set; get; }
        private String _TableName { set; get; }
        #endregion


        public InsertT(
            ref ITable operateTable, 
            MMILDataManipulationContext mmil = null,
            List<String> valueList = null, 
            String tableName = null)
        {
            _OperateTable = operateTable;
            _MMIL = mmil;
            this.valuesList = valueList;
            _TableName = tableName;

        }


        #region Value Declaration
        private NotNullOrPK _NotNUllOrPk = new NotNullOrPK();
        private PKOrUnique _PKOrUnique = new PKOrUnique();
        private FKReferences _FKReferences = new FKReferences();
        private LikeSpecification _LikeSpecification = new LikeSpecification();
        private AutoIncrementAndNotEmpty _AutoIncrementAndNotEmpty = new AutoIncrementAndNotEmpty();
        private AutoIncrementAndEmpty _AutoIncrementAndEmpty = new AutoIncrementAndEmpty();
        private DefaultValue _DefaultValue = new DefaultValue();
        private IncludeObject _IncludedObject = new IncludeObject();
        #endregion

        private ITransactionMethodResult _CurrentState;


        public ITransactionMethodResult Insert()
        {


            ITableInfo ti = DBMSIO.GetCurrentDataBase().GetTableMetaByName(_TableName);

            Dictionary
            <System.String, Dictionary<System.String, String>> constraint
              = ti.ColumnConstraint;

            ITable _TriggerUpdateTable_ = new Table();
            _TriggerUpdateTable_.TableRow = new Dictionary<string, TableRow>();


            foreach (System.String item in valuesList)
            {

                System.String[] insertedItem = item.Split(PQLGrammar.InsertValueSeparator[0]);
                if (insertedItem.Length < ti.ColumnTypes.Count)
                    return new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.INSERT_VALUE_NOT_EQU_INSETED_COLUMN_ERROR]);

                TableRow row = new TableRow();
                row.RowCells = new List<TableCell>(0);
                System.String PK = 0.ToString();
                if (!ti.IsIndexed) 
                    PK = _OperateTable.TableRow.Count.ToString();
                 if (_OperateTable.TableRow.Keys.Count > 0 && ti.IsIndexed)
                    PK = _OperateTable.TableRow.Keys.ElementAt(_OperateTable.TableRow.Count - 1);




                for (System.Int32 i = 0; i < insertedItem.Length; ++i)
                {

                    Dictionary<System.String, String> consItem = constraint.ElementAt(i).Value;
                    Dictionary<System.String, PacificDataBaseDataType> typesConstr   = ti.ColumnTypes;
                    var KeyMain = constraint.Keys;
                    PacificDataBaseDataType typeCell;
                    TableCell tc = null;
                    Boolean checkFlag = false;
                    typesConstr.TryGetValue(KeyMain.ElementAt(i), out typeCell);


                    if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
                    {

                        for (System.Int32 j = 0; j < consItem.Keys.Count; ++j)
                        {

                            String key = consItem.Keys.ElementAt(j);

                            #region Assertation Check
                            if (_CurrentState != null)
                                if (_CurrentState.AddedInfo != null)
                                    _CurrentState.AddedInfo = null;



                            _CurrentState = _NotNUllOrPk.SetValues(key, insertedItem[i], KeyMain.ElementAt(i)).Invoke(null);
                            checkFlag = _CurrentState.IsSuccess;
                            if (!checkFlag && (consItem.ContainsKey(PQLGrammar.DEFAULT) || consItem.ContainsKey(PQLGrammar.AUTO_INCREMENT)))
                                continue;
                            else if (!checkFlag && (!consItem.ContainsKey(PQLGrammar.DEFAULT) || !consItem.ContainsKey(PQLGrammar.AUTO_INCREMENT)))
                                return _CurrentState;


                            _CurrentState = _PKOrUnique.SetValues(ref typesConstr, key, KeyMain.ElementAt(i), ref _OperateTable, insertedItem[i], ref tc, typeCell).Invoke(null);
                            checkFlag = _CurrentState.IsSuccess;
                            if (!checkFlag && consItem.ContainsKey(PQLGrammar.AUTO_INCREMENT))
                                if (!checkFlag && key == PQLGrammar.PRIMARY_KEY && !Int32.TryParse(insertedItem[i], out _))
                                    return _CurrentState;
                                else
                                    continue;

                            else if (!checkFlag && !consItem.ContainsKey(PQLGrammar.AUTO_INCREMENT))
                                return _CurrentState;


                            if ((_CurrentState.IsSuccess) && (tc = _CurrentState.AddedInfo as TableCell) != null) continue;




                            _CurrentState = _FKReferences.SetValue(key, insertedItem[i], KeyMain.ElementAt(i), typeCell, ref consItem, ref tc).Invoke(null);
                            checkFlag = _CurrentState.IsSuccess; if (!checkFlag) return _CurrentState;
                            if (_CurrentState.IsSuccess && (tc = _CurrentState.AddedInfo as TableCell) != null) continue;

                            _CurrentState = _LikeSpecification.SetValue(key, insertedItem[i], KeyMain.ElementAt(i), typeCell, ref consItem, ref tc).Invoke(null);
                            checkFlag = _CurrentState.IsSuccess; if (!checkFlag) return _CurrentState;
                            if (_CurrentState.IsSuccess && (tc = _CurrentState.AddedInfo as TableCell) != null) continue;




                            _CurrentState = _AutoIncrementAndNotEmpty.SetValue(key, insertedItem[i], KeyMain.ElementAt(i)).Invoke(null);
                            checkFlag = _CurrentState.IsSuccess; if (!checkFlag) return _CurrentState;

                            _CurrentState = _AutoIncrementAndEmpty.SetValue(key, insertedItem[i], KeyMain.ElementAt(i), ref _OperateTable, ref tc, typeCell).Invoke(null);
                            checkFlag = _CurrentState.IsSuccess; if (!checkFlag) return _CurrentState;
                            if (_CurrentState.IsSuccess && (tc = _CurrentState.AddedInfo as TableCell) != null) continue;

                            _CurrentState = _IncludedObject.SetValue(key, insertedItem[i], KeyMain.ElementAt(i), typeCell, ref consItem, ref tc).Invoke(null);
                            checkFlag = _CurrentState.IsSuccess; if (!checkFlag) return _CurrentState;
                            if (_CurrentState.IsSuccess && (tc = _CurrentState.AddedInfo as TableCell) != null) continue;

                            _CurrentState = _DefaultValue.SetValue(key, insertedItem[i], KeyMain.ElementAt(i), ref tc, ref consItem, typeCell).Invoke(null);
                            checkFlag = _CurrentState.IsSuccess; if (!checkFlag) return _CurrentState;
                            if (_CurrentState.IsSuccess && (tc = _CurrentState.AddedInfo as TableCell) != null) continue;
                            #endregion




                        }

                    }

                    if (consItem.ContainsKey(PQLGrammar.PRIMARY_KEY))
                        if (tc != null) PK = tc.ColumnData;

                    if (tc == null)
                    {
                        if (insertedItem[i].Trim() == String.Empty)
                            insertedItem[i] = "NULL";

                        tc = new TableCell()
                        {
                            Alias = KeyMain.ElementAt(i),
                            ColumnData = insertedItem[i],
                            DataType = typeCell

                        };
                    }

                    row.RowCells.Add(tc);


                }


                if (!ti.IsIndexed)
                    while (_OperateTable.TableRow.ContainsKey((PK).ToString()))
                    {
                        Int32 pk = Int32.Parse(PK);
                        PK = (pk++).ToString();
                    }

                _TriggerUpdateTable_.TableRow.Add((PK), row);
                _OperateTable.TableRow.Add((PK), row);
               

            }


            
            if (DBMSIO.GetCurrentDataBase().IsTableHasInsertTrigger(_TableName))
            {
                DBMSIO.SetNewTmpTableToTriggerDS(StaticContextTablesID.UPDATED_TABLE, _TriggerUpdateTable_);
                DBMSIO.GetCurrentDataBase().ExecuteTriggerOnTable(_TableName, Core.NodeType.INSERT_TRIGGER);
                DBMSIO.ReseTriggerGloalTmpTable();
            }

            ti.RowCount += valuesList.Count;
            return new JsonMethodResult(true, $"Query inserted  {valuesList.Count} records in table with name {_TableName} from DataBase ") { AddedInfo = ti };
        }



    }
}
