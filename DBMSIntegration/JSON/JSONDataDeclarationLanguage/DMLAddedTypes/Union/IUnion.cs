﻿using DBMS.Core.Table;

namespace DBMS.Integration.JSON.SQL
{
    internal interface IUnion
    {
        ITable Unite(ITable firtTable, ITable secondTable);

    }
}
