﻿using DBMS.Core.IO;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.Table;
using System.Linq;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Secure.UserRights;
using DBMS.Core;

namespace DBMS.Integration.JSON.SQL
{
    internal sealed class  Union
        : IUnion
    {

        #region Variable Declaration
        private ITableInfo _InfoRes;
        private ITableInfo _InfoSelected;

        #endregion

        public Union(ITableInfo infoRes, ITableInfo selected)
        {
            _InfoRes = infoRes;
            _InfoSelected = selected;
        }

        public ITable Unite(ITable firtTable, ITable secondTable)
        {

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {

                System.Boolean isSizeCpmpatibile = firtTable.TableRow.ElementAt(0).Value.RowCells.Count == secondTable.TableRow.ElementAt(0).Value.RowCells.Count;

                if (!isSizeCpmpatibile)
                    throw new ExecutError();

                for (System.Int32 i = 0; i < firtTable.TableRow.ElementAt(0).Value.RowCells.Count; ++i)
                {
                    PacificDataBaseDataType type = _InfoRes.ColumnTypes.ElementAt(i).Value;
                    PacificDataBaseDataType typeIn = _InfoSelected.ColumnTypes.ElementAt(i).Value;
                    if (type != typeIn)
                        throw new ExecutError();

                }

            }


            foreach (TableRow tr in secondTable.TableRow.Values)
            {
                System.Int32 key = firtTable.TableRow.Count;

                if (firtTable.TableRow.ContainsKey(key.ToString()))
                    while (firtTable.TableRow.ContainsKey(key.ToString()))
                        key++;

                firtTable.TableRow.Add(key.ToString(), tr.Clone() as TableRow);

            }


                    return firtTable;
        }

    }
}
