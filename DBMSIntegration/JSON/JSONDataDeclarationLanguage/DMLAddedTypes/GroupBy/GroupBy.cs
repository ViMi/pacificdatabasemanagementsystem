﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBMS.Core.Table;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.Helpers;

using DBMS.Core.IO;
using DBMS.Core.InfoTypes;

namespace DBMS.Integration.JSON.SQL
{
    internal sealed class GroupBy
        : IGroupBy
    {


        #region Documentation
#if DocTrue
        static GroupBy() { }
#endif
        #endregion



        private String _GroupByRow = String.Empty;
        private ITable _FiltereTable = default;
        public GroupBy(String groupByRow, ref ITable filtereTable)
        {
            this._GroupByRow = groupByRow;
            this._FiltereTable = filtereTable;

        }



        public ITransactionMethodResult Group()
        {


            String[] groupNames = _GroupByRow.Split(DBMSIO.DBMSConfig.StandartSeparator, (Char)StringSplitOptions.RemoveEmptyEntries);


            TableRow cmpTr = _FiltereTable.TableRow.ElementAt(0).Value;
            foreach (String name in groupNames)
                if(!cmpTr.RowCells.Exists(x=>x.Alias == name))
                    throw new Exception("Error of Grouping");
            



            Dictionary<String, TableRow> operateTR = _FiltereTable.TableRow;
            ITable resTable = null;

            for (System.Int32 i = 0; i < groupNames.Length; ++i)
            {
                operateTR = resTable == null ? operateTR : resTable.TableRow;

                resTable = new Table();
                resTable.TableRow = new Dictionary<String, TableRow>();


                foreach (TableRow tr in operateTR.Values)
                {
                    TableRow row = new TableRow();
                    row.RowCells = new List<TableCell>();

                    row.RowCells.Add(tr.RowCells.Find(x => x.Alias == groupNames.ElementAt(i)));

                    foreach (TableCell cell in tr.RowCells)
                        if (cell.Alias != groupNames.ElementAt(i))
                            row.RowCells.Add(cell);



                    resTable.TableRow.Add(resTable.TableRow.Count.ToString(), row);
                }
            }


            return new JsonMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE]) { AddedInfo = resTable };

        }

    }
}
