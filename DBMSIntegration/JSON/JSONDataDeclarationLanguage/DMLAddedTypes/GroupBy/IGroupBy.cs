﻿using DBMS.Core.InfoTypes;

namespace DBMS.Integration.JSON.SQL
{
    internal interface IGroupBy
    {
        public ITransactionMethodResult Group();
    }
}
