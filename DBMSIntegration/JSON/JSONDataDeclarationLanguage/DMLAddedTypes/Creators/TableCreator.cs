﻿using System;
using System.Collections.Generic;
using DBMS.Core.Table;

namespace DBMS.Integration.JSON.SQL
{
    internal static class TableCreator
    {

        internal static ITableCell CreateTableCell(
            String alias, 
            PacificDataBaseDataType columnType,    
            String columnData) => new TableCell() {

            Alias = alias,
            ColumnData = columnData,
            DataType = columnType
        };

        internal static ITableRow CreateTableRow()
        {
            return null;
        }

        internal static ITable CreateTable()
        {
            return null;
        }


    }
}
