﻿using DBMS.Core.Table;
using System;
using System.Collections.Generic;
using DBMS.PQL.Grammar
    ;
namespace DBMS.Integration.JSON.SQL
{


    public enum CompareType
    {
        STRING = 0,
        INTEGER = 1,
        BYTE = 2,
        TINYINT = 3
    };


    public enum CompareResult
    {
        LESS = -1,
        EQU = 0,
        GRT = 1,

        UNDEFINED = 1000000
    }


    public enum WhereCompareStrategy
    {
        LESS = -1,
        EQU = 0,
        GRT = 1,
        LESS_EQU = 2,
        EQU_GRT = 3
    }


    internal sealed class ComparatorUnit
    {


        #region Documentation
#if DocTrue
        static ComparatorUnit() 
        {

            //Documentation.AddHistoryToSection(
            //   "DML", "CompareUnit", "Class", ""
                
            //    );
          
        
        }
#endif
        #endregion



        #region varialbe Definition
        System.String _compareOperator = String.Empty;
        
        System.String _constraint = String.Empty;

        
        String _lValue = String.Empty;
        String _rValue = String.Empty;

        PacificDataBaseDataType _DataType = default;
        #endregion


        public ComparatorUnit(System.String constraint, String lValue, String rValue, PacificDataBaseDataType dataType)
        {

            this._constraint = constraint;
            this._lValue = lValue;
            this._rValue = rValue;
            this._DataType = dataType;
        }


        public ComparatorUnit(ITableCell lValue, System.String constratin, ITableCell rValue)
        {
            this._constraint = constratin;
            this._lValue = lValue.ColumnData;
            this._rValue = rValue.ColumnData;
            this._DataType = rValue.DataType;

        }


        public void Prepare()
        {
         //   GetCompareStrategy();

        }

        public System.Boolean Execute()
        {
            Resolve();
            return _Execute();
        }

        private void Resolve()
        {

            if (PQLGrammar.compareOperators.Contains(_constraint))
            {
                _compareOperator = _constraint;
            }

        }


        private System.Boolean _Execute()
        {

            CompareResult result = this.Compare();
            
            switch (this._compareOperator)
            {
                case PQLGrammar.EQU: { return result == CompareResult.EQU; }
                case PQLGrammar.LSS: { return result == CompareResult.LESS; }
                case PQLGrammar.GRT: { return result == CompareResult.GRT; }
                case PQLGrammar.LSS_EQU: { return result == CompareResult.LESS || result == CompareResult.EQU; }
                case PQLGrammar.GRT_EQU: { return result == CompareResult.EQU || result == CompareResult.GRT; }
                default: { throw new Exception("Not Compatibile Compare Operator"); }

            }


        }


        private CompareResult Compare()
        {


            if (_lValue == null && _rValue == null)
                return CompareResult.EQU;

            if (_lValue == null && _rValue != null)
                return CompareResult.LESS;

            if (_lValue != null && _rValue == null)
                return CompareResult.GRT;

            switch (_DataType)
            {
                case PacificDataBaseDataType.NUMBER: {


                        Int32 left = System.Int32.Parse(_lValue);
                        Int32 right = System.Int32.Parse(_rValue);

                        if (left == right)
                            return CompareResult.EQU;

                        if (left > right)
                            return CompareResult.GRT;

                        if (left < right)
                            return CompareResult.LESS;

                        return CompareResult.UNDEFINED;

                    }

                case PacificDataBaseDataType.STRING: {

                        String first = _lValue.Trim().Trim(PQLGrammar.ConstantLiteralStringBracetChar);
                        String second = _rValue.Trim().Trim(PQLGrammar.ConstantLiteralStringBracetChar);
                        Int32 res = String.Compare(first, second);
                        if (res == 0)
                            return CompareResult.EQU;

                        if (res == 1)
                            return CompareResult.GRT;

                        if (res == -1)
                            return CompareResult.LESS;


                        return CompareResult.UNDEFINED;
                    
                    }
                 default: { return CompareResult.UNDEFINED; }
            }

        }

    }
}
