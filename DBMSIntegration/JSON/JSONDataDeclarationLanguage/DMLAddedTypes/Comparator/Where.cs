﻿using System;
using System.Linq;
using DBMS.Core.TableCollections;
using DBMS.Core.Table;
using System.Collections.Generic;
using DBMS.Integration.JSON.SQL.Specification;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.PQL.Grammar;
using DBMS.Core.IO;
using DBMS.Core;

namespace DBMS.Integration.JSON.SQL
{


    internal sealed class Where
        : IWhere
    {

        #region Documentation
#if DocTrue
        static Where() { }
#endif
        #endregion



        #region Varialbe Definition
        private System.String _Constraint = System.String.Empty;
        private ITable  _OperateInfo = null;
     

        private String _AndPQLWhereStatement = PQLGrammar.AND;
        private String _OrPQLWhereStatement = PQLGrammar.OR;
        private Char _WherePQLSeparator =  PQLGrammar.WhereSeparator[0];

        private Char _SchemaSeparator = PQLGrammar.ShemasSeparator;
        private String _StringLiteralBrace = PQLGrammar.ConstantLiteralStringBracet;
      
        private Char _AndPlaceholder = '@';
        private Char _OrPlaceholder = '#';

        private IsConstatnt _ConstStatement = new IsConstatnt();
        private IsNumeric _NumericStatement = new IsNumeric();
        private IsStatTableSchema _TableSchemaStatement = new IsStatTableSchema();
        #endregion


        public Where(System.String constraint, ITable operateInfo)
        {
            this._Constraint = constraint;
            this._OperateInfo = operateInfo;
        }


        public Where(System.String constraint, ITableCollection tableCollection)
        {
            this._Constraint = constraint;
            this._OperateInfo = tableCollection._TableCollection;
        }




        public ITable GetFilteredTable()
        {

            ITable mergedTable = new Table();
            mergedTable.TableRow = new Dictionary<String, TableRow>();


            

            
            if (_Constraint != null)
                try
                {

                    System.String[] orGroup = _Constraint.Replace(DBMSIO.DBMSConfig.StandartSeparator+_OrPQLWhereStatement+DBMSIO.DBMSConfig.StandartSeparator, _OrPlaceholder.ToString()).Split(_OrPlaceholder);

                    Dictionary<String, TableRow> mergedRowsAnd = new Dictionary<String, TableRow>();
                    Dictionary<String, TableRow> mergerRowsOr = new Dictionary<String, TableRow>();

                    for (System.Int32 i = 0; i <= orGroup.Length; ++i)
                    {
                        if (mergedRowsAnd.Count != 0)
                            for (System.Int32 p2 = 0; p2 < mergedRowsAnd.Count; ++p2)
                                if (!mergerRowsOr.ContainsKey(mergedRowsAnd.ElementAt(p2).Key))
                                    mergerRowsOr.Add(mergedRowsAnd.ElementAt(p2).Key, mergedRowsAnd.ElementAt(p2).Value);


                        if (i >= orGroup.Length)
                            break;


                        System.String[] andGroup =
                         orGroup[i].Replace(DBMSIO.DBMSConfig.StandartSeparator + _AndPQLWhereStatement + DBMSIO.DBMSConfig.StandartSeparator, _AndPlaceholder.ToString()).Split(_AndPlaceholder);


                        List<Dictionary<String, TableRow>> constaintsSet = new List<Dictionary<String, TableRow>>();




                        for (System.Int32 j = 0; j < andGroup.Length; ++j)
                        {

                            String[] val = andGroup[j].Trim().Split(_WherePQLSeparator);

                            if (val.Length != 3)
                                throw new Exception("Error of Compre Unit Size Check Correctness");

                            ComparatorUnit comparer;

                            Dictionary<String, TableRow> andSet = new Dictionary<String, TableRow>();


                            Boolean ruleLeftConst = _ConstStatement.Invoke(new JsonMethodInfo(){Parameters = val[0], AddedInfo = _StringLiteralBrace }).IsSuccess;
                            Boolean ruleRightConst = _ConstStatement.Invoke(new JsonMethodInfo(){Parameters = val[2], AddedInfo = _StringLiteralBrace }).IsSuccess;

                            Boolean leftTableValueDefRule = _TableSchemaStatement.Invoke(new JsonMethodInfo() {Parameters = val[0], DataBaseName = _SchemaSeparator.ToString(), UserName  = _StringLiteralBrace }).IsSuccess;
                            Boolean rightTableValuedefRule = _TableSchemaStatement.Invoke(new JsonMethodInfo() {Parameters = val[2], DataBaseName = _SchemaSeparator.ToString(), UserName  = _StringLiteralBrace }).IsSuccess;

                            Boolean ItIsNumericLeft = _NumericStatement.Invoke(new JsonMethodInfo() { Parameters = val[0], AddedInfo = leftTableValueDefRule, DataBaseName = _StringLiteralBrace }).IsSuccess;
                            Boolean ItIsNumericRight = _NumericStatement.Invoke(new JsonMethodInfo() { Parameters = val[2], AddedInfo = rightTableValuedefRule, DataBaseName = _StringLiteralBrace }).IsSuccess;


                            (new TableStatement(ruleRightConst, rightTableValuedefRule, val[0].Split(_SchemaSeparator)[0])).Invoke(null);
                            (new TableStatement(ruleLeftConst, leftTableValueDefRule, val[2].Split(_SchemaSeparator)[0])).Invoke(null);




                            for (System.Int32 k = 0; k < _OperateInfo.TableRow.Count; ++k)
                            {

                                String innerTr = _OperateInfo.TableRow.ElementAt(k).Key;
                                TableRow innerTrV = _OperateInfo.TableRow.ElementAt(k).Value;

                                TableCell leftValue = default;
                                TableCell rightValue = default;

                                String leftValName = String.Empty;
                                String rightValName = String.Empty;


                                leftValue = 
                                    (new FindCellValueForComparer(leftTableValueDefRule, true, val[0], _SchemaSeparator, ref innerTrV)).Invoke(null).AddedInfo as TableCell;
                                rightValue = 
                                    (new FindCellValueForComparer(rightTableValuedefRule, true, val[2], _SchemaSeparator, ref innerTrV)).Invoke(null).AddedInfo as TableCell; 


                                if (ruleLeftConst)
                                {
                                    if (ItIsNumericLeft) leftValue.DataType = PacificDataBaseDataType.NUMBER;
                                    else leftValue.DataType = PacificDataBaseDataType.STRING;

                                    leftValue.ColumnData = val[0];
                                }

                                if (ruleRightConst)
                                {
                                    if (ItIsNumericRight) rightValue.DataType = PacificDataBaseDataType.NUMBER;
                                    else rightValue.DataType = PacificDataBaseDataType.STRING;

                                    rightValue.ColumnData = val[2];
                                }

                                comparer = new ComparatorUnit(leftValue, val[1], rightValue);

                                if (comparer.Execute())
                                    andSet.Add(innerTr, innerTrV);



                            }


                            constaintsSet.Add(andSet);
                        }






                        mergedRowsAnd = constaintsSet.ElementAt(0);
                            for(System.Int32 p2 = 1; p2 < constaintsSet.Count; ++p2)
                            {
                                var col = mergedRowsAnd.Intersect(constaintsSet.ElementAt(p2));
                            mergedRowsAnd = new Dictionary<string, TableRow>();

                                foreach (var item in col)
                                   mergedRowsAnd.Add(item.Key, item.Value);
                               
                            }
                        



                    }





                    mergedTable.TableRow = mergerRowsOr;

                }
                catch
                {
                    throw new SelectError();
                }
            else
                return _OperateInfo;

            return mergedTable;

        }
    }
}
