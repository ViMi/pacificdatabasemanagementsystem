﻿using DBMS.Core.Table;

namespace DBMS.Integration.JSON.SQL
{
     public interface IWhere
    {

        ITable GetFilteredTable();

    }
}
