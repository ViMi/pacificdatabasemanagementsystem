﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBMS.Core.IO;
using DBMS.Core.Table;
using DBMS.Core.MMIL.ILConfiguration;
using DBMS.Core.MMIL;
using DBMS.Core.InfoTypes;
using DBMS.Core.Helpers;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.TableCollections;
using DBMS.Integration.JSON.SQL.StorageCollection;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.UsersFromRoles;
using DBMS.PQL.Grammar;

namespace DBMS.Integration.JSON.SQL
{
        internal sealed class SelectT
        : ISelectStrategy
    {


        #region Documentation
#if DocTrue
        static SelectT() { }
#endif
        #endregion


        private MMILDataManipulationContext _MMIL { set; get; }
        private Dictionary<String, ITableCollection> _SelectedCollections { set; get; }

        public SelectT(MMILDataManipulationContext mmil = null) => _MMIL = mmil;


        public ITransactionMethodResult Select()
        {


            ITable resTable = new Table();
            resTable.TableRow = new Dictionary<String, TableRow>();


            System.String destinationTableName = _MMIL.GetContextValue(MMILDataManipulationConfigure.FromDeclarationCommad);
            if (destinationTableName == null)
                destinationTableName = _MMIL.GetContextValue(MMILDataManipulationConfigure.ToDeclarationCommand);

            String[] fromSelected
                = destinationTableName.Split(DBMSIO.DBMSConfig.StandartSeparator);

            _SelectedCollections = GetTableStorageFromTablesList(fromSelected);


            System.String selectedString = _MMIL.GetContextValue(MMILDataManipulationConfigure.RowsDeclarationCommand);

            if (selectedString == null && _MMIL._ContextName == PQLGrammar.SELECT_COMMAND)
                throw new Exception();

            else if(_MMIL._ContextName != PQLGrammar.SELECT_COMMAND)selectedString = PQLGrammar.ALL_ROWS_CONST;


            List<Dictionary<String, TableRow>> rows = new List<Dictionary<String, TableRow>>();
            List<Dictionary<String, TableRow>> rowsTmp = new List<Dictionary<String, TableRow>>();
            System.Int32 maxSize = 0;


            for (System.Int32 i = 0; i < _SelectedCollections.Count; ++i)
            {
                rows.Add(_SelectedCollections.ElementAt(i).Value._TableCollection.TableRow);
                if (_SelectedCollections.ElementAt(i).Value._TableCollection.TableRow.Count > maxSize)
                    maxSize = _SelectedCollections.ElementAt(i).Value._TableCollection.TableRow.Count;
            }
            


            if (selectedString == PQLGrammar.ALL_ROWS_CONST && selectedString.Split(' ').Length > 1)
                throw new AllColumnDisplayError();



            Dictionary<String, TableRow> tableRows = new Dictionary<String, TableRow>(maxSize);
            List<List<TableCell>> resTc = new List<List<TableCell>>(maxSize);


            for (System.Int32 i = 0; i < resTc.Capacity; ++i)
                resTc.Add(new List<TableCell>(5));


            if (selectedString == PQLGrammar.ALL_ROWS_CONST)
            {


                for(System.Int32 i = 0; i < rows.Count; ++i)
                {
                    var row1 = rows.ElementAt(i);
     
                    List<List<TableCell>> tmpTc = new List<List<TableCell>>();



                    for (System.Int32 j = 0; j < row1.Count; ++j)
                    {

                        tmpTc.Add(row1.ElementAt(j).Value.RowCells);

                    }



                    for (System.Int32 k = 0; k < maxSize; ++k)
                        if (k >= tmpTc.Count)
                        {
                            List<TableCell> tmp = new List<TableCell>();
                            for (System.Int32 p2 = 0; p2 < tmpTc.ElementAt(0).Count; ++p2)
                                tmp.Add(new TableCell() { Alias = "NULL", ColumnData = "NULL" });
                            resTc.ElementAt(k).AddRange(tmp);

                        }
                        else
                            resTc.ElementAt(k).AddRange(tmpTc.ElementAt(k));
                    

                }




            } else 
            {
                System.String[] selectedRowsCOll = selectedString.Split(' ');

                System.Int32 ind = 0;
                foreach(String item in selectedRowsCOll)
                {
                    foreach (String item2 in selectedRowsCOll)
                        if (item == item2)
                            ind++;

                    if (ind > 1)
                        return new JsonMethodResult(false, "");
                    ind = 0;
                }





                for (System.Int32 i = 0; i < rows.Count; ++i)
                {
                    Dictionary<String, TableRow> currentTableRows = rows.ElementAt(i);
                    List<List<TableCell>> tmpAllSelectedRow = new List<List<TableCell>>();



                    foreach (TableRow innerTr in currentTableRows.Values)
                    {
                        List<TableCell> tmpOneRow = new List<TableCell>();

                        foreach (TableCell innerTc in innerTr.RowCells)
                            if (selectedRowsCOll.Contains(innerTc.Alias) || selectedRowsCOll.Contains(innerTc.Alias))
                                tmpOneRow.Add(innerTc);


                        tmpAllSelectedRow.Add(tmpOneRow);
                    }



                    for (System.Int32 k = 0; k < maxSize; ++k)

                        if (k >= tmpAllSelectedRow.Count)
                        {
                            List<TableCell> tmp = new List<TableCell>();
                            for (System.Int32 p2 = 0; p2 < tmpAllSelectedRow.ElementAt(0).Count; ++p2)
                                tmp.Add(new TableCell() { Alias = "NULL", ColumnData = "NULL" });
                            resTc.ElementAt(k).AddRange(tmp);
                        }
                        else
                            resTc.ElementAt(k).AddRange(tmpAllSelectedRow.ElementAt(k));

                }



            }





            for (System.Int32 i = 0; i < maxSize; ++i)
                resTable.TableRow.Add(i.ToString(), new TableRow() { RowCells = resTc.ElementAt(i) });



            return new JsonMethodResult(true, $"Selected from {_SelectedCollections.Count} collections") { AddedInfo = resTable };

        }




        public ITransactionMethodResult GetTableCollection() => new JsonMethodResult(true, String.Empty) { AddedInfo = _SelectedCollections };


        internal static Dictionary<String, ITableCollection> GetTableStorageFromTablesList(String[] fromSelected)
        {

            Dictionary<String, ITableCollection> selected = new Dictionary<string, ITableCollection>();

            for (Int32 i = 0; i < fromSelected.Length; ++i)
            {
                ITableCollection coll = null;


                if (DBMSIO.GetCurrentDataBase().IsTableExist(fromSelected[i]))
                {
                    ITransactionInfo ti_
                    = new JsonTransactionInfo(DBMSIO.GetCurrentLogin().NodeName, DBMSIO.GetCurrentDataBase().GetDataBaseName(), fromSelected[i]);
                    coll = GetTableStorageByName(ti_);

                }
                else if (DBMSIO.GetCurrentDataBase().IsViewExist(fromSelected[i]))
                {
                    ITable viewTable = DBMSIO.GetCurrentDataBase().GetViewsTable(fromSelected[i]);
                    coll = new VirtualTableCollection
                        (DBMSIO.GetCurrentDataBase().GetDataBaseName(), DBMSIO.GetCurrentDataBase().GetDataBaseOwner(), fromSelected[i], viewTable);
                }
                else if(DBMSIO.GetTriggerTmpTableTypeByName(fromSelected[i].Trim()) == StaticContextTablesID.DELETED_TABLE 
                    || DBMSIO.GetTriggerTmpTableTypeByName(fromSelected[i].Trim()) == StaticContextTablesID.UPDATED_TABLE)
                {
                    ITable tmp = DBMSIO.GetTriggerTmpTable(DBMSIO.GetTriggerTmpTableTypeByName(fromSelected[i]));

                    if (tmp != null)
                    {

                        foreach (TableRow tr in tmp.TableRow.Values)
                            tr.RowCells.ForEach(x => x.Alias = fromSelected[i].Trim() + PQLGrammar.ShemasSeparator + x.Alias);

                        coll
                            = new VirtualTableCollection(
                                DBMSIO.GetTriggerTmpTableTypeByName(fromSelected[i]).ToString(),
                                DBMSIO.GetTriggerTmpTableTypeByName(fromSelected[i]).ToString(),
                                fromSelected[i], tmp);
                    }
                    else throw new TableNotExist();


                }
                else throw new TableNotExist();

                selected.Add(fromSelected[i], coll);
            }


            return selected;
        }



        internal static ITableCollection GetTableStorageByName(ITransactionInfo info)
        {

            ITableInfo ti =
                DBMSIO.GetCurrentDataBase().GetTableMetaByName(info.Parameters);
            ITableCollection selected = default;


            if (ti.IsIndexed)
                selected = new IndexedTableCollection(info.DataBaseName, info.UserName, info.Parameters);
            
            else
                selected = new HeapTableCollection(info.DataBaseName, info.UserName, info.Parameters);
            

            selected.ReadStoredTable();
            return selected;
        }
    }
}
