﻿using DBMS.Core.InfoTypes;

namespace DBMS.Integration.JSON.SQL
{
    internal interface  ISelectStrategy
    {
        ITransactionMethodResult Select();
        ITransactionMethodResult GetTableCollection();
    }
}
