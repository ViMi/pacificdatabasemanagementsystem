﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBMS.Core.IO;
using DBMS.Core.Table;
using DBMS.Core.MMIL.ILConfiguration;
using DBMS.Core.MMIL;
using DBMS.Core.InfoTypes;
using DBMS.Core.Secure.UserRights;
using DBMS.Integration.JSON.SQL.Specification;
using DBMS.PQL.Grammar;
using DBMS.Core;


namespace DBMS.Integration.JSON.SQL
{
    internal sealed class UpdateT
        : IUpdateStrategy
    {

        #region Documentation
#if DocTrue
        static UpdateT() { }
#endif
        #endregion



        #region Variable Definition
        ITable _OperateTable;
        MMILDataManipulationContext _MMIL { set; get; }
        #endregion



        #region Rules
        private NotNullOrPK _NotNUllOrPk = new NotNullOrPK();
        private PKOrUnique _PKOrUnique = new PKOrUnique();
        private FKReferences _FKReferences = new FKReferences();
        private LikeSpecification _LikeSpecification = new LikeSpecification();
        private AutoIncrementAndNotEmpty _AutoIncrementAndNotEmpty = new AutoIncrementAndNotEmpty();
        private AutoIncrementAndEmpty _AutoIncrementAndEmpty = new AutoIncrementAndEmpty();
        private DefaultValue _DefaultValue = new DefaultValue();
        private IncludeObject _IncludeObject = new IncludeObject();
        #endregion


        public UpdateT(ref ITable operateTable, MMILDataManipulationContext mmil = null)
        {
            this._OperateTable = operateTable;
            _MMIL = mmil;
        }



        public ITransactionMethodResult Update()
        {


            System.String Setters = _MMIL.GetContextValue(MMILDataManipulationConfigure.ValuesDeclarationCommand);
            String[] fromSelected = _MMIL.GetContextValue(MMILDataManipulationConfigure.FromDeclarationCommad).Split(DBMSIO.DBMSConfig.StandartSeparator);

            System.String[] SettersSet = null;
            SelectedTable update = new SelectedTable(_MMIL);


            Dictionary<String, String> NameValue = new Dictionary<string, string>();
            System.Int32 countOfUpdated = 0;


            ITable _TriggerDeleteTable_ = new Table();
            ITable _TriggerUpdateTable_ = new Table();

            _TriggerDeleteTable_.TableRow = new Dictionary<string, TableRow>();
            _TriggerUpdateTable_.TableRow = new Dictionary<string, TableRow>();


            //№1 -- > Проверка сетеров
            try
            {

                SettersSet = Setters.Split(PQLGrammar.PackegSeparator[0]);
                for (Int32 i = 0; i < SettersSet.Length; ++i)
                {
                    String[] setter = SettersSet[i].Replace(" ", String.Empty).Split(PQLGrammar.SetSimbol[0]);
                    NameValue.Add(setter[0], setter[1]);
                }
            }
            catch (Exception ex)
            {
                return new JsonMethodResult(false, "error of Update -> " +
                    "Group of setters was recodnized incorrect fix set " +
                    $"statement parameters or call system admined-\n" +
                    $" ---- Error message {ex.Message} -- \n Error Code {ex.HResult} -- \nHELP Link {ex.HelpLink}");
            }




            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                //#3 -- > Проверка атрибуотов


                ITableInfo ti = DBMSIO.GetCurrentDataBase().GetTableMetaByName(fromSelected[0]);

                Dictionary<System.String, Dictionary<System.String, String>>
                    constraint = ti.ColumnConstraint;

                ITransactionMethodResult _CurrentState = null;
                for (Int32 i = 0; i < NameValue.Count; ++i)
                {
                    String field = NameValue.ElementAt(i).Key;
                    String value = NameValue.ElementAt(i).Value;

                    Dictionary<System.String, String> consItem;

                    if (!constraint.ContainsKey(field))
                        continue;
                    else constraint.TryGetValue(field, out consItem);


                    Dictionary<System.String, PacificDataBaseDataType> typesConstr = ti.ColumnTypes;
                    var KeyMain = constraint.Keys;
                    PacificDataBaseDataType typeCell;
                    TableCell tc = null;
                    Boolean checkFlag = false;
                    typesConstr.TryGetValue(KeyMain.ElementAt(i), out typeCell);

                    


                    for (System.Int32 j = 0; j < consItem.Keys.Count; ++j)
                    {

                        String key = consItem.Keys.ElementAt(j);

                        #region Assertation Check
                        if (_CurrentState != null)
                            if (_CurrentState.AddedInfo != null)
                                _CurrentState.AddedInfo = null;

                        _CurrentState = _NotNUllOrPk.SetValues(key, value, KeyMain.ElementAt(i)).Invoke(null);
                        checkFlag = _CurrentState.IsSuccess;
                        if (!checkFlag && (consItem.ContainsKey(PQLGrammar.DEFAULT) || consItem.ContainsKey(PQLGrammar.AUTO_INCREMENT)))
                            continue;
                        else if (!checkFlag && (!consItem.ContainsKey(PQLGrammar.DEFAULT) || !consItem.ContainsKey(PQLGrammar.AUTO_INCREMENT)))
                            return _CurrentState;


                        _CurrentState = _PKOrUnique.SetValues(ref typesConstr, key, KeyMain.ElementAt(i), ref _OperateTable, value, ref tc, typeCell).Invoke(null);
                        checkFlag = _CurrentState.IsSuccess;
                        if (!checkFlag && consItem.ContainsKey(PQLGrammar.AUTO_INCREMENT))
                            if (!checkFlag && key == PQLGrammar.PRIMARY_KEY && !Int32.TryParse(value, out _))
                                return _CurrentState;
                            else
                                continue;

                        else if (!checkFlag && !consItem.ContainsKey(PQLGrammar.AUTO_INCREMENT))
                            return _CurrentState;
                        else if (key == PQLGrammar.PRIMARY_KEY && !Int32.TryParse(value, out _))
                            return _CurrentState;


                        if ((_CurrentState.IsSuccess) && (tc = _CurrentState.AddedInfo as TableCell) != null) continue;




                        _CurrentState = _FKReferences.SetValue(key, value, KeyMain.ElementAt(i), typeCell, ref consItem, ref tc).Invoke(null);
                        checkFlag = _CurrentState.IsSuccess; if (!checkFlag) return _CurrentState;
                        if (_CurrentState.IsSuccess && (tc = _CurrentState.AddedInfo as TableCell) != null) continue;

                        _CurrentState = _LikeSpecification.SetValue(key, value, KeyMain.ElementAt(i), typeCell, ref consItem, ref tc).Invoke(null);
                        checkFlag = _CurrentState.IsSuccess; if (!checkFlag) return _CurrentState;
                        if (_CurrentState.IsSuccess && (tc = _CurrentState.AddedInfo as TableCell) != null) continue;



                        _CurrentState = _AutoIncrementAndNotEmpty.SetValue(key, value, KeyMain.ElementAt(i)).Invoke(null);
                        checkFlag = _CurrentState.IsSuccess; if (!checkFlag) return _CurrentState;

                        _CurrentState = _AutoIncrementAndEmpty.SetValue(key, value, KeyMain.ElementAt(i), ref _OperateTable, ref tc, typeCell).Invoke(null);
                        checkFlag = _CurrentState.IsSuccess; if (!checkFlag) return _CurrentState;
                        if (_CurrentState.IsSuccess && (tc = _CurrentState.AddedInfo as TableCell) != null) continue;



                        _CurrentState = _DefaultValue.SetValue(key, value, KeyMain.ElementAt(i), ref tc, ref consItem, typeCell).Invoke(null);
                        checkFlag = _CurrentState.IsSuccess; if (!checkFlag) return _CurrentState;
                        if (_CurrentState.IsSuccess && (tc = _CurrentState.AddedInfo as TableCell) != null) continue;
                        #endregion


                    }

                }
            }


            //№2 -- > Обновление 
            foreach (TableRow row in _OperateTable.TableRow.Values)
            {


                
                _TriggerDeleteTable_.TableRow.Add(countOfUpdated.ToString(), row.Clone() as TableRow);


                _TriggerUpdateTable_.TableRow.Add(countOfUpdated.ToString(), row);

                foreach (TableCell cell in row.RowCells)
                {

                    for (Int32 i = 0; i < NameValue.Count; ++i)
                    {
                        String name = NameValue.ElementAt(i).Key;
                        String val = NameValue.ElementAt(i).Value;

                        //#2.1 - 2.2
                        if (cell.Alias == name)
                        {
                            if (cell.DataType == PacificDataBaseDataType.STRING
                                || cell.DataType == PacificDataBaseDataType.NUMBER && Int32.TryParse(val, out _))
                            {


                                if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
                                {
                                    if (DBMSIO.GetCurrentDataBase()
                                    .IsExistForeignConstraintToTable(fromSelected[0]))
                                        if (DBMSIO.GetCurrentDataBase().
                                            IsExistForeignConstraintToCell(cell, fromSelected[0]))
                                            if (DBMSIO.GetCurrentDataBase().IsExistForeignConstraintToCellWithValue(cell, fromSelected[0]))
                                                return new JsonMethodResult(false, "Update to table " +
                                                    $"{fromSelected[0]} to cell " +
                                                    $"{cell.Alias} with " +
                                                    $"value {cell.ColumnData} " +
                                                    $"imposible because exist foreign key constraint to this cell");
                                }

                                if (cell.DataType == PacificDataBaseDataType.INCLUDED_TABLE)
                                {
                                    TableCell tc = null;
                                    ITableInfo ti = DBMSIO.GetCurrentDataBase().GetTableMetaByName(fromSelected[0]);
                                    Dictionary<String, String> coll;
                                    ti.ColumnConstraint.TryGetValue(name.Split(PQLGrammar.ShemasSeparator)[1], out coll);
                                    


                                    ITransactionMethodResult  _CurrentState = _IncludeObject.SetValue(
                                        PQLGrammar.INCLUDE_FOREIGN_TABLE_BY_KEY,
                                        val,
                                        name.Split(PQLGrammar.ShemasSeparator)[1], 
                                        PacificDataBaseDataType.INCLUDED_TABLE,
                                        ref coll, 
                                        ref tc).Invoke(null);
                                   if (!_CurrentState.IsSuccess) return _CurrentState;

                                    cell.ColumnData = (_CurrentState.AddedInfo as TableCell).ColumnData;

                                }
                                else
                                {

                                    cell.ColumnData = val;
                                }
                                countOfUpdated++;

                            }
                            else
                                return new JsonMethodResult(false, "Not compotibile data Type");
                            
                        }
                    }

                }




            }




            if (DBMSIO.GetCurrentDataBase().IsTableHasUpdateTrigger(fromSelected[0]))
            {

                DBMSIO.SetNewTmpTableToTriggerDS(StaticContextTablesID.DELETED_TABLE, _TriggerDeleteTable_);
                DBMSIO.SetNewTmpTableToTriggerDS(StaticContextTablesID.UPDATED_TABLE, _TriggerUpdateTable_);

                DBMSIO.GetCurrentDataBase().ExecuteTriggerOnTable(fromSelected[0], Core.NodeType.UPDATE_TRIGGER);

                DBMSIO.ReseTriggerGloalTmpTable();
            }


            return new JsonMethodResult(true,
           "Query updated " + countOfUpdated + " records from table with name "
           + fromSelected[0] + " from DataBase "
           + DBMSIO.GetCurrentDataBase().GetDataBaseName());
        }

    }
}
