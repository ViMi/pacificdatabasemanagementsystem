﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBMS.Core;
using DBMS.Core.InfoTypes;
using DBMS.Core.IO;

using DBMS.Core.MetaInfoTables;
using System.IO;
using System.Text.Json;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.UsersFromRoles;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.Helpers;
using DBMS.Core.Helpers.RulesControl;
using DBMS.Core.Triggers;


namespace DBMS.Integration.JSON.SQL
{

    internal enum ServerRulesID 
        : System.Byte
    {
        LOGIN_EXIST,
        USER_EXIST,
        DATABASE_SETTED,
        DATABASE_EXIST,
        TABLE_EXIST,
        VIEW_EXIST,
        TRIGGER_EXIST,
        PROCEDURE_EXIST,
        IT_IS_CURRENT_LOGIN,
        IT_IS_CURRENT_DATABASE,
      
    }





    internal sealed class ServerRules
        : IRulesControlObject  
    {

        #region Documentation
#if DocTrue
        static ServerRules()
        {

        }
#endif
        #endregion




        #region Rules Initiate
        public List<IStatement> Statements { get; set; } = new List<IStatement>()
        {



            new ServerStatement(ServerRulesID.DATABASE_EXIST, ServerRulesID.DATABASE_EXIST.ToString())
            {    
                StatementDefinition = (ITransactionInfo ti)=>{
                    
                    if (!DBMSIO.IsDataBaseExist(ti.Parameters))
                        return new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.DATABASE_NOT_FOUND]);

                    return new JsonMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.DATABASE_ALREADY_EXIST]);

                }

            },



            new ServerStatement(ServerRulesID.DATABASE_SETTED, ServerRulesID.DATABASE_SETTED.ToString())
            {         
               
                StatementDefinition = (ITransactionInfo ti)=>{

                    if (DBMSIO.GetCurrentDataBase() != null)
                        return new JsonMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.DATABASE_SETTED]);

                    return new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.DATABASE_UNSET]);

                }

            },



            new ServerStatement(ServerRulesID.LOGIN_EXIST, ServerRulesID.LOGIN_EXIST.ToString())
            {

                StatementDefinition = (ITransactionInfo ti)=>{

                    if (!DBMSIO.IsLoginExist(ti.Parameters))
                        return new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.LOGIN_NOT_FOUND]);

                    return new JsonMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.LOGIN_ALREADY_EXIST]);

                     }

            },



              new ServerStatement(ServerRulesID.USER_EXIST, ServerRulesID.USER_EXIST.ToString())
            {

                StatementDefinition = (ITransactionInfo ti)=>{

                    if (!DBMSIO.GetCurrentDataBase().IsUserExist(ti.Parameters))
                        return new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.USER_NOT_FOUND]);

                    return new JsonMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.LOGIN_ALREADY_EXIST]);

                     }

            },



            new ServerStatement(ServerRulesID.TABLE_EXIST, ServerRulesID.TABLE_EXIST.ToString())
            {

                StatementDefinition = (ITransactionInfo ti)=>{

                        if (!DBMSIO.GetCurrentDataBase().IsTableExist(ti.Parameters))
                        return new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.TABLE_NOT_FOUND]);

                    return new JsonMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.TABLE_ALREAD_EXIST]);

                }

            },



            new ServerStatement(ServerRulesID.TRIGGER_EXIST, ServerRulesID.TRIGGER_EXIST.ToString())
            {

                StatementDefinition = (ITransactionInfo ti)=>{

                       if (!DBMSIO.GetCurrentDataBase().IsTriggerExist(ti.Parameters))
                        return new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.TRIGGER_NOT_EXIST]);

                    return new JsonMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.TRIGGER_ALREADY_EXIST]);


                }

            },



            new ServerStatement(ServerRulesID.VIEW_EXIST, ServerRulesID.VIEW_EXIST.ToString())
            {

                StatementDefinition = (ITransactionInfo ti)=>{

                        if (!DBMSIO.GetCurrentDataBase().IsViewExist(ti.Parameters))
                        return new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.VIEW_NOT_EXIST]);

                    return new JsonMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.VIEW_ALREADY_EXIST]);


                }

            },



            new ServerStatement(ServerRulesID.PROCEDURE_EXIST, ServerRulesID.PROCEDURE_EXIST.ToString())
            {
                StatementDefinition = (ITransactionInfo ti) =>
                {
                    if(!DBMSIO.GetCurrentDataBase().IsProcedureExist(ti.Parameters))
                        return new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.PROCEDURE_NOT_EXIST]);

                   return new JsonMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.PROCEDURE_ALREADY_EXIST]);

                }
            },



            new ServerStatement(ServerRulesID.IT_IS_CURRENT_DATABASE, ServerRulesID.IT_IS_CURRENT_DATABASE.ToString())
            {
                StatementDefinition = (ITransactionInfo ti) =>
                {
                    if((DBMSIO.GetCurrentDataBase().GetDataBaseName() != ti.Parameters))
                        return new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.FAILURE_EXECUTE]);

                   return new JsonMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE]);

                }
            },



            new ServerStatement(ServerRulesID.IT_IS_CURRENT_LOGIN, ServerRulesID.IT_IS_CURRENT_LOGIN.ToString())
            {
                StatementDefinition = (ITransactionInfo ti) =>
                {
                    if((DBMSIO.GetCurrentLogin().NodeName != ti.Parameters))
                        return new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.FAILURE_EXECUTE]);

                   return new JsonMethodResult(true, "IT IS CURRENT LOGIN YOU CAN NOT CHANGE IT \n");

                }
            }




        };

        #endregion


        public IStatement FindStatement(System.String statmentName) => this.Statements.Find(x => x.StatementName == statmentName);
        public IStatement FindStatement(System.Int32 statementID) => this.Statements.Find( x => x.StatementID == statementID);
        public ITransactionMethodResult Invoke(System.Int32 statementID, ITransactionInfo ti) => FindStatement(statementID).Invoke(ti);
        public ITransactionMethodResult Invoke(System.String statementName, ITransactionInfo ti) => FindStatement(statementName).Invoke(ti);
    }
}
