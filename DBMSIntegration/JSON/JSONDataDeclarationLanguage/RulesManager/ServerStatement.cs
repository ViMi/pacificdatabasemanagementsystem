﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBMS.Core.Helpers.RulesControl;
using DBMS.Core.InfoTypes;

namespace DBMS.Integration.JSON.SQL
{
    internal sealed class ServerStatement
        : IStatement
    {
        public System.Int32 StatementID { get; set; }
        public System.String StatementName { get; set; }
        public Statement StatementDefinition { get; set; }


        public ServerStatement(ServerRulesID ruleID, System.String ruleName)
        {
            this.StatementID = (Byte)ruleID;
            this.StatementName = ruleName;
        }

        public ITransactionMethodResult Invoke(ITransactionInfo ti) => StatementDefinition(ti);
    }
}
