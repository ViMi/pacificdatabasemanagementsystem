﻿using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using DBMS.Core;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.MMIL;
using DBMS.Core.MMIL.ILConfiguration;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.Secure;
using DBMS.Core.Helpers;



namespace DBMS.Integration.JSON.SQL
{
    internal class JSONAdministrationExtension
    {

        #region Documentation
#if DocTrue
        static JSONAdministrationExtension() { }
#endif
        #endregion


        public ITransactionMethodResult ExecuteCommand(DBCommand command, ITransactionInfo info) => command switch
        {

            DBCommand.GET_COUNT_OF_PROCEDURES_ON_SERVER => GetCountOfProceduresOnServer(info),
            DBCommand.GET_COUNT_OF_TABLE_ON_SERVER => GetCountOfTableOnServer(info),
            DBCommand.GET_COUNT_OF_VIEW_ON_SERVER => GetCountOfViewOnServer(info),
            DBCommand.GET_COUNT_OF_USER_ON_SERVER => GetCountOfUserOnServer(info),
            DBCommand.GET_COUNT_OF_LOGIN_ON_SERVER => GetCountOfLoginOnServer(info),
            DBCommand.GET_CURRENT_LOGIN_INFO => GetCurrentLoginInfo(info),
            DBCommand.GET_CURRENT_DATABASE_INFO => GetCurrentDatabaseInfo(info),
            DBCommand.GET_SERVER_NAME => GetServerName(info),
            DBCommand.GET_LOG_FILE_CONTENT => GetLogFileContent(info),
            DBCommand.GET_PREVIOUS_START_DATE => GetPreviousStartDate(info),
            DBCommand.GET_LAST_START_DATE => GetLastStartDate(info),
            DBCommand.GET_SERVER_TYPE => GetServerType(info),
            DBCommand.GET_COUNT_OF_PROCEDURES_ON_CURRENT_DATABASE => GetCountOfProceduresOnCurrentDatabase(info),
            DBCommand.GET_COUNT_OF_VIEW_ON_CURRENT_DATABASE => GetCountOfViewOnCurrentDatabase(info),
            DBCommand.GET_COUNT_OF_USER_ON_CURRENT_DATABASE => GetCountOfUserOnCurrentDatabase(info),
            DBCommand.GET_COUNT_OF_TABLES_ON_CURRENT_DATABASE => GetCountOfTablesOnCurrentDatabase(info),
            DBCommand.GET_ALL_PROCEDURES_NAME_ON_CURRENT_DATABASE => GetAllProceduresNameOnCurrentDatabase(info),
            DBCommand.GET_ALL_TABLE_NAME_ON_CURRENT_DATABASE => GetAllTableNameOnCurrentDatabase(info),
            DBCommand.GET_ALL_VIEW_NAME_ON_CURRENT_DATABASE => GetAllViewNameOnCurrenDatabase(info),
            DBCommand.GET_ALL_PROCEDURES_NAME_ON_SERVER => GetAllProceduredNameOnServer(info),
            DBCommand.GET_ALL_TABLE_NAME_ON_SERVER => GetAllTableNameOnServer(info),
            DBCommand.GET_ALL_VIEW_NAME_ON_SERVER => GetAllViewNameOnServer(info),
            DBCommand.SET_SERVER_NAME => SetServerName(info),
            DBCommand.SET_SERVER_TYPE => SetServerType(info),
            DBCommand.SET_SERVER_PASSWORD => SetServerPassword(info),
            DBCommand.GET_TABLE_COLUMNS_DATAS => GetTableColumnData(info),
            DBCommand.GET_TABLE_COLUMNS_CONSTRAINTS => GetTableConstraint(info),
            DBCommand.GET_STORED_PROCEDURE_TEXT => GetStoredProcedureText(info),
            DBCommand.GET_VIEW_TEXT => GetViewText(info),
            DBCommand.RESTORE_SERVER => RestoreServer(info),
            DBCommand.GET_SERVER_CONFIG => GetServerConfig(info),
            DBCommand.SET_STANDART_MODE => StdMode(info),
            DBCommand.SET_UNSAFE_MODE => UnsafeMode(info),
            DBCommand.SET_LNG_UNSAFE_MODE => WithoutLngSafeMode(info),


            _ => new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.UNKNOWN_COMMAND])
        };



        public ITransactionMethodResult UnsafeMode(ITransactionInfo ti)
        {

            DBMSIO.SetDBMSMode(DBMSMode.UNSAFE_MODE);

            return new JsonMethodResult(true, $"DBMS Execution Systems setted in {DBMSMode.UNSAFE_MODE}");
        }


        public ITransactionMethodResult StdMode(ITransactionInfo ti)
        {

            DBMSIO.SetDBMSMode(DBMSMode.STANDART_MODE);

            return new JsonMethodResult(true, $"DBMS Execution Systems setted in {DBMSMode.STANDART_MODE} ");
        }

        public ITransactionMethodResult WithoutLngSafeMode(ITransactionInfo ti)
        {
            DBMSIO.SetDBMSMode(DBMSMode.WITHOUT_LNG_CONTROL);

            return new JsonMethodResult(true, $"DBMS Execution Systems setted in {DBMSMode.WITHOUT_LNG_CONTROL}");
        }



        public ITransactionMethodResult GetServerConfig(ITransactionInfo ti)
        {
            String res = 
                $"Base Directory : [{DBMSIO.DBMSConfig.BaseDir}] {Environment.NewLine}" +
                $"DataBase Director : [{DBMSIO.DBMSConfig.BaseDataBaseDir}] {Environment.NewLine}" +
                $"Security Directory : [{DBMSIO.DBMSConfig.BaseSecurityDir}] {Environment.NewLine}" +
                $"Base Login Directory : [{DBMSIO.DBMSConfig.BaseLoginDir}] {Environment.NewLine}" +
                $"Path To Log File : [{DBMSIO.DBMSConfig.PathToLog}] {Environment.NewLine}" +
                $"Journal Directory : [{DBMSIO.DBMSConfig.BaseJournalDir}] {Environment.NewLine}" +
                $"DataBase Procedure Directory : [{DBMSIO.DBMSConfig.DataBasesProcedureDir}] {Environment.NewLine}" +
                $"DataBase User Directory : [{DBMSIO.DBMSConfig.DataBasesUsersDir}] {Environment.NewLine}" +
                $"DataBase View Directory : [{DBMSIO.DBMSConfig.DataBaseViewDir}] {Environment.NewLine}" +
                $"DataBase Table Directory : [{DBMSIO.DBMSConfig.DataBaseTableDir}] {Environment.NewLine}" +
                $"DataBase Trigger Directory : [{DBMSIO.DBMSConfig.TriggersBaseDir}] {Environment.NewLine}" +
                $"Operationg System Path separator : [{DBMSIO.DBMSConfig.OSDirectoryWaySeparator}] {Environment.NewLine}" +
                $"Inner Server Standart Separator : [{DBMSIO.DBMSConfig.StandartSeparator}] {Environment.NewLine}" +
                $"Full Meta File Name : [{DBMSIO.DBMSConfig.FullMetaFileName}] {Environment.NewLine}" +
                $"Log File Extension : [{DBMSIO.DBMSConfig.StandartLogFileExtension}] {Environment.NewLine}" +
                $"Log File Name : [{DBMSIO.DBMSConfig.StandartLogFileName}] {Environment.NewLine}" +
                $"Journal File Extension : [{DBMSIO.DBMSConfig.BaseJournalFileExtension}] {Environment.NewLine}" +
                $"Journal File Name : [{DBMSIO.DBMSConfig.BaseJournalFileName}] {Environment.NewLine}" +
                $"Max Number : [{DBMSIO.DBMSConfig.MaxNumber}] {Environment.NewLine}" +
                $"Max String Name : [{DBMSIO.DBMSConfig.MaxStringLength}] {Environment.NewLine}" +
                $"System Administrator Name : [{DBMSIO.DBMSConfig.SimpleSysAdminName}] {Environment.NewLine}" +
                $"Secure Admin Name : [{DBMSIO.DBMSConfig.SimpleSecureAdmin}] {Environment.NewLine}" +
                $"DataBaseCreator Name : [{DBMSIO.DBMSConfig.SimpleDBCreator}] {Environment.NewLine}  " +
                $"IL File Extension : [{DBMSIO.DBMSConfig.ILExtension}] {Environment.NewLine}";

            return new JsonMethodResult(true, res);
        }


        public ITransactionMethodResult RestoreServer(ITransactionInfo ti)
        {
            DBMSIO.SetResoreMode(true);
            DBMSIO.RestoreServerByJournal();
            DBMSIO.SetResoreMode(false);
            return new JsonMethodResult(true, "SERVER RESTORE START");
        }

        public ITransactionMethodResult GetCountOfProceduresOnServer(ITransactionInfo ti)
            => new JsonMethodResult(true, $"SERVER CONTAINS --- {DBMSIO.GetCountOfAllProceduresOnServer()} PROCEDURES");


        public ITransactionMethodResult GetCountOfTableOnServer(ITransactionInfo ti)
         => new JsonMethodResult(true, $"SERVER CONTAINS --- {DBMSIO.GetCountOfAllTablesOnServer()} TABLES");


        public ITransactionMethodResult GetCountOfViewOnServer(ITransactionInfo ti)
          => new JsonMethodResult(true, $"SERVER CONTAINS --- {DBMSIO.GetCountOfAllViewOnServer()} VIEWS");




        public ITransactionMethodResult GetCountOfUserOnServer(ITransactionInfo ti)
            => new JsonMethodResult(true, $"Server contains {DBMSIO.GetCountOfAllUsersOnServer()} Users");

        public ITransactionMethodResult GetCountOfLoginOnServer(ITransactionInfo ti)
            => new JsonMethodResult(true, $"This Server Contains {DBMSIO.GetCountOfLoginOnServer()} Logins");



        public ITransactionMethodResult GetCurrentLoginInfo(ITransactionInfo ti)
        {

            List<String> db = DBMSIO.GetAllDataBaseNames();

           String dbNames = String.Empty;
            foreach (String item in db)
                dbNames += item + DBMSIO.DBMSConfig.StandartSeparator;
            

            List<String> userNames = DBMSIO.GetAllDataBaseUserName();
            String users = String.Empty;
            foreach (String item in userNames)
                users += item + DBMSIO.DBMSConfig.StandartSeparator;
            

            return new JsonMethodResult(true, $"CURRENT LOGIN NAME --- {DBMSIO.GetCurrentLogin().NodeName} " +
                $"\n CURRENT LOGIN HAVE USERS ---- {DBMSIO.GetCountOfUser()} " +
                $"\n CURRENT USERS NAMES  ---- {users} " +
                $"\n IN NEXT DATABASES  ---- {dbNames} " +
                $"\n CURRENT LOGIN HAVE PASSWORD {DBMSIO.GetCurrentLogin().NodeAccessPassword._Password} " +
                $"\n WITH ENCRIPTION TYPE {DBMSIO.GetCurrentLogin().NodeAccessPassword.Encription} " +
                $"\n ");

        }
        public ITransactionMethodResult GetCurrentDatabaseInfo(ITransactionInfo ti)
        => new JsonMethodResult(true,
                $"\nCURRENT DATABASE HAS NAME {DBMSIO.GetCurrentDataBase().GetDataBaseName()}" +
                $"\nIT IS LOCATED IN {DBMSIO.GetCurrentDataBase().GetWayToDataBaseRoot()}" +
                $"\nDATABASE HAS {DBMSIO.GetCurrentDataBase().GetCountOfViews()} VIEWS" +
                $"\nDATABASE HAS {DBMSIO.GetCurrentDataBase().GetCountOfTables()} TABLES" +
                $"\nDATABASE HAS {DBMSIO.GetCurrentDataBase().GetCountOfProcedures()} PROCEDURES" +
                $"\nDATABASE HAS {DBMSIO.GetCurrentDataBase().GetCountOfUser()} USERS"
                );


        public ITransactionMethodResult GetServerName(ITransactionInfo ti)
            => new JsonMethodResult(true, DBMSIO.ServerMetaInformation.NodeName);
        public ITransactionMethodResult GetLogFileContent(ITransactionInfo ti)
            => new JsonMethodResult(true, DBMSIO.GetLoggerFileContent());



        public ITransactionMethodResult GetPreviousStartDate(ITransactionInfo ti)
            => new JsonMethodResult(true, $"Tha server was started in {DBMSIO.ServerMetaInformation.LastSeenDate}");

        public ITransactionMethodResult GetLastStartDate(ITransactionInfo ti)
            => new JsonMethodResult(true, $"Tha server was started in {DBMSIO.ServerMetaInformation.CurrentSeenDate}");

        public ITransactionMethodResult GetServerType(ITransactionInfo ti)
            => new JsonMethodResult(true, $"That server is {DBMSIO.ServerMetaInformation.ServerType.ToString()}");

        public ITransactionMethodResult GetCountOfProceduresOnCurrentDatabase(ITransactionInfo ti)
              => new JsonMethodResult(true, $"\nDATABASE HAS {DBMSIO.GetCurrentDataBase().GetCountOfProcedures()} PROCEDURES");


        public ITransactionMethodResult GetCountOfViewOnCurrentDatabase(ITransactionInfo ti)
            => new JsonMethodResult(true, $"\nDATABASE HAS {DBMSIO.GetCurrentDataBase().GetCountOfViews()} VIEWS");




        public ITransactionMethodResult GetCountOfUserOnCurrentDatabase(ITransactionInfo ti)
            => new JsonMethodResult(true,
                $"\nDATABASE HAS {DBMSIO.GetCurrentDataBase().GetCountOfUser()} USERS");


        public ITransactionMethodResult GetCountOfTablesOnCurrentDatabase(ITransactionInfo ti)
               => new JsonMethodResult(true,
                $"\nDATABASE HAS {DBMSIO.GetCurrentDataBase().GetCountOfTables()} TABLES");


        public ITransactionMethodResult GetAllProceduresNameOnCurrentDatabase(ITransactionInfo ti)
        {

            String dbProcedures = String.Empty;
            List<String> items = DBMSIO.GetCurrentDataBase().GetProceduresName();
            foreach (String item in items)
                dbProcedures += item + DBMSIO.DBMSConfig.StandartSeparator;
            

            return new JsonMethodResult(true,
                $"\nDATABASE PROCEDURES IT`S {dbProcedures}");
        }






        public ITransactionMethodResult GetAllTableNameOnCurrentDatabase(ITransactionInfo ti)
        {
            String tables = String.Empty;
            List<String> items = DBMSIO.GetCurrentDataBase().GetTableNames();
            foreach (String item in items)
                tables += item + DBMSIO.DBMSConfig.StandartSeparator;
            

            return new JsonMethodResult(true, $"\nDATABASe TABLES IT`S {tables}");

        }


        public ITransactionMethodResult GetAllViewNameOnCurrenDatabase(ITransactionInfo ti)
        {
            String views = String.Empty;

            List<String> items = DBMSIO.GetCurrentDataBase().GetViewsNames();
            foreach (String item in items)
                views += item + DBMSIO.DBMSConfig.StandartSeparator;
            

            return new JsonMethodResult(true, $"\nDATABASE VIEW IT`s {views}");
        }





        public ITransactionMethodResult GetAllProceduredNameOnServer(ITransactionInfo ti)
        {

            String resStr = String.Empty;
            List<String> names = DBMSIO.GetNamesOfAllUserOnServer();
            foreach (String item in names)
                resStr += item + Environment.NewLine;
            
            return new JsonMethodResult(true, resStr);

        }


        public ITransactionMethodResult GetAllTableNameOnServer(ITransactionInfo ti)
        {

            String resStr = String.Empty;
            List<String> names = DBMSIO.GetNamesOfAllTablesOnServer();
            foreach (String item in names)
                resStr += item + Environment.NewLine;
            
            return new JsonMethodResult(true, resStr);

        }


        public ITransactionMethodResult GetAllViewNameOnServer(ITransactionInfo ti)
        {

            String resStr = String.Empty;
            List<String> names = DBMSIO.GetNamesOfAllViewOnServer();
            foreach (String item in names)
                resStr += item + Environment.NewLine;
            

            return new JsonMethodResult(true, resStr);

        }







        public ITransactionMethodResult SetServerName(ITransactionInfo ti)
        {
            
            DBMSIO.SetNewserverName(ti.Parameters);
            return new JsonMethodResult(true, $"Server Name Setted in {ti.Parameters}");

        }

        public ITransactionMethodResult SetServerType(ITransactionInfo ti)
        {
           DBMSIO.SetNewServerType((ServerType)ti.AddedInfo);
            return new JsonMethodResult(true, $"Server Type setted in {(ServerType)ti.AddedInfo}");
        }

        public ITransactionMethodResult SetServerPassword(ITransactionInfo ti)
        {

            String pass = ti.Parameters;
            IEncriptor ecn = ti.AddedInfo as IEncriptor;
            

            Password password = new Password(ecn.type, ecn.Encript(pass));
            DBMSIO.SetNewServerPassword(password);
            

            return new JsonMethodResult(true, $"New Password {pass} With Encriptor Strategy {ecn.type}");

        }







        public ITransactionMethodResult GetStoredProcedureText(ITransactionInfo ti)
            => new JsonMethodResult(true, DBMSIO.GetCurrentDataBase().GetStoredProcedureText(ti.Parameters) + Environment.NewLine);


        public ITransactionMethodResult GetViewText(ITransactionInfo ti)
            => new JsonMethodResult(true, DBMSIO.GetCurrentDataBase().GetViewText(ti.Parameters) + Environment.NewLine);


        public ITransactionMethodResult GetTriggerText(ITransactionInfo ti)
            => new JsonMethodResult(true, DBMSIO.GetCurrentDataBase().GetTriggerText(ti.Parameters) + Environment.NewLine);


        public ITransactionMethodResult GetTableConstraint(ITransactionInfo ti)
        {
            ITableInfo table = DBMSIO.GetCurrentDataBase().GetTableMetaByName(ti.Parameters);

            String res = String.Empty;

            foreach (var item in table.ColumnConstraint)
            {
                res += $"[{item.Key}]->";

                foreach(var item2 in item.Value)
                {
                    res += $"__{item2.Key}__ {item2.Value}__| ";
                }

                res += Environment.NewLine;
            }


            return new JsonMethodResult(true, res);
        }


        public ITransactionMethodResult GetTableColumnData(ITransactionInfo ti)
        {
            ITableInfo table = DBMSIO.GetCurrentDataBase().GetTableMetaByName(ti.Parameters);

            String res = String.Empty;

            foreach (var item in table.ColumnTypes)
            {
                res += $"[{item.Key}] --> [{item.Value}]" + Environment.NewLine;

            }

            return new JsonMethodResult(true, res);

        }


    }
}
