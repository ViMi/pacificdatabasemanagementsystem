﻿using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using System;
using System.IO;
using DBMS.Core;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.Helpers;


namespace DBMS.Integration.JSON.SQL
{
    internal sealed class JSONExtensionComands
    {

        #region Documentation
#if DocTrue
        static JSONExtensionComands() { }
#endif
        #endregion



        public ITransactionMethodResult ExecuteCommand(DBCommand command, ITransactionInfo info) => command switch
        {
            DBCommand.USE => Use(info),
            DBCommand.BACKUP => BackUp(info),
            DBCommand.BACKUP_WITH_LOG => BackUpWithLog(info),
            DBCommand.MIGRATE => Migrate(info),
            DBCommand.EXECUTE => Execute(info),


            _ => new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.UNKNOWN_COMMAND])

        };




        #region Extends Methods
        private ITransactionMethodResult BackUp(ITransactionInfo info)
        {

            #region RuleCheck
            ITransactionMethodResult res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.IT_IS_CURRENT_DATABASE, new JsonTransactionInfo() { Parameters = info.DataBaseName });
            if (!res.IsSuccess) return res;
            #endregion

      

            IBackUpInfo backUp = info.AddedInfo as BackUpInfo;

            String to = backUp.DestinationPoint.Replace('/', '\\');

            String[] arr = to.Split('\\');
            to = "";
            for (Int32 i = 0; i < arr.Length; ++i)
            {
                if (i == 0)
                    to += arr[i] + ":" + DBMSIO.DBMSConfig.OSDirectoryWaySeparator;
                else if (i < arr.Length && i != arr.Length - 1)
                {
                    if (arr[i] != String.Empty)
                        to += arr[i] + DBMSIO.DBMSConfig.OSDirectoryWaySeparator;
                }
                else
                {
                    if (arr[i] != String.Empty)
                        to += arr[i];
                }
            }
            this.CopyDir(DBMSIO.GetCurrentDataBase().GetWayToDataBaseRoot(), to);

            if (backUp.LogConfig.Log)
            {
                File.Copy(DBMSIO.DBMSConfig.BaseSecurityDir+DBMSIO.DBMSConfig.OSDirectoryWaySeparator+DBMSIO.DBMSConfig.PathToLog+ "LogFile.log", to);

                if (backUp.LogConfig.Truncate)
                {
                    using FileStream fs = 
                        new FileStream(DBMSIO.DBMSConfig.BaseSecurityDir + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + DBMSIO.DBMSConfig.PathToLog + "LogFile.log", FileMode.Truncate);
                }

            }

            return new JsonMethodResult
                (true, "DataBase With name"+ info.DataBaseName + " copied" + info.Parameters + "By user " + info.UserName);
        }

        private ITransactionMethodResult BackUpWithLog(ITransactionInfo info)
        {
            return new JsonMethodResult
                (true, "DataBase With name " + info.DataBaseName + " copied to " + info.Parameters + " By user " + info.UserName);
        }

        private ITransactionMethodResult Migrate(ITransactionInfo info)
        {

            IMigrate migr = info.AddedInfo as MigrateStatement;


            #region RuleCheck
            ITransactionMethodResult res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.IT_IS_CURRENT_DATABASE, new JsonTransactionInfo() { Parameters = migr.SourcePoint });
            if (!res.IsSuccess) return res;
            #endregion
           

            


            String to = migr.DestinationPoint.Replace('/', '\\');

            String[] arr = to.Split('\\');
            to = String.Empty;
            for (Int32 i = 0; i < arr.Length; ++i)
            {
                if (i == 0)
                    to += arr[i] + ":" + DBMSIO.DBMSConfig.OSDirectoryWaySeparator;
                else if (i < arr.Length && i != arr.Length - 1)
                {
                    if (arr[i] != String.Empty)
                        to += arr[i] + DBMSIO.DBMSConfig.OSDirectoryWaySeparator;
                }
                else
                {
                    if (arr[i] != String.Empty)
                        to += arr[i];
                }
            }


            try
            {
                Directory.Move(
                    DBMSIO.GetCurrentDataBase().GetWayToDataBaseRoot(),
                    to
                    );
            } catch (Exception ex)

            {
                return new JsonMethodResult(false, $"ERROR {ex.Message} {Environment.NewLine}");

            }

            DBMSIO.TruncateDataBase(migr.SourcePoint);
            JSON json = new JSON();
            json.TruncateSchemaFromAllLogins(migr.SourcePoint);

            return new JsonMethodResult
                (true, "DataBase With name "+ migr.SourcePoint + " migrated to "+ migr.DestinationPoint + " By user " + DBMSIO.GetCurrentLogin().NodeName);
        }

        private ITransactionMethodResult Execute(ITransactionInfo info)
        {

            #region RuleCheck
            ITransactionMethodResult res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_SETTED, info);
            if (!res.IsSuccess) return res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.PROCEDURE_EXIST, info);
            if (!res.IsSuccess) return res;
            #endregion


           DBMSIO.GetCurrentDataBase().GetStoredProcedure(info.Parameters).Invoke();


            return new JsonMethodResult(true,
                "Stored Procedure with name " + info.Parameters + " From database " + info.DataBaseName + " Executed by user " + info.UserName);
        }

        private ITransactionMethodResult Use(ITransactionInfo info)
        {

            #region RuleCheck
             ITransactionMethodResult res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_EXIST, info);
            if (!res.IsSuccess) return res;
            #endregion


            DBMSIO.SetCurrentDataBase(new DataBaseIO(info.Parameters, info.UserName));

            return 
                new JsonMethodResult(true, "Current DataBase set in "+ info.Parameters);
        }

        #endregion




        #region File System. Methods
        private void CopyDir(System.String from, System.String to)
        {


            Directory.CreateDirectory(to);

            foreach (System.String item1 in Directory.GetFiles(from))
            {
                System.String item2 = to + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + Path.GetFileName(item1);
                File.Copy(item1, item2);
            }
            foreach (System.String s in Directory.GetDirectories(from))
            {
                CopyDir(s, to + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + Path.GetFileName(s));
            }
        }
        #endregion

    }
}
