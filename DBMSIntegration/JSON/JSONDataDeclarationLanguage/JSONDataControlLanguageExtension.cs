﻿using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using DBMS.Core;
using System;
using System.IO;
using System.Text;
using System.Text.Json;
using DBMS.Core.Secure;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.User;
using DBMS.Core.Helpers;
using DBMS.Core.ErrorControlSystem.Exeption;



namespace DBMS.Integration.JSON.SQL
{
    internal sealed class JSONDataControlLanguageExtension
    {

        #region Documentaiton
#if DocTrue
        static JSONDataControlLanguageExtension() { }
#endif
        #endregion


        public ITransactionMethodResult ExecuteCommand(DBCommand command, ITransactionInfo info) => command switch
        {
            DBCommand.CHANGE_PASSWORD_TO_LOGIN => ChangePasswordToLogin(info),
            DBCommand.CHANGE_PASSWORD_TO_USER => ChangePasswordToUser(info),

            _ => new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.UNKNOWN_COMMAND])
        };



        #region Change Password Methods
        private ITransactionMethodResult ChangePasswordToLogin(ITransactionInfo ti)
        {

            ILogin token = this.ReadRightsFile<Login>(DBMSIO.DBMSConfig.FullPathToLoginRoot
                  + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                  + ti.UserName
                  + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                  + DBMSIO.DBMSConfig.FullMetaFileName);




            String passString = ti.Parameters;

            IEncriptor encriptor = ti.AddedInfo as IEncriptor;
            if (encriptor == null)
                return new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.CRIPTER_UNSET]);
            EncriptionType type = encriptor.type;
            String resPassStr = encriptor.Encript(passString);

            Password newPassword = new Password(type, resPassStr);


            token.NodeAccessPassword = newPassword;




            this.WriteRightsFile<ILogin>(DBMSIO.DBMSConfig.FullPathToLoginRoot
                 + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                 + ti.UserName
                 + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                 + DBMSIO.DBMSConfig.FullMetaFileName, token);



            return new JsonMethodResult(true, 
                $"New Password settet to login {ti.UserName}." +
                $" New password ---- {passString}  " +
                $"encription Type {encriptor.type} ");
        }

        private ITransactionMethodResult ChangePasswordToUser(ITransactionInfo ti) 
        {

            IUserToken token = this.ReadRightsFile<User>(
                DBMSIO.GetCurrentDataBase().GetWayToDataBaseUserRoot()
                            + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                            + ti.UserName
                            + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                            + DBMSIO.DBMSConfig.FullMetaFileName);



            String passString = ti.Parameters;

            IEncriptor encriptor = ti.AddedInfo as IEncriptor;
            if (encriptor != null)
                return new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.CRIPTER_UNSET]);
            EncriptionType type = encriptor.type;
            Password newPassword = new Password(type, encriptor.Encript(passString));


            token.NodeAccessPassword = newPassword;




            this.WriteRightsFile<IUserToken>(
                DBMSIO.GetCurrentDataBase().GetWayToDataBaseUserRoot()
                    + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                    + ti.UserName
                    + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                    + DBMSIO.DBMSConfig.FullMetaFileName, token);


            return new JsonMethodResult(true, $"New Password settet to user " +
                $"{ti.UserName}. New password ----" +
                $" {passString}  encription Type {encriptor.type} ");

        }
        #endregion





        #region File System Methods
        private void WriteRightsFile<T>(System.String path, T token)
        {
            try
            {
                using (FileStream fs = new FileStream(path, FileMode.Truncate))
                {
                    String json = JsonSerializer.Serialize<T>(token);

                    System.Byte[] arr
                        = Encoding.UTF8.GetBytes(json);
                    fs.Write(arr, 0, arr.Length);

                }
            }
            catch (Exception ex)
            {
                if (ex.IsFileLocked())
                    throw new FileLockedError();
                else
                    throw ex;
            }

        }

        private T ReadRightsFile<T>(System.String path)
        {
            T token = default;
            try
            {
                using (StreamReader fs = new StreamReader(path))
                {
                    token = JsonSerializer.Deserialize<T>(fs.ReadToEnd());
                }
            }
            catch (Exception ex)
            {
                if (ex.IsFileLocked())
                    throw new FileLockedError();
                else
                    throw ex;
            }

            return token;

        }
      
        #endregion

    }
}
