﻿using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using System;
using System.Text;
using DBMS.Core;
using DBMS.Core.MetaInfoTables;
using System.IO;
using System.Text.Json;
using DBMS.Core.ErrorControlSystem.Exeption; 
using DBMS.Core.Secure.UserRights;
using DBMS.Core.UsersFromRoles;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.Helpers;
using DBMS.Core.Triggers;
using DBMS.PQL.Grammar;

namespace DBMS.Integration.JSON.SQL
{
    internal sealed class JSONDataDeclarationLanguage
    {

        #region Documentation
#if DocTrue
        static JSONDataDeclarationLanguage() { }
#endif
        #endregion


        public ITransactionMethodResult ExecuteCommand(DBCommand command, ITransactionInfo info) => command switch
        {
            DBCommand.CREATE_DATABASE => CreateDataBase(info),
            DBCommand.CREATE_LOGIN => CreateLogin(info),
            DBCommand.CREATE_PROCEDURE => CreateProcedure(info),
            DBCommand.CREATE_TABLE => CreateTable(info),
            DBCommand.CREATE_TRIGGER => CreateTrigger(info),
            DBCommand.CREATE_USER => CreateUser(info),
            DBCommand.CREATE_VIEW => CreateView(info),



            DBCommand.ALTER_DATABASE => AlterDataBase(info),
            DBCommand.ALTER_LOGIN => AlterLogin(info),
            DBCommand.ALTER_PROCEDURE => AlterProcedure(info),
            DBCommand.ALTER_TABLE => AlterTable(info),
            DBCommand.ALTER_TRIGGER => AlterTrigger(info),
            DBCommand.ALTER_USER => AlterUser(info),
            DBCommand.ALTER_VIEW => AlterView(info),



            DBCommand.DROP_DATABASE => DropDataBase(info),
            DBCommand.DROP_LOGIN => DropLogin(info),
            DBCommand.DROP_PROCEDURE => DropProcedure(info),
            DBCommand.DROP_TABLE => DropTable(info),
            DBCommand.DROP_TRIGGER => DropTrigger(info),
            DBCommand.DROP_USER => DropUser(info),
            DBCommand.DROP_VIEW => DropView(info),

            _ => new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.UNKNOWN_COMMAND])
        };



        #region Create Methods
        private ITransactionMethodResult CreateDataBase(ITransactionInfo info)
        {
             
            #region RuleCheck
            ITransactionMethodResult res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_EXIST ,info);
            if (res.IsSuccess) return res;
            #endregion



            System.String rootDBway = 
                DBMSIO.DBMSConfig.GetDataBaseRootWay
                (info.UserName, info.Parameters);


            System.String viewDBWay =
                rootDBway
            + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
            + DBMSIO.DBMSConfig.DataBaseViewDir;


            System.String usersDBWay = rootDBway
            + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
            + DBMSIO.DBMSConfig.DataBasesUsersDir;


            System.String tableDBWay = rootDBway
            + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
            + DBMSIO.DBMSConfig.DataBaseTableDir;


            System.String procDBway = rootDBway
            + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
            + DBMSIO.DBMSConfig.DataBasesProcedureDir;


            System.String triggerDBWay =
                rootDBway
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.TriggersBaseDir;


            Directory.CreateDirectory(rootDBway);
            Directory.CreateDirectory(procDBway);
            Directory.CreateDirectory(tableDBWay);
            Directory.CreateDirectory(usersDBWay);
            Directory.CreateDirectory(viewDBWay);
            Directory.CreateDirectory(triggerDBWay);


            System.String rootMetaFile = rootDBway
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.FullMetaFileName;

            System.String usersMetaFile = usersDBWay
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.FullMetaFileName;

            System.String viewMetaFile = viewDBWay
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.FullMetaFileName;

            System.String procMetaFile = procDBway
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.FullMetaFileName;

            System.String tableMetaFile = tableDBWay
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.FullMetaFileName;

            System.String triggerMetaFile = triggerDBWay
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.FullMetaFileName;


            System.String viewDir
                = DBMSIO.DBMSConfig.DataBaseViewDir.Replace("\\", "").Replace("/", "").Trim(); 
            System.String userDir 
                = DBMSIO.DBMSConfig.DataBasesUsersDir.Replace("\\", "").Replace("/", "").Trim();
            System.String procDir 
                = DBMSIO.DBMSConfig.DataBasesProcedureDir.Replace("\\", "").Replace("/", "").Trim();
            System.String triggerDir 
                = DBMSIO.DBMSConfig.TriggersBaseDir.Replace("\\", "").Replace("/", "").Trim();
            System.String tableDir 
                = DBMSIO.DBMSConfig.DataBaseTableDir.Replace("\\", "").Replace("/", "").Trim();

            DataBaseMetaInfo dbinfo = new DataBaseMetaInfo();
            dbinfo.NodeName = info.Parameters;
            dbinfo.Type = NodeType.DATABASE;
            dbinfo.Owner = info.UserName;
            dbinfo.Parent = info.Parameters;
            dbinfo.NodeAccessPassword = null;
            dbinfo.ChildrenList 
                = viewDir
                + DBMSIO.DBMSConfig.StandartSeparator
                + userDir
                + DBMSIO.DBMSConfig.StandartSeparator
                + procDir
                + DBMSIO.DBMSConfig.StandartSeparator
                + triggerDir
                + DBMSIO.DBMSConfig.StandartSeparator
                + tableDir;

            this.CreateFileWithMetaFile<IMetaInfo>(rootMetaFile, dbinfo);


            dbinfo
                  = new DataBaseMetaInfo();
            dbinfo.NodeName = viewDir;
            dbinfo.Type = NodeType.SYSTEM_DIRECTORY;
            dbinfo.Owner = info.UserName;
            dbinfo.Parent = info.Parameters;
            dbinfo.NodeAccessPassword = null;
            dbinfo.ChildrenList = System.String.Empty;

            this.CreateFileWithMetaFile<IMetaInfo>(viewMetaFile, dbinfo);

     


            dbinfo = new DataBaseMetaInfo();
            dbinfo.NodeName = userDir;
            dbinfo.Type = NodeType.SYSTEM_DIRECTORY;
            dbinfo.Owner = info.UserName;
            dbinfo.Parent = info.Parameters;
            dbinfo.NodeAccessPassword = null;
            dbinfo.ChildrenList = DBMSIO.DBMSConfig.SimpleSysAdminName
                + DBMSIO.DBMSConfig.StandartSeparator
                + DBMSIO.DBMSConfig.SimpleSecureAdmin
                + DBMSIO.DBMSConfig.StandartSeparator
                + DBMSIO.DBMSConfig.SimpleDBCreator;


            this.CreateFileWithMetaFile<IMetaInfo>(usersMetaFile, dbinfo);


            dbinfo = new DataBaseMetaInfo();
            dbinfo.NodeName = procDir;
            dbinfo.Type = NodeType.SYSTEM_DIRECTORY;
            dbinfo.Owner = info.UserName;
            dbinfo.Parent = info.Parameters;
            dbinfo.NodeAccessPassword = null;
            dbinfo.ChildrenList = System.String.Empty;

            this.CreateFileWithMetaFile<IMetaInfo>(procMetaFile, dbinfo);


            dbinfo = new DataBaseMetaInfo();
            dbinfo.NodeName = tableDir;
            dbinfo.Type = NodeType.SYSTEM_DIRECTORY;
            dbinfo.Owner = info.UserName;
            dbinfo.Parent = info.Parameters;
            dbinfo.NodeAccessPassword = null;
            dbinfo.ChildrenList = System.String.Empty;

            this.CreateFileWithMetaFile<IMetaInfo>(tableMetaFile, dbinfo);


            dbinfo = new DataBaseMetaInfo();
            dbinfo.NodeName = triggerDir;
            dbinfo.Type = NodeType.SYSTEM_DIRECTORY;
            dbinfo.Owner = info.UserName;
            dbinfo.Parent = info.Parameters;
            dbinfo.NodeAccessPassword = null;
            dbinfo.ChildrenList = System.String.Empty;

            this.CreateFileWithMetaFile<IMetaInfo>(triggerMetaFile, dbinfo);


            IUserToken sysAdmin = new SystemAdministratorUser(info.Parameters);
            IUserToken secAdmin = new SecurityAdministratorUser(info.Parameters);
            IUserToken dbCreator = new DataBaseCreatorUser(info.Parameters);

            Directory.CreateDirectory(usersDBWay + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + DBMSIO.DBMSConfig.SimpleDBCreator);
            Directory.CreateDirectory(usersDBWay + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + DBMSIO.DBMSConfig.SimpleSecureAdmin);
            Directory.CreateDirectory(usersDBWay + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + DBMSIO.DBMSConfig.SimpleSysAdminName);


            CreateFileWithMetaFile<IUserToken>(usersDBWay
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator 
                + DBMSIO.DBMSConfig.SimpleDBCreator 
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator 
                + DBMSIO.DBMSConfig.FullMetaFileName, dbCreator);


            CreateFileWithMetaFile<IUserToken>(usersDBWay
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.SimpleSecureAdmin
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.FullMetaFileName, secAdmin);

            CreateFileWithMetaFile<IUserToken>(usersDBWay
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.SimpleSysAdminName
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.FullMetaFileName, sysAdmin);


            DBMSIO.AppendDataBase(info.Parameters);


            #region Append Right Shemas to Login
            JSON json = new JSON();
            json.AppedSchemaToLogin(DBMSIO.DBMSConfig.SimpleSysAdminName, info.Parameters, NodeType.DATABASE);
            json.AppedSchemaToLogin(DBMSIO.DBMSConfig.SimpleSecureAdmin, info.Parameters, NodeType.DATABASE);
            json.AppedSchemaToLogin(DBMSIO.DBMSConfig.SimpleDBCreator, info.Parameters, NodeType.DATABASE);
            json.AppedSchemaToLogin(DBMSIO.GetCurrentLogin().NodeName, info.Parameters, NodeType.DATABASE);

            DBMSIO.GetCurrentLogin().LoginRights.Add(info.Parameters, NodeType.DATABASE);
            DBMSIO.ReadUserByLogin(DBMSIO.GetCurrentLogin());
            #endregion

            return new JsonMethodResult
                (true, "DataBase with name " + info.Parameters + " Created");
        }
        
        private ITransactionMethodResult CreateLogin(ITransactionInfo info)
        {
            #region RuleCheck
            ITransactionMethodResult res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.LOGIN_EXIST, info);
            if (res.IsSuccess) return res;
            #endregion


            System.String path =
                DBMSIO.DBMSConfig.FullPathToLoginRoot
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + info.Parameters;



            ILogin login = info.AddedInfo as ILogin;

            if (DBMSIO.IsLoginExist(login.NodeName))
                return new JsonMethodResult(false, "LOGIN ALREADY EXIST");


            Directory.CreateDirectory(path);

            this.CreateFileWithMetaFile<ILogin>(path
                    + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                    + DBMSIO.DBMSConfig.FullMetaFileName,
                    login
                    );

            
            
            DBMSIO.AppendLogin(info.Parameters);



            JSON json = new JSON();

            //Current
            IUserToken token;
            DBMSIO.GetUserToken(DBMSIO.GetCurrentDataBase().GetDataBaseName(), out token);

            #region Append Right Shemas to Login
            json.AppendRightToUser(ref token, PQLGrammar.DROP_COMMAND, info.Parameters, NodeType.LOGIN, true);
            json.AppendRightToUser(ref token, PQLGrammar.GRANT_COMMAND, info.Parameters, NodeType.LOGIN, true);
            json.AppendRightToUser(ref token, PQLGrammar.DENY_COMMAND, info.Parameters, NodeType.LOGIN, true);
            json.AppendRightToUser(ref token, PQLGrammar.REVOKE_COMMAND, info.Parameters, NodeType.LOGIN, true);
       
            json.AppendRightToUser(ref token, PQLGrammar.CHANGE_PASSWORD, info.Parameters, NodeType.LOGIN, true);
            #endregion

            return new JsonMethodResult(true, "Login With name " + info.Parameters + " Created");
        }

        private ITransactionMethodResult CreateProcedure(ITransactionInfo info)
        {

            #region RuleCheck
            ITransactionMethodResult res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_SETTED, info);
            if (!res.IsSuccess) return res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.PROCEDURE_EXIST, info);
            if (res.IsSuccess) return res;
            #endregion


            StoredProcedure sp = info.AddedInfo as StoredProcedure;
            

            this.CreateFileWithMetaFile<StoredProcedure>
                (DBMSIO.DBMSConfig.GetDataBaseRootWay
                (info.UserName, DBMSIO.GetCurrentDataBase().GetDataBaseName())
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.DataBasesProcedureDir
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + info.Parameters
                + DBMSIO.DBMSConfig.ILExtension,
                sp);



            DBMSIO.GetCurrentDataBase().AppendNewProcedure(info.Parameters);


            JSON json = new JSON();

            #region Append Right Shemas to Login
            //Current
            IUserToken token;
            DBMSIO.GetUserToken(DBMSIO.GetCurrentDataBase().GetDataBaseName(), out token);
            json.AppendRightToUser(ref token, PQLGrammar.EXECUTE_COMMAND, info.Parameters, NodeType.PROCEDURE, true);
            json.AppendRightToUser(ref token, PQLGrammar.DROP_COMMAND, info.Parameters, NodeType.PROCEDURE, true);
            #endregion

            return new JsonMethodResult(true, "Procedure with name" + info.Parameters + " Created");
        }
        
        private ITransactionMethodResult CreateTable(ITransactionInfo info)
        {
            #region RuleCheck
            ITransactionMethodResult res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_SETTED, info);
            if (!res.IsSuccess) return res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.VIEW_EXIST, info);
            if (res.IsSuccess) return res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.TABLE_EXIST, info);
            if (res.IsSuccess) return res;
            #endregion


            System.String wayToDir =
                DBMSIO.GetCurrentDataBase().GetWayToDataBaseTableRoot()
             + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
             + info.Parameters;
            
            Directory.CreateDirectory(wayToDir);

            this.CreateFileWithMetaFile<ITableInfo>(wayToDir
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.FullMetaFileName, 
                info.AddedInfo as ITableInfo
                );

            
            DBMSIO.GetCurrentDataBase().AppendNewTable(info.Parameters);
            DBMSIO.GetCurrentDataBase().AppendTableMetaInfo(info.AddedInfo as ITableInfo);

            JSON json = new JSON();

            //Current
            IUserToken token;
            DBMSIO.GetUserToken(DBMSIO.GetCurrentDataBase().GetDataBaseName(), out token);

            #region Append Right Shemas to Login
            json.AppendRightToUser(ref token, PQLGrammar.SELECT_COMMAND, info.Parameters, NodeType.TABLE, true);
            json.AppendRightToUser(ref token, PQLGrammar.DROP_COMMAND, info.Parameters, NodeType.TABLE, true);
            json.AppendRightToUser(ref token, PQLGrammar.DELETE_COMMAND, info.Parameters, NodeType.TABLE, true);
            json.AppendRightToUser(ref token, PQLGrammar.INSERT_COMMAND, info.Parameters, NodeType.TABLE, true);
            json.AppendRightToUser(ref token, PQLGrammar.UPDATE_COMMAND, info.Parameters, NodeType.TABLE, true);
            #endregion

            return new JsonMethodResult(true, "Table with name " + info.Parameters + " Created");
        }

        private ITransactionMethodResult CreateUser(ITransactionInfo info)
        {


            IUserToken user = info.AddedInfo as IUserToken;



            #region RuleCheck
            ITransactionMethodResult res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_SETTED, info);
            if (!res.IsSuccess) return res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.USER_EXIST, info);
            if (res.IsSuccess) return res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.LOGIN_EXIST, new JsonTransactionInfo() {Parameters = (info.AddedInfo as IUserToken).Owner });
            if (!res.IsSuccess) return res;
            if (DBMSIO.GetCurrentDataBase().IsUserWithLoginExist(user.Owner))
                return new JsonMethodResult(false, $"USER WITH LOGIN {user.Owner} ALREADY EXIST");
            #endregion





            System.String wayToDir =
    DBMSIO.GetCurrentDataBase().GetWayToDataBaseUserRoot()
    + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
    + info.Parameters;



            Directory.CreateDirectory(wayToDir);

            
            this.CreateFileWithMetaFile<IUserToken>(wayToDir
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.FullMetaFileName,
                user
                );


            DBMSIO.GetCurrentDataBase().AppendNewUser(info.Parameters);

            JSON json = new JSON();

            //Current
            IUserToken token;
            DBMSIO.GetUserToken(DBMSIO.GetCurrentDataBase().GetDataBaseName(), out token);

            #region Append Right Shemas to Login
            json.AppendRightToUser(ref token, PQLGrammar.DROP_COMMAND, info.Parameters, NodeType.USER, true);
            json.AppendRightToUser(ref token, PQLGrammar.GRANT_COMMAND, info.Parameters, NodeType.USER, true);
            json.AppendRightToUser(ref token, PQLGrammar.DENY_COMMAND, info.Parameters, NodeType.USER, true);
            json.AppendRightToUser(ref token, PQLGrammar.REVOKE_COMMAND, info.Parameters, NodeType.USER, true);
            json.AppendRightToUser(ref token, PQLGrammar.CHANGE_PASSWORD, info.Parameters, NodeType.USER, true);
            #endregion


            return new JsonMethodResult(true, "User Wit name " + info.Parameters + " Created in DataBase " + DBMSIO.GetCurrentDataBase().GetDataBaseName());
        }

        private ITransactionMethodResult CreateTrigger(ITransactionInfo info)
        {
            #region RuleCheck
            ITransactionMethodResult res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_SETTED, info);
            if (!res.IsSuccess) return res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.TRIGGER_EXIST, info);
            if (res.IsSuccess) return res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.TABLE_EXIST, new JsonMethodInfo() {Parameters = info.DataBaseName});
            if (!res.IsSuccess) return res;
            #endregion


            Trigger trigger = info.AddedInfo as Trigger;


            System.String wayToDir =
                DBMSIO.GetCurrentDataBase().GetWayToDataBaseTriggerRoot()
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + info.Parameters;


            NodeType trigType = NodeType.TRIGGER;
            switch(trigger.TriggerType)
            {
                case TriggerType.ON_DELETE: trigType = NodeType.DELETE_TRIGGER; break;
                case TriggerType.ON_INSERT: trigType = NodeType.INSERT_TRIGGER; break;
                case TriggerType.ON_UPDATE: trigType = NodeType.UPDATE_TRIGGER; break;
            }


            IMetaInfo table = new TableMetaInfo();
            table.ChildrenList = info.Parameters;
            table.NodeAccessPassword = null;
            table.Type = trigType;
            table.Parent = DBMSIO.DBMSConfig.TriggersBaseDir.Replace("\\","").Replace("//","").Trim();
            table.Owner = info.DataBaseName;


            Directory.CreateDirectory(wayToDir);
            this.CreateFileWithMetaFile<IMetaInfo>(wayToDir
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.FullMetaFileName,
                table
                );


            CreateFileWithMetaFile<Trigger>(wayToDir
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + info.Parameters
                + DBMSIO.DBMSConfig.ILExtension,
                trigger
                );



            DBMSIO.GetCurrentDataBase().AppendNewTrigger(info.Parameters);

            JSON json = new JSON();

            //Current
            IUserToken token;
            DBMSIO.GetUserToken(DBMSIO.GetCurrentDataBase().GetDataBaseName(), out token);

            #region Append Right Shemas to Login
            json.AppendRightToUser(ref token, PQLGrammar.DROP_COMMAND, info.Parameters, NodeType.TRIGGER, true);
            #endregion

            return new JsonMethodResult(true, "Trigger with name " + info.Parameters + " Created");
        }

        private ITransactionMethodResult CreateView(ITransactionInfo info)
        {


            #region RuleCheck
            ITransactionMethodResult res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_SETTED, info);
            if (!res.IsSuccess) return res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.TABLE_EXIST, info);
            if (res.IsSuccess) return res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.VIEW_EXIST, info);
            if (res.IsSuccess) return res;
            #endregion


            System.String wayToDir =
           DBMSIO.DBMSConfig.GetDataBaseRootWay(info.UserName, DBMSIO.GetCurrentDataBase().GetDataBaseName())
             + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
             + DBMSIO.DBMSConfig.DataBaseViewDir;

         
            IView view = info.AddedInfo as View;

    

                this.CreateFileWithMetaFile<IView> (wayToDir
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + info.Parameters
                + DBMSIO.DBMSConfig.ILExtension,
                view
                );

            DBMSIO.GetCurrentDataBase().AppendNewView(info.Parameters);


            JSON json = new JSON();

            //Current
            IUserToken token;
            DBMSIO.GetUserToken(DBMSIO.GetCurrentDataBase().GetDataBaseName(), out token);

            #region Append Right Shemas to Login
            json.AppendRightToUser(ref token, PQLGrammar.SELECT_COMMAND, info.Parameters, NodeType.VIEW, true);
            json.AppendRightToUser(ref token, PQLGrammar.DROP_COMMAND, info.Parameters, NodeType.VIEW, true);
            #endregion

            return new JsonMethodResult(true, "View with name " + info.Parameters + " Created");
        }

        #endregion


        #region Alter Methods
        private ITransactionMethodResult AlterDataBase(ITransactionInfo info)
        {

            #region RuleCheck
            ITransactionMethodResult res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_EXIST, info);
            if (!res.IsSuccess) return res;
            #endregion




            Directory.Move(
                DBMSIO.DBMSConfig.GetDataBaseRootWay(
                    DBMSIO.GetCurrentDataBase().GetDataBaseName(),
                    DBMSIO.GetCurrentLogin().NodeName),

                DBMSIO.DBMSConfig.BaseDir
                +DBMSIO.DBMSConfig.DataBasesStandartDir
                +DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                +info.Parameters
                );

            Directory.Delete(
                DBMSIO.DBMSConfig.GetDataBaseRootWay(
                        DBMSIO.GetCurrentDataBase().GetDataBaseName(),
                        DBMSIO.GetCurrentLogin().NodeName
                    )
                );


            return new JsonMethodResult(true, "Current DataBase Was renamed by  "+ info.Parameters  + "  By user " + info.UserName);
        }  

        private ITransactionMethodResult AlterTable(ITransactionInfo info)
        {


            #region RuleCheck
            ITransactionMethodResult res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_SETTED, info);
            if (!res.IsSuccess) return res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.TABLE_EXIST, info);
            if (!res.IsSuccess) return res;
            #endregion




            this.AlterInfoWithTruncate<String>(DBMSIO.DBMSConfig.GetDataBaseRootWay(
                        DBMSIO.GetCurrentLogin().NodeName
                       , DBMSIO.GetCurrentDataBase().GetDataBaseName()
                      ) + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                       + DBMSIO.DBMSConfig.DataBaseTableDir
                       + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                       + info.Parameters
                       + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                       + DBMSIO.DBMSConfig.FullMetaFileName,
                       info.AddedInfo as String);

            
            return new JsonMethodResult(true, "Table With name " + info.Parameters + " altered in DataBase " + info.DataBaseName + " by User " + info.UserName);
        } 

        private ITransactionMethodResult AlterUser(ITransactionInfo info)
        {


            #region RuleCheck
            ITransactionMethodResult res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_SETTED, info);
            if (!res.IsSuccess) return res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.USER_EXIST, info);
            if (!res.IsSuccess) return res;
            #endregion




            this.AlterInfoWithTruncate<String>(DBMSIO.DBMSConfig.GetDataBaseRootWay(
                        DBMSIO.GetCurrentLogin().NodeName
                       , DBMSIO.GetCurrentDataBase().GetDataBaseName()
                      ) + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                       + DBMSIO.DBMSConfig.DataBasesUsersDir
                       + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                       + info.Parameters
                       + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                       + DBMSIO.DBMSConfig.FullMetaFileName,
                       info.AddedInfo as String);

            return new JsonMethodResult(true, "User Wit name " + info.Parameters + " altered in DataBase " + info.DataBaseName + "  By User " + info.UserName);
        }

        private ITransactionMethodResult AlterView(ITransactionInfo info)
        {

            #region RuleCheck
            ITransactionMethodResult res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_SETTED, info);
            if (!res.IsSuccess) return res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.VIEW_EXIST, info);
            if (!res.IsSuccess) return res;
            #endregion


          System.String wayToDir =
           DBMSIO.DBMSConfig.GetDataBaseRootWay(info.UserName, DBMSIO.GetCurrentDataBase().GetDataBaseName())
             + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
             + DBMSIO.DBMSConfig.DataBaseViewDir;

         
            IView view = info.AddedInfo as View;

            this.AlterInfoWithTruncate<IView> (wayToDir
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + info.Parameters
                + DBMSIO.DBMSConfig.ILExtension,
                view
                );


            return new JsonMethodResult(true, "View With name " + info.Parameters + " altered in DataBase " + info.DataBaseName + " By User "+ info.UserName);
        }   

        private ITransactionMethodResult AlterTrigger(ITransactionInfo info)
        {

            #region RuleCheck
            ITransactionMethodResult res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_SETTED, info);
            if (!res.IsSuccess) return res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.TRIGGER_EXIST, info);
            if (!res.IsSuccess) return res;
            #endregion


            Trigger trigger = info.AddedInfo as Trigger;


            System.String wayToDir =
       DBMSIO.GetCurrentDataBase().GetWayToDataBaseTriggerRoot()
       + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
       + info.Parameters;



            this.AlterInfoWithTruncate<Trigger>(wayToDir
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + info.Parameters
                + DBMSIO.DBMSConfig.ILExtension,
                trigger);

            return new JsonMethodResult(true, "Trigger Wit name " + info.Parameters + " altered in DataBase " + info.DataBaseName);
        }

        private ITransactionMethodResult AlterProcedure(ITransactionInfo info)
        {
            #region RuleCheck
            ITransactionMethodResult res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_SETTED, info);
            if (!res.IsSuccess) return res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.PROCEDURE_EXIST, info);
            if (!res.IsSuccess) return res;
            #endregion


            StoredProcedure sp = info.AddedInfo as StoredProcedure;


            this.AlterInfoWithTruncate<StoredProcedure>(DBMSIO.DBMSConfig.GetDataBaseRootWay
                (info.UserName, DBMSIO.GetCurrentDataBase().GetDataBaseName())
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.DataBasesProcedureDir
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + info.Parameters
                + DBMSIO.DBMSConfig.ILExtension,
                sp);


            return new JsonMethodResult(true, "Procedure Wit name " + info.Parameters + " altered in DataBase " + info.DataBaseName);
        }

        private ITransactionMethodResult AlterLogin(ITransactionInfo info)
        {
            #region RuleCkehc
            ITransactionMethodResult res;
            res = JSON.RulesManager.Invoke((Byte)ServerRulesID.LOGIN_EXIST, info);
            if (!res.IsSuccess) return res;
            #endregion

            if (!DBMSIO.IsLoginExist(info.Parameters))
                return new JsonMethodResult(false, Core.ErrorControlSystem.ErrorMessagesCore.MessageDictionary[(Byte)Core.ErrorControlSystem.ErrorCodes.LOGIN_NOT_FOUND]);

            this.AlterInfoWithTruncate<String>(DBMSIO.DBMSConfig.FullPathToLoginRoot
                        + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                        + info.Parameters
                        + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                        + DBMSIO.DBMSConfig.FullMetaFileName,
                        info.AddedInfo as String);

            return new JsonMethodResult(true, "User With name " + info.Parameters + " altered in DataBase " + info.DataBaseName);

        }
        #endregion


        #region Drop Methods
        private ITransactionMethodResult DropDataBase(ITransactionInfo info)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                ITransactionMethodResult res;
                res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_EXIST, info);
                if (!res.IsSuccess) return res;
                #endregion
            }

            Directory.Delete
                (DBMSIO.DBMSConfig.GetDataBaseRootWay(info.UserName, info.Parameters), true);

            DBMSIO.TruncateDataBase(info.Parameters);


            JSON json = new JSON();

            #region Remove Rights Schemas from login
            json.RemoveSchemaInLogin(DBMSIO.DBMSConfig.SimpleSysAdminName, info.Parameters, NodeType.DATABASE);
            json.RemoveSchemaInLogin(DBMSIO.DBMSConfig.SimpleSecureAdmin, info.Parameters, NodeType.DATABASE);
            json.RemoveSchemaInLogin(DBMSIO.DBMSConfig.SimpleDBCreator, info.Parameters, NodeType.DATABASE);
            json.RemoveSchemaInLogin(DBMSIO.GetCurrentLogin().NodeName, info.Parameters, NodeType.DATABASE);

            json.RemoveSchemaInAllLogin(info.Parameters, NodeType.DATABASE);
            #endregion

            return new JsonMethodResult(true, "DataBase with name " + info.Parameters + " Deleted");


        }

        private ITransactionMethodResult DropTable(ITransactionInfo info)
        {

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                ITransactionMethodResult res;
                res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_SETTED, info);
                if (!res.IsSuccess) return res;
                res = JSON.RulesManager.Invoke((Byte)ServerRulesID.TABLE_EXIST, info);
                if (!res.IsSuccess) return res;
                #endregion
            }

            System.String pathToRoot = 
                DBMSIO.DBMSConfig.GetDataBaseRootWay(info.UserName, DBMSIO.GetCurrentDataBase().GetDataBaseName())
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.DataBaseTableDir
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + info.Parameters;

            if (DBMSIO.GetCurrentDataBase().IsExistForeignConstraintToTable(info.Parameters))
                return new JsonMethodResult(false, $"Error table has FOREIGN_KEY_REFERENCES {info.Parameters} and can no be delete");

            Directory.Delete(pathToRoot, true);
            DBMSIO.GetCurrentDataBase().TruncateTable(info.Parameters);
            DBMSIO.GetCurrentDataBase().FlushTableMeta(info.Parameters);

            JSON json = new JSON();

            //Current
            IUserToken token;
            DBMSIO.GetUserToken(DBMSIO.GetCurrentDataBase().GetDataBaseName(), out token);

            #region Remove Rights Schemas from login
            json.TruncateRightFromUser(ref token, PQLGrammar.SELECT_COMMAND, info.Parameters, NodeType.TABLE);
            json.TruncateRightFromUser(ref token, PQLGrammar.DROP_COMMAND, info.Parameters, NodeType.TABLE);
            json.TruncateRightFromUser(ref token, PQLGrammar.DELETE_COMMAND, info.Parameters, NodeType.TABLE);
            json.TruncateRightFromUser(ref token, PQLGrammar.INSERT_COMMAND, info.Parameters, NodeType.TABLE);
            json.TruncateRightFromUser(ref token, PQLGrammar.UPDATE_COMMAND, info.Parameters, NodeType.TABLE);

            json.RemoveObjectFromAllUsers(PQLGrammar.DELETE_COMMAND, info.Parameters);
            json.RemoveObjectFromAllUsers(PQLGrammar.INSERT_COMMAND, info.Parameters);
            json.RemoveObjectFromAllUsers(PQLGrammar.SELECT_COMMAND, info.Parameters);
            json.RemoveObjectFromAllUsers(PQLGrammar.UPDATE_COMMAND, info.Parameters);
            json.RemoveObjectFromAllUsers(PQLGrammar.DROP_COMMAND, info.Parameters);

            #endregion

            return new JsonMethodResult(true, "Table with name " + info.Parameters + " in DataBase " + DBMSIO.GetCurrentDataBase().GetDataBaseName() + " Deleted");
        }

        private ITransactionMethodResult DropLogin(ITransactionInfo info)
        {

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                ITransactionMethodResult res;
                res = JSON.RulesManager.Invoke((Byte)ServerRulesID.IT_IS_CURRENT_LOGIN, info);
                if (res.IsSuccess) return res;
                res = JSON.RulesManager.Invoke((Byte)ServerRulesID.LOGIN_EXIST, info);
                if (!res.IsSuccess) return res;
                if (DBMSIO.GetCurrentLogin().NodeName == info.Parameters)
                    return new JsonMethodResult(false, "YOU CAN NOT DELETE CURRENT LOGIN");

                #endregion
            }



            if (DBMSIO.TruncateLogin(info.Parameters))
            {

                System.String path =
                    DBMSIO.DBMSConfig.FullPathToLoginRoot
                    + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                    + info.Parameters;
                
                    Directory.Delete(path, true);

                DBMSIO.TruncateLogin(info.Parameters);
                if (DBMSIO.GetCurrentDataBase() != null)
                {
                    JSON json = new JSON();
                    IUserToken token;
                    DBMSIO.GetUserToken(DBMSIO.GetCurrentDataBase()?.GetDataBaseName(), out token);

                    #region Remove Rights Schemas from login
                    json.TruncateRightFromUser(ref token, PQLGrammar.DROP_COMMAND, info.Parameters, NodeType.LOGIN);
                    json.TruncateRightFromUser(ref token, PQLGrammar.GRANT_COMMAND, info.Parameters, NodeType.LOGIN);
                    json.TruncateRightFromUser(ref token, PQLGrammar.DENY_COMMAND, info.Parameters, NodeType.LOGIN);
                    json.TruncateRightFromUser(ref token, PQLGrammar.REVOKE_COMMAND, info.Parameters, NodeType.LOGIN);

                    json.TruncateRightFromUser(ref token, Rbac.CHANGE_PASSWORD, info.Parameters, NodeType.LOGIN);

                    json.RemoveObjectFromAllUsers(PQLGrammar.CHANGE_PASSWORD, info.Parameters);
                    json.RemoveObjectFromAllUsers(PQLGrammar.GRANT_COMMAND, info.Parameters);
                    json.RemoveObjectFromAllUsers(PQLGrammar.DENY_COMMAND, info.Parameters);
                    json.RemoveObjectFromAllUsers(PQLGrammar.REVOKE_COMMAND, info.Parameters);
                    #endregion
                }

                return new JsonMethodResult(true, "Login with name " + info.Parameters + "  at Server [Pacific v 0.1]  Deleted by User " + info.UserName);
            } else
            {
                return new JsonMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.FAILURE_EXECUTE]);
            }

        }

        private ITransactionMethodResult DropProcedure(ITransactionInfo info)
        {

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                ITransactionMethodResult res;
                res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_SETTED, info);
                if (!res.IsSuccess) return res;
                res = JSON.RulesManager.Invoke((Byte)ServerRulesID.PROCEDURE_EXIST, info);
                if (!res.IsSuccess) return res;
                #endregion
            }

            System.String pathToRoot = DBMSIO.DBMSConfig.GetDataBaseRootWay(info.UserName, DBMSIO.GetCurrentDataBase().GetDataBaseName())
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.DataBasesProcedureDir
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + info.Parameters
                + DBMSIO.DBMSConfig.ILExtension;

            File.Delete(pathToRoot);
            DBMSIO.GetCurrentDataBase().TruncateProcedure(info.Parameters);


            JSON json = new JSON();

            //Current
            IUserToken token;
            DBMSIO.GetUserToken(DBMSIO.GetCurrentDataBase().GetDataBaseName(), out token);

            #region Remove Rights Schemas from login
            json.TruncateRightFromUser(ref token, PQLGrammar.EXECUTE_COMMAND, info.Parameters, NodeType.PROCEDURE);
            json.TruncateRightFromUser(ref token, PQLGrammar.DROP_COMMAND, info.Parameters, NodeType.PROCEDURE);
            json.RemoveObjectFromAllUsers(PQLGrammar.EXECUTE_COMMAND, info.Parameters);

            #endregion


            return new JsonMethodResult(true, "Procedure with name " + info.Parameters + " in DataBase " + DBMSIO.GetCurrentDataBase().GetDataBaseName() + " Deleted");
        }

        private ITransactionMethodResult DropTrigger(ITransactionInfo info)
        {

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                ITransactionMethodResult res;
                res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_SETTED, info);
                if (!res.IsSuccess) return res;
                res = JSON.RulesManager.Invoke((Byte)ServerRulesID.TRIGGER_EXIST, info);
                if (!res.IsSuccess) return res;
                #endregion
            }
        

            
            DBMSIO.GetCurrentDataBase().TruncateTrigger(info.Parameters);

            Directory.Delete(DBMSIO.GetCurrentDataBase().GetWayToDataBaseTriggerRoot() + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + info.Parameters, true);

            JSON json = new JSON();

            //Current
            IUserToken token;
            DBMSIO.GetUserToken(DBMSIO.GetCurrentDataBase().GetDataBaseName(), out token);

            #region Remove Rights Schemas from login
            json.TruncateRightFromUser(ref token, PQLGrammar.DROP_COMMAND, info.Parameters, NodeType.TRIGGER);

     
            #endregion

            return new JsonMethodResult(true, "Trigger with name " + info.Parameters + " in DataBase " + DBMSIO.GetCurrentDataBase().GetDataBaseName() + " Deleted");
        }

        private ITransactionMethodResult DropUser(ITransactionInfo info)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                ITransactionMethodResult res;
                res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_SETTED, info);
                if (!res.IsSuccess) return res;
                res = JSON.RulesManager.Invoke((Byte)ServerRulesID.USER_EXIST, info);
                if (!res.IsSuccess) return res;
                if (DBMSIO.GetCurrentDataBase().GetUserByLoginName(DBMSIO.GetCurrentLogin().NodeName).NodeName == info.Parameters)
                    return new JsonMethodResult(false, "YOU CAN NOT DELETE USER WICH CURRENT USE");
                #endregion
            }

            System.String pathToRoot = DBMSIO.DBMSConfig.GetDataBaseRootWay(info.UserName, DBMSIO.GetCurrentDataBase().GetDataBaseName())
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.DataBasesUsersDir
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + info.Parameters;

            Directory.Delete(pathToRoot, true);
            DBMSIO.GetCurrentDataBase().TruncateUser(info.Parameters);

            JSON json = new JSON();

            //Current
            IUserToken token;
            DBMSIO.GetUserToken(DBMSIO.GetCurrentDataBase().GetDataBaseName(), out token);

            #region Remove Rights Schemas from login
            json.TruncateRightFromUser(ref token, PQLGrammar.DROP_COMMAND, info.Parameters, NodeType.USER);
            json.TruncateRightFromUser(ref token, PQLGrammar.GRANT_COMMAND, info.Parameters, NodeType.USER);
            json.TruncateRightFromUser(ref token, PQLGrammar.DENY_COMMAND, info.Parameters, NodeType.USER);
            json.TruncateRightFromUser(ref token, PQLGrammar.REVOKE_COMMAND, info.Parameters, NodeType.USER);
            json.TruncateRightFromUser(ref token, PQLGrammar.CHANGE_PASSWORD, info.Parameters, NodeType.USER);

            json.RemoveObjectFromAllUsers(PQLGrammar.GRANT_COMMAND, info.Parameters);
            json.RemoveObjectFromAllUsers(PQLGrammar.DENY_COMMAND, info.Parameters);
            json.RemoveObjectFromAllUsers(PQLGrammar.REVOKE_COMMAND, info.Parameters);
            json.RemoveObjectFromAllUsers(PQLGrammar.CHANGE_PASSWORD, info.Parameters);
            #endregion

            return new JsonMethodResult(true, "User with name " + info.Parameters + " in DataBase " + DBMSIO.GetCurrentDataBase().GetDataBaseName() + " Deleted");
        }

        private ITransactionMethodResult DropView(ITransactionInfo info)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                ITransactionMethodResult res;
                res = JSON.RulesManager.Invoke((Byte)ServerRulesID.DATABASE_SETTED, info);
                if (!res.IsSuccess) return res;
                res = JSON.RulesManager.Invoke((Byte)ServerRulesID.VIEW_EXIST, info);
                if (!res.IsSuccess) return res;
                #endregion
            }

            System.String pathToRoot = DBMSIO.DBMSConfig.GetDataBaseRootWay(info.UserName, DBMSIO.GetCurrentDataBase().GetDataBaseName())
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.DataBaseViewDir
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + info.Parameters
                + DBMSIO.DBMSConfig.ILExtension;

            File.Delete(pathToRoot);
            DBMSIO.GetCurrentDataBase().TruncateView(info.Parameters);
            JSON json = new JSON();

            //Current
            IUserToken token;
            DBMSIO.GetUserToken(DBMSIO.GetCurrentDataBase().GetDataBaseName(), out token);

            #region Remove Rights Schemas from login
            json.TruncateRightFromUser(ref token, PQLGrammar.SELECT_COMMAND, info.Parameters, NodeType.VIEW);
            json.TruncateRightFromUser(ref token, PQLGrammar.INSERT_COMMAND, info.Parameters, NodeType.VIEW);
            json.TruncateRightFromUser(ref token, PQLGrammar.UPDATE_COMMAND, info.Parameters, NodeType.VIEW);
            json.TruncateRightFromUser(ref token, PQLGrammar.DELETE_COMMAND, info.Parameters, NodeType.VIEW);

            json.RemoveObjectFromAllUsers(PQLGrammar.SELECT_COMMAND, info.Parameters);
            json.RemoveObjectFromAllUsers(PQLGrammar.UPDATE_COMMAND, info.Parameters);
            json.RemoveObjectFromAllUsers(PQLGrammar.INSERT_COMMAND, info.Parameters);
            json.RemoveObjectFromAllUsers(PQLGrammar.DELETE_COMMAND, info.Parameters);

            #endregion

            return new JsonMethodResult(true, "View with name " + info.Parameters + " in DataBase " + DBMSIO.GetCurrentDataBase().GetDataBaseName() + " Deleted");
        }



        private void RemoveRightFromAllUsers(String command, String objName, NodeType type)
        {
            JSON json = new JSON();
            json.RemoveObjectFromAllUsers(command, objName);
        }


        private void RemoveSchemasFromAllLogins(String schemasName, NodeType type)
        {
            JSON json = new JSON();
            json.RemoveSchemaInAllLogin(schemasName, type);

        }
        #endregion




        #region File System Methods
        internal void CreateFileWithMetaFile<T>(System.String path, T info)
        {
            try
            {
                using (FileStream fs = new FileStream(path, FileMode.Create))
                {
                    String serializaConfig
                        = JsonSerializer.Serialize<T>(info);
                    Byte[] byteConfig
                        = System.Text.Encoding.UTF8.GetBytes(serializaConfig);

                    fs.Write(byteConfig, 0, byteConfig.Length);
                }
            } catch (Exception ex)
            {
                if(ex.IsFileLocked())
                    throw new FileLockedError();
               else
                    throw ex;
            }
        }
        internal void AlterInfoWithTruncate<T>(System.String path, T info)
        {
            try
            {
                using (FileStream fs = new FileStream(path, FileMode.Truncate))
                {
                    System.Byte[] arr
                        = Encoding.UTF8.GetBytes(info as String);
                    fs.Write(arr, 0, arr.Length);
                }
            } catch (Exception ex)
            {
                if(ex.IsFileLocked())
                    throw new FileLockedError();
                 else
                    throw ex;
                
            }
        }
        internal void RewriteObject<T>(System.String path, T info)
        {
            try
            {
                using (FileStream fs = new FileStream(path, FileMode.Truncate))
                {
                    String str = JsonSerializer.Serialize<T>(info);
                    System.Byte[] arr
                        = Encoding.UTF8.GetBytes(str);
                    fs.Write(arr, 0, arr.Length);

                }
            } catch (Exception ex)
            {
                if(ex.IsFileLocked())
                    throw new FileLockedError();
                 else
                    throw ex;
            }
        }

        #endregion
    }
}
