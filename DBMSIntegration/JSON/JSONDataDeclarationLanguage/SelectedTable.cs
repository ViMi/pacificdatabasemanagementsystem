﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Table;
using DBMS.Core.TableCollections;
using DBMS.Core.IO;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.InfoTypes;
using DBMS.Core.MMIL;
using DBMS.Core.MMIL.ILConfiguration;
using DBMS.Integration.JSON.SQL.StorageCollection;
using DBMS.Core.Helpers;
using DBMS.Core;
using DBMS.Core.UsersFromRoles;
using DBMS.PQL.Grammar;

namespace DBMS.Integration.JSON.SQL
{
    internal sealed class SelectedTable
        : IQueryObject
    {


        #region Documentaiton
#if DocTrue
        static SelectedTable() { }
#endif
        #endregion


        #region Variable Declaration
        private MMILDataManipulationContext _MMIL { set; get; }
        private Dictionary<String, ITableCollection> _SelectedCollections { set; get; }
        private ITable _ResultationTable;
        private System.String[] _SelectedRows { set; get; }
        private List<String> _AddedData { set; get; }
        private String _AddedInfoPartTwo { set; get; }
        private ISelectStrategy _SelectStrategy { set; get; }
        private IUpdateStrategy _UpdateStrategy { set; get; }
        private IInsertStrategy _InsertStrategy { set; get; }
        private IDeleteStrategy _DeleteStrategy { set; get; }
        private IJoin _JoinStrategy { set; get; }
        private String _ExecutionContext { set; get; }

        #endregion



        public SelectedTable(MMILDataManipulationContext mmil = null, List<String> extra = null, String extra2 = null)
        {
            this._MMIL = mmil;
            _AddedData = extra;
            _AddedInfoPartTwo = extra2;
            _ExecutionContext = _MMIL._ContextName;
        }



        #region Select Options
        public IQueryObject Select(MMILDataManipulationContext mmil = null)
        {
             _MMIL = mmil ?? _MMIL;

            _SelectStrategy = new SelectT(_MMIL);
            _ResultationTable = _SelectStrategy.Select().AddedInfo as ITable;
            _SelectedCollections = _SelectStrategy.GetTableCollection().AddedInfo as Dictionary<String, ITableCollection>;
            return this;
        }

    
        
        public IQueryObject Having(MMILDataManipulationContext mmil = null)
        {
            String having = _MMIL.GetContextValue(MMILDataManipulationConfigure.HavingDeclarationCommand);

            if (having != null)
            {

                throw new NotImplementedException();
                //HavingStatement h = new HavingStatement(having, PacificDataBaseDataType.STRING, ref this._ResultationTable);
              //      this._ResultationTable = (h.Execute().AddedInfo as Table);
 
            }


            return this;
        }

        public IQueryObject GroupBy(MMILDataManipulationContext mmil = null)
        {

            String groupBy = _MMIL.GetContextValue(MMILDataManipulationConfigure.GroupByDeclarationCommand);

            if (groupBy != null)
            {
                GroupBy gb = new GroupBy(groupBy, ref this._ResultationTable);
                this._ResultationTable = gb.Group().AddedInfo as Table;
            }


            return this;
        }


        
        

        public IQueryObject Union(MMILDataManipulationContext mmil = null)
        {
            String union = _MMIL.GetContextValue(MMILDataManipulationConfigure.UnionDeclarationCommand);

            if(union != null)
            {

                if (_SelectedCollections.Count > 1)
                    throw new Exception("Error of Union. One table Must be selected in From");

                String[] names = union.Split(DBMSIO.DBMSConfig.StandartSeparator);

                foreach(String name in names)
                {
                    if (name == String.Empty)
                        continue;

                    MMILDataManipulationContext _mmil = new MMILDataManipulationContext();
                    _mmil.InitExecutionContext(PQLGrammar.SELECT_COMMAND);
                    _mmil.SetRows(PQLGrammar.ALL_ROWS_CONST);
                    _mmil.SetFrom(name);
                    _mmil.EndExecuteContext();
                    ISelectStrategy select = new SelectT(_mmil);
                    ITable selectedTable = select.Select().AddedInfo as ITable;
                    ITableInfo infoRes = _SelectedCollections.ElementAt(0).Value.GetMetaInfoTable();
                    ITableInfo infoSelected = (select.GetTableCollection().AddedInfo as Dictionary<String, ITableCollection>).ElementAt(0).Value.GetMetaInfoTable();

                    SQL.Union uni = new Union(infoRes, infoSelected);
                    _ResultationTable = uni.Unite(_ResultationTable, selectedTable);

                }
            }


            return this;
        }


        public ITransactionMethodResult GetDomainInfo()
        {
            String result = String.Empty;
            Dictionary<String, Dictionary<String, String>> tr;


                #region Rule Check
                if (_SelectedCollections != null && _SelectedCollections.ElementAt(0).Value.GetWayToStored() != VirtualTableCollection.VIRTUAL_OBJECT)
                    tr = _SelectedCollections.ElementAt(0).Value.GetMetaInfoTable().ColumnConstraint;
                else
                    return new JsonMethodResult(false, "Error of select table");
                #endregion
            

            for (System.Int32 i = 0; i < tr.Count; ++i)
            {

                result += $"{Environment.NewLine}--column  {tr.ElementAt(i).Key}{Environment.NewLine}";

                for(System.Int32 j = 0; j < tr.ElementAt(i).Value.Count; ++j)
                    result += $"Type --> {tr.ElementAt(i).Value.ElementAt(j).Key}  | Data --> {tr.ElementAt(i).Value.ElementAt(j).Value} {Environment.NewLine}";

            }

            return new JsonMethodResult(true, result);
        }



        public ITransactionMethodResult GetCollumnTypes()
        {
            String result = String.Empty;

            Dictionary<String, PacificDataBaseDataType> type = new Dictionary<string, PacificDataBaseDataType>();

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                if (_SelectedCollections != null)
                    type = _SelectedCollections.ElementAt(0).Value.GetMetaInfoTable().ColumnTypes;
                else if (_SelectedCollections == null)
                    return new JsonMethodResult(false, "Error of select table");

                #endregion
            }

            for(System.Int32 i = 0; i < type.Count; ++i)
                result += $"--column {type.ElementAt(i).Key} ___ Data Type {type.ElementAt(i).Value}  {Environment.NewLine}";


            return new JsonMethodResult(true, result);

        }

        public IQueryObject OrderBy(MMILDataManipulationContext mmil = null)
        {
            String orderBy = _MMIL.GetContextValue(MMILDataManipulationConfigure.OrderByDeclarationCommand);

            if (orderBy != null)
            {
                throw new NotImplementedException();
                //OrderByStatement ob = new OrderByStatement(orderBy, ref this._ResultationTable);
             //   this._ResultationTable = ob.Order().AddedInfo as Table;

            }


            return this;
        }



        #region Limitation 
        public IQueryObject Skip(MMILDataManipulationContext mmil = null) => SkipLimit();

        public IQueryObject Limit(MMILDataManipulationContext mmil = null) => SkipLimit();
        public IQueryObject SkipLimit(MMILDataManipulationContext mmil = null)
        {
            ITable resultationTable = new Table();
            resultationTable.TableRow = new Dictionary<string, TableRow>();


            String skip = _MMIL.GetContextValue(MMILDataManipulationConfigure.SkipDeclarationCommand);
            String limit = _MMIL.GetContextValue(MMILDataManipulationConfigure.LimitDeclarationCommand);


            Int32 skipValues = 0;
            Int32 limitValues = -1;

            Int32 count = 0;

            if (skip != null)
                skipValues = Int32.Parse(skip);

            if (limit != null)
                limitValues = Int32.Parse(limit);


            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                if (skipValues >= this._ResultationTable.TableRow.Count)
                    throw new SkipSizeError();

                if (skipValues < 0)
                    throw new SkipNegativeError();
            }


                    for (; skipValues < this._ResultationTable.TableRow.Count; ++skipValues, ++count)
                    {

                        if (limitValues >= 0 && count == limitValues)
                            break;

                        String key = this._ResultationTable.TableRow.ElementAt(skipValues).Key;
                        TableRow row = this._ResultationTable.TableRow.ElementAt(skipValues).Value;

                        resultationTable.TableRow.Add(key, row);
                    }


            this._ResultationTable = resultationTable as Table;

            return this;
        }

        public IQueryObject Top(MMILDataManipulationContext mmil = null)
        {


            ITable resultationTable = new Table();
            resultationTable.TableRow = new Dictionary<string, TableRow>();

            Int32 topInt = Int32.Parse(_MMIL.GetContextValue(MMILDataManipulationConfigure.TopDeclarationCommand));

            for (; topInt >= 0; --topInt)
            {


                String key = this._ResultationTable.TableRow.ElementAt(topInt).Key;
                TableRow row = this._ResultationTable.TableRow.ElementAt(topInt).Value;

                resultationTable.TableRow.Add(key, row);
            }

            this._ResultationTable = resultationTable as Table;

            return this;

        }

        #endregion

      
        #region Filter

        public IQueryObject Where(MMILDataManipulationContext mmil = null)
        {

            Where whereFilter
                = new Where(_MMIL.GetContextValue(MMILDataManipulationConfigure.WhereDeclarationCommand), 
                _ExecutionContext == PQLGrammar.SELECT_COMMAND ? _ResultationTable : _SelectedCollections.ElementAt(0).Value._TableCollection);


            this._ResultationTable = whereFilter.GetFilteredTable() as Table;

            return this;
        }

        public IQueryObject CrossJoin(MMILDataManipulationContext mmil = null)
        {
            String joinWith = _MMIL.GetContextValue(MMILDataManipulationConfigure.JoinDeclarationCommand);

            if (joinWith != null)
            {
                _JoinStrategy = new Join(joinWith);
                _ResultationTable = _JoinStrategy.Joined(JoinType.CROSS_JOIN, _SelectedCollections);
            }

            return this;
        }

        public IQueryObject Distinct()
        {
            var table = this._ResultationTable.TableRow.Distinct();
            ITable tmpTable = new Table();
            tmpTable.TableRow = new Dictionary<System.String, TableRow>();
            foreach (var tr in table)
                tmpTable.TableRow.Add(tr.Key, tr.Value);
            


            this._ResultationTable = tmpTable;

            return this;
        }


        #endregion


        #region Agregate
        public ITransactionMethodResult Count(MMILDataManipulationContext mmil = null)
            => new Count().Execute(ref _ResultationTable, PacificDataBaseDataType.STRING, _MMIL.GetContextValue(MMILDataManipulationConfigure.CountDeclarationCommand));
        public ITransactionMethodResult Max(MMILDataManipulationContext mmil = null)
            => new Max().Execute(ref _ResultationTable, PacificDataBaseDataType.NUMBER, _MMIL.GetContextValue(MMILDataManipulationConfigure.MaxDeclarationCommand));
        public ITransactionMethodResult Min(MMILDataManipulationContext mmil = null)
            => new Min().Execute(ref _ResultationTable, PacificDataBaseDataType.STRING, _MMIL.GetContextValue(MMILDataManipulationConfigure.MinDeclarationCommand));
        public ITransactionMethodResult Avg(MMILDataManipulationContext mmil = null)
            => new Avg().Execute(ref _ResultationTable, PacificDataBaseDataType.STRING, _MMIL.GetContextValue(MMILDataManipulationConfigure.AvgDeclarationCommand));
        public ITransactionMethodResult Sum(MMILDataManipulationContext mmil = null)
            => new Sum().Execute(ref _ResultationTable, PacificDataBaseDataType.NUMBER, _MMIL.GetContextValue(MMILDataManipulationConfigure.SumDeclarationCommand));
        #endregion


        #endregion


        #region DML Operation (Delete / Update / Insert )
        public ITransactionMethodResult Delete()
        {

            #region Rule Check

             if (_ResultationTable != null && _SelectedCollections.ElementAt(0).Value.GetWayToStored() == VirtualTableCollection.VIRTUAL_OBJECT)
                if (!IsViewContainsOnlyOne(_ResultationTable))
                    throw new Exception("Error of Delete");
                else
                {
                    MMILDataManipulationContext mmil = new MMILDataManipulationContext();
                    mmil.InitExecutionContext(PQLGrammar.DELETE_COMMAND);
                    mmil.SetTable(GetTableNameView(_ResultationTable).Trim());
                    mmil.SetTo(GetTableNameView(_ResultationTable).Trim());
                    mmil.SetFrom(GetTableNameView(_ResultationTable).Trim());
                    mmil.SetRows(_MMIL.GetContextValue(MMILDataManipulationConfigure.RowsDeclarationCommand));
                    mmil.SetWhere(_MMIL.GetContextValue(MMILDataManipulationConfigure.WhereDeclarationCommand));
                    mmil.EndExecuteContext();
                    Select(mmil);
                    Where();
                }
            #endregion


            _DeleteStrategy = new DeleteT(ref _ResultationTable, _SelectedCollections.ElementAt(0).Value._TableCollection, _SelectedCollections.ElementAt(0).Value.GetMetaInfoTable().TableName, _MMIL);
           ITransactionMethodResult  tres = _DeleteStrategy.Delete();

            _SelectedCollections.ElementAt(0).Value._TableInfo = tres.AddedInfo as ITableInfo;

            return tres;
        }

        public ITransactionMethodResult Update()
        {

            #region Rule Check
            if (_MMIL.GetContextValue(MMILDataManipulationConfigure.FromDeclarationCommad).Split(DBMSIO.DBMSConfig.StandartSeparator).Length != 1)
                throw new MultiplyUpdateTableError();

            if (_ResultationTable != null && _SelectedCollections.ElementAt(0).Value.GetWayToStored() == VirtualTableCollection.VIRTUAL_OBJECT)
                if (!IsViewContainsOnlyOne(_ResultationTable))
                    throw new Exception("Error of Update");
                else
                {
                    MMILDataManipulationContext mmil = new MMILDataManipulationContext();
                    mmil.InitExecutionContext(PQLGrammar.UPDATE_COMMAND);
                    mmil.SetTable(GetTableNameView(_ResultationTable).Trim());
                    mmil.SetTo(GetTableNameView(_ResultationTable).Trim());
                    mmil.SetFrom(GetTableNameView(_ResultationTable).Trim());
                    mmil.SetRows(_MMIL.GetContextValue(MMILDataManipulationConfigure.RowsDeclarationCommand));
                    mmil.SetValues(_MMIL.GetContextValue(MMILDataManipulationConfigure.ValuesDeclarationCommand));
                    mmil.SetWhere(_MMIL.GetContextValue(MMILDataManipulationConfigure.WhereDeclarationCommand));
                    mmil.EndExecuteContext();
                    Select(mmil);
                    Where();
                }

            #endregion

            _UpdateStrategy = new UpdateT(ref _ResultationTable, _MMIL);
          return  _UpdateStrategy.Update();

        }

        public ITransactionMethodResult Insert()
        {
            #region Rule Check
            if (_ResultationTable != null && _SelectedCollections.ElementAt(0).Value.GetWayToStored() != VirtualTableCollection.VIRTUAL_OBJECT && _SelectedCollections.Count == 1)
                _ResultationTable = _SelectedCollections.ElementAt(0).Value._TableCollection;
            else if (_ResultationTable != null && _SelectedCollections.ElementAt(0).Value.GetWayToStored() == VirtualTableCollection.VIRTUAL_OBJECT)
                if (!IsViewContainsOnlyOne(_ResultationTable))
                    throw new Exception("Error of Insertation");
                else
                {
                    MMILDataManipulationContext mmil = new MMILDataManipulationContext();
                    mmil.InitExecutionContext(PQLGrammar.INSERT_COMMAND);
                    mmil.SetTable(GetTableNameView(_ResultationTable).Trim());
                    mmil.SetTo(GetTableNameView(_ResultationTable).Trim());
                    mmil.SetFrom(GetTableNameView(_ResultationTable).Trim());
                    mmil.EndExecuteContext();
                    Select(mmil);

                    _AddedInfoPartTwo = _SelectedCollections.ElementAt(0).Key;
                    _ResultationTable = _SelectedCollections.ElementAt(0).Value._TableCollection;
                }
            #endregion

            _InsertStrategy = new InsertT(ref _ResultationTable, _MMIL, _AddedData, _AddedInfoPartTwo);
            ITransactionMethodResult tres = this._InsertStrategy.Insert();
            _SelectedCollections.ElementAt(0).Value._TableInfo = tres.AddedInfo as ITableInfo;
            return tres;


        }
        #endregion


        #region Collection Operate Methdos
        public ITable GetResult()
        {

            System.String top = _MMIL.GetContextValue(MMILDataManipulationConfigure.TopDeclarationCommand);


            if (top != null)
                this.Top();
             else
            {

                String skip = _MMIL.GetContextValue(MMILDataManipulationConfigure.SkipDeclarationCommand);
                String limit = _MMIL.GetContextValue(MMILDataManipulationConfigure.LimitDeclarationCommand);


                if (limit != null && skip != null)
                    this.SkipLimit();
                 else if(skip != null)
                    this.Skip();
                else if (limit != null)
                    this.Limit();
               

            }


            return this._ResultationTable;

        }

        public IQueryObject Save()
        {
            if (_SelectedCollections.Count > 1)
                throw new MultiplySaveError();

            foreach (var item in _SelectedCollections)
                item.Value.StoreCurrentTable();

            return this;
        }


        #endregion


        private Boolean IsViewContainsOnlyOne(ITable tableView) 
        {
            if (tableView.TableRow.Count == 0)
                return false;

            TableRow tr = tableView.TableRow.ElementAt(0).Value;

            if (tr.RowCells.Count == 0)
                return false;


            String tableName = tr.RowCells.ElementAt(0).Alias.Split('*')[0];
            foreach(TableCell tc in tr.RowCells)
                if (tableName != tc.Alias.Split('*')[0])
                    return false;
            


            return true;
        
        }



        private String GetTableNameView(ITable tableView) => tableView.TableRow.ElementAt(0).Value.RowCells.ElementAt(0).Alias.Split('*')[0];

    }
}
