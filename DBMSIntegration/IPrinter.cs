﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBMS.Integration
{
    public interface IPrinter
    {

        void Print(System.String printContent);

    }
}
