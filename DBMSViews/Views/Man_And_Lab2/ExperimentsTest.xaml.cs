﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;


namespace ManMachineIntermediate_Lab1.Views.Man_And_Lab2
{
    /// <summary>
    /// Логика взаимодействия для ExperimentsTest.xaml
    /// </summary>
    public partial class ExperimentsTest : Window
    {
        /// <summary>
        /// 
        /// </summary>
        public ExperimentsTest()
        {
            InitializeComponent();
        }




        #region variable
        private Random x = new Random();
        private Random y = new Random();
        private System.Int32 Status_exp = 0;
        private DateTime Start;
        private DateTime Stoped;
        private TimeSpan Elapsed = new TimeSpan();

        /// <summary>
        /// 
        /// </summary>
        public Point p = new Point();

        /// <summary>
        /// 
        /// </summary>
        public Point[] pi = new Point[2];

        /// <summary>
        /// 
        /// </summary>
        public Point s;
        #endregion


        #region BehindCode
        private void icDrow_MouseLeftButtonUp(System.Object sender, MouseButtonEventArgs e)
        {




            if (Status_exp > 0)
            {
                Point[] n =
                     Array.FindAll(pi, element => (Math.Abs(element.X - e.GetPosition(this).X) < 20)
                     && (Math.Abs(element.Y - e.GetPosition(this).Y) < 20));

                if (Array.Exists(pi, element => (Math.Abs(element.X - e.GetPosition(this).X) < 20) && (Math.Abs(element.Y - e.GetPosition(this).Y) < 20)))
                {
                    if (!((Math.Abs(s.X - e.GetPosition(this).X) < 10) && (Math.Abs(s.Y - e.GetPosition(this).Y) < 10)))
                    {
                        s = e.GetPosition(this);

                        {

                            if (Start.Ticks == 0) { Start = DateTime.Now; }
                            else
                            {
                                Stoped = DateTime.Now;
                                Elapsed = Stoped.Subtract(Start);
                                System.Int64 elapsedTicks = Stoped.Ticks - Start.Ticks;
                                TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);
                                System.Double rez = Math.Sqrt(Math.Pow(pi[0].X - pi[1].X, 2) + Math.Pow(pi[0].Y - pi[1].Y, 2));

                                lbRes.Items.Add("Experiment,Time,Way:" + Status_exp.ToString() + "," + elapsedSpan.Milliseconds.ToString() + "," + rez);

                                if (Status_exp < 100) Expir();
                            }

                        }
                    }

                }
            }
        }

        private void Button1_Click(System.Object sender, RoutedEventArgs e)
        {

            this.lbRes.Items.Clear();
            //  this.lbRes.Items.Add("Results");
            this.Expir();

        }


        private Point DrowObj(Point p)
        {
            Rectangle el = new Rectangle();
            el.Width = 10;
            el.Height = 10;
            p.X = x.Next(Convert.ToInt32(this.Width - el.Width));
            p.Y = y.Next(Convert.ToInt32(this.icDrow.ActualHeight - el.Height));
            el.Fill = Brushes.Red;
            InkCanvas.SetLeft(el, p.X);
            InkCanvas.SetTop(el, p.Y);
            icDrow.Children.Add(el);
            return p;
        }


        /// <summary>
        /// 
        /// </summary>
        public void Expir()
        {
            System.Int32[] rez_arr = new System.Int32[2];
            Array.Clear(pi, 0, pi.Length - 1);
            Start = new DateTime(0);
            icDrow.Children.Clear();
            icDrow.Strokes.Clear();

            for (System.Byte i = 0; i < 2; ++i)
            {
                pi[i] = DrowObj(p);
            }
            Status_exp = Status_exp + 1;
        }


        private void CreateExecelButton_Click(System.Object sender, RoutedEventArgs e)
        {
            ItemCollection items = this.lbRes.Items;
            System.String[] values = new System.String[items.Count];

            System.Int32 range = 0;
            foreach (var i in items)
            {
                values[range++] = i.ToString();
            }

            System.String resultationString = values[0].Split(':')[0]+":";


            for(System.Int32 i = 0; i < values.Length; ++i)
            {
                resultationString += (i != 0 ? "," : "")+ values[i].Split(':')[1];
            }

      //      new InputToExcell().Print(resultationString);
        }

        #endregion



    }
}
