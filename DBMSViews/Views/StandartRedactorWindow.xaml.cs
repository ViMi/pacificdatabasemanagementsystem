﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;


using MessageBox = System.Windows.MessageBox;
using SaveFileDialog = Microsoft.Win32.OpenFileDialog;
using ManMachineIntermediate_Lab1.Views;
using DBMS.Redactor.ViewModels;
using DBMS.Redactor.MDIStrategy;

using System.Windows.Forms;

namespace ManMachineIntermediate_Lab1
{
    /// <summary>
    /// Логика взаимодействия для StandartRedactorWindow.xaml
    /// </summary>
    public partial class StandartRedactorWindow : Window
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vm"></param>
        public StandartRedactorWindow(RedactorViewModel vm)
        {
            InitializeComponent();


            


            vm.MDI = new MDIMaintein
            (
                this._DocumentContextStackPanel.Children,
                (Style)this.FindResource("BtnContextDocumentStyle"),
                (Style)this.FindResource("CheckBoxStandartStyleRed"),
                (ControlTemplate)this.FindResource("ButtonBaseControlTemplateStandartApplication"),
                (ControlTemplate)this.FindResource("StandartCheckBoxTemplate"),
                vm

            );

            this.HideSplitChk.Checked += vm.MDI.HideSplitChk_Click;
            this.DataContext = vm;
            this._workArea.TextChanged += vm.MDI._workArea_TextChanged;

        }


       
        private void Property_MouseDown(object sender, MouseButtonEventArgs e) => new PropertiyWindow(((RedactorViewModel)this.DataContext)).Show();
        private void Print_MouseDown(object sender, MouseButtonEventArgs e)
        {
            PropertiyWindow pw = new PropertiyWindow(((RedactorViewModel)this.DataContext));

            pw.DataContext = this.DataContext;
            pw.PrintPanelInit();
            pw.Show();

        }
        private void PropertyItem_Click(object sender, RoutedEventArgs e) => new PropertiyWindow(((RedactorViewModel)this.DataContext)).Show();
        private void TextSettingItem_Click(object sender, RoutedEventArgs e)
        {
            PropertiyWindow pw = new PropertiyWindow(((RedactorViewModel)this.DataContext));
            pw.DataContext = this.DataContext;
            pw.TextPanelInit();
            pw.Show();
        }
        private void PrintItem_Click(object sender, RoutedEventArgs e)
        {
            PropertiyWindow pw = new PropertiyWindow(((RedactorViewModel)this.DataContext));
            pw.DataContext = this.DataContext;
            pw.PrintPanelInit();
            pw.Show();

        }
        private void _FileItem_Print_Click(object sender, RoutedEventArgs e) 
        {

            PropertiyWindow pw = new PropertiyWindow(((RedactorViewModel)this.DataContext));
            pw.DataContext = this.DataContext;
            pw.PrintPanelInit();
            pw.Show();


        }       
        private void _WorkAreaItem_General_Click(object sender, RoutedEventArgs e)
        {
            PropertiyWindow pw = new PropertiyWindow(((RedactorViewModel)this.DataContext));
            pw.DataContext = this.DataContext;
            pw.GeneralPanelInit();
            pw.Show();
        }      
        private void _ProjectItem_Translation_Click(object sender, RoutedEventArgs e) 
        {

            PropertiyWindow pw = new PropertiyWindow(((RedactorViewModel)this.DataContext));
            pw.DataContext = this.DataContext;
            pw.TranslatinonPanelInit();
            pw.Show();


        }


        private void _FileItem_Create_Click(object sender, RoutedEventArgs e)
        {
            CreateNewFileWindow createNewFileWindow = new CreateNewFileWindow();

            if (createNewFileWindow.ShowDialog() == true)
            {
                ((RedactorViewModel)this.DataContext).CreateNew.Execute(createNewFileWindow.FileName);
             
            }
        }
        private void HelpItem_Click(object sender, RoutedEventArgs e) => ((RedactorViewModel)this.DataContext).VisitDocumentationSite.Execute(null);
        private void link_mouseDown(object sender, MouseButtonEventArgs e) => ((RedactorViewModel)this.DataContext).VisitComunitySite.Execute(null);

        /// <summary>
        /// 
        /// </summary>
        public void Open() => ((RedactorViewModel)this.DataContext).OpenFile.Execute(null);

/// <summary>
/// 
/// </summary>
/// <param name="name"></param>
        public void OpenByName(System.String name)=> ((RedactorViewModel)this.DataContext).CreateNew.Execute(name);
    
        
        /// <summary>
        /// 
        /// </summary>
        public void CloseDoc()
        {
            ((RedactorViewModel)this.DataContext).CloseFile.Execute(null);
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            AuthorizationWindow authorizationWindow = new AuthorizationWindow((RedactorViewModel)this.DataContext);

            if(authorizationWindow.ShowDialog() == true)
              ((RedactorViewModel)this.DataContext).RefreshDBInfo.Execute(null);
              
            
        }
    }
}
