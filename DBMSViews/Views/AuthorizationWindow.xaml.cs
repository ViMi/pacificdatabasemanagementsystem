﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DBMS.Redactor.ViewModels;


namespace ManMachineIntermediate_Lab1.Views
{
    /// <summary>
    /// Логика взаимодействия для AuthorizationWindow.xaml
    /// </summary>
    public partial class AuthorizationWindow : Window
    {
     
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vm"></param>
        public AuthorizationWindow(RedactorViewModel vm)
        {
            InitializeComponent();
            this.DataContext = vm;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
                ((RedactorViewModel)this.DataContext).AuthorizationViewModel.GetLogin.Execute(null);
                this.DialogResult = true;   
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!((RedactorViewModel)this.DataContext).AuthorizationViewModel.IsAuthorized)
            {
                ((RedactorViewModel)this.DataContext).AuthorizationViewModel.Reset();
            }
        }
    }
}
