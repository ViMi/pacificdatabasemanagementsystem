﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ManMachineIntermediate_Lab1.Views
{
    /// <summary>
    /// Логика взаимодействия для CreateNewFileWindow.xaml
    /// </summary>
    public partial class CreateNewFileWindow : Window
    {
        /// <summary>
        /// 
        /// </summary>
        public CreateNewFileWindow()
        {
            InitializeComponent();
        }


        /// <summary>
        /// 
        /// </summary>
        public System.String FileName {
            get
            {
                return this.InputField.Text;
            }
                
                }
        Regex regex = new Regex("^[a-zA-Z0-9_]+$");
        bool IsSuccessEnter = false;
        System.Byte MaxTokens = 32;

        private void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.IsSuccessEnter)
            {
                this.DialogResult = true;
            } 

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

            try
            {
                this.IsSuccessEnter = regex.Match(this.InputField.Text).Success && this.InputField.Text.Length <= this.MaxTokens;

                if (IsSuccessEnter)
                {

                    this.ErrnoTextBlock.Text = System.String.Empty;
                }
                else
                {

                    this.ErrnoTextBlock.Text = "File name must contains only letter"+Environment.NewLine+ " and have lenth less than 32 and gretter than 0";
                }
            }
            catch
            {
                return;
            }
        }
    }
}
