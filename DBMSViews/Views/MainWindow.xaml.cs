﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Ribbon;
using System.Windows.Media.Animation;
using System.Windows.Media.Media3D;

using Microsoft.Win32;
using System.IO;
using MessageBox = System.Windows.MessageBox;
using SaveFileDialog = Microsoft.Win32.OpenFileDialog;

using DBMS.Redactor.ViewModels;
using ManMachineIntermediate_Lab1.Views;

namespace ManMachineIntermediate_Lab1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window 
    {



        private RedactorViewModel RedactorViewModel { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();


            this.RedactorViewModel = new RedactorViewModel();

            this.ErrorReport.Text=this.RedactorViewModel.GetServerInitCode();
        }

        private void CreateBtn_Click(System.Object sender, RoutedEventArgs e)
        {

            CreateNewFileWindow createNewWindow = new CreateNewFileWindow();

            if (createNewWindow.ShowDialog() == true)
            {
                StandartRedactorWindow standartRedactor = new StandartRedactorWindow(this.RedactorViewModel);
                standartRedactor.OpenByName(createNewWindow.FileName);
                standartRedactor.Show();
                this.Close();

            }

        }

        private void OpenBtn_Click(System.Object sender, RoutedEventArgs e)
        {

            StandartRedactorWindow standartRedactor = new StandartRedactorWindow(this.RedactorViewModel);
            standartRedactor.Open();
            standartRedactor.Show();
            this.Close();

        }





        private void SettingImg_MouseDown(System.Object sender, MouseButtonEventArgs e) => (new PropertiyWindow(this.RedactorViewModel)).Show();
        private void link_mouseDown(System.Object sender, MouseButtonEventArgs e) => ((RedactorViewModel)this.DataContext).VisitDocumentationSite.Execute(null);


    }
}
