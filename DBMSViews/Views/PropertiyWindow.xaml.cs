﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.Encodings;

using System.IO;
using System.Text.Json;
using System.Drawing;

using System.Text.RegularExpressions;
using System.Security.RightsManagement;
using System.Windows.Forms.VisualStyles;

using System.Windows.Forms;
using DBMS.Redactor.ViewModels;
using System.Drawing.Printing;


using ManMachineIntermediate_Lab1.Views.Man_And_Lab2;

namespace ManMachineIntermediate_Lab1
{

    /// <summary>
    /// Логика взаимодействия для PropertiyWindow.xaml
    /// </summary>
    public partial class PropertiyWindow : Window
    {




        /// <summary>
        /// 
        /// </summary>
        /// <param name="vm"></param>
        public PropertiyWindow(RedactorViewModel vm)
        {
            InitializeComponent();

            this.DataContext = vm; 
        }



        private void CancelBtn_Click(object sender, RoutedEventArgs e) => this.Close();
        private void link_mouseDown(object sender, MouseButtonEventArgs e) => ((RedactorViewModel)this.DataContext).VisitDocumentationSite.Execute(null);





        private void GeneralSettingBtn_Click(object sender, RoutedEventArgs e) => this.GeneralPanelInit();
        private void ExtraBtn_Click(object sender, RoutedEventArgs e) => new ExperimentsTest().Show();
        private void TranslationSettingBtn_Click(object sender, RoutedEventArgs e) => this.TranslatinonPanelInit();
        private void TextSettingBtn_Click(object sender, RoutedEventArgs e) => this.TextPanelInit();
        private void PrintBtn_Click(object sender, RoutedEventArgs e) => this.PrintPanelInit();

        /// <summary>
        /// 
        /// </summary>
        public void TranslatinonPanelInit()
        {
            this.HeaderPrint.Visibility = Visibility.Collapsed;
            this.HeaderTranslation.Visibility = Visibility.Visible;
            this.HeaderGeneral.Visibility = Visibility.Collapsed;
            this.HeaderText.Visibility = Visibility.Collapsed;
        }


        /// <summary>
        /// 
        /// </summary>
        public void GeneralPanelInit()
        {
            this.HeaderPrint.Visibility = Visibility.Collapsed;
            this.HeaderTranslation.Visibility = Visibility.Collapsed;
            this.HeaderGeneral.Visibility = Visibility.Visible;
            this.HeaderText.Visibility = Visibility.Collapsed;

        }


        /// <summary>
        /// 
        /// </summary>
        public void TextPanelInit()
        {
            this.HeaderPrint.Visibility = Visibility.Collapsed;
            this.HeaderTranslation.Visibility = Visibility.Collapsed;
            this.HeaderGeneral.Visibility = Visibility.Collapsed;
            this.HeaderText.Visibility = Visibility.Visible;

        }


        /// <summary>
        /// 
        /// </summary>
        public void PrintPanelInit()
        {
            this.HeaderGeneral.Visibility = Visibility.Collapsed;
            this.HeaderTranslation.Visibility = Visibility.Collapsed;
            this.HeaderPrint.Visibility = Visibility.Visible;
            this.HeaderText.Visibility = Visibility.Collapsed;
        }



    }
}
