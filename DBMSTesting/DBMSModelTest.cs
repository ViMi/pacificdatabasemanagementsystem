using Microsoft.VisualStudio.TestTools.UnitTesting;
using DBMS;
using DBMS.Core;
using System;
using Test.ViewModels.NotifyProperties;
using DBMS.Core.IO;
using DBMS.Core.Helpers;

using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Integration;
using DBMS.Integration.JSON;

using DBMS.Redactor.MDIStrategy;
using DBMS.Redactor.Project;
using DBMS.Core.Interprater;
using DBMS.Redactor.FileSystem;
using DBMS.PQL;
using DBMS.Formatter;
using DBMS.Core.ErrorControlSystem;

namespace DBMSTesting
{
    [TestClass]
    public class DBMSModelTest
    {
        [TestMethod]
        public void InterprateTest()
        {

            #region Test Set Declaration
            System.String resString = System.String.Empty;
            System.String TestiongSet = "Console.Print(\"Hello IamConsole\");";
            System.String curreLogin = "root";
            System.String curreLoginPass = String.Empty;
            #endregion






            DBMSIO.DBMSConfig = new DBMSConfiguration();

            JSON json = new JSON();


            DBMSIO.IOIntegrationModule = json;

            DBMSIO.ServerCheck();


            DBMSIO.Logger = new StandartLogger(
                DBMSIO.DBMSConfig.BaseDir
            + DBMSIO.DBMSConfig.BaseSecurityDir
            + DBMSIO.DBMSConfig.PathToLog
            + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
            + DBMSIO.DBMSConfig.StandartLogFileName
            + DBMSIO.DBMSConfig.StandartLogFileExtension
            );


            DBMSIO.RegisterNewEvent(DBMSStandartEventNames.ClearConsole, ()=> { });


            DBMSIO.IOIntegrationModule = new JSON();

            ILogin User = DBMSIO.GetLoginWithPassword(curreLogin, curreLoginPass);
            DBMSIO.ReadUserByLogin(User);
            DBMSIO.SetCurrentLogin(User);
            DBMSIO.SetCurrentLogin(User);
            DBMSIO.SetGlobalExecuteStrategy(new NoSQLExecuter(new NoSQlExecuteSecureStrategy(), new ExecuteResultNotFormat()));
            DBMSIO.SetGlobalParseConfiguration(new NoSQLParseModule(new NoSQLLanguageSecureStrategy(TestiongSet), TestiongSet));
            resString = String.Empty;
            resString = DBMSIO.Interpret(new NoSQLSecureStrategy());

            Assert.IsTrue(resString != String.Empty, resString);

        }
    }
}
