﻿using DBMS.Core.MMIL;
using DBMS.Core.IO;
using DBMS.Core.Helpers;

using System.Collections.Generic;

namespace DBMS.Core
{
    public sealed class View 
        : IView
    {

        #region Documenttion
#if DocTrue
        static View() 
        {
            Documentation.AddHistoryToSection(
            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreComponents,
            "View",
            PQLDocGlobal.TypeClass,
            PQLDocGlobal.DescriptionUndefined);


            Documentation.AddHistoryToSection(
            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreComponents,
            "IView",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined);
       

        
        }

#endif
        #endregion



        public NodeType Type { get; set; } = NodeType.VIEW;
        public List<MMILDataBaseContext> PrecompiledILCommand { set; get; }
        public System.Object Invoke() => DBMSIO.ExecuteILCode(new MMILCollection() {ILUnits = PrecompiledILCommand});

    }
}
