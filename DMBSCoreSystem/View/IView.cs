﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBMS.Core;
using DBMS.Core.MMIL;
using DMBS.Core;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.IO;
using DBMS.Core.Table;


namespace DBMS.Core
{
    public interface IView
    {

        NodeType Type { get; set; }
        List<MMILDataBaseContext> PrecompiledILCommand { get; set; }
        System.Object Invoke();

    }
}
