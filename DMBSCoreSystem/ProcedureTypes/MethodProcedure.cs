﻿using System;
using DBMS.Core.Helpers;

using DBMS.Core.Table;

namespace DBMS.Core
{
    public sealed class MethodParameter
        : IMethodParameter
    {

        #region Documentation
#if DocTrue
        static MethodParameter()
        {
            Documentation.AddHistoryToSection(
             PQLDocGlobal.CoreSystem,
             PQLDocGlobal.CoreUsersFromRoles,
             "MethodParameter",
             PQLDocGlobal.TypeClass,
             PQLDocGlobal.DescriptionUndefined,
             "IMethodParameter"
         );


            Documentation.AddHistoryToSection(
                   PQLDocGlobal.CoreSystem,
                   PQLDocGlobal.CoreUsersFromRoles,
                   "IMethodParameter",
                   PQLDocGlobal.TypeInterface,
                   PQLDocGlobal.DescriptionUndefined

                );
        }
#endif
        #endregion


        public String Name { get; set; }
        public PacificDataBaseDataType DataType { get; set; }
        public String Value { get; set; }
    }
}
