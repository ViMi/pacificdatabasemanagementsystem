﻿using System;
using System.Collections.Generic;
using DBMS.Core.MMIL;
using DBMS.Core.Helpers;

using DBMS.Core.IO;
using DBMS.Core.Table;


namespace DBMS.Core
{

    public sealed class StoredProcedure
        : IProcedure
    {

        #region Documentation
#if DocTrue
        static StoredProcedure()
        {

            Documentation.AddHistoryToSection(
             PQLDocGlobal.CoreSystem,
             PQLDocGlobal.CoreUsersFromRoles,
             "StoredProcedure",
             PQLDocGlobal.TypeClass,
             PQLDocGlobal.DescriptionUndefined,
             "IProcedure"
         );


            Documentation.AddHistoryToSection(
                   PQLDocGlobal.CoreSystem,
                   PQLDocGlobal.CoreUsersFromRoles,
                   "IProcedure",
                   PQLDocGlobal.TypeInterface,
                   PQLDocGlobal.DescriptionUndefined

                );

        }
#endif
        #endregion

        public NodeType Type { get; set; } = NodeType.PROCEDURE;
        public ProcedureType ProcType { set; get; } = ProcedureType.NO_PARAMES;
        public Dictionary<String, PacificDataBaseDataType> FormalParameters { set; get; }
        public List<MMILDataBaseContext> PrecompiledILCommand { set; get; }
        public System.Object Invoke() => DBMSIO.ExecuteILCode(new MMILCollection(){ ILUnits = PrecompiledILCommand});
    }
}
