﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBMS.Core.Helpers.Tests
{




    public static class TestTimer
    {

        private static DateTime _Now;
        private static TimeSpan _Result;


       public static void InitNewTimer() => _Now = DateTime.Now;

        public static void StopTimer() => _Result = DateTime.Now - _Now;

        public static TimeSpan GetResultTime() => _Result;

    }
}
