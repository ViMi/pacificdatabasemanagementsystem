﻿using System;
using System.Runtime.InteropServices;


using System.Text;

using System.IO;
using DBMS.Core.ErrorControlSystem.Exeption;



namespace DBMS.Core.Helpers
{
    public static class FileSystem
    {

        #region Documentaiton
#if DocTrue
        static FileSystem() => Documentation.AddHistoryToSection(
                        PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreHelpers,
            "FileSystem",
            PQLDocGlobal.TypeStaticClass,
            PQLDocGlobal.DescriptionUndefined

            );
#endif
        #endregion




        private const Int32 ERROR_SHARING_VIOLATION = 32;
        private const Int32 ERROR_LOCK_VIOLATION = 33;

        public static bool IsFileLocked(this Exception exception)
        {
            Int32 errorCode = Marshal.GetHRForException(exception) & ((1 << 16) - 1);
            return errorCode == ERROR_SHARING_VIOLATION || errorCode == ERROR_LOCK_VIOLATION;
        }





    }
}
