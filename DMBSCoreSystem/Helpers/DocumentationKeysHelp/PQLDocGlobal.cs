﻿using System;
using System.Collections.Generic;
using System.Text;

#if DocTrue

namespace DBMS.Core.Helpers.DocKeys
{
    public static class PQLDocGlobal
    {


        static PQLDocGlobal() => Documentation.AddHistoryToSection(

            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreHelpers,
            "PQLDocGlobal",
            PQLDocGlobal.TypeStaticClass,
            PQLDocGlobal.DescriptionUndefined

            );



        #region Item Types
        public readonly static System.String TypeClass             = "Class";
        public readonly static System.String TypeClassDTO          = "Class(DTO)";
        public readonly static System.String TypeStaticClass       = "Static Class";
        public readonly static System.String TypeEnum              = "Enum";
        public readonly static System.String TypeStruct            = "Struct";
        public readonly static System.String TypeInterface         = "Interface";
        public readonly static System.String TypeSealedStaticClass = "Static Sealed Class";
        public readonly static System.String TypeSealedClass       = "Sealed Class";
        #endregion



        #region Main Section
        public readonly static System.String DML                   = "DML";
        public readonly static System.String DDL                   = "DDL";
        public readonly static System.String DCL                   = "DCL";
        public readonly static System.String ExteraCommands        = "Extra_Commands";
        public readonly static System.String CoreSystem            = "Core";
        public readonly static System.String Redactor              = "Redactor";
        public readonly static System.String PQLFlowSystems        = "PQL_Flow_Systems";
        public readonly static System.String OutPutFormatters      = "OutPut_Formatters";
        public readonly static System.String IntegrationSystems    = "Integration_Systems";
        #endregion


        #region SubSection
            #region CoreSubSection
        public readonly static System.String CoreIO                 = "Core_IO";
        public readonly static System.String CoreEvent              = "Core_Event";
        public readonly static System.String CoreSecurity           = "Core_Security";
        public readonly static System.String CoreComponents         = "Core_Components";
        public readonly static System.String CoreErrorControlSystem = "Core_ErrorControlSystems";
        public readonly static System.String CoreHelpers            = "Core_Helpers";
        public readonly static System.String CoreLoggers            = "Core_Loggers";
        public readonly static System.String CoreMMIL               = "Core_MMIL";
        public readonly static System.String CoreProcedureTypes     = "Core_ProcedureTypes";
        public readonly static System.String CoreGlobalInterfaces   = "Core_GlobalInterfaces";
        public readonly static System.String CoreRoles              = "Core_Roles";
        public readonly static System.String CoreTable              = "Core_Table";
        public readonly static System.String CoreTriggers           = "Core_Triggers";
        public readonly static System.String CoreUsersFromRoles     = "Core_UsersFromRoles";
        public readonly static System.String CoreView               = "Core_View";
        public readonly static System.String CoreFlow               = "Core_Flow";
            #endregion

            #region RedactorSubSection
        public readonly static System.String RedactorConfig     = "Redactor_Config";
        public readonly static System.String RedactorMDI        = "Redactor_MDI";
        public readonly static System.String RedactorProject    = "Redactor_Project";
        public readonly static System.String RedactorIO         = "Redactor_IO";
            #endregion


           #region PQLSubSection 
        public readonly static System.String PQLFlow        = "PQL_Flow";
        public readonly static System.String PQLExtension   = "PQL_Extension";
        public readonly static System.String PQLCoreTypes   = "PQL_CoreTypes";
            #endregion


            #region OutPutSubSections
        public readonly static System.String OutPutTypes = "OutPutFormatters_Formatters";
           #endregion


            #region IntegrationSubSections
        public readonly static System.String IntegrationJSON            = "Integration_JSON";
        public readonly static System.String IntegrationMicrosoftOffice = "Integration_MSOffice";
        public readonly static System.String IntegrationWeb             = "Integration_Web";
        public readonly static System.String IntegrationWindows         = "Integration_Windows";
        public readonly static System.String IntegrationCommunity       = "Integration_Community";
        #endregion

        #endregion



        public readonly static System.String DescriptionUndefined = "DOCUMENTATION_OF_ITEM_NOT_IMPLEMENTED_YET";

    }
}

#endif