﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBMS.Core.Helpers
{

    public sealed class CommandDescription
    {
        private System.String _CommandName = System.String.Empty;
        private System.String _CommandFamily = System.String.Empty;
        private System.String _RightFormat = System.String.Empty;

        public CommandDescription(System.String commandName, System.String commandFamily, System.String rightFormat)
        {
            _CommandName = commandName;
            _CommandFamily = commandFamily;
            _RightFormat = rightFormat;
        }


        public System.String GetCommandDescription() => 
            "_Command_ [" + _CommandName + "]. _Family_ [" + _CommandFamily + "]" + Environment.NewLine;

    }




    public static class CommandTracer
    {
        public static CommandDescription CommanDesc;
        public static System.Int32 Line;


        public static System.String GetTracePath() => $"::: TRACE {CommandTracer.CommanDesc.GetCommandDescription()}_Line_.{CommandTracer.Line}";
    }
}
