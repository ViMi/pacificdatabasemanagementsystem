﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using DBMS.Core.IO;


#if DocTrue

namespace DBMS.Core.Helpers
{
    public static class Documentation
    {


        #region Documentation
        static Documentation() => Documentation.AddHistoryToSection
            (
            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreHelpers,
            "Documentation",
            PQLDocGlobal.TypeStaticClass,
            "Данный класс отвечает за автоматизированное создание описания подсистем"
            );
        #endregion



        #region Variable Declaration
        public static Dictionary<String, Dictionary<String, String>> Doc = new Dictionary<String, Dictionary<String, String>>();
        public static readonly System.String DocHeader = "PQL_DOCUMENTATION";

        public static readonly System.String Path =  "Doc.txt";
        #endregion


        public static void SaveHistory()
        {

            String historys = DocHeader;


            foreach(var item in Doc)
            {
                historys += Environment.NewLine + item.Key;

                foreach(var item2 in item.Value)
                {
                    historys += Environment.NewLine + "\t[ " +item2.Key + " ]";
                    historys += "\t\t" + item.Value + Environment.NewLine;
                }

            }



            Documentation.IfDocFileExist();




            using (FileStream fs = new FileStream(Path, FileMode.Create))
            {
                Byte[] arr = System.Text.Encoding.UTF8.GetBytes(historys);
                fs.Write(arr, 0, arr.Length);
            }

        }

        public static void AddHistoryToSection
            (System.String docSection,
            System.String docSubSection,
            System.String docItem, 
            System.String docType,
            System.String docItemContent,
            System.String typeBaseOn = "System.Object")
        {

            Dictionary<String, String> docItemsDic = null;

            
            if (Doc.ContainsKey(docSection))
                if(Doc.TryGetValue(docSection, out docItemsDic))
                    if(docItemsDic.ContainsKey(docItem))
                    {
                        docItemsDic.Remove(docItem);
                        docItemsDic.Add(docItem, docItemContent);
                    } else
                        docItemsDic.Add(docItem, docItemContent);
                    
                 else
                    Doc.Add(docSection, new Dictionary<String, String>());
                

             else
                Doc.Add(docSection, new Dictionary<String, String>());
            


        }


        private static void IfDocFileExist()
        {
            if (File.Exists(Path))
                File.Delete(Path);
        }


        private static void CreateDirectory(System.String pathTo)
        {
        
        
        }


    }
}

#endif