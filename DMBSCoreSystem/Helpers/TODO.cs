﻿using DBMS.Core.ErrorControlSystem.Exeption;


namespace DBMS.Core.Helpers
{
    public static class TODO
    {

        #region Documentation
#if DocTrue
        static TODO() => Documentation.AddHistoryToSection(
                        PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreHelpers,
            "TODO",
            PQLDocGlobal.TypeStaticClass,
            PQLDocGlobal.DescriptionUndefined
            );

#endif
        #endregion

        public static void TODONotification(System.String Notification)
        {
            throw new TODONotificationExcep(Notification); 
        }

    }
}
