﻿using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.InfoTypes;

namespace DBMS.Core.Helpers.RulesControl
{
    public delegate ITransactionMethodResult Statement(ITransactionInfo _ti);

    public interface IStatement
    {
        System.Int32 StatementID { set; get; }
        System.String StatementName { set; get; }
        Statement StatementDefinition { set; get; }

        ITransactionMethodResult Invoke(ITransactionInfo ti);

    }
}
