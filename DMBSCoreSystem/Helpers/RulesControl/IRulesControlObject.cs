﻿using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.InfoTypes;


namespace DBMS.Core.Helpers.RulesControl
{
    public interface IRulesControlObject
    {
        List<IStatement> Statements { set; get; }

        IStatement FindStatement(System.String statmentName);
        IStatement FindStatement(System.Int32 statementID);
        ITransactionMethodResult Invoke(System.Int32 statementID, ITransactionInfo ti);
        ITransactionMethodResult Invoke(System.String statementName, ITransactionInfo ti);

    }
}
