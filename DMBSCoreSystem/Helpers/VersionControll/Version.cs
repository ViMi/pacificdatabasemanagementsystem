﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBMS.Core.Helpers
{
    public static class Version
    {

        public const System.String ALPHA_VERSION = "[ALPHA_VERSION]";
        public const System.String BETA_VERSION = "[BETA_VERSION]";
        public const System.String PROTOTYPE_VERSION = "[PROTOTYPE_VERSION]";
        public const System.String DEPRECATED_VERSION = "[DEPRECATED_VERSION]";
        public const System.String RELEASE_VERSION = "[RELEASE_VERSION]";


    }
}
