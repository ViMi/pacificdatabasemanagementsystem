﻿using System;
using System.Collections.Generic;
using DBMS.Core.Helpers;




namespace DBMS.Core.ErrorControlSystem
{
    public static class ErrorMessagesCore
    {
#if DocTrue
        static ErrorMessagesCore()
        {
            Documentation.AddHistoryToSection(
                    PQLDocGlobal.CoreSystem,
                    PQLDocGlobal.CoreErrorControlSystem,
                    "ErrorMessageCore",
                    PQLDocGlobal.TypeStaticClass,
                    PQLDocGlobal.DescriptionUndefined

       
            );


            Documentation.AddHistoryToSection(

                                        PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "ErrorCodes",
            PQLDocGlobal.TypeEnum,
            PQLDocGlobal.DescriptionUndefined

                );

        }
#endif


        public static Dictionary<System.Byte, String> MessageDictionary = new Dictionary<byte, System.String>()
        {

            [(Byte)(ErrorCodes.SUCCESS_EXECUTE)] = "SUCCESS EXECUTE" + Environment.NewLine,
            [(Byte)(ErrorCodes.FAILURE_EXECUTE)] = "FAILURE EECUTE (UNCATCHED ERROR)" + Environment.NewLine,
            [(Byte)(ErrorCodes.TABLE_NOT_FOUND)] = "TABLE NOT FOUND" + Environment.NewLine,
            [(Byte)(ErrorCodes.DATABASE_NOT_FOUND)] = "DATABASE NOT FOUND" + Environment.NewLine,
            [(Byte)(ErrorCodes.USER_NOT_FOUND)] = "USER NOT FOUND" + Environment.NewLine,
            [(Byte)(ErrorCodes.LOGIN_ERROR)] = "LOGIN ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.PASSWORD_ERROR)] = "PASSWORD ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.SYNTAX_ERROR)] = "SYNTAX ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.INSERT_ERROR)] = "INSERTATION ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.SELECT_ERROR)] = "SELECT ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.UPDATE_ERROR)] = "UPDATE ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.DACLARE_ERROR)] = "DECLARED ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.PARSE_ERROR)] = "PARSE ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.EXECUTION_ERROR)] = "EXECUTION ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.RIGHT_ERROR)] = "RIGHT ACCES ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.GRANTED_ERROR)] = "GRANTED ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.REVOKE_ERROR)] = "REVOKE ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.JOIN_ERROR)] = "JOIN ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.COLUMN_NOT_EXIST)] = "COLUMN NOT EXIST" + Environment.NewLine,
            [(Byte)(ErrorCodes.VALUE_ERROR)] = "VALUE ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.TYPE_ERROR)] = "TYPE ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.CHECK_ERROR)] = "CHECK ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.INTERMEDUATE_LANGUAGE_ERROR)] = "INTEDMEDIATE LANGUAGE ERROR" + Environment.NewLine,

            [(Byte)(ErrorCodes.DATABASE_ERROR)] = "DATABASE ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.DATA_MAPPING_ERROR)] = "DATA MAPPING ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.EXECUTE_CONTROL_SECURITY_ERROR)] = "EXECUTE CONTROL SECURITY ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.EXECUTE_RUNTIME_ERROR)] = "EXECUTE RUNTIME ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.INTERMEDIATE_LANGUAGE_CONTEXT_CLOSED)] = "INTERMEDIATE LANGUAGE CONTEXT CLOSED" + Environment.NewLine,
            [(Byte)(ErrorCodes.INTERMEDUATE_LANGUAGE_CONTEXT_UNSET)] = "INTERMEDIATE LANGUAGE CONTEXT UNSET" + Environment.NewLine,
            [(Byte)(ErrorCodes.PARSE_CONTROL_SECURITY_ERROR)] = "PARSE CONTROL SECURITY ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.PARSE_SECURE_ERROR)] = "PARSE SECURE ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.PROCEDURE_NOT_EXIST)] = "PROCEDURE NOT EXIST" + Environment.NewLine,
            [(Byte)(ErrorCodes.ROLLBACK_ERROR)] = "ROLLBACK ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.SAVE_ERROR)] = "SAVE ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.SERVER_NOT_EXIST)] = "SERVER NOT EXIST ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.STORED_PROCEDURE_EXECUTION_ERROR)] = "STORED PROCEDURE EXECUTION ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.TRANSACTION_ERROR)] = "TRANSACTION ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.TRIGGER_NOT_EXIST)] = "TRIGGER NOT EXIST" + Environment.NewLine,
            [(Byte)(ErrorCodes.VIEW_NOT_EXIST)] = "VIEW NOT EXIST" + Environment.NewLine,
            [(Byte)(ErrorCodes.SEMANTIC_ERROR)] = "SEMANTIC ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.LOGIN_NOT_FOUND)] = "LOGIN NOT FOUND" + Environment.NewLine,


            [(Byte)(ErrorCodes.CRIPTER_UNSET)] = "CRIPT STRATEGY UNSET" + Environment.NewLine,
            [(Byte)(ErrorCodes.CRIPTER_UNDEFINED)] = "CRIPTE UNDEFINED" + Environment.NewLine,


            [(Byte)(ErrorCodes.DATABASE_ALREADY_EXIST)] = "DATABASE ALREADY EXIST" + Environment.NewLine,
            [(Byte)(ErrorCodes.TABLE_ALREAD_EXIST)] = "TABLE AREADY EXIST" + Environment.NewLine,
            [(Byte)(ErrorCodes.TABLE_COLUMN_ALREADY_EXIST)] = "TABLE COLUMN ALREADY EXIST" + Environment.NewLine,
            [(Byte)(ErrorCodes.AS_PARAMETER_UNSETED)] = "AS PARAMETER UNSETED (Data Base Name can contains only letters,digits and _)" + Environment.NewLine,
            [(Byte)(ErrorCodes.ATTRIBUTE_ALREADY_SETTED)] = "ATTRIBUTE ALREADY SETTED" + Environment.NewLine,
            [(Byte)(ErrorCodes.VIEW_ALREADY_EXIST)] = "VIEW ALREADY EXIST" + Environment.NewLine,
            [(Byte)(ErrorCodes.LOGIN_ALREADY_EXIST)] = "LOGIN ALREADY EXIST" + Environment.NewLine,
            [(Byte)(ErrorCodes.USER_ALREADY_EXIST)] = "USER ALREADY EXIST" + Environment.NewLine,
            [(Byte)(ErrorCodes.PROCEDURE_ALREADY_EXIST)] = "PROCEDURE ALREADY EXIST" + Environment.NewLine,
            [(Byte)(ErrorCodes.TRIGGER_ALREADY_EXIST)] = "TRIGGER ALREADY EXIST" + Environment.NewLine,
            [(Byte)(ErrorCodes.DATABASE_UNSET)] = "DATABASE UNSETTED " + Environment.NewLine,

            [(Byte)(ErrorCodes.FAIL_OF_LOGIN_ALTER)] = "LOGIN ALTER FAIL" + Environment.NewLine,
            [(Byte)(ErrorCodes.FAIL_OF_PROCEDURE_ALTER)] = "PROCEDURE ALTER FAIL " + Environment.NewLine,
            [(Byte)(ErrorCodes.FAIL_OF_TABLE_ALTER)] = "TABLE ALTER FAIL " + Environment.NewLine,
            [(Byte)(ErrorCodes.FAIL_OF_TRIGGER_ALTER)] = "TRIGGER ALTER FAIL" + Environment.NewLine,
            [(Byte)(ErrorCodes.FAIL_OF_USER_ALTER)] = "USER ALTER FAIL" + Environment.NewLine,
            [(Byte)(ErrorCodes.FAIL_OF_VIEW_ALTER)] = "VIEW ALTER FAIL" + Environment.NewLine,
            [(Byte)(ErrorCodes.SERVER_TYPE_UNDEFINED)] = "SERVER TYPE UNDEFINED" + Environment.NewLine,
            [(Byte)(ErrorCodes.UNAUTHORIZED_ERROR)] = "YOU ARE UNAUTHORIZED ENTER LOGIN AND PASSWORD " + Environment.NewLine,
            [(Byte)(ErrorCodes.UNKNOWN_COMMAND)] = "UNKNOWN COMMAND RECONGNIZED " + Environment.NewLine,
            [(Byte)(ErrorCodes.ERROR_OF_FILTERING_TABLE)] = "ERROR OF FILTERING TABLE CREATION (occure in SELECT) " + Environment.NewLine,
            [(Byte)(ErrorCodes.MULTIPLY_DELETE_TABLE_ERROR)] = "ERROR OF DELETE (System. can not delete fromm several tables, set one) " + Environment.NewLine,
            [(Byte)(ErrorCodes.MULTIPLY_UPDATE_TABLE_ERROR)] = "ERROR OF UPDATE (System can not update record from several tables choose one) " + Environment.NewLine,
            [(Byte)(ErrorCodes.INSERT_VALUE_NOT_EQU_INSETED_COLUMN_ERROR)] = "INSERT VALUE NOT EQU INSETED COLUMN ERROR " + Environment.NewLine,
            [(Byte)(ErrorCodes.UNKNOWN_ORDER_STRATEGY)] = "UNKNOWN ORDER STRATEGY" + Environment.NewLine,
            [(Byte)(ErrorCodes.SELECT_COUNT_ITEMS_ERROR)] = "SELECT COUNT ITEMS ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.MULTIPLY_CONFI_VALUE_ERROR)] = "MULTIPLY CONFIGURATION ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.MULTIPLY_DEFINITION_ERROR)] = "MULTIPLY DEFNITION (probably AS statement) IN QUERY (ERROR)" + Environment.NewLine,

            [(Byte)(ErrorCodes.TABLE_WITH_NAME_EXIST_DURING_VIEW_CREATIONG)] = "TABLE WITH NAME EXIST YOU CAN NOT CREATE A VIEW WITH SAME NAME" + Environment.NewLine,
            [(Byte)(ErrorCodes.VIEW_WITH_NAME_EXIST_DURING_TABLE_CREATIONG)] = "VIEW STORED LIKE A VIRTUAL TABLE YOU CAN NOT CREATE A TABLE WITH SAME NAME" + Environment.NewLine,
            [(Byte)(ErrorCodes.SERVER_WAS_RESTORED_BECAUSE_OF_ERROR_IN_DEFAULT)] = "SERVER WAS RESTORED BECAUSE OF ERROR \n(Server configurations and data setted in default) FOR CORRECT WORK RELOAD APP" + Environment.NewLine,
            [(Byte)(ErrorCodes.REFLECTION_TYPE_TRANSAFORM_ERROR)] = "REFLECTION TYPE TRANSAFORM ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.FILE_LOCKED)] = "FILE LOCKED" + Environment.NewLine,
            [(Byte)(ErrorCodes.UNREGESTED_EVENT)] = "UNREGESTRED EVENT" + Environment.NewLine,
            [(Byte)(ErrorCodes.HOST_OPENED)] = "HOST OPENED" + Environment.NewLine,
            [(Byte)(ErrorCodes.ALL_COLUMN_UNDEF_SELECT_ERROR)] = " ALL COLUMN UNDEFINED SELECT ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.EMPTY_SET)] = " EMPTY SET" + Environment.NewLine,
            [(Byte)(ErrorCodes.MULTIPLY_SAVE_ERROR)] = "MULTIPLY SAVE ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.SELECT_ERROR)] = "SELECT ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.SKIP_NEGATIVE_ERROR)] = "SKIP ERROR CAN NOT BE NAGATIVE" + Environment.NewLine,
            [(Byte)(ErrorCodes.SKIP_ENTERED_VALUE_ERROR)] = "SKIP ENTERED VALUE ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.SHEMA_NOT_EXIST)] = "SHEMA NOT EXIST" + Environment.NewLine,
            [(Byte)(ErrorCodes.DATABASE_SETTED)] = "DATABASE SETTED" + Environment.NewLine,



        };

      



    }
}
