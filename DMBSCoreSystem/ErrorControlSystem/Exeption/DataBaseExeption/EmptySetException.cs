﻿using System;
using DBMS.Core.Helpers;


namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class EmptySetException
        : DBExeption
    {

#if DocTrue
        static EmptySetException() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "EmptySetException",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"

            );
#endif
        public EmptySetException() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.EMPTY_SET];
    }
}
