﻿using DBMS.Core.Helpers;



namespace DBMS.Core.ErrorControlSystem.Exeption 
{ 
   public sealed class ExecutorRuntimeError
        : DBExeption
    {

#if DocTrue
        static ExecutorRuntimeError() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "ExecutorRuntimeError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"

            );
#endif

        public ExecutorRuntimeError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)(ErrorCodes.EXECUTE_RUNTIME_ERROR)];

    }
}
