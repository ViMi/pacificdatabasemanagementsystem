﻿using DBMS.Core.Helpers;



namespace DBMS.Core.ErrorControlSystem.Exeption.DataBaseExeption
{
    public sealed class InsertError
        : DBExeption
    {

#if DocTrue
        static InsertError() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "InsertError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"


            );
#endif

        public InsertError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)(ErrorCodes.INSERT_ERROR)];
    }
}
