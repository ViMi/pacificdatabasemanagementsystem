﻿using DBMS.Core.Helpers;




namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class SyntaxError 
        : DBExeption
    {

#if DocTrue
        static SyntaxError() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "SyntaxError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"


            );
#endif

        public SyntaxError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)(ErrorCodes.SYNTAX_ERROR)];
    }
}
