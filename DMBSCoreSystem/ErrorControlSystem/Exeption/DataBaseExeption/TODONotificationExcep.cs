﻿using DBMS.Core.Helpers;




namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class TODONotificationExcep
        : DBExeption
    {

#if DocTrue
        static TODONotificationExcep() => Documentation.AddHistoryToSection(

                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "TODONotificationExcep",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"

            );
#endif

        public TODONotificationExcep(System.String Notification) => this.ServerMessageError = Notification;

    }
}
