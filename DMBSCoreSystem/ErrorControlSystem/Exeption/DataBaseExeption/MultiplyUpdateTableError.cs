﻿using System;
using DBMS.Core.Helpers;



namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class MultiplyUpdateTableError
        : DBExeption
    {

#if DocTrue
        static MultiplyUpdateTableError() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "MultiplyUpdateTableError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"


            );
#endif

        public MultiplyUpdateTableError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.MULTIPLY_UPDATE_TABLE_ERROR];

    }
}
