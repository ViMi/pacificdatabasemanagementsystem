﻿using DBMS.Core.Helpers;



namespace DBMS.Core.ErrorControlSystem.Exeption.DataBaseExeption
{
    public sealed class SaveError
        : DBExeption
    {


#if DocTrue
        static SaveError() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "SaveError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"


            );
#endif

        public SaveError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)(ErrorCodes.SAVE_ERROR)];

    }
}
