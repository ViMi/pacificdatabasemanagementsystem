﻿using DBMS.Core.Helpers;



namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class TableNotExist
        : DBExeption
    {
#if DocTrue
        static TableNotExist() => Documentation.AddHistoryToSection(
            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "TableNotExist",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"


            );
#endif

        public TableNotExist() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)(ErrorCodes.TABLE_NOT_FOUND)];

    }
}
