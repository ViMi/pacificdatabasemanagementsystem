﻿using DBMS.Core.Helpers;



namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class ParserControllSecureError 
        : DBExeption
    {

#if DocTrue
        static ParserControllSecureError() => Documentation.AddHistoryToSection(

                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "PArserControllSecureError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"

            );
#endif

        public ParserControllSecureError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)(ErrorCodes.PARSE_CONTROL_SECURITY_ERROR)];

    }
}
