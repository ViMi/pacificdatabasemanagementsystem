﻿using DBMS.Core.Helpers;



namespace DBMS.Core.ErrorControlSystem.Exeption.DataBaseExeption
{
    public sealed class RollbackError 
        : DBExeption
    {


#if DocTrue
        static RollbackError() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "RollbackError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"


            );
#endif
        public RollbackError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)(ErrorCodes.ROLLBACK_ERROR)];


    }
}
