﻿using DBMS.Core.Helpers;


namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class DataMappingError
        : DBExeption
    {

#if DocTrue
        static DataMappingError() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "DataMappingError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"

            );
#endif


        public DataMappingError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)(ErrorCodes.DATA_MAPPING_ERROR)];

    }
}
