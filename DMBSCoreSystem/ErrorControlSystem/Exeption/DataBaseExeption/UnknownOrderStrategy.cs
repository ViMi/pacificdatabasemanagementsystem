﻿using DBMS.Core.Helpers;



namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class UnknownOrderStrategy 
        : DBExeption
    {

#if DocTrue
        static UnknownOrderStrategy() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "UnknownOrderStrategy",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"


            );
#endif

        public UnknownOrderStrategy() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)ErrorCodes.UNKNOWN_ORDER_STRATEGY];

    }
}
