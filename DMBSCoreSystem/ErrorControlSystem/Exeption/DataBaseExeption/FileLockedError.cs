﻿using System;
using DBMS.Core.Helpers;


namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class FileLockedError
        : DBExeption
    {

#if DocTrue
        static FileLockedError() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "FileLockedError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"


            );
#endif


        public FileLockedError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.FILE_LOCKED];
    }

}
