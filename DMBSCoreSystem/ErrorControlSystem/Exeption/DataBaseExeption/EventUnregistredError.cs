﻿using System;
using DBMS.Core.Helpers;



namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class EventUnregistredError
        : DBExeption
    {

#if DocTrue
        static EventUnregistredError() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "EventUnregisteredError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"


            );
#endif

        public EventUnregistredError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.UNREGESTED_EVENT];

    }
}
