﻿using DBMS.Core.Helpers;



namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class ServerRestoredBecauseOfError
        : DBExeption
    {

#if DocTrue
        static ServerRestoredBecauseOfError() => Documentation.AddHistoryToSection(

                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "ServerRestoredBecauseOfError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"

            );
#endif

        public ServerRestoredBecauseOfError() =>
              this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)(ErrorCodes.SERVER_WAS_RESTORED_BECAUSE_OF_ERROR_IN_DEFAULT)];

    }
}
