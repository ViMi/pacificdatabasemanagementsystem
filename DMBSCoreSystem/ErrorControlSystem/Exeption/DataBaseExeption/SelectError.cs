﻿using DBMS.Core.Helpers;


namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class SelectError
         : DBExeption
    {

#if DocTrue
        static SelectError() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "SelectError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"

            );
#endif

        public SelectError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)(ErrorCodes.SELECT_ERROR)];

    }
}
