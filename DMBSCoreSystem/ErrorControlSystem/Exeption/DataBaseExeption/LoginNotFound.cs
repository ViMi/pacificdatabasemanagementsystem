﻿using DBMS.Core.Helpers;



namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class LoginNotFound
        : DBExeption
    {

#if DocTrue
        static LoginNotFound() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "LogiNotFound",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"

            );
#endif

        public LoginNotFound() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)ErrorCodes.LOGIN_NOT_FOUND];
    }
}
