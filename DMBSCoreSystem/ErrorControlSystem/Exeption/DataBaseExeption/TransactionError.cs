﻿using DBMS.Core.Helpers;



namespace DBMS.Core.ErrorControlSystem.Exeption.DataBaseExeption
{
    public sealed class TransactionError
        : DBExeption
    {

#if DocTrue
        static TransactionError() => Documentation.AddHistoryToSection(

                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "TransactionError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"


            );
#endif


        public TransactionError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)(ErrorCodes.TRANSACTION_ERROR)];

    }
}
