﻿using DBMS.Core.Helpers;



namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class SkipNegativeError
        : DBExeption
    {


#if DocTrue
        static SkipNegativeError() => Documentation.AddHistoryToSection(

                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "SkipNegativeError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"

            );
#endif

        public SkipNegativeError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)(ErrorCodes.SKIP_NEGATIVE_ERROR)];

    }
}
