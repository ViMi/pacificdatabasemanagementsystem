﻿using System;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.Helpers;


namespace DBMS.Core.ErrorControlSystem.Exeption
{
    /// <summary>
    /// This class present DataBase base Exceprition
    /// </summary>
    public class DBExeption
        : Exception
    {

        #region Documentationa
#if DocTrue
        static DBExeption() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "DBException",
            PQLDocGlobal.TypeClass,
            PQLDocGlobal.DescriptionUndefined,
            "Exception"


            );
#endif
        #endregion

        public DBExeption() { }

        public DBExeption(String message) => ServerMessageError = message;

        public String ServerMessageError { set; get; } = ErrorMessagesCore.MessageDictionary[(System.Byte)ErrorCodes.FAILURE_EXECUTE];


    }
}
