﻿using DBMS.Core.Helpers;



namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class ILContextClosedError
        : DBExeption
    {


#if DocTrue
        static ILContextClosedError() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "ILContextClosedError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"


            );
#endif

        public ILContextClosedError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)(ErrorCodes.INTERMEDIATE_LANGUAGE_CONTEXT_CLOSED)];

    }
}
