﻿using DBMS.Core.Helpers;



namespace DBMS.Core.ErrorControlSystem.Exeption { 
    public sealed class ViewNotExist
        : DBExeption
    {

#if DocTrue
        static ViewNotExist() => Documentation.AddHistoryToSection(
                        PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "ViewNotExist",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"

            );
#endif


        public ViewNotExist() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)(ErrorCodes.VIEW_NOT_EXIST)];
    }

}
