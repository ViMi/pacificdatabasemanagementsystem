﻿using System;
using DBMS.Core.Helpers;



namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class SelectCountItemsError
        : DBExeption
    {


#if DocTrue
        static SelectCountItemsError() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "SelectCountItemsError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"

            );
#endif


        public SelectCountItemsError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SELECT_COUNT_ITEMS_ERROR];

    }
}
