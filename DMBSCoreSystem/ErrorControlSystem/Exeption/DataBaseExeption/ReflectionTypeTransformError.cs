﻿using DBMS.Core.Helpers;


namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class ReflectionTypeTransformError
         : DBExeption
    {

#if DocTrue
        static ReflectionTypeTransformError() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "ReflectionTypeTransformationError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"


            );
#endif

        public ReflectionTypeTransformError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)(ErrorCodes.REFLECTION_TYPE_TRANSAFORM_ERROR)];

    }
}
