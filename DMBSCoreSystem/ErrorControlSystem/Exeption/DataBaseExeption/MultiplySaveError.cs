﻿using System;
using DBMS.Core.Helpers;



namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class MultiplySaveError
        : DBExeption
    {

#if DocTrue
        static MultiplySaveError() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "MultiplySaveError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"

            );
#endif

        public MultiplySaveError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.MULTIPLY_SAVE_ERROR];

    }
}
