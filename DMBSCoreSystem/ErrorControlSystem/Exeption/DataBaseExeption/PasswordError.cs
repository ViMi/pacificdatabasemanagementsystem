﻿using DBMS.Core.Helpers;


namespace DBMS.Core.ErrorControlSystem.Exeption.DataBaseExeption
{
    public sealed class PasswordError 
        : DBExeption
    {

#if DocTrue
        static PasswordError() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "PasswordError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"


            );
#endif

        public PasswordError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)(ErrorCodes.PASSWORD_ERROR)];

    }
}
