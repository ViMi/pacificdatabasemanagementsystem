﻿using DBMS.Core.Helpers;


namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class ExecutorControlSecurityError
        : DBExeption
    {

#if DocTrue
        static ExecutorControlSecurityError() => Documentation.AddHistoryToSection(

                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "ExecutorControlSecurotyError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"

            );
#endif

        public ExecutorControlSecurityError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)(ErrorCodes.EXECUTE_CONTROL_SECURITY_ERROR)];


    }
}
