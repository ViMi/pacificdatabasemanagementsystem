﻿using DBMS.Core.Helpers;


namespace DBMS.Core.ErrorControlSystem.Exeption
{
    public sealed class StoredProcedureExecutionError 
        :DBExeption
    {

#if DocTrue
        static StoredProcedureExecutionError() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "StoredProcedureExecutionError",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "DBExeption"


            );
#endif

        public StoredProcedureExecutionError() => this.ServerMessageError = ErrorMessagesCore.MessageDictionary[(System.Byte)(ErrorCodes.STORED_PROCEDURE_EXECUTION_ERROR)];

    }
}
