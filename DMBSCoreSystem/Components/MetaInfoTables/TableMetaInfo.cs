﻿using DBMS.Core.Secure;
using DBMS.Core.Helpers;



namespace DBMS.Core.MetaInfoTables
{


    public sealed class TableMetaInfo 
        : IMetaInfo
    {

        #region Documentation
#if DocTrue
        static TableMetaInfo()
        {
            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreComponents,
            "TableMetaInfo",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "IMetaInfo"
);


            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreComponents,
            "IMetaInfo",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined
);

        }
#endif
        #endregion
        /// <summary>
        /// Contains UserName
        /// </summary>
        public System.String NodeName { set; get; }

        /// <summary>
        /// Contains List of database what own
        /// </summary>
        public System.String ChildrenList { set; get; }

        /// <summary>
        /// Contains Password about user
        /// </summary>
        public Password NodeAccessPassword { set; get; }

        public NodeType Type { get; set; }
        public System.String Parent { get; set; }
        public System.String Owner { get; set; }

    }
}
