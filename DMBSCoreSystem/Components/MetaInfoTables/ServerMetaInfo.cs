﻿
using DBMS.Core.Secure;
using DBMS.Core.Helpers;


namespace DBMS.Core.MetaInfoTables
{
    public sealed class ServerMetaInfo
        : IServerMetaInfo 
    {


        #region Documentation
#if DocTrue
        static ServerMetaInfo()
        {
            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreComponents,
            "ServerMetaInfo",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "IServerMetaInfo"
);


            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreComponents,
            "IServerMetaINfo",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined
);


        }
#endif
        #endregion

        public System.String ServerName { get; set; }
        public System.String LastSeenDate { get; set; }
        public System.String CurrentSeenDate { get; set; }
        public ServerType ServerType { get; set; }
        public System.String ServerOwner { get; set; }
        public System.String NodeName { get; set; }
        public System.String ChildrenList { get; set; }
        public Password NodeAccessPassword { get; set; }
        public NodeType Type { get; set; }
        public System.String Parent { get; set; }
        public System.String Owner { get; set; }
    }
}
