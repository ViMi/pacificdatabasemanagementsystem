﻿using DBMS.Core.Helpers;

using DBMS.Core.Secure;

namespace DBMS.Core.MetaInfoTables
{
    public sealed class DataBaseMetaInfo
        : IMetaInfo
    {

        #region Documentation
#if DocTrue
        static DataBaseMetaInfo() => Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreComponents,
            "DataBaseMetaInfo",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "IMetaInfo"
);
#endif
        #endregion


        /// <summary>
        /// Contains UserName
        /// </summary>
        public System.String NodeName { set; get; }

        /// <summary>
        /// Contains List of database what own
        /// </summary>
        public System.String ChildrenList { set; get; }

        /// <summary>
        /// Contains Password about user
        /// </summary>
        public Password NodeAccessPassword { set; get; }
        public NodeType Type { get; set; }
        public string Parent { get; set; }
        public string Owner { get; set; }
    }
}
