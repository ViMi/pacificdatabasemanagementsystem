﻿using DBMS.Core.Interprater;
using DBMS.Core.MMIL;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.Table;
using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.Secure;
using DBMS.Core.Helpers;

using DBMS.Core.InfoTypes;



namespace DBMS.Core.MetaInfoTables
{
    public sealed class TableRecordMetaInfo
        : ITableInfo
    {

        #region Documentaiton
#if DocTrue
        static TableRecordMetaInfo()
        {
            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreComponents,
            "TableRecordMetaInfo",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "ITableInfo"
);


            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreComponents,
            "ITableInfo",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined
);

        }
#endif
        #endregion

        public void Reset()
        {
            NodeName = null;
            ChildrenList = null;
            NodeAccessPassword = null;
            Type = NodeType.TABLE;
            Parent = null;
            Owner = null;
            ServerName = null;
            DataBaseName = null;
            TableName = null;
            RecordsCount = 0;
            ColumnConstraint = null;
            ColumnTypes = null;
            FK = null;
            PK = null;
        }


        #region Variable Declaration
        public System.String NodeName { set; get; }
        public  System.String ChildrenList { set; get; }
        public  Password NodeAccessPassword { set; get; }
        /// <summary>
        /// Contains Type of node
        /// </summary>
        public NodeType Type { set; get; } = NodeType.TABLE;

       public System.String Parent { set; get; }
       public System.String Owner { set; get; }

        public ITransactionMethodResult _CurrentTI { set; get; }
        public System.String ServerName { get; set; }
        public System.String DataBaseName { get; set; }
        public System.String TableName { get; set; }
        public System.Int32 RecordsCount { get; set; }
        public System.Int32  FileCount { get; set; }
        public System.Int32 RowCount { get; set; }
        public System.Boolean IsIndexed { get; set; }
        public Dictionary<string, Dictionary<string, string>> ColumnConstraint { get; set ; }
        public Dictionary<System.String, PacificDataBaseDataType> ColumnTypes { set; get; }

        public List<String> FK { set; get; }
        public String PK { set; get; }
        #endregion


        public void Append(string value) {throw new NotImplementedException();}

        public void Append(object obj) { throw new NotImplementedException(); }

        public object Clone() => new TableRecordMetaInfo()
        {
            ServerName = this.ServerName,
            DataBaseName = this.DataBaseName,
            TableName = this.TableName,
            RecordsCount = this.RecordsCount,
            FileCount = this.FileCount,
            RowCount = this.RowCount
        };
        public string GetExecuteResult(){ throw new NotImplementedException();}

        public object GetExecuteResultObject() => this;

        public string GetParseResult() => null;

        public Dictionary<string, string> GetParseResultDictionary() { throw new NotImplementedException();  }

        public List<Dictionary<string, string>> GetParseResultListDictionary() { throw new NotImplementedException();}

        public List<UnityType> GetParseResultType(){ throw new NotImplementedException(); }

        public List<MMILDataBaseContext> GetTranslationUnits() { throw new NotImplementedException(); }

        public void SetExecuteResult(string executeResult) {throw new NotImplementedException(); }

        public void SetExecuteResult(object executeResult) { }
    }
}
