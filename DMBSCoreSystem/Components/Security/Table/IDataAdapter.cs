﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBMS.Core.Table
{
    public interface IDataAdapter
    {

        Object GetData();
        void SetData(Object Obj);

    }
}
