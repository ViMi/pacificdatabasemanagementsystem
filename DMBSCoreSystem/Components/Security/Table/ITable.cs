﻿using DBMS.Core.Interprater;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBMS.Core.InfoTypes;
using DBMS.Core.MMIL;

namespace DBMS.Core.Table
{
    public interface ITable
        : IParseResult
    {
        Dictionary<System.String, TableRow> TableRow { set; get; }


        ITable Distinct();
        ITable Reverse();
        ITable OrderByDesc(System.String aliasList);
        ITable OrderByAsc(System.String aliasList);

    }
}
