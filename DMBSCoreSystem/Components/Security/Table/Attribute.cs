﻿using System;
using DBMS.Core.Helpers;



namespace DBMS.Core.Table
{
    public sealed class Attribute
        : IAttribute
    {
        #region Documentation
#if DocTrue
        static Attribute()
        {
            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreTable,
            "Attribute",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "IAttrbute"
);


            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "IAttribute",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined
);
        }
#endif
        #endregion

        public Attribute(AttributeType type, System.String data)
        {
            this.AttributeData = data;
            this.AttrType = type;
        }

        public Attribute() { }

        public AttributeType AttrType { get; set; }
        public String AttributeData { get; set; }
    }
}
