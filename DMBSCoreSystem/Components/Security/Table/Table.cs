﻿using DBMS.Core.Interprater;
using DBMS.Core.MMIL;
using System.Collections.Generic;
using DBMS.Core.Helpers;
using System.Linq;

namespace DBMS.Core.Table
{
    public sealed class Table
        : ITable
    {

        #region Documentation
#if DocTrue
        static Table()
        {
            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreTable,
            "Table",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "ITable"
);


            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "ITable",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined
);

        }
#endif
        #endregion

        public Dictionary<System.String, TableRow> TableRow  { get; set; }


        public object Clone() { throw new System.NotImplementedException(); }

        public string GetParseResult() { throw new System.NotImplementedException(); }

        public Dictionary<string, string> GetParseResultDictionary() { throw new System.NotImplementedException(); }

        public List<Dictionary<string, string>> GetParseResultListDictionary() {throw new System.NotImplementedException();}

        public List<UnityType> GetParseResultType()   { throw new System.NotImplementedException();}

        public List<MMILDataBaseContext> GetTranslationUnits() {  throw new System.NotImplementedException();}


        public ITable Distinct()
        {
            ITable res = new Table();
            res.TableRow = new Dictionary<string, TableRow>();


            for(System.Int32 i = 0; i < TableRow.Count; ++i)
            {
                TableRow tr = TableRow.ElementAt(i).Value;

                for(System.Int32 j = 0; j < TableRow.Count; ++j)
                {
                    if (j == i) continue;

                    System.Int32 eq = 0;
                    TableRow inner = TableRow.ElementAt(j).Value;
                    
                    for(System.Int32 k = 0; k < inner.RowCells.Count; ++k)
                    {
                        TableCell tc = tr.RowCells.ElementAt(k);
                        TableCell tcInner = inner.RowCells.ElementAt(k);
                        if (tc.CompareTo(tcInner) != 0) eq++;
                    }

                    if (eq != inner.RowCells.Count)
                        res.TableRow.Add(res.TableRow.Count.ToString(), inner);
                }

                
            }


            return res;
        }


        public ITable Reverse()
        {
            ITable res = new Table();
            res.TableRow = new Dictionary<string, Core.Table.TableRow>();
          
            foreach (TableRow tr in this.TableRow.Values)
                res.TableRow.Add(res.TableRow.Count.ToString(), tr.Clone() as TableRow);
            

            ITable res2 = new Table();
            res2.TableRow = new Dictionary<string, Core.Table.TableRow>();

            for (System.Int32 i = TableRow.Count-1; i >= 0; --i)
                res2.TableRow.Add(res2.TableRow.Count.ToString(), res.TableRow.ElementAt(i).Value);



                return res2;

        }



        public ITable OrderByAsc(System.String aliasList) {

            ITable res = new Table();
            res.TableRow = new Dictionary<string, TableRow>();

            foreach (TableRow tr in this.TableRow.Values)
                res.TableRow.Add(res.TableRow.Count.ToString(), tr.Clone() as TableRow);

            System.String[] aliases = aliasList.Split(' ');

          

            foreach (System.String alias in aliases)
            {
                if (alias == System.String.Empty)
                    continue;

                System.Boolean flag = true;

                if (TableRow.Count <= 1) break;
                while (flag)
                {
                    for (System.Int32 i = 0; i < res.TableRow.Count - 1; ++i)
                    {
                        TableRow tr = res.TableRow.ElementAt(i).Value;
                        TableRow inner = null;
                        System.String key = res.TableRow.ElementAt(i).Key;
                        System.String keyInner = null;


                        for (System.Int32 j = i + 1; j < res.TableRow.Count; ++j)
                        {
                            inner = res.TableRow.ElementAt(j).Value;
                            keyInner = res.TableRow.ElementAt(j).Key;
                            for (System.Int32 k = 0; k < inner.RowCells.Count; ++k)
                            {
                                TableCell tc = tr.RowCells.ElementAt(k);
                                TableCell tcInner = inner.RowCells.ElementAt(k);
                                if (tc.Alias == alias)
                                {
                                    if (tc.CompareTo(tcInner) == 1)
                                    {
                                        res.TableRow.Remove(key);
                                        res.TableRow.Remove(keyInner);

                                        res.TableRow.Add(keyInner, tr);
                                        res.TableRow.Add(key, inner);
                                        flag = true;
                                        break;
                                    }
                                    else flag = false;
                                }
                            
                            }

                            if (flag) break;
                        }


                    }

                }
            }



            return res;
        }


        public ITable OrderByDesc(System.String aliasList) {


            ITable res = this.OrderByAsc(aliasList);
            var reversed = res.TableRow.Reverse();
            res.TableRow = new Dictionary<string, TableRow>();
            foreach (var item in reversed)
                     res.TableRow.Add(item.Key, item.Value);

            return res;

        }


    }
}
