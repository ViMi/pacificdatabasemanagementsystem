﻿using System;
using System.Collections.Generic;

namespace DBMS.Core.Table
{


    public enum PacificDataBaseDataType
        : System.Byte
    {
        NUMBER = 0,
        STRING = 1,
        INCLUDED_TABLE = 2,
    };

    public interface ITableRow
    {
 
        List<TableCell> RowCells { set; get; }
       
    }



    public interface ITableCell
    {
        System.String ColumnData { set; get; }
      //  IDataAdapter ColumnData { set; get; }
        System.String Alias { set; get; }

       PacificDataBaseDataType DataType { set; get; }
   //    List<Attribute> Attributes { set; get; }

    }
}
