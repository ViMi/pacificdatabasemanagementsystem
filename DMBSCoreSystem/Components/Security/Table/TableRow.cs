﻿using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.Table;
using System;
using System.Collections.Generic;
using DBMS.Core.Helpers;



namespace DBMS.Core.Table
{
    public sealed class TableRow 
        : ITableRow
        , IComparable
        , ICloneable
    {

        #region Documentation
#if DocTrue
        static TableRow()
        {
            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "TableRow",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "ITableRow"
);


            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreTable,
            "ITableRow",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined
);
        }
#endif
        #endregion
        public List<TableCell> RowCells { get; set; }

        public Int32 CompareTo(Object obj)
        {
            TableRow tr = obj as TableRow;
            if (tr != null)
            {
                Int32 res = 0;
                foreach (TableCell tc in RowCells)
                    foreach (TableCell tcInner in tr.RowCells)
                        res = tc.CompareTo(tcInner);

                return res;
            }
            else throw new Exception("Uncomparable value");
        }



        public Object Clone()
        {
            TableRow tmp = new TableRow();
            List<TableCell> tmpTc = new List<TableCell>();

            foreach (TableCell tc in this.RowCells)
                tmpTc.Add(new TableCell() { Alias = tc.Alias, ColumnData = tc.ColumnData, DataType = tc.DataType });

            tmp.RowCells = tmpTc;

            return tmp;

        }


    }






    public sealed class TableCell
        : ITableCell
        , IComparable
    {

        #region Documentation
#if DocTrue
        static TableCell()
        {
            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreTable,
            "TableCell",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "ITableCell"
);


            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreTable,
            "ITableCell",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined
);

        }
#endif
        #endregion

        public String ColumnData { get; set; }
     //   public IDataAdapter ColumnData { get; set; }
        public String Alias { get; set; }
        public PacificDataBaseDataType DataType { get; set; }
    //    public List<Attribute> Attributes { get; set; }

        public Int32 CompareTo(Object obj)
        {
            TableCell tc = obj as TableCell;

            if (tc != null)
            {
                if (tc.DataType == PacificDataBaseDataType.NUMBER) {
                    if (this.DataType == PacificDataBaseDataType.NUMBER)
                    {
                        Int32 res = Int32.Parse(this.ColumnData).CompareTo(Int32.Parse(tc.ColumnData));
                        return res;
                    }
                    else throw new Exception("Uncomparable value");
                }

                else if (tc.DataType == PacificDataBaseDataType.STRING) {
                    if (this.DataType == PacificDataBaseDataType.STRING)
                    {
                        String left = this.ColumnData.Trim().Replace("\"", String.Empty);
                        String right = tc.ColumnData.Trim().Replace("\"", String.Empty);
                        Int32 res = left.CompareTo(right);
                        return res;
                    }
                    else throw new Exception("Uncomparable value");
                } else throw new Exception("Uncomparable value");

            }
            else throw new Exception("Uncomparable value");
        }
    }
}
