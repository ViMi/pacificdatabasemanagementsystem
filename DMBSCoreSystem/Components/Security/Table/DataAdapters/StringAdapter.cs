﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBMS.Core.Table
{
    public sealed class StringAdapter
        : IDataAdapter
    {

        private System.String Str;

        public System.Object GetData() => Str;
        public void SetData(System.Object obj) => Str = obj as String;
    }
}
