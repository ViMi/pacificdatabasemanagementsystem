﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBMS.Core.Table
{
    public sealed class NumberAdapter
        : IDataAdapter
    {

        private Int32? Number;
        public System.Object GetData() => Number;
        public void SetData(System.Object obj) => Number = obj as Int32?;
    }
}
