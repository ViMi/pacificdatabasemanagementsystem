﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBMS.Core.Table
{
    public sealed class TableAdapter
        : IDataAdapter
    {


        private ITable Table;

        public System.Object GetData() => Table;
        public void SetData(System.Object obj) => Table = obj as ITable;
    }
}
