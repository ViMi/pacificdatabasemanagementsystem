﻿using DBMS.Core.Secure;
using DBMS.Core.Helpers;



namespace DBMS.Core.Components.Security.Decoders
{
    public sealed class PlaneTextCriptex
        : IEncriptor
    {


#if DocTrue
        static PlaneTextCriptex()
        {
            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreSecurity,
            "PlaceTextCriptex",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "IEncriptor"
);


            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreSecurity,
            "IEncriptor",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined
);

        }
#endif

        public EncriptionType type { get { return EncriptionType.PlaneText; } }

        public string Decript(string password) => password;

        public string Encript(string password) => password;
    }
}
