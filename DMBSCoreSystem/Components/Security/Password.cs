﻿
using DBMS.Core.Helpers;


namespace DBMS.Core.Secure
{


    public sealed class Password
        : IPassword
    {
        #region Documentation
#if DocTrue
        static Password() { 
            
            Documentation.AddHistoryToSection(
                PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreSecurity,
            "Password",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "IPassword"
); 

            Documentation.AddHistoryToSection(
                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreSecurity,
            "IPassword",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined
                ); 
        
        }
#endif
        #endregion


        public Password() { }


        public Password(EncriptionType type, System.String password)
        {
            Encription = type;
            _Password = password;
        }

        public System.String _Password { set; get; }

        public System.String Decript() => (Cript.Decoder != null ? _Password = Cript.Decript(_Password) : (_Password = Cript.Decript(_Password, Encription)));
        public System.Boolean Encript() => (Cript.Decoder != null ?_Password=Cript.Encript(_Password) :(_Password = Cript.Encript(_Password, Encription))) != null;
        public EncriptionType Encription { set; get; }


    }
}
