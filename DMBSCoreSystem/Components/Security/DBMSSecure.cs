﻿using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.Helpers;


namespace DBMS.Core.Secure
{


    public enum RandomStringType
        : System.Byte
    {
        ONLY_CAPITAL = 0,
        ONLY_SMALL = 1,
        ONLY_SPECIAL_SYMBOLS = 2,
        COMBINED_LETTERS = 3,
        COMBINED_ALL = 4,
    }

    public static class DBMSSecure
    {

        #region Documentation
#if DocTrue
        static DBMSSecure()
        {
            Documentation.AddHistoryToSection(

               PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreSecurity,
            "DBMSSecure",
            PQLDocGlobal.TypeStaticClass,
            PQLDocGlobal.DescriptionUndefined


                );



            Documentation.AddHistoryToSection(
                                        PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreSecurity,
            "RundomStringType",
            PQLDocGlobal.TypeEnum,
            PQLDocGlobal.DescriptionUndefined

                );

        }
#endif
        #endregion


        private static List<Char> _Letters = new List<Char>()
        {
          'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q',
          'R','S','T','U','V','W','X','Y','Z',

          'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q',
          'r','s','t','u','v','w','x','y','z'
        };

       

        public static String GetRandomString
            (Int32 length = 32, RandomStringType type = RandomStringType.COMBINED_LETTERS) 
        {
            Random rand = new Random();
            String word = String.Empty;
            for(Int32 i =0; i < length; ++i)
            {
                Int32 symbol = rand.Next(0, _Letters.Count - 1);
                word += _Letters[symbol];
            }

            return word;        
        
        }



    }
}
