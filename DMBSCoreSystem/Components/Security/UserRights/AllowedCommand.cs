﻿using DBMS.Core.Secure.UserRights.RightTable;
using System.Collections.Generic;
using DBMS.Core.Interprater;
using DBMS.Core.Helpers;

using System;
using DBMS.Core.InfoTypes;


namespace DBMS.Core.Secure.UserRights
{
    public sealed class AllowedCommand 
        : IAllowedCommand
        , IExecuteResult
    {


        #region Documentation
#if DocTrue
        static AllowedCommand()
        {
            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreSecurity,
            "ViewNotExist",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "IAllowedCommand  IExecuteResult"
);

            Documentation.AddHistoryToSection(
                                        PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreSecurity,
            "IAllowedCommand",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined

                );


            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreGlobalInterfaces,
            "IExecuteResult",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined
);

        }
#endif
        #endregion

        public void Reset() => this.AllowCommand = null;
        public AllowedCommand() { }

        public AllowedCommand(Dictionary<System.String, CommandRight> allowed) => this.AllowCommand = allowed;

        public Dictionary<System.String, CommandRight> AllowCommand { set; get; }


        public ITransactionMethodResult _CurrentTI { set; get; }
        public void Append(String value) { throw new System.NotImplementedException();}

        public void Append(Object obj) { throw new System.NotImplementedException(); }

        public Object Clone() { throw new System.NotImplementedException();}

        public String GetExecuteResult() { throw new System.NotImplementedException();}

        public Object GetExecuteResultObject() { throw new System.NotImplementedException(); }

        public void SetExecuteResult(String executeResult) { throw new System.NotImplementedException();}

        public void SetExecuteResult(Object executeResult) {  throw new System.NotImplementedException();}
    }
}
