﻿using DBMS.Core.Secure.UserRights;
using DBMS.Core.Secure.UserRights.RightTable;
using System.Collections.Generic;
using DBMS.Core.Helpers;



namespace DMBS.Core.Secure.UserRights.RightTable
{
    public sealed class AllowedCommandWithDB 
        : IAllowedCommandOnObject
    {


        #region Documentation
#if DocTrue
        static AllowedCommandWithDB()
        {
            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreSecurity,
            "AllowedCommandWithDB",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "IAllowedCommandOnObject"
);


            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreSecurity,
            "IAllowedCommandOnObject",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined
);

        }
#endif
        #endregion
        public AllowedCommandWithDB()  { }

        public AllowedCommandWithDB(Dictionary<System.String, AllowedCommand> allowed) => this.AllowedCommandOn = allowed;

        public Dictionary<System.String, AllowedCommand> AllowedCommandOn { set; get; }
    }
}
