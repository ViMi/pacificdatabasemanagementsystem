﻿using DBMS.Core.Secure.UserRights.RightTable;
using System.Collections.Generic;
using DBMS.Core.Helpers;



namespace DBMS.Core.Secure.UserRights
{
     public sealed class CommandRight 
        : ICommandRight
    {

        #region Docuemntaiton
#if DocTrue
        static CommandRight() { 


            Documentation.AddHistoryToSection(
                PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreSecurity,
            "CommandRight",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "ICommandRight"
);


            Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreSecurity,
            "ICommandRight",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined
);
        
        
        }
#endif
        #endregion

        public CommandRight() { }

        public CommandRight(Dictionary<System.String, System.Boolean> allowed) => this.CommandRights = allowed;
        public Dictionary<System.String, System.Boolean> CommandRights { set; get; }

    }
}
