﻿using System;
using System.Collections.Generic;
using DBMS.Core.Secure;
using DBMS.Core.Secure.UserRights.RightTable;
using DMBS.Core.Secure.UserRights.RightTable;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.IO;
using DBMS.Core.Helpers;



namespace DBMS.Core.User
{
    public sealed class User 
        : IUserToken
    {

        #region Documentation
#if DocTrue
        static User() {


            Documentation.AddHistoryToSection(

                                        PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreSecurity,
            "User",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "IUserToken"

                );
        
        }
#endif
        #endregion



        /// <summary>
        /// Contains UserName
        /// </summary>
        public System.String NodeName { set; get; }

        /// <summary>
        /// Contains List of database what own
        /// </summary>
        public System.String ChildrenList { set; get; }

        /// <summary>
        /// Contains Password about user
        /// </summary>
        public Password NodeAccessPassword { set; get; }
      //  public IPassword NodeAccessPassword { set; get; }
      //  public IAllowedCommandOnObject UserRights { set; get; }
        public AllowedCommandWithDB UserRights { set; get; }


        public NodeType Type { get; set; }
        public string Parent { get; set; }
        public string Owner { get; set; }



        /// <summary>
        /// Create User
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="ChildList"></param>
        /// <param name="parent"></param>
        /// <param name="owner"></param>
        /// <param name="password"></param>
        /// <param name="allowed"></param>
        public User
            (System.String userName,
            System.String ChildList,
            System.String parent,
            System.String owner,
            Password password, 
            AllowedCommandWithDB allowed)
        {

            this.NodeName = userName;
            this.ChildrenList = ChildList;
            this.Parent = parent;
            this.Owner = owner;
            this.NodeAccessPassword = password;
            this.UserRights = allowed;
            this.Type = NodeType.USER;
        }


        public User(){ }

        public System.Boolean IsUserWorkWith(System.String checkKey) => this.UserRights.AllowedCommandOn.TryGetValue(checkKey, out _);

        public System.Boolean GetDataBaseAccess(System.String dataBaseName)
        {
            System.String[] db = this.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator);

            foreach (System.String i in db)
                if (i == dataBaseName)
                    return true;

            return false;
        }




        public IAllowedCommand GetTableRight(System.String tableName, System.String db)
        {

            AllowedCommand rights = null;
            if (this.UserRights.AllowedCommandOn.TryGetValue(db, out rights))
                return rights;
        
            throw new Exception();
             
        }



        public Dictionary<System.String, IAllowedCommand> GetTableRightForEach(System.String[] tableNames, System.String db)
        {
            Dictionary<System.String, IAllowedCommand> rightsForEach
                = new Dictionary<string, IAllowedCommand>(10);

            for (System.Int32 i = 0; i < tableNames.Length; ++i)
                rightsForEach.Add(tableNames[i], this.GetTableRight(tableNames[i], db));
            

            return rightsForEach;

        }


        public System.String GetUserLogin() => this.NodeName;
        public IPassword GetUserPassword() => this.NodeAccessPassword;

    }
}
