﻿using DBMS.Core.Secure;
using System.Collections.Generic;
using DBMS.Core.Helpers;



namespace DBMS.Core.User
{
    public sealed class Login
        : ILogin
    {
        #region Documentation
#if DocTrue
        static Login() => Documentation.AddHistoryToSection(
                                    PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreSecurity,
            "Login",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "ILogin"


            );
#endif
        #endregion


        #region Varialbe Declaration
        public Dictionary<System.String, NodeType> LoginRights { get; set; }
        public System.String NodeName { get; set; }
        public System.String ChildrenList { get; set; }
        public Password NodeAccessPassword { get; set; }
        public NodeType Type { get; set; } = NodeType.LOGIN;
        public System.String Parent { get; set; }
        public System.String Owner { get; set; }
        #endregion


        public System.String GetUserLogin() => this.NodeName;
        public IPassword GetUserPassword() => this.NodeAccessPassword;
    }
}
