﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBMS.Core
{


    public enum DBMSMode
        : System.Byte
    {
        STANDART_MODE = 0,
        WITHOUT_LNG_CONTROL = 1,
        UNSAFE_MODE = 2
    };



    internal static class DBMSModes
    {

        private static DBMSMode CurrentMode = DBMSMode.STANDART_MODE;

        public static void SetDBMSMode(DBMSMode mode) => DBMSModes.CurrentMode = mode;
        public static DBMSMode GetDBMSMode() => DBMSModes.CurrentMode;

    }
}
