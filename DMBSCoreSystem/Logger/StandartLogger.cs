﻿using System;
using System.Text;
using System.IO;
using DBMS.Core.Helpers;
using DBMS.Core.ErrorControlSystem.Exeption;

namespace DBMS.Core
{
    public sealed class StandartLogger
       : ILogger
    {

        #region Documentation
#if DocTrue
        static StandartLogger()

        { 
            Documentation.AddHistoryToSection
            (

                        PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreLoggers,
            "StandartLogger",
            PQLDocGlobal.TypeClass,
            PQLDocGlobal.DescriptionUndefined,
            "ILogger"

            );


            Documentation.AddHistoryToSection(

                            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreGlobalInterfaces,
            "ILogger",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined
                );
         }
#endif
        #endregion


        private System.String CurrentLogContent = String.Empty;
        public System.String PathToLog { get; }


        public StandartLogger(System.String pathToLogFile)
        {
            this.PathToLog = pathToLogFile;
            if (!this.IsLogExist())
                this.CreateLogFile();

            this.AppendInfoToLog(DateTime.Now.ToLongDateString());
        }




        public void Reset() => this.CurrentLogContent = String.Empty;

        public override string ToString()
        {
            String content = String.Empty;

            try
            {
                using (StreamReader sr = new StreamReader(this.PathToLog))
                    content = sr.ReadToEnd();
                
            } catch (Exception e)
            {
                if (e.IsFileLocked())
                    ToString();  
                
            }

            return content;

        }



        public void CreateLogFile() => File.Create(this.PathToLog);
        
        public void AppendInfoToLog(System.String info) => this.CurrentLogContent += info + Environment.NewLine;

        public System.Boolean IsLogExist() => File.Exists(this.PathToLog);


        public void Flush()
        {
            try
            {
                using (FileStream fs = new FileStream(this.PathToLog, FileMode.Append))
                {
                    System.Byte[] arr = Encoding.UTF8.GetBytes(this.CurrentLogContent);
                    fs.Write(arr, 0, arr.Length);
                }

            } catch(Exception e)
            {
                if(e.IsFileLocked())
                    return;
                
            }

        }

    }
}
