﻿using System;
using System.Collections.Generic;
using System.IO;
using DBMS.Core.InfoTypes;
using DBMS.Core.Interprater;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.TableCollections;

using DMBS.Core.Secure.UserRights.RightTable;
using DBMS.Core.UsersFromRoles;
using DBMS.Core.Secure;
using DBMS.Core.MMIL;
using DBMS.Core.Helpers;
using static DBMS.Core.DBMSEventControlSystem;
using DBMS.Core.Journal;
using DBMS.Core.IO.InternalComponents;
using DBMS.Core.Table;

namespace DBMS.Core.IO
{

    public static class DBMSIO
    {

        #region Documentation
#if DocTrue
        static DBMSIO() => Documentation.AddHistoryToSection(
            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreIO,
            "DBMSIO",
            PQLDocGlobal.TypeStaticClass,
            PQLDocGlobal.DescriptionUndefined

            );

#endif
        #endregion



        #region Variable Documentation
        public static System.Boolean VIEW_TABLE = false;
        private static Dictionary<System.String, IUserToken> _UserTokins { set; get; } = new Dictionary<String, IUserToken>(1);
        public static IDBMSConfig DBMSConfig { set; get; }
        public static IServerMetaInfo ServerMetaInformation { set; get; }
        public static IOIntegrateDBMS IOIntegrationModule { set; get; }
        public static ILogger Logger { set; get; }
        public static IServerJournize ServerJournal { set; get; }
        #endregion


        #region Server Connect & Check Methods
        public static System.String ServerCheck()
        {
            String res = String.Empty;

            if (!IOIntegrationModule.IsServerExist(DBMSConfig.BaseDir))
            {
                IOIntegrationModule.RestoreServer();
                res = (new ServerRestoredBecauseOfError()).ServerMessageError;
            }

            ServerMetaInformation = DBMSIO.GetServerMetaInfo();
            DBMSIO.RenewServerDate();

            return res;

           
        }

        public static System.Boolean ServerConnect(System.String serverName) 
        {
           if(File.Exists(DBMSConfig.BaseDir))
                return true;

            throw new ServerNotExist();
        }
        private static void RenewServerDate()
        {
            DBMSIO.IOIntegrationModule.SetNewPrevSeenDate(DBMSIO.ServerMetaInformation.CurrentSeenDate, DBMSIO.ServerMetaInformation);
            DBMSIO.IOIntegrationModule.SetNewCurrentDate(DateTime.Now.ToLongDateString(), DBMSIO.ServerMetaInformation);

        }
        #endregion



        #region ExecuteXxx Methods
        public static System.Object ExecuteILCode(MMILCollection mmil) => Executer.GlobalExecuteStrategy.Execute(mmil);
        public static ITransactionMethodResult ExecuteCommand(System.String command, ITransactionInfo info) => IOIntegrationModule.ExecuteCommand(command, info);
        public static ITransactionMethodResult ExecuteCommand(DBCommand command, ITransactionInfo info) => IOIntegrationModule.ExecuteCommand(command, info);

        public static void ReseTriggerGloalTmpTable() => GlobalCurentOperateDataStorage.Reset();
        #endregion


        #region GetXxx Methods
        public static StaticContextTablesID GetTriggerTmpTableTypeByName(String tableName) => GlobalCurentOperateDataStorage.ResolveByConstName(tableName);
        public static ITable GetTriggerTmpTable(StaticContextTablesID tableType) => GlobalCurentOperateDataStorage.GetStoredTable(tableType);
        public static ILogin GetLoginWithPassword(System.String login, System.String password)
        {
            ILogin user = IOIntegrationModule.GetLogin(login);
            Secure.IPassword pass = user.GetUserPassword();

            Secure.IEncriptor encriptor = Cript.ResolveEncriptorByName(pass.Encription.ToString());

            return pass.Decript() == encriptor.Encript(password) ? user : null;

        }
        public static List<String> GetAllDataBaseNames()
        {

            List<String> dbNames = new List<String>();

            foreach(String item in _UserTokins.Keys)
                dbNames.Add(item);

            return dbNames;

        }
        public static List<String> GetAllDataBaseUserName()
        {
            List<String> userNames = new List<string>();

            foreach(IUserToken item in _UserTokins.Values)
                userNames.Add(item.NodeName);
 
            return userNames;

        }
        public static Int32 GetCountOfUser() => _UserTokins.Count;
        public static List<AllowedCommandWithDB> GetAllDataBaseUsersRights()
        {
            List<AllowedCommandWithDB> alloweds = new List<AllowedCommandWithDB>();

            foreach(IUserToken item in _UserTokins.Values)
                alloweds.Add(item.UserRights);
           
            return alloweds;

        }
        public static Int32 GetCountOfLoginOnServer() => IOIntegrationModule.GetCountOfLoginOnServer();
        public static ILogin GetCurrentLogin() => GlobalExecutionContext.CurrentLogin;
        public static IDataBaseIO GetCurrentDataBase() => GlobalExecutionContext.CurrentDataBase;
        public static List<String> GetNamesOfAllLoginsOnServer() => IOIntegrationModule.GetNamesOfAllLoginsOnServer();
        public static List<String> GetSchemasList() => IOIntegrationModule.GetSchemasList();
        public static System.Boolean GetUserToken(System.String dataBaseName, out IUserToken token) => _UserTokins.TryGetValue(dataBaseName, out token);
        public static ITableCollection GetReadedTableDate(ITransactionInfo info) => IOIntegrationModule.GetReadedTableData(info);
        public static Int32 GetCountOfDataBasesOnServer() => IOIntegrationModule.GetCountOfDataBasesOnServer();
        public static Int32 GetCountOfAllViewOnServer() => IOIntegrationModule.GetCountOfAllViewOnServer();
        public static Int32 GetCountOfAllProceduresOnServer() => IOIntegrationModule.GetCountOfAllProceduresOnServer();
        public static Int32 GetCountOfAllTablesOnServer() => IOIntegrationModule.GetCountOfAllTablesOnServer();
        public static Int32 GetCountOfAllUsersOnServer() => IOIntegrationModule.GetCountOfAllUsersOnServer();
        public static List<String> GetAllDataBaseNamesOnServer() => IOIntegrationModule.GetAllDataBaseNames();
        public static List<String> GetNamesOfAllTablesOnServer() => IOIntegrationModule.GetNamesOfAllTablesOnServer();
        public static List<String> GetNamesOfAllProceduresOnServer() => IOIntegrationModule.GetNamesOfAllProceduresOnServer();
        public static List<String> GetNamesOfAllViewOnServer() => IOIntegrationModule.GetNamesOfAllViewOnServer();
        public static List<String> GetNamesOfAllUserOnServer() => IOIntegrationModule.GetNamesOfAllUserOnServer();
        public static Dictionary<String, List<String>> GetRightMap() => Rbac.RightMap;
        public static IServerMetaInfo GetServerMetaInfo() => IOIntegrationModule.GetServerMetaInfo();
        public static String GetLoggerFileContent() => Logger != null ? Logger.ToString() : "Logger undefined";
        public static void RestoreServerByJournal()
        {
            if (ServerJournal != null)
                ExecuteILCode(ServerJournal.GetServerJournal());
            else throw new Exception("Error Journal Not Created");
        }
        public static IServerMetaInfo GetServerInfo() => IOIntegrationModule.GetServerInfo(DBMSConfig.FullPathToServerMetaFile);
        public static ILogin GetLogin(System.String login) => IOIntegrationModule.GetLogin(login);

        public static DBMSMode GetDBMSMode() => DBMSModes.GetDBMSMode();


        #endregion


        #region IsXxx Methods
        public static System.Boolean IsLoginExist(System.String login) =>  IOIntegrationModule.IsLoginExist(login);
        public static System.Boolean IsDataBaseExist(System.String dataBaseName) => IOIntegrationModule.IsDataBaseExist(dataBaseName);

        #endregion


        #region AppendXxx Methods
        public static System.Boolean AppendLogin(System.String login)  => IOIntegrationModule.AppendLogin(login);
        public static System.Boolean AppendDataBase(System.String dataBaseName) => IOIntegrationModule.AppendDataBase(dataBaseName);
        #endregion


        #region TruncateXxx Methods
        public static System.Boolean TruncateDataBase(System.String info)   => IOIntegrationModule.TruncateDataBase(info);
        public static System.Boolean TruncateLogin(System.String login)   => IOIntegrationModule.TruncateLogin(login);
        #endregion


        #region SetXxx Methods
        public static void SetResoreMode(Boolean isRestore) => GlobalCurentOperateDataStorage.RestoreNow = isRestore;
        public static void SetNewTmpTableToTriggerDS(StaticContextTablesID tableType, ITable setTable) => GlobalCurentOperateDataStorage.SetTable(tableType, setTable);
        public static void SetNewserverName(String serverName) => IOIntegrationModule.SetNewServerName(serverName, DBMSIO.ServerMetaInformation);
        public static void SetNewServerPassword(Password password) => IOIntegrationModule.SetNewServerPassword(password, DBMSIO.ServerMetaInformation);
        public static void SetNewServerType(ServerType type) => IOIntegrationModule.SetNewServerType(type, DBMSIO.ServerMetaInformation);
        public static void SetGlobalParseConfiguration(IParseStrategy parseStr) => Parser.GlobalParseStrategy = parseStr;
        public static void SetLoggerConfiguration(ILogger logger) => Logger = logger;
        public static void SetDBMSConfig(IDBMSConfig configUnit) => DBMSConfig = configUnit;
        public static void SetGlobalExecuteStrategy(IExecuteStrategy executeStrategy) => Executer.GlobalExecuteStrategy = executeStrategy;
        public static void SetCurrentDataBase(IDataBaseIO dataBase) => GlobalExecutionContext.CurrentDataBase = dataBase;
        public static void SetCurrentLogin(ILogin login)   => GlobalExecutionContext.CurrentLogin = login;
        public static void SetRightMap(Dictionary<String, List<String>> map) => Rbac.RightMap = map;

        public static void SetDBMSMode(DBMSMode mode) => DBMSModes.SetDBMSMode(mode);



        #endregion


        #region Interprat Method 
        public static System.String Interpret(ISecureStrategy secureStrategy = null) => Interprater.Interprater.Execute(secureStrategy);
        #endregion


        #region Event Methods
        public static void RegisterNewEvent(String name, DBMSEvent ev)  => DBMSEventControlSystem.RegisterNewEvent(name, ev);

        public static void ExecuteEvent(String name) => DBMSEventControlSystem.ExecuteEvent(name);

        #endregion


        #region ReadXxx/ReWriteXxx Methods
        public static T ReadObject<T>(String way) => IOIntegrationModule.ReadObject<T>(way);
        public static ITableInfo ReadTableMetaInfo(System.String path) => IOIntegrationModule.ReadTableMetaInfo(path);
        public static Dictionary<System.String, IUserToken> ReadUserByLogin(ILogin login) => _UserTokins = IOIntegrationModule.ReadUserByLogin(login);
        public static void ReWriteTableMetaInfo(ITableInfo info, System.String path)  => IOIntegrationModule.ReWriteTableMetaInfo(info, path);
        public static IUserToken ReadUserTokenByName(System.String name) => IOIntegrationModule.ReadUserTokenByName(DBMSIO.GetCurrentDataBase().GetWayToDataBaseUserRoot(), name);
        public static void ReWriteUserTokenByname(System.String name, IUserToken ut)   => 
            IOIntegrationModule.ReWriteUserTokenByName(DBMSIO.GetCurrentDataBase().GetWayToDataBaseUserRoot(), name, ut);
        public static IMetaInfo ReadMetaInfo(System.String path) => IOIntegrationModule.GetMetaInfoByPath(path);
        public static System.Boolean ReWriteMetaInfo(IMetaInfo info, System.String path) => IOIntegrationModule.ReWriteMetaInfo(info, path);


        #endregion

    }
}
