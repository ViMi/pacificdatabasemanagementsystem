﻿using DBMS.Core;
using DBMS.Core.IO;
using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.Helpers;



namespace DBMS.Core
{
    internal static class GlobalExecutionContext
    {
        public static ILogin CurrentLogin { set; get; }
        public static IDataBaseIO CurrentDataBase { set; get; }


        #region Documentation
#if DocTrue

        static GlobalExecutionContext() => Documentation.AddHistoryToSection(
            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreFlow,
            "GlobalExecutionContext",
            PQLDocGlobal.TypeStaticClass,
            PQLDocGlobal.DescriptionUndefined
            
            );

#endif
        #endregion

    }
}
