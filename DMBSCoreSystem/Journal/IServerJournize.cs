﻿using System;
using System.Collections.Generic;
using System.Text;

using DBMS.Core.MMIL;

namespace DBMS.Core.Journal
{
    public  interface IServerJournize 
    {
        MMILCollection GetServerJournal();
        void Journilize(MMILCollection contextCollection);

    }
}
