﻿using DBMS.Core.MMIL;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DBMS.Core.UsersFromRoles;
using DBMS.Core.IO;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DBMS.Core.Journal
{
    public sealed class StandartJournal
        : IServerJournize
    {

        private String _PathToStorage = String.Empty;
        private MMILCollection _Collection = new MMILCollection();

 
        public StandartJournal(String path)
        {
            _PathToStorage = path;
            _Collection.ILUnits = new List<MMILDataBaseContext>();
            Init();
        }

        public void Journilize(MMILCollection contextCollection)
        {
            Boolean flushFlag = false;
            if(!GlobalCurentOperateDataStorage.RestoreNow)
              for (System.Int32 i = 0; i < contextCollection.ILUnits.Count; ++i)
                    if (contextCollection.ILUnits.ElementAt(i)._ContextName.ToUpper() == Rbac.CREATE_COMMAND
                        || contextCollection.ILUnits.ElementAt(i)._ContextName.ToUpper() == Rbac.INSERT_COMMAND
                        || contextCollection.ILUnits.ElementAt(i)._ContextName.ToUpper() == Rbac.DENY_COMMAND
                        || contextCollection.ILUnits.ElementAt(i)._ContextName.ToUpper() == Rbac.GRANT_COMMAND
                        || contextCollection.ILUnits.ElementAt(i)._ContextName.ToUpper() == Rbac.REVOKE_COMMAND
                        || contextCollection.ILUnits.ElementAt(i)._ContextName.ToUpper() == Rbac.USE_COMMAND
                        || contextCollection.ILUnits.ElementAt(i)._ContextName.ToUpper() == Rbac.UPDATE_COMMAND
                        || contextCollection.ILUnits.ElementAt(i)._ContextName.ToUpper() == Rbac.DELETE_COMMAND
                        || contextCollection.ILUnits.ElementAt(i)._ContextName.ToUpper() == Rbac.DROP_COMMAND
                        || contextCollection.ILUnits.ElementAt(i)._ContextName.ToUpper() == Rbac.ALTER_COMMAND
                        || contextCollection.ILUnits.ElementAt(i)._ContextName.ToUpper() == Rbac.CHANGE_PASSWORD) {
                        AppendNewObjectInCollection(contextCollection.ILUnits.ElementAt(i));
                        flushFlag = true;
                        }

            if(flushFlag)
                if(_Collection.ILUnits.Count >= 250)
                 Flush();
        }


        public MMILCollection GetServerJournal() => _Collection;

        private void AppendNewObjectInCollection(MMILDataBaseContext baseContext) => _Collection.ILUnits.Add(baseContext);

        private void Flush()
        {

            if(!File.Exists(_PathToStorage))
                ConverCollectionToByArray(FileMode.Create);
            else
                ConverCollectionToByArray(FileMode.Append);
        }


        private void Init()
        {
            if(File.Exists(_PathToStorage))
            {
                BinaryFormatter bf = new BinaryFormatter();
                using (FileStream fs = new FileStream(_PathToStorage, FileMode.Open))
                {
                    _Collection.ILUnits = (List<MMILDataBaseContext>)bf.Deserialize(fs);

                }
            }
        }


        private void ConverCollectionToByArray(FileMode fm)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (FileStream ms = new FileStream(_PathToStorage, fm))
            {
                bf.Serialize(ms, _Collection.ILUnits);
            }
            
        }


   
    }
}
