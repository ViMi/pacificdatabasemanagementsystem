﻿using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.MMIL.ILConfiguration;
using DBMS.Core.Helpers;



namespace DBMS.Core.MMIL
{
    public class MMILDataControlMap
        : MMILDataBaseContext
    {


#if DocTrue
        static MMILDataControlMap() => Documentation.AddHistoryToSection(
            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreMMIL,
            "MMILDataControlMap",
            PQLDocGlobal.TypeClass,
            PQLDocGlobal.DescriptionUndefined,
            "MMILDataBaseContext"
            );
#endif


        public override string StandartContextName { get; set; } = "CONTROL_MAP";


        public MMILDataControlMap SetOnObject(System.String on)
        {
            this.IsThatContextClosed();
            SetOnObjectUnsafe(on);
            return this;
        }


        public MMILDataControlMap SetRights(System.String right)
        {
            this.IsThatContextClosed();
            SetRightsUnsafe(right);
            return this;
        }


        public void SetOnObjectUnsafe(System.String on) => this.ILlisting +=
              MMILDataControlMapConfiguration.OnObjectDeclarationCommand
            + MMILDataControlMapConfiguration.ParameterSepartorFromCommand
            + on.Trim()
            + MMILDataControlMapConfiguration.EndOfCommandSymbol
            + MMILDataControlMapConfiguration.NewLineCommand;


        public void SetRightsUnsafe(System.String rights) => this.ILlisting +=
              MMILDataControlMapConfiguration.AllowedCommandDeclarationCommand
            + MMILDataControlMapConfiguration.ParameterSepartorFromCommand
            + rights.Trim()
            + MMILDataControlMapConfiguration.EndOfCommandSymbol
            + MMILDataControlMapConfiguration.NewLineCommand;
    }
}
