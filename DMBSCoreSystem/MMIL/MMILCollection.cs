﻿using System.Collections.Generic;
using System.Linq;
using DBMS.Core.Interprater;
using DBMS.Core.Helpers;

using System;

namespace DBMS.Core.MMIL
{



    /// <summary>
    /// This class contains list of object which realize IMMILTranslationUnit
    /// </summary>
    /// 
    [Serializable]
    public class MMILCollection
        : IParseResult
    {



#if DocTrue
        static MMILCollection() {

            Documentation.AddHistoryToSection(
                            PQLDocGlobal.CoreSystem,
                            PQLDocGlobal.CoreMMIL,
                            "MMILDataControlMap",
                            PQLDocGlobal.TypeClass,
                            PQLDocGlobal.DescriptionUndefined,
                            "IParseResult"

                );

        }
#endif


        /// <summary>
        /// This List contsins ILUnits
        /// </summary>
        public List<MMILDataBaseContext> ILUnits = new List<MMILDataBaseContext>(10);

        public System.Object Clone()
        {
            MMILCollection col = new MMILCollection();

            for(System.Int32 i = 0; i < ILUnits.Count; ++i)
                col.ILUnits.Add(this.ILUnits.ElementAt(i));
            

            return col;

        }



        /// <summary>
        /// This method return contatenation of all listings of ILUnits in Collection
        /// </summary>
        /// <returns></returns>
        public System.String GetParseResult()
        {
            System.String result = System.String.Empty;
            
            for(System.Int16 i = 0; i < this.ILUnits.Count; ++i)
                result += ILUnits.ElementAt(i).GetListiongIL();
            

            return result;

        }


        public Dictionary<System.String, System.String> GetParseResultDictionary() => null;


       public List<Dictionary<System.String, System.String>> GetParseResultListDictionary()
        {
            List<Dictionary<System.String, System.String>> dict = new List<Dictionary<System.String, System.String>>(10);
         
            for(System.Int32 i = 0; i < this.ILUnits.Count; ++i)
            {
                Dictionary<System.String, System.String> contextDictionary = ILUnits.ElementAt(i).GetILDictionary();
                dict.Add(contextDictionary);
            }

            return dict;


        }

        public List<UnityType> GetParseResultType()
        {
            List<UnityType> types = new List<UnityType>(10);

            for (System.Int32 i = 0; i < this.ILUnits.Count; ++i)
                types.Add(this.ILUnits.ElementAt(i)._UnityType);

            return types;
        }



        public List<MMILDataBaseContext> GetTranslationUnits() => this.ILUnits;
    }
}
