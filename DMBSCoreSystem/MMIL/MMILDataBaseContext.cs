﻿using DBMS.Core.Interprater;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using DBMS.Core.MMIL.ILConfiguration;

using DBMS.Core.Helpers;
using DBMS.Core.ErrorControlSystem.Exeption;

namespace DBMS.Core.MMIL
{

    /// <summary>
    /// This class Represent methods anf field neded to work with DBMS systems 
    /// </summary>
    /// 
    [Serializable]
    public class MMILDataBaseContext
        : IMMILTranslationUnit
        , IParseResult
    {

        #region Documentation
#if DocTrue
        static MMILDataBaseContext()
        {
            Documentation.AddHistoryToSection(
                 PQLDocGlobal.CoreSystem,
                 PQLDocGlobal.CoreMMIL,
                "MMILDataBaseContext",
                PQLDocGlobal.TypeClass,
                PQLDocGlobal.DescriptionUndefined,  
                "IMMILTransationUnit IParseResult"

                );


            Documentation.AddHistoryToSection(
                            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreMMIL,
            "IMMILTransationUnit",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined
                );


            Documentation.AddHistoryToSection(
                            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreMMIL,
            "IParseResult",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined

                );
        }
#endif
        #endregion


        /// <summary>
        /// This property declare what type of unit will be added in executor
        /// </summary>
        virtual public UnityType _UnityType { get; } = UnityType.DATA_BASE_IL_TRANSLATION_UNIT;
        virtual public String StandartContextName { set; get; } = "DATA_BASE_CONTEXT";

        virtual public System.String ILlisting { set; get; } = System.String.Empty;

        virtual public System.Boolean _IsEnded { set; get; } = false;
        virtual public System.String _ContextName { set; get; } = System.String.Empty;

        virtual public Dictionary<System.String, System.String> _DictionaryListing { set; get; } = null;
        virtual public Dictionary<System.String, List<System.String>> _DictionaryCommandListing { set; get; } = new Dictionary<String, List<String>>(1);

        public MMILDataBaseContext(){ }


        public MMILDataBaseContext(System.String listing)=> this.ILlisting = listing;


        public MMILDataBaseContext(MMILDataBaseContext dataBaseContext) => this.ILlisting = dataBaseContext.GetListiongIL();


        /// <summary>
        /// This method is save version of end execution context. Use Unsavemethod Version
        /// </summary>
        public void EndExecuteContext()
        {

            this.IsThatContextClosed();
            this.IsThancontextSetted();
            this.EndExecuteContextUnsave();

        }





        /// <summary>
        /// This is save version of method which set Context Server
        /// </summary>
        /// <param name="serverName"></param>
        public MMILDataBaseContext SetServer(System.String serverName)
        {
            this.IsThatContextClosed();

            this.SetServerUnsafe(serverName);
            return this;

        }



        /// <summary>
        /// This is save version of method which set DataBase context
        /// </summary>
        /// <param name="dataBaseName"></param>
        public MMILDataBaseContext SetDatabase(System.String dataBaseName)
        {
            this.IsThatContextClosed();

            this.SetDatabaseUnsafe(dataBaseName);
            return this;
        }




        /// <summary>
        /// This is save version of method which et User name context
        /// </summary>
        /// <param name="userName"></param>
        public MMILDataBaseContext SetUser(System.String userName)
        {
            this.IsThatContextClosed();

            this.SetUserUnsafe(userName);
            return this;
        }



        /// <summary>
        /// This is save version of Method which set privifiage level
        /// </summary>
        /// <param name="priviliageLevel"></param>
        public MMILDataBaseContext SetUserPrivilige(System.String priviliageLevel)
        {
            this.IsThatContextClosed();

            this.SetUserPriviligeUnsafe(priviliageLevel);
            return this;

        }


        /// <summary>
        /// This is save version of method which set tables context
        /// </summary>
        /// <param name="tableName"></param>
        public MMILDataBaseContext SetTable(System.String tableName)
        {
            this.IsThatContextClosed();
            this.SetTableUnsafe(tableName);
            return this;
        }




        /// <summary>
        /// This is save version of methdo which set commansd context
        /// </summary>
        /// <param name="commandName"></param>
        public MMILDataBaseContext SetCommand(System.String commandName)
        {
            this.IsThatContextClosed();
            this.SetCommandUnsafe(commandName);
            return this;
        }




        /// <summary>
        /// This is save version of method which set command parameters context. Parameters must go in the same orders how commands
        /// </summary>
        /// <param name="parameters"></param>
        public MMILDataBaseContext SetParameters(System.String parameters)
        {
            this.IsThatContextClosed();
            this.SetParametersUnsafe(parameters); 
            return this;
        }



        /// <summary>
        /// This method get string array of IL commands 
        /// </summary>
        /// <returns></returns>
        public System.String[] GetAllStringIL() => this.ILlisting
            .Trim()
            .Replace(MMILDataBaseConfig.OpenContextSymbol, System.String.Empty)
            .Replace(MMILDataBaseConfig.CloseContextSymbol, System.String.Empty)
            .Replace(MMILDataBaseConfig.NewLineCommand, System.String.Empty)
            .Split(MMILDataBaseConfig.EndOfCommandSymbol[0]);



        /// <summary>
        /// This Method return dictionary of IL commands where Key is command and Value is parametr of command
        /// </summary>
        /// <returns></returns>
        public Dictionary<System.String, System.String> GetILDictionary()
        {


            if (this._IsEnded)
            {

                if (this._DictionaryListing == null)
                {

                    System.String[] content = this.ILlisting
                        .Trim()
                        .Replace(MMILDataBaseConfig.CloseContextSymbol, System.String.Empty)
                        .Replace(MMILDataBaseConfig.OpenContextSymbol, System.String.Empty)
                        .Replace(MMILDataBaseConfig.NewLineCommand, System.String.Empty)
                        .Trim()
                        .Split(MMILDataBaseConfig.EndOfCommandSymbol[0], (System.Char)StringSplitOptions.RemoveEmptyEntries)
                       ;

                    Dictionary<System.String, System.String> commandDictionary = new Dictionary<string, string>();

                    for (System.Byte i = 0; i < content.Length; ++i)
                    {
                        if (content[i] == System.String.Empty)
                            continue;
                        System.String[] workStr = content[i].Split(MMILDataBaseConfig.ParameterSepartorFromCommand[0]);


                        commandDictionary.Add(workStr[0], workStr[1]);
                    }

                    this._DictionaryListing = commandDictionary;

                    return commandDictionary;

                    

                }

                else
                {
                    return this._DictionaryListing;
                }

            }


            throw new ILContextClosedError();

            }


        /// <summary>
        /// This method get unworked listing of IL
        /// </summary>
        /// <returns></returns>
        public System.String GetListiongIL()=> this.ILlisting;


        /// <summary>
        /// This is save version of initiation context method
        /// </summary>
        /// <param name="contexName"></param>
        public void InitExecutionContext(System.String contexName)
        {
            this.IsThatContextClosed();

            this.InitExecutionContextUnsafe(contexName);

        }








        /// <summary>
        /// This unsave version of method. Without checks
        /// </summary>
        /// <param name="contexName"></param>
        public void InitExecutionContextUnsafe(System.String contexName) => 
            this.ILlisting +=
            MMILDataBaseConfig.NewLineCommand 
            + MMILDataBaseConfig.OpenContextSymbol 
            + MMILDataBaseConfig.NewLineCommand
            + MMILDataBaseConfig.StartContextCommand
            + MMILDataBaseConfig.ParameterSepartorFromCommand
            + (this._ContextName=contexName.Trim())
            + MMILDataBaseConfig.EndOfCommandSymbol
            + MMILDataBaseConfig.NewLineCommand;



        /// <summary>
        /// This unsave version of method. Without checks
        /// </summary>
        /// <param name="serverName"></param>
        public void SetServerUnsafe(System.String serverName) => 
            this.ILlisting += 
              MMILDataBaseConfig.ServerDeclarationCommand
            + MMILDataBaseConfig.ParameterSepartorFromCommand
            + serverName.Trim()
            + MMILDataBaseConfig.EndOfCommandSymbol
            + MMILDataBaseConfig.NewLineCommand;


        /// <summary>
        /// This unsave version of method. Without checks
        /// </summary>
        /// <param name="dataBaseName"></param>
        public void SetDatabaseUnsafe(System.String dataBaseName) => 
            this.ILlisting +=
              MMILDataBaseConfig.DataBaseDeclarationCommand
            + MMILDataBaseConfig.ParameterSepartorFromCommand
            + dataBaseName.Trim()
            + MMILDataBaseConfig.EndOfCommandSymbol
            + MMILDataBaseConfig.NewLineCommand;

        /// <summary>
        /// This unsave version of method. Without checks
        /// </summary>
        /// <param name="userName"></param>
        public void SetUserUnsafe(System.String userName) =>
            this.ILlisting +=
              MMILDataBaseConfig.UserDeclarationCommand
            + MMILDataBaseConfig.ParameterSepartorFromCommand
            + userName.Trim()
            + MMILDataBaseConfig.EndOfCommandSymbol
            + MMILDataBaseConfig.NewLineCommand;



        /// <summary>
        /// This unsave version of method. Without checks
        /// </summary>
        /// <param name="priviliageLevel"></param>
        public void SetUserPriviligeUnsafe(System.String priviliageLevel)=> 
            this.ILlisting += 
              MMILDataBaseConfig.UserPrevilegeDeclarationCommand
            + MMILDataBaseConfig.ParameterSepartorFromCommand
            + priviliageLevel.Trim()
            + MMILDataBaseConfig.EndOfCommandSymbol
            + MMILDataBaseConfig.NewLineCommand;


        /// <summary>
        /// This unsave version of method. Without checks
        /// </summary>
        /// <param name="tableName"></param>
        public void SetTableUnsafe(System.String tableName) => 
            this.ILlisting +=
              MMILDataBaseConfig.TableDeclarationCommand
            + MMILDataBaseConfig.ParameterSepartorFromCommand
            + tableName.Trim()
            + MMILDataBaseConfig.EndOfCommandSymbol
            + MMILDataBaseConfig.NewLineCommand;


        /// <summary>
        /// This unsave version of method. Without checks
        /// </summary>
        /// <param name="commandName"></param>
        public void SetCommandUnsafe(System.String commandName) => 
            this.ILlisting +=
              MMILDataBaseConfig.CommandDeclarationCommand
            + MMILDataBaseConfig.ParameterSepartorFromCommand 
            + commandName.Trim()
            + MMILDataBaseConfig.EndOfCommandSymbol
            + MMILDataBaseConfig.NewLineCommand;


        /// <summary>
        /// This unsave version of method. Without checks
        /// </summary>
        /// <param name="parameters"></param>
        public void SetParametersUnsafe(System.String parameters) =>
            this.ILlisting +=
              MMILDataBaseConfig.CommandParameterDeclaration
            + MMILDataBaseConfig.ParameterSepartorFromCommand
            + parameters.Trim()
            + MMILDataBaseConfig.EndOfCommandSymbol
            + MMILDataBaseConfig.NewLineCommand;

        /// <summary>
        /// This unsave version of method. Without checks
        /// </summary>
        public void EndExecuteContextUnsave()
        {
            if (!this._IsEnded)
            {
                this.ILlisting +=
                         MMILDataBaseConfig.CloseContextComand
                       + MMILDataBaseConfig.ParameterSepartorFromCommand
                       + this._ContextName
                       + MMILDataBaseConfig.EndOfCommandSymbol
                       + MMILDataBaseConfig.NewLineCommand
                       + MMILDataBaseConfig.CloseContextSymbol;
                   
                this._IsEnded = true;
            }
        }



        /// <summary>
        /// This method get unworked listing IL
        /// </summary>
        /// <returns></returns>
        public System.String GetParseResult() => this.GetListiongIL();
        public Dictionary<string, string> GetParseResultDictionary() => this.GetILDictionary();

        public List<Dictionary<string, string>> GetParseResultListDictionary()
        {
            throw new NotImplementedException();
        }



        public System.String[] GetCommandParametersArray()
        {

            if (this._IsEnded)
            {
                Regex regex = new Regex("\\(\"[\\w \\[ \\] \" \\d , = ! > \\* \\ \\+ \\. \\? а-яА-Я  < { } : \\- / \\| @]*\"\\)");

                System.String param;
                if (this._DictionaryListing == null)
                    this.GetILDictionary();

                this._DictionaryListing.TryGetValue(MMILDataBaseConfig.CommandParameterDeclaration, out param);


                MatchCollection matches = regex.Matches(param);
                System.String[] parameters = new System.String[matches.Count];

                for (System.Int32 i = 0; i < matches.Count; ++i)
                {
                    parameters[i] = matches[i].Value;
                }


                return parameters;
            }
            throw new ILContextClosedError();
        }


        public System.String[] GetCommandArray()
        {

            if (this._IsEnded)
            {
                System.String commands;

                if (this._DictionaryListing == null)
                    this.GetILDictionary();
                
                this._DictionaryListing.TryGetValue(MMILDataBaseConfig.CommandDeclarationCommand, out commands);
                return commands.Split(' ', (System.Char)StringSplitOptions.RemoveEmptyEntries);


            }

            throw new ILContextClosedError();


        }


        public System.String GetDataBaseName()
        {
            if (this._IsEnded)
            {
                System.String dataBaseName;

                if (this._DictionaryListing == null)
                    this.GetILDictionary();

                this._DictionaryListing.TryGetValue(MMILDataBaseConfig.DataBaseDeclarationCommand, out dataBaseName);
                return dataBaseName;

            }

            throw new ILContextClosedError();
        }


        public System.String GetUserName()
        {
            if (this._IsEnded)
            {
                System.String userName;

                if (this._DictionaryListing == null)
                    this.GetILDictionary();

                this._DictionaryListing.TryGetValue(MMILDataBaseConfig.UserDeclarationCommand, out userName);

                return userName;

            }

            throw new ILContextClosedError();
        }


        public System.String GetUserPrivilege()
        {
            if (this._IsEnded)
            {
                System.String userPrivilege;

                if (this._DictionaryListing == null)
                    this.GetILDictionary();

                this._DictionaryListing.TryGetValue(MMILDataBaseConfig.UserPrevilegeDeclarationCommand, out userPrivilege);
                return userPrivilege;


            }

            throw new ILContextClosedError();
        }


        public System.String GetCommandContext()
        {

            if(this._IsEnded)  
               return this._ContextName;

           

            throw new ILContextClosedError();

        }


        public List<MMILDataBaseContext> GetTranslationUnits()
        {
            List<MMILDataBaseContext> list = new List<MMILDataBaseContext>(1)
            {
                this
            };

            return list;
        }


        public List<UnityType> GetParseResultType()
        {

            List<UnityType> list = new List<UnityType>(1)
            {
                this._UnityType
            };

            return list;

        }


        virtual public object Clone()
            => new MMILDataBaseContext() { _ContextName = this._ContextName, _IsEnded = this._IsEnded, ILlisting = this.ILlisting };


        public String GetContextValue(String commandName)
        {
            System.String buff;
            this.GetILDictionary().TryGetValue(commandName, out buff);
            return buff;
        }





        protected void IsThatContextClosed()
        {
            if (this._IsEnded)
                throw new ILContextClosedError();
        }


        protected void IsThancontextSetted()
        {
            if (this._ContextName == System.String.Empty)
                throw new ILContextNotSet();
        }

    }
}
