﻿using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.MMIL.ILConfiguration;
using DBMS.Core.Helpers;


namespace DBMS.Core.MMIL
{
   public class MMILDataManipulationContext 
        : MMILDataBaseContext
    {

        #region Documentation
#if DocTrue
        static MMILDataManipulationContext()
        {
            Documentation.AddHistoryToSection(
             PQLDocGlobal.CoreSystem,
             PQLDocGlobal.CoreMMIL,
             "MMILDataManipulationContext",
             PQLDocGlobal.TypeClass,
             PQLDocGlobal.DescriptionUndefined,
             "MMILDataBaseContext"
         );

        }

#endif
        #endregion

        public override string StandartContextName { get; set; } = "DATA_MANIPULATION_CONTEX";




        #region Save Methods
        public MMILDataManipulationContext SetWhere(System.String where)
        {
            this.IsThatContextClosed();
            SetWhereUnsafe(where);

            return this;
        }

        public MMILDataManipulationContext SetSum(System.String sum)
        {
            this.IsThatContextClosed();
            SetSumUnsafe(sum);

            return this;
        }

        public MMILDataManipulationContext SetAvg(System.String avg)
        {
            this.IsThatContextClosed();
            SetAvgUnsafe(avg);

            return this;
        }

        public MMILDataManipulationContext SetCount(System.String count)
        {
            this.IsThatContextClosed();
            SetCounUnsafe(count);

            return this;
        }

        public MMILDataManipulationContext SetMin(System.String min)
        {
            this.IsThatContextClosed();
            SetMinUnsafe(min);

            return this;
        }

        public MMILDataManipulationContext SetMax(System.String max)
        {
            this.IsThatContextClosed();
            SetMaxUnsafe(max);

            return this;
        }

        public MMILDataManipulationContext SetDistinct(System.String distinct)
        {
            this.IsThatContextClosed();
            SetDistinctUnsafe(distinct);

            return this;
        }

        public MMILDataManipulationContext SetHaving(System.String having)
        {
            this.IsThatContextClosed();

            SetHavingUnsafe(having);
            return this;
        }


        public MMILDataManipulationContext SetFrom(System.String from)
        {
            this.IsThatContextClosed();

            SetFromUnsafe(from);
            return this;
        }


        public MMILDataManipulationContext SetRows(System.String rows)
        {
            this.IsThatContextClosed();

            SetRowsUnsafe(rows);
            return this;

        }


        public MMILDataManipulationContext SetJoin(System.String join)
        {
            this.IsThatContextClosed();

            SetJoinUnsafe(join);
            return this;
        }


        public MMILDataManipulationContext SetOrderBy(System.String orderBy)
        {
            this.IsThatContextClosed();

            SetOrderByUnsafe(orderBy);
            return this;
        }


        public MMILDataManipulationContext SetGroupBy(System.String groupBy)
        {
            this.IsThatContextClosed();

            SetGroupByUnsefe(groupBy);
            return this;
        }


        public MMILDataManipulationContext SetValues(System.String values)
        {
            this.IsThatContextClosed();

            SetValuesUnsafe(values);
            return this;
        }


        public MMILDataManipulationContext SetTop(System.String top)
        {
            this.IsThatContextClosed();

            SetTopUnsafe(top);
            return this;

        }


        public MMILDataManipulationContext SetSkip(System.String skip)
        {
            this.IsThatContextClosed();


            SetSkipUnsafe(skip);
            return this;
        }


        public MMILDataManipulationContext SetLimit(System.String limit)
        {
            this.IsThatContextClosed();

            SetLimitUnsafe(limit);
            return this;
        }


        public MMILDataManipulationContext SetTo(System.String to)
        {
            this.IsThatContextClosed();

            this.SetToUnsafe(to);
            return this;
        }

        
        public MMILDataManipulationContext SetGetType(System.String type)
        {
            this.IsThatContextClosed();

            this.SetGetTypeUnsafe(type);
            return this;

        }


        public MMILDataManipulationContext SetgetDomain(System.String domain)
        {
            this.IsThatContextClosed();

            this.SetGetDomainUnsae(domain);
            return this;
        }

        public MMILDataManipulationContext SetUnion(System.String union)
        {
            this.IsThatContextClosed();

            this.SetUnionUnsafe(union);
            return this;
        }


        public MMILDataManipulationContext SetBounded(System.String bounded)
        {
            this.IsThatContextClosed();

            this.SetBoundedUnsafe(bounded);

            return this;
        }


        public MMILDataManipulationContext SetReverse(System.String reverse)
        {
            this.IsThatContextClosed();

            this.SetReverseUnsafe(reverse);

            return this;
        }


        public MMILDataManipulationContext SetOrderByAsc(System.String aliasAsc)
        {
            this.IsThatContextClosed();

            this.SetOrderByAscUnsafe(aliasAsc);

            return this;
        }


        public MMILDataManipulationContext SetOrderByDesc(System.String aliasDesc)
        {
            this.IsThatContextClosed();

            this.SetOrderByDescUnsafe(aliasDesc);

            return this;
        }

        #endregion





        #region Unsafe Metods
        public void SetReverseUnsafe(System.String reverse)
            => this.ILlisting +=
          MMILDataManipulationConfigure.ReverseDeclarationCommand
        + MMILDataManipulationConfigure.ParameterSepartorFromCommand
        + reverse.Trim()
        + MMILDataManipulationConfigure.EndOfCommandSymbol
        + MMILDataManipulationConfigure.NewLineCommand;

        public void SetOrderByAscUnsafe(System.String reverse)
    => this.ILlisting +=
  MMILDataManipulationConfigure.OrderByAscDeclarationCommand
+ MMILDataManipulationConfigure.ParameterSepartorFromCommand
+ reverse.Trim()
+ MMILDataManipulationConfigure.EndOfCommandSymbol
+ MMILDataManipulationConfigure.NewLineCommand;

        public void SetOrderByDescUnsafe(System.String reverse)
    => this.ILlisting +=
  MMILDataManipulationConfigure.OrderByDescDeclarationCommand
+ MMILDataManipulationConfigure.ParameterSepartorFromCommand
+ reverse.Trim()
+ MMILDataManipulationConfigure.EndOfCommandSymbol
+ MMILDataManipulationConfigure.NewLineCommand;

        public void SetTopUnsafe(System.String top) =>
            this.ILlisting +=
              MMILDataManipulationConfigure.TopDeclarationCommand
            + MMILDataManipulationConfigure.ParameterSepartorFromCommand
            + top.Trim()
            + MMILDataManipulationConfigure.EndOfCommandSymbol
            + MMILDataManipulationConfigure.NewLineCommand;

        public void SetBoundedUnsafe(System.String bounded) =>
        this.ILlisting +=
          MMILDataManipulationConfigure.GetBoundedEntitiesCommand
        + MMILDataManipulationConfigure.ParameterSepartorFromCommand
        + bounded.Trim()
        + MMILDataManipulationConfigure.EndOfCommandSymbol
        + MMILDataManipulationConfigure.NewLineCommand;

        public void SetGetDomainUnsae(System.String domain) =>
           this.ILlisting +=
             MMILDataManipulationConfigure.GetDomainDeclarationCommand
           + MMILDataManipulationConfigure.ParameterSepartorFromCommand
           + domain.Trim()
           + MMILDataManipulationConfigure.EndOfCommandSymbol
           + MMILDataManipulationConfigure.NewLineCommand;

        public void SetGetTypeUnsafe(System.String type) =>
           this.ILlisting +=
             MMILDataManipulationConfigure.GetColumnTypesDeclarationCommand
           + MMILDataManipulationConfigure.ParameterSepartorFromCommand
           + type.Trim()
           + MMILDataManipulationConfigure.EndOfCommandSymbol
           + MMILDataManipulationConfigure.NewLineCommand;

        public void SetDistinctUnsafe(System.String distinct) =>
            this.ILlisting +=
              MMILDataManipulationConfigure.DistinctDeclarationCommand
            + MMILDataManipulationConfigure.ParameterSepartorFromCommand
            + distinct.Trim()
            + MMILDataManipulationConfigure.EndOfCommandSymbol
            + MMILDataManipulationConfigure.NewLineCommand;

        public void SetMaxUnsafe(System.String max) =>
       this.ILlisting +=
         MMILDataManipulationConfigure.MaxDeclarationCommand
       + MMILDataManipulationConfigure.ParameterSepartorFromCommand
       + max.Trim()
       + MMILDataManipulationConfigure.EndOfCommandSymbol
       + MMILDataManipulationConfigure.NewLineCommand;

        public void SetMinUnsafe(System.String min) =>
    this.ILlisting +=
      MMILDataManipulationConfigure.MinDeclarationCommand
    + MMILDataManipulationConfigure.ParameterSepartorFromCommand
    + min.Trim()
    + MMILDataManipulationConfigure.EndOfCommandSymbol
    + MMILDataManipulationConfigure.NewLineCommand;

        public void SetAvgUnsafe(System.String avg) =>
    this.ILlisting +=
      MMILDataManipulationConfigure.AvgDeclarationCommand
    + MMILDataManipulationConfigure.ParameterSepartorFromCommand
    + avg.Trim()
    + MMILDataManipulationConfigure.EndOfCommandSymbol
    + MMILDataManipulationConfigure.NewLineCommand;

        public void SetCounUnsafe(System.String count) =>
    this.ILlisting +=
      MMILDataManipulationConfigure.CountDeclarationCommand
    + MMILDataManipulationConfigure.ParameterSepartorFromCommand
    + count.Trim()
    + MMILDataManipulationConfigure.EndOfCommandSymbol
    + MMILDataManipulationConfigure.NewLineCommand;

        public void SetSumUnsafe(System.String sum) =>
    this.ILlisting +=
      MMILDataManipulationConfigure.SumDeclarationCommand
    + MMILDataManipulationConfigure.ParameterSepartorFromCommand
    + sum.Trim()
    + MMILDataManipulationConfigure.EndOfCommandSymbol
    + MMILDataManipulationConfigure.NewLineCommand;

        public void SetToUnsafe(System.String to) =>
            this.ILlisting +=
              MMILDataManipulationConfigure.ToDeclarationCommand
            + MMILDataManipulationConfigure.ParameterSepartorFromCommand
            + to.Trim()
            + MMILDataManipulationConfigure.EndOfCommandSymbol
            + MMILDataManipulationConfigure.NewLineCommand;

        public void SetFromUnsafe(System.String from) =>
            this.ILlisting +=
            MMILDataManipulationConfigure.FromDeclarationCommad
          + MMILDataManipulationConfigure.ParameterSepartorFromCommand
          + from.Trim()
          + MMILDataManipulationConfigure.EndOfCommandSymbol
          + MMILDataManipulationConfigure.NewLineCommand;

        public void SetSkipUnsafe(System.String skip) =>
           this.ILlisting +=
            MMILDataManipulationConfigure.SkipDeclarationCommand
          + MMILDataManipulationConfigure.ParameterSepartorFromCommand
          + skip.Trim()
          + MMILDataManipulationConfigure.EndOfCommandSymbol
          + MMILDataManipulationConfigure.NewLineCommand;

        public void SetWhereUnsafe(System.String where) =>
          this.ILlisting +=
            MMILDataManipulationConfigure.WhereDeclarationCommand
          + MMILDataManipulationConfigure.ParameterSepartorFromCommand
          + where.Trim()
          + MMILDataManipulationConfigure.EndOfCommandSymbol
          + MMILDataManipulationConfigure.NewLineCommand;

        public void SetLimitUnsafe(System.String limit) =>
           this.ILlisting +=
            MMILDataManipulationConfigure.LimitDeclarationCommand
          + MMILDataManipulationConfigure.ParameterSepartorFromCommand
          + limit.Trim()
          + MMILDataManipulationConfigure.EndOfCommandSymbol
          + MMILDataManipulationConfigure.NewLineCommand;

        public void SetJoinUnsafe(System.String join)=>
             this.ILlisting +=
            MMILDataManipulationConfigure.JoinDeclarationCommand
          + MMILDataManipulationConfigure.ParameterSepartorFromCommand
          + join.Trim()
          + MMILDataManipulationConfigure.EndOfCommandSymbol
          + MMILDataManipulationConfigure.NewLineCommand;

        public void SetUnionUnsafe(System.String union)
            =>
             this.ILlisting +=
            MMILDataManipulationConfigure.UnionDeclarationCommand
          + MMILDataManipulationConfigure.ParameterSepartorFromCommand
          + union.Trim()
          + MMILDataManipulationConfigure.EndOfCommandSymbol
          + MMILDataManipulationConfigure.NewLineCommand;
        public void SetHavingUnsafe(System.String having) =>
               this.ILlisting +=
                 MMILDataManipulationConfigure.HavingDeclarationCommand
               + MMILDataManipulationConfigure.ParameterSepartorFromCommand
               + having.Trim()
               + MMILDataManipulationConfigure.EndOfCommandSymbol
               + MMILDataManipulationConfigure.NewLineCommand;

        public void SetRowsUnsafe(System.String rows)=>
               this.ILlisting +=
                   MMILDataManipulationConfigure.RowsDeclarationCommand
                 + MMILDataManipulationConfigure.ParameterSepartorFromCommand
                 + rows.Trim()
                 + MMILDataManipulationConfigure.EndOfCommandSymbol
                 + MMILDataManipulationConfigure.NewLineCommand;

        public void SetOrderByUnsafe(System.String orderBy) =>
               this.ILlisting +=
                 MMILDataManipulationConfigure.OrderByDeclarationCommand
               + MMILDataManipulationConfigure.ParameterSepartorFromCommand
               + orderBy.Trim()
               + MMILDataManipulationConfigure.EndOfCommandSymbol
               + MMILDataManipulationConfigure.NewLineCommand;

        public void SetValuesUnsafe(System.String values) =>
               this.ILlisting +=
                 MMILDataManipulationConfigure.ValuesDeclarationCommand
               + MMILDataManipulationConfigure.ParameterSepartorFromCommand
               + values.Trim()
               + MMILDataManipulationConfigure.EndOfCommandSymbol
               + MMILDataManipulationConfigure.NewLineCommand;

       public void SetGroupByUnsefe(System.String groupBy) =>
            this.ILlisting +=
              MMILDataManipulationConfigure.GroupByDeclarationCommand
            + MMILDataManipulationConfigure.ParameterSepartorFromCommand
            + groupBy.Trim()
            + MMILDataManipulationConfigure.EndOfCommandSymbol
            + MMILDataManipulationConfigure.NewLineCommand;

        #endregion
    }
}
