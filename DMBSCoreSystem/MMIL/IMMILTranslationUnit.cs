﻿using DBMS.Core.Interprater;
using System;
using System.Collections.Generic;

namespace DBMS.Core.MMIL
{


    /// <summary>
    /// This interface define commin method for Mercurie`s Might Intermediate Language Units (MMILUnit)
    /// </summary>
    public interface IMMILTranslationUnit
    {



        UnityType _UnityType { get; }
        String StandartContextName { set; get; }

        /// <summary>
        /// This method define start of execution context. Parametr contexName contsins name of object which provide execution context
        /// </summary>
        /// <param name="contexName"></param>
        void InitExecutionContext(System.String contexName);


        /// <summary>
        /// This Method define end of execution context
        /// </summary>
        void EndExecuteContext();



        /// <summary>
        /// Return Intermediate Language (IL) comands by strings
        /// </summary>
        /// <returns></returns>
        System.String[] GetAllStringIL();
        
        /// <summary>
        /// Return listing of Intermediate Language (IL) in one string
        /// </summary>
        /// <returns></returns>
        System.String GetListiongIL();


        /// <summary>
        /// Retur Dictionary where key it IL(Intermediate Language) comand and value is command paramentr
        /// </summary>
        /// <returns></returns>
        Dictionary<System.String, System.String> GetILDictionary();

    }
}
