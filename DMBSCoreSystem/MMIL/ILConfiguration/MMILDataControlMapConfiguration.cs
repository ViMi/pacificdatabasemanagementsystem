﻿using DBMS.Core.Helpers;


namespace DBMS.Core.MMIL.ILConfiguration
{
    public sealed class MMILDataControlMapConfiguration
        : MMILBaseConfiguration
    {


#if DocTrue
        static MMILDataControlMapConfiguration() => Documentation.AddHistoryToSection(

                        PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreMMIL,
            "MMILDataControlMapConfigConfiguration",
            PQLDocGlobal.TypeClass,
            PQLDocGlobal.DescriptionUndefined,
            "MMILBaseConfiguration"

            );
#endif

        public static readonly System.String UserDeclartionCommand            = "USER";
        public static readonly System.String AllowedCommandDeclarationCommand = "ALLOWED_COMMAND";
        public static readonly System.String OnObjectDeclarationCommand       = "ON";
    }
}
