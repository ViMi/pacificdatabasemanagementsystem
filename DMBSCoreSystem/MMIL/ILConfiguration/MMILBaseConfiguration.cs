﻿using System;
using DBMS.Core.Helpers;


namespace DBMS.Core.MMIL.ILConfiguration
{
    public class MMILBaseConfiguration
    {

#if DocTrue
        static MMILBaseConfiguration() => Documentation.AddHistoryToSection(PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreMMIL,
            "MMILBaseConfiguration",
            PQLDocGlobal.TypeClass,
            PQLDocGlobal.DescriptionUndefined);
#endif


        public static readonly System.String OpenContextSymbol            = "{";
        public static readonly System.String CloseContextSymbol           = "}";
        public static readonly System.String ParameterSepartorFromCommand = ":";
        public static readonly System.String ValuesSeparator              = " ";
        public static readonly System.String StartContextCommand          = "BEGIN_EXECUTION_CONTEXT";
        public static readonly System.String CloseContextComand           = "END_CONTEXT";
        public static readonly System.String CommandDeclarationCommand    = "COMMAND";
        public static readonly System.String CommandParameterDeclaration  = "COMMAND_PARAMETER";
        public static readonly System.String EndOfCommandSymbol           = ";";
        public static readonly System.String NewLineCommand               = Environment.NewLine;
    }

}
