﻿using DBMS.Core.Helpers;


namespace DBMS.Core.MMIL.ILConfiguration
{
    public sealed class MMILDataBaseTableMappConfig
        : MMILDataBaseConfig
    {

#if DocTrue
        static MMILDataBaseTableMappConfig() => Documentation.AddHistoryToSection
            (

                        PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreMMIL,
            "MMILDataBaseTableMappConfig",
            PQLDocGlobal.TypeClass,
            PQLDocGlobal.DescriptionUndefined,
            "MMILDataBaseConfig"

            );
#endif


       public static readonly System.String DataTypeDeclarationCommand       = "DATA_TYPE";
       public static readonly System.String AttributesDecalarationCommand    = "ATTRIBUTE";
       public static readonly System.String AliasDeclarationCommand          = "ALIAS";
       public static readonly System.String AttributeValueDeclarationCommand = "ATTRIBUTE_VALUE";
       public static readonly System.String DataClassNameDaclarationCommand  = "NAME"; 
    }
}
