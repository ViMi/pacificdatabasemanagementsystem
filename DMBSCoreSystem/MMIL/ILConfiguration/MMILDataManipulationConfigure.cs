﻿using DBMS.Core.Helpers;



namespace DBMS.Core.MMIL.ILConfiguration
{
    public sealed class MMILDataManipulationConfigure
        : MMILDataBaseConfig
    {

#if DocTrue
        static MMILDataManipulationConfigure() => Documentation.AddHistoryToSection
            (

                        PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreMMIL,
            "MMILDataManipulationConfigure",
            PQLDocGlobal.TypeClass,
            PQLDocGlobal.DescriptionUndefined,
            "MMILDataBaseConfig"

            );
#endif

        public static readonly System.String FromDeclarationCommad      = "FROM";
        public static readonly System.String ToDeclarationCommand       = "TO";
        public static readonly System.String WhereDeclarationCommand    = "WHERE";
        public static readonly System.String ValuesDeclarationCommand   = "VALUE";
        public static readonly System.String OrderByDeclarationCommand  = "ORDER_BY";

        public static readonly System.String OrderByAscDeclarationCommand  = "ORDER_BY_ASC";
        public static readonly System.String OrderByDescDeclarationCommand  = "ORDER_BY_DESC";
        public static readonly System.String HavingDeclarationCommand   = "HAVING";
        public static readonly System.String RowsDeclarationCommand     = "ROWS";
        public static readonly System.String GroupByDeclarationCommand  = "GROUP_BY";
        public static readonly System.String JoinDeclarationCommand     = "JOIN";
        public static readonly System.String SkipDeclarationCommand     = "SKIP";
        public static readonly System.String LimitDeclarationCommand    = "LIMIT";
        public static readonly System.String TopDeclarationCommand      = "TOP";
        public static readonly System.String DistinctDeclarationCommand = "DISTINCT";

        public static readonly System.String GetColumnTypesDeclarationCommand = "GET_COLUMN_DATA_TYPES";
        public static readonly System.String GetDomainDeclarationCommand      = "GET_DOMAINS";
        public static readonly System.String GetBoundedEntitiesCommand        = "GET_BOUNDED_ENTITY";
        public static readonly System.String ReverseDeclarationCommand        = "REVERSE";


        public static readonly System.String MaxDeclarationCommand = "MAX";
        public static readonly System.String MinDeclarationCommand = "MIN";
        public static readonly System.String AvgDeclarationCommand = "AVG";
        public static readonly System.String SumDeclarationCommand = "SUM";
        public static readonly System.String CountDeclarationCommand = "COUNT";
        public static readonly System.String UnionDeclarationCommand = "UNION";

    }
}
