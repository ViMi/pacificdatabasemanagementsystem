﻿using DBMS.Core.Helpers;


namespace DBMS.Core.MMIL.ILConfiguration
{
    public class MMILDataBaseConfig
        : MMILBaseConfiguration
    {

#if DocTrue
        static MMILDataBaseConfig() => Documentation.AddHistoryToSection(
                        PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreMMIL,
            "MMILDataBaseConfig",
            PQLDocGlobal.TypeClass,
            PQLDocGlobal.DescriptionUndefined,
            "MMILBaseConfiguration"
            );
#endif

        public static readonly System.String ServerDeclarationCommand         = "SERVER";
        public static readonly System.String UserDeclarationCommand           = "USER";
        public static readonly System.String DataBaseDeclarationCommand       = "DATABASE";
        public static readonly System.String UserPrevilegeDeclarationCommand  = "USER_PRIVILEGE_LEVEL";
        public static readonly System.String TableDeclarationCommand          = "TABLE";
    }

}
