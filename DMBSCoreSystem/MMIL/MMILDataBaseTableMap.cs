﻿using DBMS.Core.Interprater;
using DBMS.Core.MMIL.ILConfiguration;
using DBMS.Core.Helpers;


namespace DBMS.Core.MMIL
{
    public class MMILDataBaseTableMap
        : MMILDataBaseContext
    {

#if DocTrue
        static MMILDataBaseTableMap() => Documentation.AddHistoryToSection(
            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreMMIL,
            "MMILDataBaseTableMap",
            PQLDocGlobal.TypeClass,
            PQLDocGlobal.DescriptionUndefined,
            "MMILDataBaseContext"
            );
#endif


        public override string StandartContextName { get; set; } = "DATA_BASE_TABLE_MAP";

        override public UnityType _UnityType { get; } = UnityType.DATA_BASE_TABLE_MAPPING_UNIT;


        /// <summary>
        /// Set Alias of Piece of Data
        /// </summary>
        /// <param name="alias"></param>
        public MMILDataBaseTableMap SetAlias(System.String alias)
        {
            this.IsThatContextClosed();
            SetAliasUnsafe(alias);
            return this;
        }



        /// <summary>
        /// Set Data Type name 
        /// </summary>
        /// <param name="dataType"></param>
        public MMILDataBaseTableMap SetDataType(System.String dataType)
        {
            this.IsThatContextClosed();
            SetDataTypeUnsafe(dataType);
            return this;
        }



        /// <summary>
        /// Set Attribute list (if it exist)
        /// </summary>
        /// <param name="attr"></param>
        public MMILDataBaseTableMap SetAttribute(System.String attr)
        {
            this.IsThatContextClosed();
            SetAttributeUnsafe(attr);
            return this;
        }


        /// <summary>
        /// Set Attribute value
        /// </summary>
        /// <param name="attrValue"></param>
        public MMILDataBaseTableMap SetAttributeValue(System.String attrValue)
        {
            this.IsThatContextClosed();
            SetAttributeValueUnsafe(attrValue);
            return this;
        }


        /// <summary>
        /// Set name of table of another container of data
        /// </summary>
        /// <param name="name"></param>
        public MMILDataBaseTableMap SetDataClassName(System.String name)
        {
            this.IsThatContextClosed();
            SetDataClassNameUnsafe(name);
            return this;

        }



        override public object Clone()
            => new MMILDataBaseTableMap()  { ILlisting = this.ILlisting, _ContextName = this._ContextName, _IsEnded = this._IsEnded };



        /// <summary>
        /// This unsave version of method. Without checks
        /// </summary>
        /// <param name="typeName"></param>
        public void SetDataTypeUnsafe(System.String typeName)=>
              this.ILlisting +=
              MMILDataBaseTableMappConfig.DataTypeDeclarationCommand
            + MMILDataBaseTableMappConfig.ParameterSepartorFromCommand
            + typeName.Trim()
            + MMILDataBaseTableMappConfig.EndOfCommandSymbol
            + MMILDataBaseTableMappConfig.NewLineCommand;

        /// <summary>
        /// This unsave version of method. Without checks
        /// </summary>
        /// <param name="name"></param>
        public void SetAliasUnsafe(System.String alias) =>
            this.ILlisting +=
            MMILDataBaseTableMappConfig.AliasDeclarationCommand
          + MMILDataBaseTableMappConfig.ParameterSepartorFromCommand
          + alias.Trim()
          + MMILDataBaseTableMappConfig.EndOfCommandSymbol
          + MMILDataBaseTableMappConfig.NewLineCommand;

        /// <summary>
        /// This unsave version of method. Without checks
        /// </summary>
        /// <param name="attribute"></param>
        public void SetAttributeUnsafe(System.String attribute) =>
            this.ILlisting +=
            MMILDataBaseTableMappConfig.AttributesDecalarationCommand
           + MMILDataBaseTableMappConfig.ParameterSepartorFromCommand
           + attribute.Trim()
           + MMILDataBaseTableMappConfig.EndOfCommandSymbol
           + MMILDataBaseTableMappConfig.NewLineCommand;



        /// <summary>
        /// This unsave version of method. Without checks
        /// </summary>
        /// <param name="attrValue"></param>
        public void SetAttributeValueUnsafe(System.String attrValue) =>
            this.ILlisting +=
            MMILDataBaseTableMappConfig.AttributeValueDeclarationCommand
            + MMILDataBaseTableMappConfig.ParameterSepartorFromCommand
            + attrValue.Trim()
            + MMILDataBaseTableMappConfig.EndOfCommandSymbol
            + MMILDataBaseTableMappConfig.NewLineCommand;



        /// <summary>
        /// This unsave version of method. Without checks
        /// </summary>
        /// <param name="name"></param>
        public void SetDataClassNameUnsafe(System.String name) =>
            this.ILlisting +=
            MMILDataBaseTableMappConfig.DataClassNameDaclarationCommand
            + MMILDataBaseTableMappConfig.ParameterSepartorFromCommand
            + name.Trim()
            + MMILDataBaseTableMappConfig.EndOfCommandSymbol
            + MMILDataBaseTableMappConfig.NewLineCommand;

    }
}
