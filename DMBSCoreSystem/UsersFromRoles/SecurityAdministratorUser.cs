﻿using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core;
using DBMS.Core.Helpers;

using DBMS.Core.Secure;
using DBMS.Core.Secure.UserRights.RightTable;
using DMBS.Core.Secure.UserRights.RightTable;
using DBMS.Core.IO;
using DBMS.Core.Secure.UserRights;


namespace DBMS.Core.UsersFromRoles
{
    public class SecurityAdministratorUser
        : IUserToken
    {

#if DocTrue
        static SecurityAdministratorUser() => Documentation.AddHistoryToSection(

            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreUsersFromRoles,
            "SecurityAdministratorUser",
            PQLDocGlobal.TypeClass,
            PQLDocGlobal.DescriptionUndefined
            );
#endif



        #region Variable Daclaration
        public AllowedCommandWithDB UserRights { get; set; }
        public String NodeName { get; set; } = DBMSIO.DBMSConfig.SimpleSecureAdmin;
        public String ChildrenList { get; set; }
        public Password NodeAccessPassword { get; set; }
        public NodeType Type { get; set; } = NodeType.USER;
        public String Parent { get; set; } = DBMSIO.DBMSConfig.DataBasesUsersDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty);
        public String Owner { get; set; } = DBMSIO.DBMSConfig.SimpleSecureAdmin;
        #endregion



        public SecurityAdministratorUser(System.String dbName)
        {

            Parent = dbName;

            UserRights = new AllowedCommandWithDB(

            new Dictionary<String, Secure.UserRights.AllowedCommand>(10)
            {
                [dbName] = new AllowedCommand(

                        new Dictionary<String, CommandRight>(20)
                        {
                            [Rbac.USE_COMMAND] = new CommandRight(
                                    new Dictionary<String, Boolean>(10)
                                    {
                                        [Rbac.GLOBAL_ALL_OBJECT_RIGHT_TOKEN] = true
                                    }
                                ),


                            [Rbac.BACKUP_COMMAND] = new CommandRight(
                                    new Dictionary<String, Boolean>(10)
                                    {
                                        [Rbac.GLOBAL_ALL_OBJECT_RIGHT_TOKEN] = true
                                    }
                                ),



                            [Rbac.MIGRATE_COMMAND] = new CommandRight(
                                    new Dictionary<String, Boolean>(10)
                                    {
                                        [Rbac.GLOBAL_ALL_OBJECT_RIGHT_TOKEN] = true
                                    }
                                 )

                        }

                    )


            }

            );
        }

  

        public IAllowedCommand GetTableRight(String tableName, String db) => null;
        public Dictionary<String, IAllowedCommand> GetTableRightForEach(String[] tableNames, String db) => null;

        public String GetUserLogin() => this.NodeName;
        public IPassword GetUserPassword() => this.NodeAccessPassword;

        public Boolean IsUserWorkWith(String chakKey) => false;
    }
}
