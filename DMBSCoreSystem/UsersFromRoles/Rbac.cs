﻿using System;
using System.Collections.Generic;
using DBMS.Core.Helpers;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Secure;

namespace DBMS.Core.UsersFromRoles
{
    public static class Rbac
    {


        #region Documentation
#if DocTrue
        static Rbac() => Documentation.AddHistoryToSection(

            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreUsersFromRoles,
            "Rbac",
            PQLDocGlobal.TypeStaticClass,
            PQLDocGlobal.DescriptionUndefined
            );

#endif
        #endregion



        #region Available Commands
        public static readonly String SELECT_COMMAND = "SELECT";
        public static readonly String CREATE_COMMAND = "CREATE";
        public static readonly String DROP_COMMAND = "DROP";
        public static readonly String ALTER_COMMAND = "ALTER";
        public static readonly String DELETE_COMMAND = "DELETE";
        public static readonly String INSERT_COMMAND = "INSERT";
        public static readonly String UPDATE_COMMAND = "UPDATE";
        public static readonly String GRANT_COMMAND = "GRANT";
        public static readonly String DENY_COMMAND = "DENY";
        public static readonly String REVOKE_COMMAND = "REVOKE";
        public static readonly String MIGRATE_COMMAND = "MIGRATE";
        public static readonly String ADMINER_COMMAND = "ADMINER";
        public static readonly String EXECUTE_COMMAND = "EXECUTE";
        public static readonly String USE_COMMAND = "USE";
        public static readonly String BACKUP_COMMAND = "BACKUP";
        public static readonly String CHANGE_PASSWORD = "CHANGE_PASSWORD";

        public static HashSet<String> AllAvailableCommnds = new HashSet<string>()
        {
            Rbac.SELECT_COMMAND,
            Rbac.CREATE_COMMAND,
            Rbac.DROP_COMMAND,
            Rbac.ALTER_COMMAND,
            Rbac.DELETE_COMMAND,
            Rbac.INSERT_COMMAND,
            Rbac.UPDATE_COMMAND,
            Rbac.GRANT_COMMAND,
            Rbac.DENY_COMMAND,
            Rbac.REVOKE_COMMAND,
            Rbac.MIGRATE_COMMAND,
            Rbac.ADMINER_COMMAND,
            Rbac.EXECUTE_COMMAND,
            Rbac.USE_COMMAND,
            Rbac.BACKUP_COMMAND,
            Rbac.CHANGE_PASSWORD
        };
        #endregion


        #region Placeholders
        public static readonly String DATABASE_PLACEHOLDER = DBMSSecure.GetRandomString();
        public static readonly String DATA_DEFAULT = "[DEFAULT]";
        public static readonly String DEFAULT_SERVER_TOKEN = "LOCAL";
        #endregion


        #region Commands Domain Tokens 
        public static readonly String DDL_TOKEN = "DDL";
        public static readonly String DML_TOKEN = "DML";
        public static readonly String DCL_TOKEN = "DCL";
        public static readonly String ADMIN_TOKEN = "ADMIN";
        public static readonly String EXTEND_TOKEN = "EXTEND";
        public static readonly String UNCLASSED_TOKEN = "UNCLASSED";
        #endregion


        #region Common Global Tokens
        public static readonly String ALL_DATA_MANIPULATION_OPERATIONS = "\\|?//?ALL+DML?\\|//";
        public static readonly String ALL_DATA_DECLARATION_OPERATIONS = "\\|?//?ALL+DDL?\\|//";
        public static readonly String ALL_DATA_CONTROL_OPERATIONS = "\\|?//?ALL+DCL?\\|//";
        public static readonly String ALL_DATA_CONTROL_EXTENDED_OPERATIONS = "\\|?//?ALL+DCLExt?\\|//";
        public static readonly String ALL_ADMINER_OPERATIONS = "\\|?//?ALL+ADMIN?\\|//";
        public static readonly String ALL_UNCLASSED_COMMAND = "\\|?//?ALL+UNCLASSED\\|//";
        public static readonly String ALL_COMPATIBILE_OPERATION_ALLOWED = "\\|?//?ALL+DML+DCL+DDL+DCLExt+ADMIN?\\|//";


        public static readonly String GLOBAL_ALL_OBJECT_RIGHT_TOKEN = "\\|?//?ALL?\\|//";
        public static readonly String GLOBAL_TABLE_RIGHT_TOKEN = "TABLES";
        public static readonly String GLOBAL_PROCEDURE_RIGHT_TOKEN = "PROCEDURES";
        public static readonly String GLOBAL_VIEW_RIGHT_TOKEN = "VIEW";
        public static readonly String GLOBAL_TRIGGER_RIGHT_TOKEN = "TRIGGER";
        public static readonly String GLOBAL_USER_RIGHT_TOKEN = "USER";
        public static readonly String GLOBAL_DATABASE_RIGHT_TOKEN = "DATABASE";

        public static readonly String UNCLASSED_COMMAND = "////|||?UNCLASSED?|||\\\\";


        #endregion




        internal static Dictionary<String, List<String>> RightMap { set; get; } = new Dictionary<string, List<string>>();

        public static System.String ResolveName(NodeType name) => name switch
        {
            NodeType.TABLE => "[TABLE]",
            NodeType.PROCEDURE => "[PROCEDURE]",
            NodeType.VIEW => "[VIEW]",
            NodeType.USER => "[USER]",
            NodeType.DATABASE => "[DATABASE]",
            NodeType.TRIGGER => "[TRIGGER]",
            NodeType.LOGIN => "[LOGIN]",
            NodeType.BACKUP => "[BACKUP]",
            NodeType.LOG_REPORT => "[LOG_REPORT]",
            NodeType.SECURITY_DIRECTORY => "[SECURITY]",
            NodeType.ROOT => "[SERVER]",
            NodeType.ROOT_DIRECTORY => "[SERVER]",
       
            _ => throw new TypeError()
        };



    }
}
