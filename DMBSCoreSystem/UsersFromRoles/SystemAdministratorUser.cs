﻿using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.Helpers;

using DBMS.Core.Secure;
using DBMS.Core.Secure.UserRights.RightTable;
using DMBS.Core.Secure.UserRights.RightTable;
using DBMS.Core.IO;
using DBMS.Core.Secure.UserRights;

namespace DBMS.Core.UsersFromRoles
{
    public class SystemAdministratorUser
        : IUserToken
    {


#if DocTrue
        static SystemAdministratorUser() => Documentation.AddHistoryToSection(
            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreUsersFromRoles,
            "SystemAdministratorUser",
            PQLDocGlobal.TypeClass,
            PQLDocGlobal.DescriptionUndefined

            );
#endif




        #region Variable Declaration
        public AllowedCommandWithDB UserRights { get; set; }

        public string NodeName { get; set; } = DBMSIO.DBMSConfig.SimpleSysAdminName;
        public string ChildrenList { get; set; }
        public Password NodeAccessPassword { get; set; }
        public NodeType Type { get; set; } = NodeType.USER;
        public string Parent { get; set; } = DBMSIO.DBMSConfig.DataBasesUsersDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty);
        public string Owner { get; set; } = DBMSIO.DBMSConfig.SimpleSysAdminName;
        #endregion



        public SystemAdministratorUser(System.String dbName)
        {

            Parent = dbName;


            UserRights = new AllowedCommandWithDB(

            new Dictionary<String, Secure.UserRights.AllowedCommand>(10)
            {
                [dbName] = new AllowedCommand(

                        new Dictionary<String, CommandRight>(20)
                        {

                            [Rbac.ALL_COMPATIBILE_OPERATION_ALLOWED] = new CommandRight(
                                    new Dictionary<String, Boolean>(10)
                                    {
                                        [Rbac.GLOBAL_ALL_OBJECT_RIGHT_TOKEN] = true
                                    }
                                )


                        }

                    )


            }
            );

        }


        public SystemAdministratorUser()
        { 

            UserRights = new AllowedCommandWithDB(

            new Dictionary<String, Secure.UserRights.AllowedCommand>(10)
            {
                [Rbac.DATABASE_PLACEHOLDER] = new AllowedCommand(

                        new Dictionary<String, CommandRight>(20)
                        {

                            [Rbac.ALL_COMPATIBILE_OPERATION_ALLOWED] = new CommandRight(
                                    new Dictionary<String, Boolean>(10)
                                    {
                                        [Rbac.GLOBAL_ALL_OBJECT_RIGHT_TOKEN] = true
                                    }
                                )


                        }

                    )


            }
            );

        }

        public IAllowedCommand GetTableRight(String tableName, String db) => null;
        public Dictionary<String, IAllowedCommand> GetTableRightForEach(String[] tableNames, String db) => null;

        public String GetUserLogin() => this.NodeName;
        public IPassword GetUserPassword() => this.NodeAccessPassword;

        public Boolean IsUserWorkWith(String chakKey) => false;
    }
}
