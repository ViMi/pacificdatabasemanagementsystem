﻿using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core;
using DBMS.Core.Secure;
using DBMS.Core.Secure.UserRights.RightTable;
using DMBS.Core.Secure.UserRights.RightTable;
using DBMS.Core.IO;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.Helpers;



namespace DBMS.Core.UsersFromRoles
{
    public class DataBaseCreatorUser
        : IUserToken
    {

#if DocTrue
        static DataBaseCreatorUser()
        {
            Documentation.AddHistoryToSection
            (
            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreUsersFromRoles,
            "DataBaseCreatorUser",
            PQLDocGlobal.TypeClass,
            PQLDocGlobal.DescriptionUndefined,
            "IUserToken"

            );

            Documentation.AddHistoryToSection(
                PQLDocGlobal.CoreSystem,
                PQLDocGlobal.CoreUsersFromRoles,
                "IUserToken",
                PQLDocGlobal.TypeInterface,
                PQLDocGlobal.DescriptionUndefined
                
                );

        }
#endif


        #region Variable Declaration
        public AllowedCommandWithDB UserRights { get ; set; }
        public String NodeName { get; set; } = DBMSIO.DBMSConfig.SimpleDBCreator;
        public String ChildrenList { get ; set; }
        public Password NodeAccessPassword { get ; set; }
        public NodeType Type { get; set; } = NodeType.USER;
        public String Parent { get; set; } = DBMSIO.DBMSConfig.DataBasesUsersDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty);
        public String Owner { get; set; } = DBMSIO.DBMSConfig.SimpleDBCreator;
        #endregion


        public DataBaseCreatorUser(System.String dbName)
        {
            Parent = dbName;


            UserRights = new AllowedCommandWithDB(

         new Dictionary<String, Secure.UserRights.AllowedCommand>(10)
         {
             [dbName] = new AllowedCommand(

                     new Dictionary<String, CommandRight>(20)
                     {


                         [Rbac.SELECT_COMMAND] = new CommandRight(
                                 new Dictionary<String, Boolean>(10)
                                 {
                                     [Rbac.GLOBAL_ALL_OBJECT_RIGHT_TOKEN] = true,

                                 }
                             ),


                         [Rbac.INSERT_COMMAND] = new CommandRight(
                                 new Dictionary<String, Boolean>(10)
                                 {
                                     [Rbac.GLOBAL_ALL_OBJECT_RIGHT_TOKEN] = true,

                                 }
                             ),

                         [Rbac.CREATE_COMMAND] = new CommandRight(
                                 new Dictionary<String, Boolean>(10)
                                 {
                                     [Rbac.GLOBAL_TABLE_RIGHT_TOKEN] = true,
                                     [Rbac.GLOBAL_PROCEDURE_RIGHT_TOKEN] = true,
                                     [Rbac.GLOBAL_VIEW_RIGHT_TOKEN] = true,
                                     [Rbac.GLOBAL_USER_RIGHT_TOKEN] = true,

                                 }
                             ),


                         [Rbac.EXECUTE_COMMAND] = new CommandRight(
                                 new Dictionary<String, Boolean>(10)
                                 {
                                     [Rbac.GLOBAL_ALL_OBJECT_RIGHT_TOKEN] = true
                                 }
                             ),


                         [Rbac.UPDATE_COMMAND] = new CommandRight(
                                 new Dictionary<String, Boolean>(10)
                                 {
                                     [Rbac.GLOBAL_ALL_OBJECT_RIGHT_TOKEN] = true,

                                 }
                             ),


                         [Rbac.ALTER_COMMAND] = new CommandRight(
                                 new Dictionary<String, Boolean>(10)
                                 {
                                     [Rbac.GLOBAL_TABLE_RIGHT_TOKEN] = true,
                                     [Rbac.GLOBAL_VIEW_RIGHT_TOKEN] = true,
                                     [Rbac.GLOBAL_PROCEDURE_RIGHT_TOKEN] = true,
                                     [Rbac.GLOBAL_USER_RIGHT_TOKEN] = true,
                                 }
                             ),

                         [Rbac.DROP_COMMAND] = new CommandRight(
                                 new Dictionary<String, Boolean>(10)
                                 {
                                     [Rbac.GLOBAL_TABLE_RIGHT_TOKEN] = true,
                                     [Rbac.GLOBAL_VIEW_RIGHT_TOKEN] = true,
                                     [Rbac.GLOBAL_PROCEDURE_RIGHT_TOKEN] = true,
                                     [Rbac.GLOBAL_USER_RIGHT_TOKEN] = true,
                                 }
                             ),


                         [Rbac.DELETE_COMMAND] = new CommandRight(
                                 new Dictionary<String, Boolean>(10)
                                 {
                                     [Rbac.GLOBAL_ALL_OBJECT_RIGHT_TOKEN] = true
                                 }
                             ),

                         [Rbac.USE_COMMAND] = new CommandRight(
                                 new Dictionary<String, Boolean>(10)
                                 {
                                     [Rbac.GLOBAL_ALL_OBJECT_RIGHT_TOKEN] = true
                                 }
                             ),


                     }

                 )


         }
         );
        }



        public IAllowedCommand GetTableRight(String tableName, String db) => null;
        public Dictionary<String, IAllowedCommand> GetTableRightForEach(String[] tableNames, String db) => null;

        public String GetUserLogin() => this.NodeName;
        public IPassword GetUserPassword() => this.NodeAccessPassword;

        public Boolean IsUserWorkWith(String chakKey) => false;
    }
}
