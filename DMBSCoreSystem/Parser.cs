﻿using DBMS.Core.Interprater;
using DBMS.Core.Helpers;



namespace DBMS.Core
{
    static internal class Parser
    {
        public static IParseStrategy GlobalParseStrategy { set; get; }



        #region Documentaiton
#if DocTrue
        static Parser() => Documentation.AddHistoryToSection(
            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreFlow,
            "Parser",
            PQLDocGlobal.TypeStaticClass,
            PQLDocGlobal.DescriptionUndefined
            );

#endif
        #endregion

    }
}
