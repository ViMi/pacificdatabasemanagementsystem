﻿using DBMS.Core.Components.Security.Decoders;
using DBMS.Core.Secure;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using DBMS.Core.Helpers;


namespace DBMS.Core
{
    public static class Cript
    {

        #region Documentation
#if DocTrue
        static Cript() => Documentation.AddHistoryToSection(
            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreSecurity,
            "Cript",
            PQLDocGlobal.TypeStaticClass,
            PQLDocGlobal.DescriptionUndefined
            );

#endif
        #endregion


        #region variable Declare
        public static IEncriptor Decoder { set; get; } = null;
        public delegate System.Boolean Resolve(EncriptionType type);
        public delegate IEncriptor CriptProvider(String name);


        public static Resolve resolve { set; get; }
        public static CriptProvider provider { set; get; }
        #endregion




        
        public static IEncriptor ResolveEncriptorByName(String criptName)
        {
            return (provider == null ? (provider = (String name) =>
            {
                switch (name)
                {
                    case "PlaneText": { return new PlaneTextCriptex(); }
                    case "Type5": { return new MD5Criptex(); }
                    default: { return null; }
                }

            })(criptName) : provider(criptName));
        }


        private static System.Boolean ResolveEncriptor(EncriptionType type)
        {



            return (resolve == null ? (resolve = (EncriptionType type) =>
             {
                 switch (type)
                 {
                     case EncriptionType.PlaneText: { Decoder = new PlaneTextCriptex(); } break;
                     case EncriptionType.Type5: { Decoder = new MD5Criptex(); } break;
                     case EncriptionType.Type7: { throw new Exception(); }
                     default: { } break;
                 }
                 return true;
             })(EncriptionType.PlaneText)
            : resolve(type)); 

        }


        public static System.String Encript(System.String password, EncriptionType type) 
        {

            ResolveEncriptor(type);
            return Decoder.Encript(password);

        }
        public static System.String Decript(System.String password, EncriptionType type) 
        {
            ResolveEncriptor(type);
            return Decoder.Decript(password);

        }


        public static System.String Decript(System.String password) => Decoder.Decript(password);

        public static System.String Encript(System.String password) => Decoder.Encript(password);

    }
}
