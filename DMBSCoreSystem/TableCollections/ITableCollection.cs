﻿using DBMS.Core.Secure.UserRights;
using DBMS.Core.InfoTypes;
using DBMS.Core.MMIL;
using System.Collections.Generic;

namespace DBMS.Core.TableCollections
{
    public interface ITableCollection
    {

        ITableCollection ReadMetaInfo();
        System.String GetWayToStored();
        ITableCollection StoreCurrentTable();
        ITableCollection ReadStoredTable();

        DBMS.Core.Table.Table _TableCollection { set; get; }
        System.String _DataBaseName { get; }
        System.String _DataBaseOwner { get; }
        System.String _WayToDB { get; }
        System.Int32 Length { set; get; }
        ITableInfo _TableInfo { set; get; }

        ITableInfo GetMetaInfoTable();

    }
}
