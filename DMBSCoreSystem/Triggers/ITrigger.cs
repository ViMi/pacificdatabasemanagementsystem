﻿using DBMS.Core;
using DBMS.Core.MMIL;
using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.IO;
using DBMS.Core.Table;



namespace DBMS.Core.Triggers
{


    public enum TriggerType
        : System.Byte
    {
        ON_DELETE = 0,
        ON_INSERT = 1,
        ON_UPDATE = 2
    }
    public interface ITrigger
    {

        NodeType Type { set; get; }
        TriggerType TriggerType { set; get; }
        List<MMILDataBaseContext> PrecompiledILCommand { set; get; }
        System.Object Invoke();

    }
}
