﻿using DBMS.Core.MMIL;
using DBMS.Core.IO;
using DBMS.Core.Helpers;

using System.Collections.Generic;

namespace DBMS.Core.Triggers
{
    public sealed class Trigger
        : ITrigger
    {

        #region Documentation
#if DocTrue
        static Trigger()
        {
          Documentation.AddHistoryToSection(
                PQLDocGlobal.CoreSystem,
                PQLDocGlobal.CoreComponents,
                "Trigger",
                PQLDocGlobal.TypeClass,
                PQLDocGlobal.DescriptionUndefined,
                "ITrigger"

            );

            Documentation.AddHistoryToSection(

            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreComponents,
            "ITrigger",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined

                );

        }
#endif
        #endregion

        public NodeType Type { get; set; } = NodeType.TRIGGER;
        public TriggerType TriggerType { get; set; } = TriggerType.ON_DELETE;
        public List<MMILDataBaseContext> PrecompiledILCommand { set; get; }

        public System.Object Invoke() => DBMSIO.ExecuteILCode(new MMILCollection() {ILUnits = PrecompiledILCommand});
    }
}
