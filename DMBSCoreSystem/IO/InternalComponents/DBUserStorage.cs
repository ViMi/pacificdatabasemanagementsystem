﻿using System;
using System.Collections.Generic;
using DBMS.Core.Secure.UserRights;
using System.Linq;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.TableCollections;
using DBMS.Core.InfoTypes;
using DBMS.Core.Helpers;

using DBMS.Core.Triggers;
using DBMS.Core.User;

using DBMS.Core.IO.InternalComponents;

using DBMS.Core.Interprater;

namespace DBMS.Core.IO.InternalComponents
{
    internal sealed class DBUserStorage
    {
        private IMetaInfo _UsersMeta = null;

        private String _WayToDB;
        private String _WayToDBRoot;
        private String _DataBaseName;
        private String _DataBaseOwner;

        public DBUserStorage(String wayToDB, String wayToDBRoot, String dbOwner, String dbName)
        {
            _WayToDBRoot = wayToDBRoot;
            _WayToDB = wayToDB;

            _DataBaseName = dbName;
            _DataBaseOwner = dbOwner;

            _UsersMeta = ReadUsersMeta();

        }

        public IUserToken GetUserByName(String userName)
        {
            String[] childName = _UsersMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator);
            String basePath = _WayToDBRoot + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + DBMSIO.DBMSConfig.BaseUserDir + DBMSIO.DBMSConfig.OSDirectoryWaySeparator;
            IUserToken info = null;

            foreach (String trig in childName)
            {
                if (trig != String.Empty)
                {
                    info = DBMSIO.ReadObject<DBMS.Core.User.User>(basePath + trig + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + DBMSIO.DBMSConfig.FullMetaFileName);
                    if (info.NodeName == userName)
                        return info;
                }
            }


            return null;

        }



        public int GetCountOfUser() => this._UsersMeta.ChildrenList.Trim().Split(DBMSIO.DBMSConfig.StandartSeparator).Length;


        public List<string> GetUserName()
        {
            List<String> viewNames = new List<String>();

            String[] vs = this._UsersMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator);

            foreach (String item in vs)
            {
                if (item != String.Empty)
                    viewNames.Add(item);
            }

            return viewNames;
        }


        public IUserToken GetUserTokenByName(System.String userName) => DBMSIO.ReadUserTokenByName(userName);



        public void ReWriteUserTokenByName(System.String name, IUserToken user) => DBMSIO.ReWriteUserTokenByname(name, user);


        private IMetaInfo ReadUsersMeta() =>
    DBMSIO.ReadMetaInfo(_WayToDB
        + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
        + DBMSIO.DBMSConfig.FullPathToTheDataBaseUsersMetaFile);


        public System.Boolean IsUserExist(System.String userName)
          => this._UsersMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator).Contains<String>(userName);



        public IUserToken GetUserByLoginName(System.String loginName)
        {
            List<String> names = this.GetUserName();
            foreach (String name in names)
            {
                if (name != String.Empty)
                {
                    IUserToken token = this.GetUserByName(name);
                    if (token != null && token.Owner == loginName)
                        return token;
                }
            }

            return null;
        }


        public System.Boolean IsUserWithLoginExist(System.String loginName) 
        {

           List<String> names = this.GetUserName();
            foreach (String name in names)
            {
                if (name != String.Empty)
                {
                    IUserToken token = this.GetUserByName(name);
                    if (token != null && token.Owner == loginName)
                        return true;
                }
            }


            return false;
        }



        public Boolean TruncateUser(System.String userName)
        {
            if (_UsersMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator).Contains<System.String>(userName))
            {

                _UsersMeta.ChildrenList
                     = _UsersMeta.ChildrenList.Replace(userName, System.String.Empty).Trim();
                DBMSIO.ReWriteMetaInfo(this._UsersMeta, _WayToDB + DBMSIO.DBMSConfig.FullPathToTheDataBaseUsersMetaFile);
                return true;
            }

            throw new UserNotExist();
        }


        public Boolean TruncateRightsFromUser(System.String userName, System.String objectOn, System.String objType, System.Boolean regim)
        {
            if (!IsUserExist(userName))
                throw new Exception();

            IUserToken token;
            if (DBMSIO.GetUserToken(_DataBaseName, out token))
            {
                AllowedCommand comm;
                if (token.UserRights.AllowedCommandOn.TryGetValue(_DataBaseName, out comm))
                {
                    CommandRight cr;

                    if (comm.AllowCommand.TryGetValue(objType, out cr))
                    {

                        cr.CommandRights.Remove(objectOn);

                    }

                }

            }


            return false;
        }
     
        
        public Boolean TruncateObjTypeToUser(System.String userName, System.String objectOn, System.String objType)
        {
            if (!IsUserExist(userName))
                throw new Exception();

            IUserToken token;
            if (DBMSIO.GetUserToken(_DataBaseName, out token))
            {
                AllowedCommand comm;
                if (token.UserRights.AllowedCommandOn.TryGetValue(_DataBaseName, out comm))
                {
                    CommandRight cr = new CommandRight();

                    comm.AllowCommand.Remove(objType);
                    return true;
                }

            }

            return false;
        }



        public Boolean AppendNewUser(String userName)
        {
            this._UsersMeta.ChildrenList += DBMSIO.DBMSConfig.StandartSeparator + userName;
            DBMSIO.ReWriteMetaInfo(this._UsersMeta, _WayToDB + DBMSIO.DBMSConfig.FullPathToTheDataBaseUsersMetaFile);

            return true;

        }



        public Boolean AppendRightsToUser(System.String userName, System.String objectOn, System.String objType, System.Boolean regim)
        {
            if (!IsUserExist(userName))
                throw new Exception();

            IUserToken token;
            if (DBMSIO.GetUserToken(_DataBaseName, out token))
            {
                AllowedCommand comm;
                if (token.UserRights.AllowedCommandOn.TryGetValue(_DataBaseName, out comm))
                {
                    CommandRight cr;

                    if (comm.AllowCommand.TryGetValue(objType, out cr))
                    {

                        cr.CommandRights.Add(objectOn, regim);

                    }

                }

            }


            return false;
        }



        public void RemoveRightFromUser(System.String userName, System.String right, System.String objName)
        {
            IUserToken token = GetUserByName(userName);
            
            foreach(AllowedCommand ac in token.UserRights.AllowedCommandOn.Values)
                if (ac.AllowCommand.ContainsKey(right))
                {
                    CommandRight cr;
                    if(ac.AllowCommand.TryGetValue(right, out cr))
                        if (cr.CommandRights.ContainsKey(objName))
                            cr.CommandRights.Remove(objName);
                        
                    
                }

        }


        public Boolean AppendObjTypeToUser(System.String userName, System.String objectOn, System.String objType)
        {
            if (!IsUserExist(userName))
                throw new Exception();

            IUserToken token;
            if (DBMSIO.GetUserToken(_DataBaseName, out token))
            {
                AllowedCommand comm;
                if (token.UserRights.AllowedCommandOn.TryGetValue(_DataBaseName, out comm))
                {
                    CommandRight cr = new CommandRight();

                    comm.AllowCommand.Add(objType, cr);
                    return true;
                }

            }

            return false;
        }


    }
}
