﻿using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.MMIL;
using System.IO;
using System.Linq;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.TableCollections;
using DBMS.Core.InfoTypes;
using DBMS.Core.Helpers;

using DBMS.Core.Triggers;
using DBMS.Core.User;

using DBMS.Core.IO.InternalComponents;

using DBMS.Core.Interprater;

namespace DBMS.Core.IO.InternalComponents
{
    internal sealed class DBProcedureStorage
    {

        private IMetaInfo _ProceduresMeta = null;
        private String _WaytToDBRoot;
        private String _WayToDB;


        public DBProcedureStorage(String wayToRoot, String wayToDB)
        {
            _WaytToDBRoot = wayToRoot;
            _WayToDB = wayToDB;
            _ProceduresMeta = ReadProcedureMeta();
        }

        public StoredProcedure GetStoredProcedure(System.String procName)
          => DBMSIO.ReadObject<StoredProcedure>(_WaytToDBRoot 
              + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
              + DBMSIO.DBMSConfig.DataBasesProcedureDir 
              + DBMSIO.DBMSConfig.OSDirectoryWaySeparator 
              + procName 
              + DBMSIO.DBMSConfig.ILExtension);

        public int GetCountOfProcedures() => this._ProceduresMeta.ChildrenList.Trim().Split(DBMSIO.DBMSConfig.StandartSeparator).Length;


        public List<string> GetProceduresName()
        {
            List<String> viewNames = new List<String>();

            String[] vs = this._ProceduresMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator);

            foreach (String item in vs)
            {
                if (item != String.Empty)
                    viewNames.Add(item);
            }

            return viewNames;
        }



        public string GetStoredProcedureText(String procName)
        {
            String str = String.Empty;

            StoredProcedure way = GetStoredProcedure(procName); 

            foreach(var item in way.PrecompiledILCommand)
                str += item.ILlisting + Environment.NewLine;
            

            return str;

        }


        private IMetaInfo ReadProcedureMeta() =>
    DBMSIO.ReadMetaInfo(_WayToDB
        + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
        + DBMSIO.DBMSConfig.FullPathToTheDataBaseProceduresMetaFile);


        public MMILDataBaseContext ReadStoredProcedureMMIL(System.String procedureName)
        {
            return (new MMILDataBaseContext(File.ReadAllText(
                   _WayToDB
                 + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                 + DBMSIO.DBMSConfig.DataBasesProcedureDir
                 + procedureName
                 + DBMSIO.DBMSConfig.FullMetaFileName)));
        }

        
        public System.Boolean IsProcedureExist(System.String procName)
        => this._ProceduresMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator).Contains<String>(procName);


        public System.Boolean AppendNewProcedure(System.String procName)
        {
            this._ProceduresMeta.ChildrenList += DBMSIO.DBMSConfig.StandartSeparator + procName;
            DBMSIO.ReWriteMetaInfo(this._ProceduresMeta, _WayToDB + DBMSIO.DBMSConfig.FullPathToTheDataBaseProceduresMetaFile);
            return true;
        }


        public Boolean TruncateProcedure(System.String procName)
        {
            if (_ProceduresMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator).Contains<System.String>(procName))
            {

                _ProceduresMeta.ChildrenList
                     = _ProceduresMeta.ChildrenList.Replace(procName, System.String.Empty).Trim();
                DBMSIO.ReWriteMetaInfo(this._ProceduresMeta, _WayToDB + DBMSIO.DBMSConfig.FullPathToTheDataBaseProceduresMetaFile);

                return true;
            }

            throw new ProcedureNotExist();
        }


    }
}
