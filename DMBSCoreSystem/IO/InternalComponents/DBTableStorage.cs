﻿using System;
using System.Collections.Generic;
using DBMS.Core.MMIL;
using DBMS.Core.Table;
using DBMS.Core.Secure.UserRights;
using System.Linq;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.TableCollections;

using DBMS.Core.IO.InternalComponents;

using DBMS.Core.Interprater;

namespace DBMS.Core.IO.InternalComponents
{
    internal sealed class DBTableStorage
    {
        
        
        private IMetaInfo _TablesMeta = null;
        public List<ITableInfo> DataBaseTablesMeta { get; set; }

        private String _WayToDB;
        private String _WayToDBRoot;
        private String _DataBaseOwner;
        private String _DataBaseName;
        
        
        public DBTableStorage(String wayToDBRoot, String wayToDB, String dataBaseOwner, String dataBaseName)
        {
            _WayToDBRoot = wayToDBRoot;
            _WayToDB = wayToDB;
            _DataBaseName = dataBaseName;
            _DataBaseOwner = dataBaseOwner;

            _TablesMeta = ReadTablesMeta();
            DataBaseTablesMeta = ReadDataBaseTablesMeta();
        }
        
        
        
        public ITableInfo GetTableMetaByName(string tableName)
        {
            for (System.Int32 i = 0; i < this.DataBaseTablesMeta.Count; ++i)
                if (this.DataBaseTablesMeta.ElementAt(i).TableName == tableName)
                    return this.DataBaseTablesMeta.ElementAt(i);



            throw new TableNotExist();
        }


        public System.Boolean AppendNewTable(System.String tableName)
        {
            this._TablesMeta.ChildrenList += DBMSIO.DBMSConfig.StandartSeparator + tableName;
            DBMSIO.ReWriteMetaInfo(this._TablesMeta, _WayToDB + DBMSIO.DBMSConfig.FullPathToTheDataBaseTableMetaFile);

            return true;
        }

    
        
        public int GetCountOfTables() => this._TablesMeta.ChildrenList.Trim().Split(DBMSIO.DBMSConfig.StandartSeparator).Length;



        public List<string> GetTableNames()
        {
            List<String> viewNames = new List<String>();

            String[] vs = this._TablesMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator);

            foreach (String item in vs)
            {
                if (item != String.Empty)
                    viewNames.Add(item);
            }

            return viewNames;
        }



        public ITable ReadTable(System.String tableName) => DBMSIO.ReadObject<ITable>(this._WayToDB + DBMSIO.DBMSConfig.FullMetaFileName);


        private IMetaInfo ReadTablesMeta() =>
    DBMSIO.ReadMetaInfo(_WayToDB
        + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
        + DBMSIO.DBMSConfig.FullPathToTheDataBaseTableMetaFile);




        private List<ITableInfo> ReadDataBaseTablesMeta()
        {
            System.String[] coll =
                this._TablesMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator);
            List<ITableInfo> list = new List<ITableInfo>(5);

            foreach (System.String i in coll)
            {
                if (i == String.Empty) continue;
                ITableInfo tableInfo =
                    DBMSIO.ReadTableMetaInfo(
                        _WayToDBRoot
                      + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                      + DBMSIO.DBMSConfig.DataBaseTableDir
                      + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                      + i
                      + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                      + DBMSIO.DBMSConfig.FullMetaFileName
                      );

                list.Add(tableInfo);
            }

            return list;
        }


        public System.Boolean IsExistForeignConstraintToCell(ITableCell cell, String tableName)
        {

            ITableInfo ti
                = DataBaseTablesMeta.Find(x => x.TableName == tableName);




            foreach (ITableInfo item in DataBaseTablesMeta)
            {

                if (item.TableName != ti.TableName && item.FK.Count != 0)
                {
                    foreach (String foreignName in item.FK)
                    {
                        Dictionary<String, string> attr;
                        item.ColumnConstraint.TryGetValue(foreignName, out attr);
                        string references;
                        attr.TryGetValue("FOREIGN_KEY_REFERENCES", out references);


                        if (references.Split('[')[0] == tableName)
                        {



                            MMILDataManipulationContext mmil = new MMILDataManipulationContext();
                            mmil.InitExecutionContext("Select");
                            mmil.SetFrom(references.Split('[')[0]);
                            mmil.SetRows("*");
                            mmil.EndExecuteContext();


                            ITable coll =
                             DBMSIO.ExecuteCommand
                             (DBCommand.SELECT,
                             new TransactionInfo
                             {
                                 UserName = _DataBaseOwner,
                                 DataBaseName = _DataBaseOwner,
                                 Parameters = item.TableName,
                                 AddedInfo = mmil
                             }).AddedInfo as ITable;


                            if (coll == null)
                                return false;


                            foreach (TableRow row in coll.TableRow.Values)
                            {
                                if (row.RowCells.Exists(x => x.Alias == cell.Alias))
                                    return true;
                            }


                        }


                    }
                }

            }






            return false;
        }
     
        
        public System.Boolean IsExistForeignConstraintToCellWithValue(ITableCell cell, string tableName)
        {


            ITableInfo ti
                = DataBaseTablesMeta.Find(x => x.TableName == tableName);



            foreach (ITableInfo item in DataBaseTablesMeta)
            {

                if (item.TableName != ti.TableName && item.FK.Count != 0)
                {

                    MMILDataManipulationContext mmil = new MMILDataManipulationContext();
                    mmil.InitExecutionContext("Select");
                    mmil.SetFrom(item.TableName);
                    mmil.SetRows("*");
                    mmil.EndExecuteContext();



                    ITable coll =
                     DBMSIO.ExecuteCommand
                     (DBCommand.SELECT,
                     new TransactionInfo
                     {
                         UserName = _DataBaseOwner,
                         DataBaseName = _DataBaseOwner,
                         Parameters = item.TableName,
                         AddedInfo = mmil
                     }).AddedInfo as ITable;

                    foreach (String foreignName in item.FK)
                    {
                        Dictionary<String, string> attr;
                        item.ColumnConstraint.TryGetValue(foreignName, out attr);
                        String references;
                        attr.TryGetValue("FOREIGN_KEY_REFERENCES", out references);
                        String refer = references as String;
                        String referField = refer.Split('[')[1].Replace("]", "").Trim('"');
                        String referFieldFull = tableName + "*" + referField;
                        if (refer.Split('[')[0] == tableName && ( referField == cell.Alias || referFieldFull == cell.Alias))
                        {

                            String fkAllies = foreignName;
                            
                        


                            if (coll == null)
                                return false;


                            foreach (TableRow row in coll.TableRow.Values)
                            {

                                foreach (TableCell tc in row.RowCells)
                                {
                                    if (tc.Alias == fkAllies)
                                    {
                                        if (tc.ColumnData != null)
                                        {
                                            if (tc.DataType == PacificDataBaseDataType.NUMBER)
                                            {
                                                if (Int32.Parse(tc.ColumnData) == Int32.Parse(cell.ColumnData))
                                                    return true;
                                            }

                                            if (tc.DataType == PacificDataBaseDataType.STRING)
                                            {
                                                if (String.Compare(tc.ColumnData, cell.ColumnData) == 0)
                                                    return true;
                                            }

                                        }
                                    }
                                }


                            }


                        }


                    }
                }

            }


            return false;

        }
   
        
        public System.Boolean IsExistForeignConstraintToTable(String tableName)
        {
            ITableInfo ti = DataBaseTablesMeta.Find(x => x.TableName == tableName);
            foreach (ITableInfo item in DataBaseTablesMeta)
            {

                if (item.TableName != ti.TableName && item.FK.Count != 0)
                {
                    foreach (String foreignName in item.FK)
                    {
                        Dictionary<String, String> attr;
                        item.ColumnConstraint.TryGetValue(foreignName, out attr);
                        String references;
                        attr.TryGetValue("FOREIGN_KEY_REFERENCES", out references);
                        String refer = references as String;

                        if (refer.Split('[')[0] == tableName)
                            return true;

                    }
                }

            }


            return false;
        }



        public System.Boolean FlushTableMeta(System.String tableName)
        {
            for (System.Int32 i = 0; i < DataBaseTablesMeta.Count; ++i)
                if (DataBaseTablesMeta.ElementAt(i).TableName == tableName)
                {
                    DataBaseTablesMeta.RemoveAt(i);
                    return true;
                }

            return false; 
        }


        public System.Boolean IsColumnWithNameExsitInTableWithName(System.String columnName, System.String tableName)
        {
            if (this.IsTableExist(tableName))
            {
                String columnNameFull = columnName.Replace("\"", "");
                ITableInfo info = this.GetTableMetaByName(tableName);
                if (info.ColumnTypes.Keys.Contains(tableName + "*" + columnNameFull) || info.ColumnTypes.Keys.Contains(columnNameFull))
                    return true;
            }
            return false;

        }
     
        
        public System.Boolean IsValueExistIntColumnWithNameInTableWithName(System.String columnName, System.String tableName, System.Object values)
        {
            if (this.IsTableExist(tableName))
            {
                ITableInfo info = this.GetTableMetaByName(tableName);
                ITableCollection collection =
                    DBMSIO.GetReadedTableDate(new TransactionInfo(this._DataBaseName, this._DataBaseOwner, tableName));


                for (Int32 i = 0; i < collection._TableCollection.TableRow.Count; ++i)
                {
                    ITableRow rows = collection._TableCollection.TableRow.ElementAt(i).Value;

                    for (Int32 j = 0; j < rows.RowCells.Count; ++j)
                    {
                        ITableCell cell = rows.RowCells.ElementAt(j);
                        String data = ((String)values).Trim();
                        String column = columnName.Replace("\"", "");
                        if ((cell.Alias == column || cell.Alias == tableName+"*"+column) && cell.ColumnData == data)
                            return true;
                    }

                }



                return false;
            }

            return false;
        }



        public System.Boolean IsTableExist(System.String tableName)
         => this._TablesMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator).Contains<String>(tableName);


        public System.Boolean IsEachTableExist(System.String[] tableNames)
        {
            System.Boolean tableCheck = false;

            foreach (System.String i in tableNames)
                tableCheck = this.IsTableExist(i);
            

            return tableCheck;
        }



        public Boolean TruncateTable(System.String tableName)
        {
            if (_TablesMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator).Contains<System.String>(tableName))
            {

                _TablesMeta.ChildrenList
                     = _TablesMeta.ChildrenList.Replace(tableName, System.String.Empty).Trim();
                DBMSIO.ReWriteMetaInfo(this._TablesMeta, _WayToDB + DBMSIO.DBMSConfig.FullPathToTheDataBaseTableMetaFile);

                return true;
            }

            throw new TableNotExist();
        }


        public void ReWriteMetaTableInfoByTableName(System.String tableName) =>
        DBMSIO.ReWriteTableMetaInfo(GetTableMetaByName(tableName),
          _WayToDBRoot
        + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
        + DBMSIO.DBMSConfig.DataBaseTableDir
        + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
        + tableName
        + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
        + DBMSIO.DBMSConfig.FullMetaFileName);



        public System.Boolean AppendTableMetaInfo(ITableInfo ti)
        {
            if (!this.DataBaseTablesMeta.Contains(ti))
            {
                this.DataBaseTablesMeta.Add(ti);
                return true;
            }
            else
                return false;

        }



        public IMetaInfo ReadTableMeta(System.String tableName)
    => DBMSIO.ReadMetaInfo(
          _WayToDB
        + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
        + DBMSIO.DBMSConfig.DataBaseTableDir
        + tableName
        + DBMSIO.DBMSConfig.FullMetaFileName);

    }
}
