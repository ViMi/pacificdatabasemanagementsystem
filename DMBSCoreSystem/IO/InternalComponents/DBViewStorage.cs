﻿using System;
using System.Collections.Generic;
using System.Text;

using DBMS.Core.MMIL;
using System.IO;
using DBMS.Core.Table;
using DBMS.Core.Secure.UserRights;
using System.Linq;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.TableCollections;
using DBMS.Core.InfoTypes;
using DBMS.Core.Helpers;

using DBMS.Core.Triggers;
using DBMS.Core.User;

using DBMS.Core.IO.InternalComponents;

using DBMS.Core.Interprater;


namespace DBMS.Core.IO.InternalComponents
{
    internal sealed class DBViewStorage
    {
        private IMetaInfo _ViewMeta = null;

        private String _WayToDBRoot;
        private String _WayToDB;

        public DBViewStorage(String watToDbRoot, String wayToDb)
        {
            _WayToDB = wayToDb;
            _WayToDBRoot = watToDbRoot;

            _ViewMeta = ReadViewMeta();
        }


        public ITable GetViewsTable(System.String viewName)
        {

            System.String path = _WayToDBRoot +
                DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.DataBaseViewDir
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + viewName + DBMSIO.DBMSConfig.ILExtension;

            DBMSIO.VIEW_TABLE = true;
            ITransactionMethodResult result = (DBMSIO.ReadObject<View>(path).Invoke() as IExecuteResult)._CurrentTI;
            DBMSIO.VIEW_TABLE = false;
            if (result.IsSuccess)
                return result.AddedInfo as ITable;

            else throw new SelectError();
        }


        public int GetCountOfViews() => this._ViewMeta.ChildrenList.Trim().Split(DBMSIO.DBMSConfig.StandartSeparator).Length;


        public List<string> GetViewsNames()
        {

            List<String> viewNames = new List<String>();

            String[] vs = this._ViewMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator);

            foreach (String item in vs)
                if (item != String.Empty)
                    viewNames.Add(item);
            

            return viewNames;

        }


        public string GetViewText(String viewName)
        {
            String str = String.Empty;


            System.String path = _WayToDBRoot +
                DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.DataBaseViewDir
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + viewName + DBMSIO.DBMSConfig.ILExtension;


            View result = DBMSIO.ReadObject<View>(path);

            foreach (var item in result.PrecompiledILCommand)
                str += item.ILlisting + Environment.NewLine;


            return str;
        }



        private IMetaInfo ReadViewMeta() =>
          DBMSIO.ReadMetaInfo(_WayToDB
        + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
        + DBMSIO.DBMSConfig.FullPathToTheDataBaseViewMetaFile);


        public System.Boolean IsViewExist(System.String viewName)
                 => this._ViewMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator).Contains<String>(viewName);



        public System.Boolean TruncateView(System.String viewName)
        {
            if (_ViewMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator).Contains<System.String>(viewName))
            {

                _ViewMeta.ChildrenList
                    = _ViewMeta.ChildrenList.Replace(viewName, System.String.Empty).Trim();
                DBMSIO.ReWriteMetaInfo(this._ViewMeta, _WayToDB + DBMSIO.DBMSConfig.FullPathToTheDataBaseViewMetaFile);

                return true;
            }

            throw new ViewNotExist();
        }



        public System.Boolean AppendNewView(System.String viewName)
        {
            this._ViewMeta.ChildrenList += DBMSIO.DBMSConfig.StandartSeparator + viewName;
            DBMSIO.ReWriteMetaInfo(this._ViewMeta, _WayToDB + DBMSIO.DBMSConfig.FullPathToTheDataBaseViewMetaFile);

            return true;
        }
    }
}
