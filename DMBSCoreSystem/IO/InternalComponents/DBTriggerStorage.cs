﻿using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.MMIL;
using DBMS.Core.Table;
using DBMS.Core.Secure.UserRights;
using System.Linq;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.TableCollections;
using DBMS.Core.InfoTypes;
using DBMS.Core.Helpers;

using DBMS.Core.Triggers;
using DBMS.Core.User;

using DBMS.Core.IO.InternalComponents;

using DBMS.Core.Interprater;

namespace DBMS.Core.IO.InternalComponents
{
    internal sealed class DBTriggerStorage
    {

        private IMetaInfo _TriggerMeta = null;

        private String _WayToDBRoot;
        private String _WayToDB;


        public DBTriggerStorage(String wayToDB, String wayTDBRoot)
        {
            _WayToDB = wayToDB;
            _WayToDBRoot = wayTDBRoot;

            _TriggerMeta = ReadTriggersMeta();
        }


        public Trigger GetTrigger(System.String trigName)
       => DBMSIO.ReadObject<Trigger>(_WayToDBRoot 
           + DBMSIO.DBMSConfig.OSDirectoryWaySeparator 
           + DBMSIO.DBMSConfig.TriggersBaseDir 
           + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
           + trigName 
           + DBMSIO.DBMSConfig.OSDirectoryWaySeparator 
           + trigName 
           + DBMSIO.DBMSConfig.ILExtension);


        public String GetTextTrigger(System.String trigName)
        {
            Trigger trig = GetTrigger(trigName);
            String res = String.Empty;
            foreach (MMILDataBaseContext contex in trig.PrecompiledILCommand)
                res += contex.ILlisting + Environment.NewLine;

            return res;
        
        }

        private IMetaInfo ReadTriggersMeta() =>
    DBMSIO.ReadMetaInfo(_WayToDB
        + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
        + DBMSIO.DBMSConfig.TriggersBaseDir
        + DBMSIO.DBMSConfig.FullMetaFileName);

        public System.Boolean IsTriggerExist(System.String triggerName)=> this._TriggerMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator).Contains<String>(triggerName);



        public System.Boolean IsTableHasDeleteTrigger(System.String tableName)
        {
            String[] childName = _TriggerMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator);
            String basePath = _WayToDBRoot + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + DBMSIO.DBMSConfig.TriggersBaseDir + DBMSIO.DBMSConfig.OSDirectoryWaySeparator;

            foreach (String trig in childName)
            {
                if (trig != String.Empty)
                {
                    IMetaInfo info = DBMSIO.ReadMetaInfo(basePath + trig + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + DBMSIO.DBMSConfig.FullMetaFileName);
                    if (info.Type == NodeType.DELETE_TRIGGER && info.Owner == tableName)
                        return true;
                }
            }


            return false;

        }


        public System.Boolean IsTableHasInsertTrigger(System.String tableName)
        {

            String[] childName = _TriggerMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator);
            String basePath = _WayToDBRoot + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + DBMSIO.DBMSConfig.TriggersBaseDir + DBMSIO.DBMSConfig.OSDirectoryWaySeparator;

            foreach (String trig in childName)
            {
                if (trig != String.Empty)
                {
                    IMetaInfo info = DBMSIO.ReadMetaInfo(basePath + trig + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + DBMSIO.DBMSConfig.FullMetaFileName);
                    if (info.Type == NodeType.INSERT_TRIGGER && info.Owner == tableName)
                        return true;
                }
            }

            return false;

        }
     
        
        public System.Boolean IsTableHasUpdateTrigger(System.String tableName)
        {
            String[] childName = _TriggerMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator);
            String basePath = _WayToDBRoot + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + DBMSIO.DBMSConfig.TriggersBaseDir + DBMSIO.DBMSConfig.OSDirectoryWaySeparator;

            foreach (String trig in childName)
            {
                if (trig != String.Empty)
                {
                    IMetaInfo info = DBMSIO.ReadMetaInfo(basePath + trig + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + DBMSIO.DBMSConfig.FullMetaFileName);
                    if (info.Type == NodeType.UPDATE_TRIGGER && info.Owner == tableName)
                        return true;
                }
            }

            return false;

        }


        public System.Boolean IsTableHasTriggers(System.String tableName)
        {
            String[] childName = _TriggerMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator);
            String basePath = _WayToDBRoot + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + DBMSIO.DBMSConfig.TriggersBaseDir + DBMSIO.DBMSConfig.OSDirectoryWaySeparator;

            foreach (String trig in childName)
            {
                if (trig != String.Empty)
                {
                    IMetaInfo info = DBMSIO.ReadMetaInfo(basePath + trig + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + DBMSIO.DBMSConfig.StandartMetaFileExtension);
                    if (info.Owner == tableName)
                        return true;
                }

            }


            return false;
        }


        public void ExecuteTriggerOnTable(System.String tableName, NodeType triggerType)
        {
            String[] childName = _TriggerMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator);
            String basePath = _WayToDBRoot + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + DBMSIO.DBMSConfig.TriggersBaseDir + DBMSIO.DBMSConfig.OSDirectoryWaySeparator;

            foreach (String trig in childName)
            {
                if (trig != String.Empty)
                {
                    IMetaInfo info = DBMSIO.ReadMetaInfo(basePath + trig + DBMSIO.DBMSConfig.OSDirectoryWaySeparator + DBMSIO.DBMSConfig.FullMetaFileName);
                    if (info.Owner == tableName && info.Type == triggerType)
                        this.GetTrigger(trig).Invoke();
                }
            }

        }

        public System.Boolean TruncateTrigger(System.String triggerName)
        {
            if (_TriggerMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator).Contains<System.String>(triggerName))
            {

                _TriggerMeta.ChildrenList
                     = _TriggerMeta.ChildrenList.Replace(triggerName, System.String.Empty).Trim();
                DBMSIO.ReWriteMetaInfo(this._TriggerMeta, _WayToDBRoot 
                    + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                    + DBMSIO.DBMSConfig.TriggersBaseDir
                    + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                    + DBMSIO.DBMSConfig.FullMetaFileName);

                return true;
            }

            throw new TriggerNotExist();
        }

        public System.Boolean AppendNewTrigger(System.String triggerName)
        {
            if (this._TriggerMeta.ChildrenList.Split(DBMSIO.DBMSConfig.StandartSeparator).Contains(triggerName))
                return false;

            this._TriggerMeta.ChildrenList += DBMSIO.DBMSConfig.StandartSeparator + triggerName;
            DBMSIO.ReWriteMetaInfo(_TriggerMeta, _WayToDBRoot 
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator 
                + DBMSIO.DBMSConfig.TriggersBaseDir 
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.FullMetaFileName);

            return true;

        }
    }
}
