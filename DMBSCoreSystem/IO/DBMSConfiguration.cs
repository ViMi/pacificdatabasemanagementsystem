﻿
using DBMS.Core.Helpers;

namespace DBMS.Core.IO
{
    public sealed class DBMSConfiguration 
        : IDBMSConfig
    {

        #region Documentation
#if DocTrue
        static DBMSConfiguration()
        {
            Documentation.AddHistoryToSection(
                            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreIO,
            "DBMSConfiguration",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "IDBMSConfig"
                );



            Documentation.AddHistoryToSection(
                PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreGlobalInterfaces,
            "IDBMSConfig",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined);

        }
#endif
        #endregion


        #region Variable Declaration
        public System.Char StandartSeparator  { set; get; }
        public System.String StandartMetaFileName { set; get; }
        public System.String StandartMetaFileExtension { set; get; }
        public System.String BaseDir { set; get; }
        public System.String BaseUserDir { set; get; }
        public System.String BaseSecurityDir { set; get; }
        public System.String BaseLoginDir { set; get; }
        public System.String BaseBackUpDir { set; get; }
        public System.String DataBasesProcedureDir { set; get; }
        public System.String DataBaseViewDir { set; get; }
        public System.String DataBasesStandartDir { set; get; }
        public System.String DataBasesUsersDir { set; get; }
        public System.String DataBaseTableDir { set; get; }
        public System.String OSDirectoryWaySeparator { set; get; }
        public System.String ILExtension { set; get; }
        public System.String PQLExtensio { set; get; }

        public System.String BaseDataBaseDir { set; get; }
        public System.String TriggersBaseDir { set; get; }

        public System.String FullMetaFileName { set; get; }
        public System.String FullPathToSecureRoot { set; get; }
        public System.String FullPathToLoginRoot { set; get; }
        public System.String FullPathToBackUp { set; get; }
        public System.String FullPathToUsers { set; get; }
        public System.String BaseJournalDir { set; get; }
        public System.String BaseJournalFileName { set; get; }
        public System.String BaseJournalFileExtension { set; get; }
        public System.String FullPathToServerMetaFile { set; get; }
        public System.String FullPathToTheUsersMetaFile { set; get; }
        public System.String FullPathToTheSecurityMetaFile { set; get; }
        public System.String FullPathToTheLoginMetaFile { set; get; }
        public System.String FullPathToDataBaseMetaFile { set; get; }
        public System.String FullPathToTheDataBaseUsersMetaFile { set; get; }
        public System.String FullPathToTheDataBaseProceduresMetaFile { set; get; }
        public System.String FullPathToTheDataBaseViewMetaFile { set; get; }
        public System.String FullPathToTheDataBaseTableMetaFile { set; get; }
        public System.String FullPathToTheBackUpMetaFile { set; get; }

        public System.String FullPathToTheDataBaseView { set; get; }
        public System.String FullPathToTheDataBaseProcedure { set; get; }
        public System.String FullPathToTheDataBaseTable { set; get; }
        public System.String FullPathToTheDataBaseUsers { set; get; }

        public System.String PathToLog { set; get; }
        public  System.String StandartLogFileName { set; get; }
        public System.String StandartLogFileExtension { set; get; }
        public System.String SimpleSysAdminName { set; get; }
        public System.String SimpleSecureAdmin { set; get; }
        public System.String SimpleDBCreator { set; get; }
        public System.String FullPathToTheAdmin { set; get; }
        public System.String FullPathToTheDBCreator { set; get; }
        public System.String FullPathToTheSecureAdmin { set; get; }

        public System.String ServerName { set; get; }


        public System.String MaxStringLength { set; get; }
        public System.String MaxNumber { set; get; }


        #endregion

        public System.String GetPathToLogin(System.String login) =>
            this.FullPathToLoginRoot
            + this.OSDirectoryWaySeparator
            + login
            + this.OSDirectoryWaySeparator
            + this.FullMetaFileName;




        public DBMSConfiguration()
        {

            MaxStringLength = "640000";
            MaxNumber = System.Int32.MaxValue.ToString();

            StandartLogFileName = "LogFile";
            StandartLogFileExtension = ".log";
            StandartSeparator = ' ';
            StandartMetaFileName = "\\MetaInfo";
            StandartMetaFileExtension = ".json";
            BaseDir = "..\\..\\..\\Server";
            BaseUserDir = "\\Users";
            BaseSecurityDir = "\\Security";
            BaseLoginDir = "\\Login";
            BaseBackUpDir = "\\BackUp`s";
            DataBasesProcedureDir = "\\Procedures";
            DataBaseViewDir = "\\Views";
            DataBasesStandartDir = "\\DataBases";
            DataBasesUsersDir = "\\Users";
            DataBaseTableDir = "\\Tables";
            PQLExtensio = ".pql";
            ILExtension = ".mmil";
            OSDirectoryWaySeparator = "\\";
            TriggersBaseDir = "\\Triggers";
            PathToLog = "\\Log";

            BaseDataBaseDir = "\\DataBases";

            SimpleDBCreator = "DataBaseCreator";
            SimpleSecureAdmin = "SecureAdmin";
            SimpleSysAdminName = "root";

            BaseJournalDir = "Journal";
            BaseJournalFileName = "journal";
            BaseJournalFileExtension = ".mmilj";


            FullMetaFileName = StandartMetaFileName + StandartMetaFileExtension;
            FullPathToBackUp = BaseDir + BaseSecurityDir + BaseBackUpDir;
            FullPathToLoginRoot = BaseDir + BaseSecurityDir + BaseLoginDir;
            FullPathToSecureRoot = BaseDir + BaseSecurityDir;
            FullPathToUsers = BaseDir + DataBasesUsersDir;


            FullPathToServerMetaFile = BaseDir + FullMetaFileName;
            FullPathToTheUsersMetaFile = FullPathToUsers + FullMetaFileName;
            FullPathToTheBackUpMetaFile = FullPathToBackUp + FullMetaFileName;
            FullPathToDataBaseMetaFile = BaseDir + DataBasesStandartDir + FullMetaFileName;


            FullPathToTheDataBaseProcedure = DataBasesProcedureDir;
            FullPathToTheDataBaseProceduresMetaFile = FullPathToTheDataBaseProcedure + FullMetaFileName;
            
            FullPathToTheDataBaseTable =  DataBaseTableDir;
            FullPathToTheDataBaseTableMetaFile = FullPathToTheDataBaseTable + FullMetaFileName;

            FullPathToTheDataBaseUsers = DataBasesUsersDir;
            FullPathToTheDataBaseUsersMetaFile = FullPathToTheDataBaseUsers + FullMetaFileName;


            FullPathToTheDataBaseView = DataBaseViewDir;
            FullPathToTheDataBaseViewMetaFile = FullPathToTheDataBaseView + FullMetaFileName;

            FullPathToTheSecurityMetaFile = FullPathToSecureRoot + OSDirectoryWaySeparator + FullMetaFileName;



            FullPathToTheLoginMetaFile = FullPathToLoginRoot + OSDirectoryWaySeparator + FullMetaFileName;

            FullPathToTheAdmin = FullPathToLoginRoot + OSDirectoryWaySeparator + SimpleSysAdminName;
            FullPathToTheDBCreator = FullPathToLoginRoot + OSDirectoryWaySeparator + SimpleDBCreator;
            FullPathToTheSecureAdmin = FullPathToLoginRoot + OSDirectoryWaySeparator + SimpleSecureAdmin;

            ServerName = "Server";
        }



       public System.String GetDataBaseRootWay(System.String ownerName, System.String dataBaseName) 
            => BaseDir + OSDirectoryWaySeparator + DataBasesStandartDir + OSDirectoryWaySeparator + dataBaseName;


    }
}
