﻿using DBMS.Core.MMIL;
using System;
using System.IO;
using DBMS.Core.Table;
using System.Collections.Generic;
using DBMS.Core.Secure.UserRights;
using System.Linq;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.TableCollections;
using DBMS.Core.InfoTypes;
using DBMS.Core.Helpers;
using DBMS.Core.Triggers;
using DBMS.Core.User;

using DBMS.Core.IO.InternalComponents;

using DBMS.Core.Interprater;


namespace DBMS.Core.IO
{


    internal sealed class TransactionInfo
        : ITransactionInfo
    {

        #region Documentation
#if DocTrue
        static TransactionInfo()
        {
            Documentation.AddHistoryToSection(
                
                PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreIO,
            "TransactionInfog",
            PQLDocGlobal.TypeClass,
            PQLDocGlobal.DescriptionUndefined,
            "ITransactionInfog");



            Documentation.AddHistoryToSection
                (
                            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreGlobalInterfaces,
            "ITransactionInfo",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined
                );



        }
#endif
        #endregion

        public TransactionInfo() { }
        public TransactionInfo(System.String dataBaseName, System.String userName, System.String parameters)
        {
            UserName = userName;
            DataBaseName = dataBaseName;
            Parameters = parameters;
        }

        public String UserName { get; set; }
        public String DataBaseName { get; set; }
        public String Parameters { get; set; }
        public Object AddedInfoFragmentTwo { get; set; }
        public Object AddedInfo { get; set; }
    }


    public sealed class DataBaseIO 
        : IDataBaseIO
    {


        #region Documentation
#if DocTrue
        static DataBaseIO()
        {
            Documentation.AddHistoryToSection(
                            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreIO,
            "DataBaseIO",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "IDataBaseIO"

                );



            Documentation.AddHistoryToSection(
                            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreGlobalInterfaces,
            "DataBaseIO",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined,
            "IDataBaseIO"

                );
        }
#endif
        #endregion



        #region Variable Documentation
        private System.String _DataBaseName = System.String.Empty;
        private System.String _DataBaseOwner = System.String.Empty;

        private IMetaInfo _Meta = null;


        private DBProcedureStorage _Procedure;
        private DBTableStorage _Tables;
        private DBViewStorage _Views;
        private DBUserStorage _User;
        private DBTriggerStorage _Trigger;



        private System.String _WayToTheDB = System.String.Empty;

        public List<ITableInfo> DataBaseTablesMeta { get { return _Tables.DataBaseTablesMeta; } set{ _Tables.DataBaseTablesMeta = value; } }

       #endregion


        public DataBaseIO(System.String dataBaseName, System.String dataBaseOwner)
        {
            this._DataBaseName = dataBaseName;
            this._DataBaseOwner = dataBaseOwner;

            this._WayToTheDB = this.GetWayToDataBaseRoot();

            this._Meta = this.ReadDataBaseMeta();

            _User = new DBUserStorage(_WayToTheDB, this.GetWayToDataBaseRoot(), dataBaseOwner, dataBaseName);
            _Tables = new DBTableStorage(this.GetWayToDataBaseRoot(), _WayToTheDB, dataBaseOwner, dataBaseName);
            _Views = new DBViewStorage(this.GetWayToDataBaseRoot(), _WayToTheDB);
            _Trigger = new DBTriggerStorage(_WayToTheDB, this.GetWayToDataBaseRoot());
            _Procedure = new DBProcedureStorage(this.GetWayToDataBaseRoot(), _WayToTheDB);
     
        }


        #region ReadXxx Methods
        public ITable ReadTable(System.String tableName) => DBMSIO.ReadObject<ITable>(this._WayToTheDB + DBMSIO.DBMSConfig.FullMetaFileName);


     
        private IMetaInfo ReadDataBaseMeta() =>
            DBMSIO.ReadMetaInfo(_WayToTheDB
                + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
                + DBMSIO.DBMSConfig.FullMetaFileName);


        public MMILDataBaseContext ReadStoredProcedureMMIL(System.String procedureName) => _Procedure.ReadStoredProcedureMMIL(procedureName);
        #endregion

        #region RemoveXxx Methods
        public System.Boolean FlushTableMeta(System.String tableName) => _Tables.FlushTableMeta(tableName);
        #endregion

        #region AppendXxx Methods
        public System.Boolean AppendNewTrigger(System.String triggerName) => _Trigger.AppendNewTrigger(triggerName);
        public System.Boolean AppendTableMetaInfo(ITableInfo ti) => _Tables.AppendTableMetaInfo(ti);
        public System.Boolean AppendNewView(System.String viewName) => _Views.AppendNewView(viewName);
        public Boolean AppendNewUser(String userName) => _User.AppendNewUser(userName);
        public System.Boolean AppendNewTable(System.String tableName) => _Tables.AppendNewTable(tableName);
        public System.Boolean AppendNewProcedure(System.String procName) => _Procedure.AppendNewProcedure(procName);
        public Boolean AppendRightsToUser(System.String userName, System.String objectOn, System.String objType, System.Boolean regim)
            => _User.AppendRightsToUser(userName, objectOn, objType, regim);
        public Boolean AppendObjTypeToUser(System.String userName, System.String objectOn, System.String objType)
            => _User.AppendObjTypeToUser(userName, objectOn, objType);
        #endregion



        #region TruncateXxx Methods
        public Boolean TruncateRightsFromUser(System.String userName, System.String objectOn, System.String objType, System.Boolean regim)
            => _User.TruncateRightsFromUser(userName, objectOn, objType, regim);
        public Boolean TruncateObjTypeToUser(System.String userName, System.String objectOn, System.String objType)
            => _User.TruncateObjTypeToUser(userName, objectOn, objType);
        public Boolean TruncateTable(System.String tableName) => _Tables.TruncateTable(tableName);
        public Boolean TruncateUser(System.String userName) => _User.TruncateUser(userName);
        public Boolean TruncateProcedure(System.String procName) => _Procedure.TruncateProcedure(procName);
        public System.Boolean TruncateTrigger(System.String triggerName) => _Trigger.TruncateTrigger(triggerName);
        public System.Boolean TruncateView(System.String viewName) => _Views.TruncateView(viewName);
        #endregion



        #region IsXxx Methods

        public System.Boolean IsExistForeignConstraintToCell(ITableCell cell, String tableName)
            => _Tables.IsExistForeignConstraintToCell(cell, tableName);
        public System.Boolean IsExistForeignConstraintToCellWithValue(ITableCell cell, string tableName) 
            => _Tables.IsExistForeignConstraintToCellWithValue(cell, tableName);
        public System.Boolean IsExistForeignConstraintToTable(String tableName) => _Tables.IsExistForeignConstraintToTable(tableName);
        public System.Boolean IsTriggerExist(System.String triggerName) => _Trigger.IsTriggerExist(triggerName);
        public System.Boolean IsColumnWithNameExsitInTableWithName(System.String columnName, System.String tableName)
            => _Tables.IsColumnWithNameExsitInTableWithName(columnName, tableName);
        public System.Boolean IsValueExistIntColumnWithNameInTableWithName(System.String columnName, System.String tableName, System.String values)
            => _Tables.IsValueExistIntColumnWithNameInTableWithName(columnName, tableName, values);
        public System.Boolean IsTableHasDeleteTrigger(System.String tableName) => _Trigger.IsTableHasDeleteTrigger(tableName);
        public System.Boolean IsTableHasInsertTrigger(System.String tableName)  => _Trigger.IsTableHasInsertTrigger(tableName);
        public System.Boolean IsTableHasUpdateTrigger(System.String tableName) => _Trigger.IsTableHasUpdateTrigger(tableName);
        public System.Boolean IsTableHasTriggers(System.String tableName)  => _Trigger.IsTableHasTriggers(tableName);
        public System.Boolean IsProcedureExist(System.String procName) => _Procedure.IsProcedureExist(procName);

        public System.Boolean IsViewExist(System.String viewName) => _Views.IsViewExist(viewName);

        public System.Boolean IsTableExist(System.String tableName)  => _Tables.IsTableExist(tableName);

        public System.Boolean IsEachTableExist(System.String[] tableNames) => _Tables.IsEachTableExist(tableNames);
        public System.Boolean IsUserExist(System.String userName) => _User.IsUserExist(userName);


        public System.Boolean IsUserWithLoginExist(System.String loginName) => _User.IsUserWithLoginExist(loginName);
        #endregion


        #region RemoveXxx Methods
        public void RemoveRightFromUser(System.String userName, System.String right, System.String objName) => _User.RemoveRightFromUser(userName, right, objName);
        #endregion


        #region Other
        public void ExecuteTriggerOnTable(System.String tableName, NodeType triggerType) => _Trigger.ExecuteTriggerOnTable(tableName, triggerType);

        public void ReWriteMetaTableInfoByTableName(System.String tableName) => _Tables.ReWriteMetaTableInfoByTableName(tableName);

        #endregion




        #region GetXxx Methods
        public IUserToken GetUserByLoginName(System.String loginName) => _User.GetUserByLoginName(loginName);
        public IUserToken GetUserByName(String userName) => _User.GetUserByName(userName);
        public ITableInfo GetTableMetaByName(string tableName) => _Tables.GetTableMetaByName(tableName);  
        public String GetWayToDataBaseRoot() => DBMSIO.DBMSConfig.GetDataBaseRootWay(this._DataBaseOwner, this._DataBaseName);
   
        public string GetWayToDataBaseTableRoot()
            => this._WayToTheDB
            + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
            + DBMSIO.DBMSConfig.DataBaseTableDir;
        
        public string GetWayToDataBaseProcRoot()
         => this._WayToTheDB
            + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
            + DBMSIO.DBMSConfig.DataBasesProcedureDir;
        
        public string GetWayToDataBaseTriggerRoot()
         => this._WayToTheDB
            + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
            + DBMSIO.DBMSConfig.TriggersBaseDir;
        
        public string GetWayToDataBaseViewRoot()
          => this._WayToTheDB
            + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
            + DBMSIO.DBMSConfig.DataBaseViewDir;
        
        public string GetWayToDataBaseUserRoot()
         => this._WayToTheDB
            + DBMSIO.DBMSConfig.OSDirectoryWaySeparator
            + DBMSIO.DBMSConfig.DataBasesUsersDir;

        public StoredProcedure GetStoredProcedure(System.String procName) => _Procedure.GetStoredProcedure(procName);
        public Trigger Gettrigger(System.String trigName) => _Trigger.GetTrigger(trigName);
        public ITable GetViewsTable(System.String viewName) => _Views.GetViewsTable(viewName);
        public Int32 GetCountOfViews() => _Views.GetCountOfViews();
        public Int32 GetCountOfTables() => _Tables.GetCountOfTables();
        public Int32 GetCountOfProcedures() => _Procedure.GetCountOfProcedures();
        public Int32 GetCountOfUser() => _User.GetCountOfUser();
        public List<String> GetViewsNames() => _Views.GetViewsNames();
        public List<String> GetTableNames() => _Tables.GetTableNames();
        public List<String> GetProceduresName() => _Procedure.GetProceduresName();
        public List<String> GetUserName() => _User.GetUserName();
        public String GetStoredProcedureText(String procName) => _Procedure.GetStoredProcedureText(procName);
        public IUserToken GetUserTokenByName(System.String userName) => _User.GetUserTokenByName(userName);
        public void ReWriteUserTokenByName(System.String name, IUserToken user) => _User.ReWriteUserTokenByName(name, user);
        public String GetViewText(String viewName) => _Views.GetViewText(viewName);

        public String GetTriggerText(String trigName) => _Trigger.GetTextTrigger(trigName);
        public System.String GetDataBaseOwner() => this._DataBaseOwner;
        public System.String GetDataBaseName() => this._DataBaseName;

        
        public IMetaInfo ReadTableMeta(string tableName) => _Tables.ReadTableMeta(tableName);

        #endregion
    }
}
