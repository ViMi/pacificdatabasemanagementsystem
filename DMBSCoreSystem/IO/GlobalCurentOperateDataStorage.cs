﻿using DBMS.Core.MMIL;
using System;
using System.IO;
using DBMS.Core.Table;
using System.Collections.Generic;
using DBMS.Core.Secure.UserRights;
using System.Linq;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.TableCollections;
using DBMS.Core.InfoTypes;
using DBMS.Core.Helpers;
using DBMS.Core.Triggers;
using DBMS.Core.User;

using DBMS.Core.IO.InternalComponents;

using DBMS.Core.Interprater;



namespace DBMS.Core.IO
{


    public enum StaticContextTablesID 
        : System.Byte
    {
        DELETED_TABLE = 0,
        UPDATED_TABLE = 1
    };

    internal static class GlobalCurentOperateDataStorage
    {

        public const String DELETED_TABLE_NAME = "*DELETED*";
        public const String UPDATED_TABLE_NAME = "*UPDATED*";

        public static Boolean RestoreNow = false;

        private static ITable _DeletedTable = null;
        private static ITable _UpdatedTable = null;
       
        public static void Reset()
        {
            _DeletedTable = null;
            _UpdatedTable = null;
        }


        public static ITable GetStoredTable(StaticContextTablesID id)
        {
            switch (id)
            {
                case StaticContextTablesID.DELETED_TABLE: { return _DeletedTable; }
                case StaticContextTablesID.UPDATED_TABLE: { return _UpdatedTable; }
            }


            throw new TableNotExist();
        }



        public static StaticContextTablesID ResolveByConstName(String name)
        {
            switch (name)
            {
                case DELETED_TABLE_NAME: { return StaticContextTablesID.DELETED_TABLE; }
                case UPDATED_TABLE_NAME: { return StaticContextTablesID.UPDATED_TABLE; }

            }


            throw new TableNotExist();
        }



        public static void SetTable(StaticContextTablesID id, ITable setTable)
        {
            switch(id)
            {
                case StaticContextTablesID.DELETED_TABLE: { _DeletedTable = setTable; }break;
                case StaticContextTablesID.UPDATED_TABLE: { _UpdatedTable = setTable; } break;
            }


            return;
        }


    }
}
