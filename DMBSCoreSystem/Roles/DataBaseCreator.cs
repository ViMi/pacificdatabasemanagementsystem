﻿using System;
using System.Collections.Generic;
using DBMS.Core.Helpers;

using DBMS.Core.IO;
using DBMS.Core.Secure;


namespace DBMS.Core.Roles
{
    public class DataBaseCreator
        : ILogin
    {


#if DocTrue
        static DataBaseCreator()
        {
            Documentation.AddHistoryToSection(
               PQLDocGlobal.CoreSystem,
               PQLDocGlobal.CoreUsersFromRoles,
               "DataBaseCreator",
               PQLDocGlobal.TypeClass,
               PQLDocGlobal.DescriptionUndefined,
               "ILogin"
           );


            Documentation.AddHistoryToSection(
                   PQLDocGlobal.CoreSystem,
                   PQLDocGlobal.CoreUsersFromRoles,
                   "ILogin",
                   PQLDocGlobal.TypeInterface,
                   PQLDocGlobal.DescriptionUndefined

                );
        }
#endif

        public DataBaseCreator()
        {

            System.String keyItem1 = DBMSIO
                .DBMSConfig
                .ServerName
             
                .Replace(
                    DBMSIO.DBMSConfig.StandartSeparator.ToString(),
                    String.Empty);


            System.String keyItem2 = DBMSIO
                .DBMSConfig
                .BaseDataBaseDir
                .Replace(
                    DBMSIO.DBMSConfig.OSDirectoryWaySeparator.ToString(),
                    String.Empty)
                .Replace(
                    DBMSIO.DBMSConfig.StandartSeparator.ToString(),
                    String.Empty);


            LoginRights = new Dictionary<String, NodeType>(2)
            {
                [keyItem1] = NodeType.ROOT_DIRECTORY,
                [keyItem2] = NodeType.DATABASE_DIRECTORY,
            };
        }


        #region Variable Declaration
        public Dictionary<String, NodeType> LoginRights { get; set; } 

        public String NodeName { get; set; } = DBMSIO.DBMSConfig.SimpleDBCreator;
        public String ChildrenList { get; set; } = String.Empty;
        public Password NodeAccessPassword { get; set; } = new Password(EncriptionType.PlaneText, String.Empty);
        public NodeType Type { get; set; } = NodeType.LOGIN;
        public String Parent { get; set; } = DBMSIO.DBMSConfig.BaseLoginDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty);
        public String Owner { get; set; } = DBMSIO.DBMSConfig.ServerName;
        #endregion



        public String GetUserLogin() => NodeName;
        public IPassword GetUserPassword() => NodeAccessPassword;


    }
}
