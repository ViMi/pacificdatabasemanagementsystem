﻿using System;
using System.Collections.Generic;
using DBMS.Core.Helpers;

using DBMS.Core.IO;
using DBMS.Core.Secure;


namespace DBMS.Core.Roles
{
    public class SecurityAdministrator
        : ILogin
    {


#if DocTrue
        static SecurityAdministrator()
        {

            Documentation.AddHistoryToSection(
             PQLDocGlobal.CoreSystem,
             PQLDocGlobal.CoreUsersFromRoles,
             "SecurityAdministrator",
             PQLDocGlobal.TypeClass,
             PQLDocGlobal.DescriptionUndefined,
             "ILogin"
         );



        }
#endif


        public SecurityAdministrator()
        {
            System.String keyItem1 = DBMSIO
                .DBMSConfig
                .ServerName
               
                .Replace(
                      DBMSIO.DBMSConfig.StandartSeparator.ToString(),
                      String.Empty);

            System.String keyItem2 = DBMSIO
                  .DBMSConfig
                  .BaseSecurityDir
                  .Replace(
                        DBMSIO.DBMSConfig.OSDirectoryWaySeparator.ToString(),
                        String.Empty)
                    .Replace(
                        DBMSIO.DBMSConfig.StandartSeparator.ToString(),
                        String.Empty);


            LoginRights = new Dictionary<String, NodeType>(3)
            {
                [keyItem1] = NodeType.ROOT_DIRECTORY,
                [keyItem2] = NodeType.SECURITY_DIRECTORY,
            };


        }


        #region Variable Declaration 
        public Dictionary<String, NodeType> LoginRights { get; set; }

        public String NodeName { get; set; } = DBMSIO.DBMSConfig.SimpleSecureAdmin;
        public String ChildrenList { get; set; } = String.Empty;
        public Password NodeAccessPassword { get; set; } = new Password(EncriptionType.PlaneText, String.Empty);
        public NodeType Type { get; set; } = NodeType.LOGIN;
        public String Parent { get; set; } = DBMSIO.DBMSConfig.BaseLoginDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty);
        public String Owner { get; set; } = DBMSIO.DBMSConfig.ServerName;
        #endregion


        public String GetUserLogin() => NodeName;
        public IPassword GetUserPassword() => NodeAccessPassword;

    }
}
