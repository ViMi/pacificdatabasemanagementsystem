﻿using System;
using System.Collections.Generic;
using DBMS.Core.Helpers;

using DBMS.Core.Secure;
using DBMS.Core.IO;

namespace DBMS.Core.Roles
{
   public class SystemAdministrator
        : ILogin
    {

#if DocTrue
        static SystemAdministrator()
        {
            Documentation.AddHistoryToSection(
                PQLDocGlobal.CoreSystem,
                PQLDocGlobal.CoreUsersFromRoles,
                "SystemAdministrator",
                PQLDocGlobal.TypeClass,
                PQLDocGlobal.DescriptionUndefined,
                "ILogin"
            );


        }
#endif

        public SystemAdministrator()
        {
            System.String keyItem1 = DBMSIO
                .DBMSConfig
                .ServerName 
                .Replace(
                    DBMSIO.DBMSConfig.StandartSeparator.ToString(),
                    String.Empty);


            System.String keyItem2 = DBMSIO
                .DBMSConfig
                .BaseSecurityDir
                .Replace(
                    DBMSIO.DBMSConfig.OSDirectoryWaySeparator.ToString(),
                    String.Empty)
                .Replace(
                    DBMSIO.DBMSConfig.StandartSeparator.ToString(),
                    String.Empty);


       

            System.String keyItem3 = DBMSIO
                .DBMSConfig
                .BaseDataBaseDir
                .Replace(
                    DBMSIO.DBMSConfig.OSDirectoryWaySeparator.ToString(),
                    String.Empty)
                .Replace(
                    DBMSIO.DBMSConfig.StandartSeparator.ToString(),
                    String.Empty);

            LoginRights = new Dictionary<String, NodeType>(4)
            {
                [keyItem1] = NodeType.ROOT_DIRECTORY,
                [keyItem2] = NodeType.SECURITY_DIRECTORY,
                [keyItem3] = NodeType.DATABASE_DIRECTORY,

            };
        }



        #region Variable Dclaration
        public Dictionary<String, NodeType> LoginRights { get; set; } 

        public String NodeName { get; set; } = DBMSIO.DBMSConfig.SimpleSysAdminName;
        public String ChildrenList { get; set; } = String.Empty;
        public Password NodeAccessPassword { get; set; } = new Password(EncriptionType.PlaneText, String.Empty);
        public NodeType Type { get; set; } = NodeType.LOGIN;
        public String Parent { get; set; } = DBMSIO.DBMSConfig.BaseLoginDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty);
        public String Owner { get; set; } = DBMSIO.DBMSConfig.ServerName;
        #endregion



        public String GetUserLogin() => NodeName;
        public IPassword GetUserPassword() => NodeAccessPassword;
    }
}
