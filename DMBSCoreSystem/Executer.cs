﻿using DBMS.Core.Interprater;
using DBMS.Core.Helpers;


namespace DBMS.Core
{
    internal static class Executer
    {


        #region Documentation
#if DocTrue
        static Executer() => Documentation.AddHistoryToSection(
            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreFlow,
            "Executer",
            PQLDocGlobal.TypeStaticClass,
            PQLDocGlobal.DescriptionUndefined
            );
#endif
        #endregion


        public static IExecuteStrategy GlobalExecuteStrategy { set; get; }

    }
}
