﻿using DBMS.Core.Interprater;
using System;

namespace DBMS.Core.Interprater
{
    public interface IParseStrategy
        : ICloneable
    {

        IParseResult Execute();

        void SetListing(System.String listing);

    }
}
