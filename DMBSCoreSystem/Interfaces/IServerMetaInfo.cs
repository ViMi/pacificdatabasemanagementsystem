﻿using System;
using DBMS.Core.Secure;

namespace DBMS.Core
{


    public enum ServerType
        : System.Byte
    {
        LOCAL = 0,
        CLUSTERED = 1
    }

    public interface IServerMetaInfo
    {

        String ServerName { set; get; }
        String LastSeenDate { set; get; }
        String CurrentSeenDate { set; get; }
        ServerType ServerType { set; get; }
        String ServerOwner { set; get; }
        string NodeName { get; set; }
        string ChildrenList { get; set; }
        Password NodeAccessPassword { get; set; }
        NodeType Type { get; set; }
        string Parent { get; set; }
        string Owner { get; set; }

    }
}
