﻿using System;
using System.Collections.Generic;


namespace DBMS.Core.Interprater.SecureStrategy
{
    public interface ILanguageSecureStrategy
        : ICloneable
    {
         System.String GetClassNameFromString(string commandStirng);
         System.Boolean IsAbstractCommand(System.String command);
         System.Boolean IsConcreteCommand(System.String command, String concreteCommand);
         Dictionary<System.String, HashSet<System.String>> GetCommandDictionary();
         System.String[] PrepareListingToTranslation();
         System.String[] PrepareContextToTranslation(System.String context);
         System.String PrepareCommandToTranslation(System.String commandString);
         System.String GetMethodNameFromString(System.String commandString);
         System.String GetParameterFromString(System.String commandString);

         System.Boolean IsType(System.String type);
         System.Boolean IsAttribute(System.String attribute);

         void RemoveOneLineComment(System.String startToken);
 
    }
}
