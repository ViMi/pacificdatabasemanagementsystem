﻿using DBMS.Core.MMIL;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBMS.Core
{
    public interface IParserSecureStrategy
    {
        System.Boolean Check(System.String param);
        System.Boolean Check(List<System.String> param);
        System.Boolean Check(Dictionary<System.String,System.String> param);
        System.Boolean Check(IMMILTranslationUnit param);
        System.Boolean Check(List<IMMILTranslationUnit> param);


    }
}
