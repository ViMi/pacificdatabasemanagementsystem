﻿
namespace DBMS.Core.Secure
{
    public interface IPassword
    {

        System.String Decript();
        System.Boolean Encript();

        public System.String _Password { set; get; }
        public EncriptionType Encription { set; get; }

    }
}
