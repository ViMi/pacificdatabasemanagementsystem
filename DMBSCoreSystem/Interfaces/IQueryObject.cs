﻿using DBMS.Core.MMIL;
using DBMS.Core;
using DBMS.Core.IO;
using DBMS.Core.Table;
using DBMS.Core.InfoTypes;

namespace DBMS.Core
{
    public interface IQueryObject
    {

        IQueryObject Select(MMILDataManipulationContext mmil = null);
        IQueryObject CrossJoin(MMILDataManipulationContext mmil = null);
        IQueryObject Having(MMILDataManipulationContext mmil = null);
        IQueryObject GroupBy(MMILDataManipulationContext mmil = null);
        IQueryObject OrderBy(MMILDataManipulationContext mmil = null);
        IQueryObject Skip(MMILDataManipulationContext mmil = null);
        IQueryObject Limit(MMILDataManipulationContext mmil = null);
        IQueryObject SkipLimit(MMILDataManipulationContext mmil = null);
        IQueryObject Top(MMILDataManipulationContext mmil = null);
        IQueryObject Where(MMILDataManipulationContext mmil = null);
        IQueryObject Union(MMILDataManipulationContext mmil = null);
        IQueryObject Save();
        IQueryObject Distinct();

        ITransactionMethodResult Delete();
        ITransactionMethodResult Update();
        ITransactionMethodResult Insert();
        ITable GetResult();



    }
}
