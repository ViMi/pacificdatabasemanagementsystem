﻿

using System;

namespace DBMS.Core.Interprater
{
    public interface IExecuteStrategy 
        : ICloneable
    {
        IExecuteResult Execute (IParseResult parseResult);


    }
}
