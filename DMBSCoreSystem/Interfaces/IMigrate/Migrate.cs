﻿using DBMS.Core.Helpers;


namespace DBMS.Core
{
    public sealed class MigrateStatement
        : IMigrate
    {
        static MigrateStatement() {

          
        
        }

        public string DestinationPoint { get; set; }
        public string SourcePoint { get; set; }
    }
}
