﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBMS.Core
{
    public interface IMigrate
    {

        String DestinationPoint { set; get; }
        String SourcePoint { set; get; }


    }
}
