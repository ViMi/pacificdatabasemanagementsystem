﻿using DBMS.Core.InfoTypes;
using DBMS.Core;
using DBMS.Core.Table;
using System.Collections.Generic;

namespace DBMS.Core
{
    public interface IPacificTransaction
    {

        List<System.String> OperateObjectName { set; get; }
        System.String CommandName { set; get; }
        List<NodeType> OperateObectType { set; get; }

        ITransactionMethodResult StartTransaction();

        ITable EndTransaction();

    }
}
