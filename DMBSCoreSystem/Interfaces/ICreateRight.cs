﻿
namespace DBMS.Core.Secure.UserRights.RightTable.OperationRightTables
{
    public interface ICreateRight
    {
        System.Boolean DataBase { set; get; }
        System.Boolean Table { set; get; }
        System.Boolean User { set; get; }
        System.Boolean View { set; get; }

        System.Boolean Procedure { set; get; }

    }
}
