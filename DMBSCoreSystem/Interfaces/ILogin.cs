﻿using DBMS.Core.Secure;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBMS.Core
{
    public interface ILogin
    {

        System.String GetUserLogin();
        IPassword GetUserPassword();

        
        Dictionary<System.String, NodeType> LoginRights { set; get; }
        System.String NodeName { set; get; }
        System.String ChildrenList { set; get; }

        Password NodeAccessPassword { set; get; }
        /// <summary>
        /// Contains Type of node
        /// </summary>
        NodeType Type { set; get; }

        System.String Parent { set; get; }
        System.String Owner { set; get; }


    }
}
