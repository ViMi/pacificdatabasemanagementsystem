﻿using DBMS.Core;
using DBMS.Core.InfoTypes;
using DBMS.Core.Interprater;
using DBMS.Core.Table;
using DBMS.Core.Secure.UserRights;

using System;
using System.Collections.Generic;
using DBMS.Core.TableCollections;
using DBMS.Core.Secure;

namespace DBMS.Core.IO
{

    
    public enum DBCommand
        : System.Byte
    {
        CREATE_DATABASE = 0,
        CREATE_TABLE = 1,
        CREATE_VIEW = 2,
        CREATE_USER = 3,
        CREATE_PROCEDURE = 4,
        CREATE_LOGIN = 5,
        CREATE_TRIGGER = 6,
   
        DROP_DATABASE = 20,
        DROP_TABLE = 21,
        DROP_VIEW = 22,
        DROP_USER = 23,
        DROP_PROCEDURE = 24,
        DROP_LOGIN = 25,
        DROP_TRIGGER = 26,

        ALTER_DATABASE = 40,
        ALTER_TABLE = 41,
        ALTER_VIEW = 42,
        ALTER_USER = 43,
        ALTER_PROCEDURE = 44,
        ALTER_LOGIN = 45,
        ALTER_TRIGGER = 46,


        GRANT_USER = 60,
        GRANT_LOGIN = 61,
        REVOKE_USER = 62,
        REVOKE_LOGIN = 63,
        DENY_USER = 64,
        DENY_LOGIN = 65,


        SELECT = 80,
        INSERT = 81,
        DELETE = 82,
        UPDATE = 83,

        BACKUP =  84,
        EXECUTE = 85,
        MIGRATE = 86,

        USE = 87,

        BACKUP_WITH_LOG = 88,



        CHANGE_PASSWORD_TO_USER = 101,
        CHANGE_PASSWORD_TO_LOGIN = 102,


        GET_COUNT_OF_PROCEDURES_ON_SERVER = 121,
        GET_COUNT_OF_TABLE_ON_SERVER = 122,
        GET_COUNT_OF_VIEW_ON_SERVER = 123,
        GET_COUNT_OF_USER_ON_SERVER = 124,
        GET_COUNT_OF_LOGIN_ON_SERVER = 125,

        GET_CURRENT_LOGIN_INFO = 126,
        GET_CURRENT_DATABASE_INFO = 127,
        GET_SERVER_NAME = 128,
        GET_LOG_FILE_CONTENT = 129,
        GET_PREVIOUS_START_DATE = 130,
        GET_LAST_START_DATE = 131,
        GET_SERVER_TYPE = 132,
        GET_COUNT_OF_PROCEDURES_ON_CURRENT_DATABASE = 133,
        GET_COUNT_OF_VIEW_ON_CURRENT_DATABASE = 134,
        GET_COUNT_OF_USER_ON_CURRENT_DATABASE = 135,
        GET_COUNT_OF_TABLES_ON_CURRENT_DATABASE = 136,
        GET_ALL_PROCEDURES_NAME_ON_CURRENT_DATABASE = 137,
        GET_ALL_TABLE_NAME_ON_CURRENT_DATABASE = 138,
        GET_ALL_VIEW_NAME_ON_CURRENT_DATABASE = 139,

        GET_ALL_PROCEDURES_NAME_ON_SERVER = 140,
        GET_ALL_TABLE_NAME_ON_SERVER = 141,
        GET_ALL_VIEW_NAME_ON_SERVER = 142,

        SET_SERVER_TYPE = 143,
        SET_SERVER_NAME = 144,
        SET_SERVER_PASSWORD = 145,

        GET_TABLE_INFO = 146,
        GET_TABLE_COLUMNS_DATAS = 147,
        GET_TABLE_COLUMNS_CONSTRAINTS = 148,
        GET_STORED_PROCEDURE_TEXT = 149,
        GET_VIEW_TEXT = 150,
        RESTORE_SERVER = 151,
        GET_TRIGGER_TEXT = 152,

        GET_SERVER_CONFIG = 153,

        GET_BOUNDED_ENTITIES = 154,

        SET_STANDART_MODE = 200,
        SET_UNSAFE_MODE = 201,
        SET_LNG_UNSAFE_MODE = 202,
    }



    public interface IOIntegrateDBMS 
        : IIOIntegrate
    {
        

        IServerMetaInfo GetServerInfo(System.String pathToServerRoot);
        IMetaInfo GetUserMetaInfo(System.String pathToUser);
        IMetaInfo GetMetaInfoByPath(System.String path);



        List<String> GetNamesOfAllLoginsOnServer();
        T ReadObject<T>(String way);

        void ReWriteTableMetaInfo(ITableInfo info, System.String path);
        Dictionary<System.String, IUserToken> ReadUserByLogin(ILogin login);
        System.Boolean IsLoginExist(System.String login);
        System.Boolean IsDataBaseExist(System.String dataBaseName);
        System.Boolean AppendLogin(System.String login);
        System.Boolean AppendDataBase(System.String dataBase);

        Int32 GetCountOfDataBasesOnServer();
        Int32 GetCountOfAllViewOnServer();
        Int32 GetCountOfAllProceduresOnServer();
        Int32 GetCountOfAllTablesOnServer();
        Int32 GetCountOfAllUsersOnServer();
        List<String> GetAllDataBaseNames();
        List<String> GetNamesOfAllTablesOnServer();
        List<String> GetNamesOfAllProceduresOnServer();
        List<String> GetNamesOfAllViewOnServer();
        List<String> GetNamesOfAllUserOnServer();

        List<String> GetSchemasList();

        IServerMetaInfo GetServerMetaInfo();
        Int32 GetCountOfLoginOnServer();


        void SetNewCurrentDate(String currentDate, IServerMetaInfo serverMeta);
        void SetNewPrevSeenDate(String lastSeenDate, IServerMetaInfo serverMeta);
        void SetNewServerName(String serverName, IServerMetaInfo serverMeta);
        void SetNewServerPassword(Password password, IServerMetaInfo serverMeta);
        void SetNewServerType(ServerType type, IServerMetaInfo serverMeta);

        System.Boolean IsServerExist(System.String pathToServer);
        void RestoreServer();

        ITableCollection GetReadedTableData(ITransactionInfo info);

        ILogin GetLogin(System.String login);



        String Serialize<T>(T obj);
        /// <summary>
        /// This function created for compatibility with db providers
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        ITransactionMethodResult ExecuteCommand(System.String command, ITransactionInfo info);
        ITransactionMethodResult ExecuteCommand(DBCommand command, ITransactionInfo info);



        System.Boolean TruncateDataBase(System.String info);

        System.Boolean TruncateLogin(System.String login);

        bool ReWriteMetaInfo(IMetaInfo info, System.String path);
        ITableInfo ReadTableMetaInfo(System.String path);



        IUserToken ReadUserTokenByName(System.String wayToUserRoot, System.String userName);
        void ReWriteUserTokenByName(System.String wayToUseRoot, System.String name, IUserToken ut);
    }
}
