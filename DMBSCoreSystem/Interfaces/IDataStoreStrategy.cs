﻿using DBMS.Core;
using DBMS.Core.Secure;
using DBMS.Core.Table;

namespace DBMS.Core
{

    public enum PacificStorageTypes : System.Byte
    {
        HEAP = 0,
        INDEXED = 1
    };


    public interface IDataStoreStrategy
    {

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        System.Boolean SaveTable(ITable table);
        ITable ReadTable(System.String pathToTableMeta);
        IMetaInfo GetStoregDeviceInfo();
        System.Boolean IsStoreDeviceExist(System.Boolean storegDeviceName);

    }
}
