﻿
namespace DBMS.Core.Secure.UserRights.RightTable.OperationRightTables
{
    public interface IDropRights
    {
        System.Boolean DataBase { set; get; }
        System.Boolean Table { set; get; }
        System.Boolean View { set; get; }
        System.Boolean Procedure { set; get; }

    }
}
