﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBMS.Core
{

    public enum AttributeType 
        : System.SByte
    {
        UNSET = -1,
        DEFAULT = 0,
        NOT_NULL = 1,
        LIKE = 2,
        IDENTYTI = 3,
        PRIMARY_KEY = 4,
        FOREIGN_KEY = 5,
        UNIQUE = 6,
        INCLUDED_TABLE = 7,
    }


    public interface IAttribute
    {
        public AttributeType AttrType { set; get; }
        public System.String AttributeData { set; get; }


    }
}
