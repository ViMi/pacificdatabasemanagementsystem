﻿
namespace DBMS.Core.Secure.UserRights.RightTable.OperationRightTables
{
    public interface IUpdateRight
    {
        System.Boolean TableRow { set; get; }

    }
}
