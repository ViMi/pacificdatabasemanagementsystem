﻿using System.Collections.Generic;


namespace DBMS.Core.Secure.UserRights.RightTable
{
    public interface ICommandRight
    {
        Dictionary<System.String, System.Boolean> CommandRights { set; get; }
    }
}
