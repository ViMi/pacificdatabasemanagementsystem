﻿
using DBMS.Core.Interprater;
using DBMS.Core.Table;
using System.Collections.Generic;

namespace DBMS.Core.Secure.UserRights
{
    public interface ITableInfo
        : IParseResult 
        , IExecuteResult
        , IMetaInfo
    {


        new System.String NodeName { set; get; }
        new System.String ChildrenList { set; get; }
        new Password NodeAccessPassword { set; get; }
        /// <summary>
        /// Contains Type of node
        /// </summary>
        new NodeType Type { set; get; }

        new System.String Parent { set; get; }
        new System.String Owner { set; get; }

        System.String ServerName { set; get; }
        System.String DataBaseName { set; get; }
        System.String TableName { set; get; }
        System.Int32 RecordsCount { set; get; }

        System.Int32 FileCount { set; get; }
        System.Int32 RowCount { set; get; }


        System.Boolean IsIndexed { set; get; }
        Dictionary<string, Dictionary<string, string>> ColumnConstraint { get; set; }

        System.String PK { set; get; }
        List<System.String> FK { set; get; }


        Dictionary<System.String, PacificDataBaseDataType> ColumnTypes { set; get; }
    }
}
