﻿using DBMS.Core.Secure.UserRights.RightTable.OperationRightTables;


namespace DBMS.Core.Secure.UserRights.RightTable
{
    public interface IDataBaseRightTable
    {
        ICreateRight Create { set; get; }
        IDeleteRight Delete { set; get; }
        IDropRights Drop { set; get; }
        ISelectRight Select { set; get; }
        IUpdateRight Update { set; get; }


    }
}
