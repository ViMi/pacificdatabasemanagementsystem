﻿using System.Collections.Generic;


namespace DBMS.Core.Interprater
{
    public interface IRightPolicies
    {
        void CheckRight(IUserToken token);
        HashSet<System.String> GetAllowsStatements();

    }
}
