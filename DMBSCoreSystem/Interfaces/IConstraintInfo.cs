﻿using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.Table;

namespace DBMS.Core
{


    public enum ConstraintType:Byte
    {
        EQU = 0,
        LSS = 1,
        GRT = 2,
        EQU_LSS = 3,
        EQU_GRT = 4
    }




    public interface IConstraintSpecialization
    {
        public ConstraintType ConstType { set; get; }
        public Object Values { set; get; }

        public PacificDataBaseDataType DataType { set; get; }
       
    }


    public interface IConstraintInfo
    {

        String NameOfCell { set; get; }
        List<IConstraintSpecialization> ConstraintSet { set; get; }

    }
}
