﻿using DBMS.Core.Helpers;



namespace DBMS.Core{
    public sealed class BackUpInfo
        : IBackUpInfo
    {

#if DocTrue
        static BackUpInfo() {
            
            Documentation.AddHistoryToSection(
                            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreComponents,
            "BackUpInfo",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "IBackUpInfo"

                ); 



            Documentation.AddHistoryToSection(
                            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreMMIL,
            "IBackUpInfo",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined
            ); 
        
        
        }
#endif


        public string DestinationPoint { get; set; }
        public string SourcePoint { get; set; }
        public LoggerInfo LogConfig { get; set; }
    }
}
