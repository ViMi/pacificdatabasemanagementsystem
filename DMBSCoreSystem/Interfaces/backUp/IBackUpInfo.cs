﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBMS.Core
{
    public interface IBackUpInfo
    {
        String DestinationPoint { set; get; }
        String SourcePoint { set; get; }
       
        LoggerInfo LogConfig { set; get; }


    }
}
