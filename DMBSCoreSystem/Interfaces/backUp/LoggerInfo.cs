﻿using DBMS.Core.Helpers;



namespace DBMS.Core
{
    public sealed class LoggerInfo
        : ILoggerInfo
    {

#if DocTrue
        static LoggerInfo()
        {

            Documentation.AddHistoryToSection(
                            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreComponents,
            "LoggerInfo",
            PQLDocGlobal.TypeSealedClass,
            PQLDocGlobal.DescriptionUndefined,
            "ILoggerInfo"

                );



            Documentation.AddHistoryToSection(
                            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreGlobalInterfaces,
            "ILoggerInfo",
            PQLDocGlobal.TypeInterface,
            PQLDocGlobal.DescriptionUndefined
                );

        }
#endif

        public bool Log { get; set; }
        public bool Truncate { get; set; }
    }
}
