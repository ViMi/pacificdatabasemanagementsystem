﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBMS.Core
{
    public interface ILoggerInfo
    {

        Boolean Log { set; get; }
        Boolean Truncate { set; get; }

    }
}
