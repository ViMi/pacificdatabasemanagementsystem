﻿

namespace DBMS.Core.Secure.UserRights.RightTable.OperationRightTables
{
    public interface IDeleteRight
    {
        public System.Boolean Row { set; get; }

    }
}
