﻿
namespace DBMS.Core.InfoTypes
{
    public interface ITransactionInfo
    {
        System.String UserName { set; get; }
        System.String DataBaseName { set; get; }
        System.String Parameters { set; get; }
        System.Object AddedInfoFragmentTwo { set; get; }
        System.Object AddedInfo { set; get; }

    }
}
