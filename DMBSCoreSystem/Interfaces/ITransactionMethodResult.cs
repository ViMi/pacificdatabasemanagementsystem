﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBMS.Core.InfoTypes
{
    public interface ITransactionMethodResult
    {
        System.Boolean IsSuccess { get; }
        System.String ResultString { set; get; }
        System.Object AddedInfo { set; get; }
    }
}
