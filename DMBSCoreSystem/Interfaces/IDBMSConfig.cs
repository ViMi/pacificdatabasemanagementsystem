﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBMS.Core.IO
{
    public interface IDBMSConfig
    {

        System.Char StandartSeparator { set; get; }
        System.String StandartMetaFileName { set; get; }
        System.String StandartMetaFileExtension { set; get; }
        System.String BaseDir { set; get; }
        System.String BaseUserDir { set; get; }
        System.String BaseSecurityDir { set; get; }
        System.String BaseLoginDir { set; get; }
        System.String BaseBackUpDir { set; get; }
        System.String DataBasesProcedureDir { set; get; }
        System.String DataBaseViewDir { set; get; }
        System.String DataBasesStandartDir { set; get; }
        System.String DataBasesUsersDir { set; get; }
        System.String DataBaseTableDir { set; get; }
        System.String OSDirectoryWaySeparator { set; get; }

        System.String BaseDataBaseDir { set; get; }
        System.String ILExtension { set; get; }
        System.String PQLExtensio { set; get; }

        System.String FullMetaFileName { set; get; }
        System.String FullPathToSecureRoot { set; get; }
        System.String FullPathToLoginRoot { set; get; }
        System.String FullPathToBackUp { set; get; }
        System.String FullPathToUsers { set; get; }
        System.String TriggersBaseDir { set; get; }

        System.String BaseJournalDir { set; get; }
        System.String BaseJournalFileName { set; get; }
        System.String BaseJournalFileExtension { set; get; }
        System.String FullPathToServerMetaFile { set; get; }
        System.String FullPathToTheUsersMetaFile { set; get; }
        System.String FullPathToTheSecurityMetaFile { set; get; }
        System.String FullPathToTheLoginMetaFile { set; get; }
        System.String FullPathToDataBaseMetaFile { set; get; }
        System.String FullPathToTheDataBaseUsersMetaFile { set; get; }
        System.String FullPathToTheDataBaseProceduresMetaFile { set; get; }
        System.String FullPathToTheDataBaseViewMetaFile { set; get; }
        System.String FullPathToTheDataBaseTableMetaFile { set; get; }
        System.String FullPathToTheBackUpMetaFile { set; get; }


        System.String FullPathToTheDataBaseView { set; get; }
        System.String FullPathToTheDataBaseProcedure { set; get; }
        System.String FullPathToTheDataBaseTable { set; get; }
        System.String FullPathToTheDataBaseUsers { set; get; }

        System.String PathToLog { set; get; }
        System.String StandartLogFileName { set; get; }
        System.String StandartLogFileExtension { set; get; }
        System.String GetDataBaseRootWay(System.String ownerName, System.String dataBaseName);

        System.String GetPathToLogin(System.String loginName);


        System.String SimpleSysAdminName { set; get; }
        System.String SimpleSecureAdmin { set; get; }
        System.String SimpleDBCreator { set; get; }
        System.String FullPathToTheAdmin { set; get; }
        System.String FullPathToTheDBCreator { set; get; }
        System.String FullPathToTheSecureAdmin { set; get; }

        System.String MaxStringLength { set; get; }
        System.String MaxNumber { set; get; }
        
        System.String ServerName { set; get; }

    }

}
