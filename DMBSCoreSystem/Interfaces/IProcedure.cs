﻿using DBMS.Core;
using DBMS.Core.MMIL;
using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.IO;
using DBMS.Core.Table;



namespace DBMS.Core
{

    public enum ProcedureType
            : System.Byte
    {
        NO_PARAMES = 0,
        WITH_PARAMS = 1
    }

    public interface IProcedure
    {

        NodeType Type { set; get; }
        ProcedureType ProcType { set; get; }
        Dictionary<String, PacificDataBaseDataType> FormalParameters { set; get; }

        List<MMILDataBaseContext> PrecompiledILCommand { set; get; }

        System.Object Invoke();
    }
}
