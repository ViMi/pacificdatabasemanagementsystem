﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBMS.Core
{
    public interface ILogger
    {

        void CreateLogFile();
        void AppendInfoToLog(System.String logFileName);

        System.Boolean IsLogExist();

        void Reset();
        void Flush();

    }
}
