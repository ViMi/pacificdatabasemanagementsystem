﻿using DBMS.Core.MMIL;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.Table;
using System.Collections.Generic;
using DBMS.Core;
using DBMS.Core.Triggers;

namespace DBMS.Core.IO
{
    public interface IDataBaseIO
    {

        List<ITableInfo> DataBaseTablesMeta { set; get; }
        
         

        #region TruncateXxx Signatures
        System.Boolean TruncateTable(System.String tableName);
        System.Boolean TruncateView(System.String viewName);
        System.Boolean TruncateProcedure(System.String procName);
        System.Boolean TruncateUser(System.String userName);
        System.Boolean TruncateTrigger(System.String triggerName);
        #endregion


        #region AppendXxx Signatures
        System.Boolean AppendNewTable(System.String tableName);
        System.Boolean AppendNewTrigger(System.String triggerName);
        System.Boolean AppendTableMetaInfo(ITableInfo tableInfo);
        System.Boolean AppendNewProcedure(System.String procName);
        System.Boolean AppendNewView(System.String viewName);
        System.Boolean AppendNewUser(System.String userName);

        #endregion


        #region IsXxx Signatures
        System.Boolean IsUserWithLoginExist(System.String loginName);
        System.Boolean IsTriggerExist(System.String triggerName);
        System.Boolean IsUserExist(System.String userName);
        System.Boolean IsExistForeignConstraintToCell(ITableCell cell, System.String tableName);
        System.Boolean IsExistForeignConstraintToTable(System.String tableName);
        System.Boolean IsColumnWithNameExsitInTableWithName(System.String columnName, System.String tableName);
        System.Boolean IsValueExistIntColumnWithNameInTableWithName(System.String columnName, System.String tableName, System.String values);
        System.Boolean IsExistForeignConstraintToCellWithValue(ITableCell cell, System.String tableName);
        System.Boolean IsTableHasDeleteTrigger(System.String tableName);
        System.Boolean IsTableHasInsertTrigger(System.String tableName);
        System.Boolean IsTableHasUpdateTrigger(System.String tableName);
        System.Boolean IsTableHasTriggers(System.String tableName);
        System.Boolean IsEachTableExist(System.String[] tableNames);
        System.Boolean IsProcedureExist(System.String procedureName);
        System.Boolean IsViewExist(System.String viewName);
        System.Boolean IsTableExist(System.String tableName);
        #endregion


        #region ReadXxx Signatures
        IMetaInfo ReadTableMeta(System.String tableName);
        MMILDataBaseContext ReadStoredProcedureMMIL(System.String procedureName);
        ITable ReadTable(System.String tableName);

        #endregion


        #region GetXxx Sigmatures
        IUserToken GetUserByLoginName(System.String loginName);
        IUserToken GetUserByName(System.String userName);
        System.String GetWayToDataBaseRoot();
        System.String GetWayToDataBaseTableRoot();
        System.String GetWayToDataBaseProcRoot();
        System.String GetWayToDataBaseTriggerRoot();
        System.String GetWayToDataBaseViewRoot();
        System.String GetWayToDataBaseUserRoot();
        ITable GetViewsTable(System.String viewName);
        StoredProcedure GetStoredProcedure(System.String procName);
        Trigger Gettrigger(System.String trigName);
        System.Int32 GetCountOfViews();
        System.Int32 GetCountOfTables();
        System.Int32 GetCountOfProcedures();
        System.Int32 GetCountOfUser();
        List<System.String> GetViewsNames();
        List<System.String> GetTableNames();
        List<System.String> GetProceduresName();
        List<System.String> GetUserName();
        IUserToken GetUserTokenByName(System.String userName);
        System.String GetStoredProcedureText(System.String procName);
        System.String GetViewText(System.String viewName);
        System.String GetDataBaseOwner();
        public System.String GetDataBaseName();
        ITableInfo GetTableMetaByName(System.String tableName);

        System.String GetTriggerText(System.String trigName);
        #endregion

        #region RemoveXxx Methods

        void RemoveRightFromUser(System.String userName, System.String right, System.String objName);
        System.Boolean FlushTableMeta(System.String tableName);

        #endregion




        void ReWriteMetaTableInfoByTableName(System.String tableName);
        void ExecuteTriggerOnTable(System.String tableName, NodeType triggerType);
        void ReWriteUserTokenByName(System.String name, IUserToken user);



    }
}
