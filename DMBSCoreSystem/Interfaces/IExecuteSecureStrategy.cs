﻿
using System;

namespace DBMS.Core.Interprater.SecureStrategy
{
    public interface IExecuteSecureStrategy 
        : ICloneable
    {

        System.Boolean CheckExecuteMainRule();


    }
}
