﻿using System;
using System.Collections.Generic;
using System.Text;


namespace DBMS.Core.Secure.UserRights.RightTable
{
    public interface IAllowedCommandOnObject
    {
        Dictionary<System.String, AllowedCommand> AllowedCommandOn { set; get; }
    }
}
