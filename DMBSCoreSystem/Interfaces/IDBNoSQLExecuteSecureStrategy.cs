﻿using DBMS.Core.IO;
using DBMS.Core.MMIL;
using System;
using System.Collections.Generic;


namespace DBMS.Core.Interprater.SecureStrategy
{
    public interface IDBNoSQLExecuteSecureStrategy
        : ICloneable
    {
        System.Boolean CheckSecureRule (Dictionary<System.String, System.String> IntermediateLanguageContent);
        System.Boolean IsContextExist(System.String context);
        System.Boolean UserCanExecuteCommand(System.String command, List<string> ObjectName, List<NodeType> ObjectType);
        System.Boolean IsCommandExist(System.String command, String context);
        System.Boolean CheckSecureRule (IMMILTranslationUnit translationUnit);
        System.Boolean CkeckTableUserRight(IUserToken user, System.String tables, System.String commands, System.String dataBaseName);
        System.Boolean GetContextNamespace(System.String contextName, out System.String result);
        String TranslateContextTo(String context);
    }
}
