﻿using System;


namespace DBMS.Core.Interprater
{
    public interface ISecureStrategy
        : ICloneable
    {
        System.String Check(Exception e);
        System.String Check(System.Int32 erorrCode, System.String Message);

    }
}
