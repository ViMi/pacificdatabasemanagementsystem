﻿using DBMS.Core.MMIL;
using System;
using System.Collections.Generic;

namespace DBMS.Core.Interprater
{


    public enum UnityType 
        : System.Byte
    {
        BASE_IL_TRANSLATION_UNIT = 0,
        DATA_BASE_IL_TRANSLATION_UNIT = 1,
        DATA_BASE_TABLE_MAPPING_UNIT = 2,
    }


    /// <summary>
    /// This Interface provide contract of Parse Result object realisation
    /// </summary>
   public interface IParseResult 
        : ICloneable
    {


        /// <summary>
        /// This method return Parse Resultation string
        /// </summary>
        /// <returns></returns>
        System.String GetParseResult();




        /// <summary>
        /// Return collections of ILMM if its exist in set alse thrwo exception 
        /// </summary>
        /// <returns></returns>
        List<MMILDataBaseContext> GetTranslationUnits();


        /// <summary>
        /// This method return parse result dictionary where key is command and value is parametr
        /// </summary>
        /// <returns></returns>
        Dictionary<System.String, System.String> GetParseResultDictionary();




        /// <summary>
        /// Get Parse Result type (if it its collection return array of types)
        /// </summary>
        /// <returns></returns>

        List<UnityType> GetParseResultType();


        List<Dictionary<System.String, System.String>> GetParseResultListDictionary();


    }
}
