﻿using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.Helpers;


namespace DBMS.Core
{
    public static class NodeTypeExtensions
    {

        #region Documentation

#if DocTrue
        static NodeTypeExtensions() => Documentation.AddHistoryToSection(

                        PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreComponents,
            "NodeTypeExtensions",
            PQLDocGlobal.TypeStaticClass,
            PQLDocGlobal.DescriptionUndefined
            );
#endif
        #endregion


        public static NodeType NodeTypeResolverByName(this NodeType _, String name) => name switch
        {

            "SECURE_ROOT" => NodeType.SECURITY_DIRECTORY,
            "DATA_BASE" => NodeType.DATABASE,
            _ => throw new Exception()
        };

}
}
