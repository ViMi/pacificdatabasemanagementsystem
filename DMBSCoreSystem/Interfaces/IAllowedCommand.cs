﻿using System.Collections.Generic;


namespace DBMS.Core.Secure.UserRights.RightTable
{
    public interface IAllowedCommand
    {
        Dictionary<System.String, CommandRight> AllowCommand { set; get; }

    }
}
