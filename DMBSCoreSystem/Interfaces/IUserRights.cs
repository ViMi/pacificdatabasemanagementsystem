﻿using System.Collections.Generic;

namespace DBMS.Core.Secure.UserRights
{
    public interface IUserRights 
    {

        System.String GetRightList();
        Dictionary<System.String, System.Boolean> GetRightDictionary();

    }
}
