﻿using DBMS.Core.Table;

namespace DBMS.Core
{
    public interface IMethodParameter
    {
        System.String Name { set; get; }
        PacificDataBaseDataType DataType { set; get; }
        System.String Value { set; get; }

    }
}
