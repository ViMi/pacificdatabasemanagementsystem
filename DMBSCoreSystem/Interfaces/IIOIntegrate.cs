﻿

namespace DBMS.Core.IO
{


    public enum IOSaveMode
    {
        REWRITE = 0,
        NO_REWRITE = 1
    }

    public interface IIOIntegrate
    {

        void Save(System.String path, IUserToken user,IOSaveMode saveMode = IOSaveMode.REWRITE);


        void Read(System.String path);


    }
}
