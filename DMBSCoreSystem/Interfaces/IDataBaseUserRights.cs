﻿using DBMS.Core.Secure;

namespace DBMS.Core.Secure.UserRights
{
    public interface IDataBaseUserRights : IUserRights
    {

        ITableInfo TableInfo { set; get; }
        IDataBaseUserRights GetRightTable();

    }
}
