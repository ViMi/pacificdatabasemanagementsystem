﻿using System;

namespace DBMS.Core.InfoTypes
{
    public interface ITransaction
    {
        Func<ITransactionMethodResult> res { set; get; }

    }
}
