﻿using DBMS.Core.Secure.UserRights.RightTable;
using DBMS.Core.Secure;
using DBMS.Core.MetaInfoTables;
using System.Collections.Generic;
using DMBS.Core.Secure.UserRights.RightTable;

namespace DBMS.Core
{
    public interface IUserToken
        : IMetaInfo
    {



        System.String GetUserLogin();
        IPassword GetUserPassword();

        IAllowedCommand GetTableRight(System.String tableName, System.String db);
        Dictionary<System.String, IAllowedCommand> GetTableRightForEach(System.String[] tableNames, System.String db);


        AllowedCommandWithDB UserRights { set; get; }
        new System.String NodeName { set; get; }
        new System.String ChildrenList { set; get; }
        new Password NodeAccessPassword { set; get; }
        /// <summary>
        /// Contains Type of node
        /// </summary>
        new NodeType Type { set; get; }

        new System.String Parent { set; get; }
        new System.String Owner { set; get; }

        System.Boolean IsUserWorkWith(System.String chakKey);


    }
}
