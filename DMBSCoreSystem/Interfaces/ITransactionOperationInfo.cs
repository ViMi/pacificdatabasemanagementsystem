﻿using DBMS.Core.MetaInfoTables;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.Table;

namespace DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes
{
    public interface ITransactionOperationInfo
    {
        ITable OperationTable { set; get; }
        ITableInfo OperationTableMeta { set; get; }
    }
}
