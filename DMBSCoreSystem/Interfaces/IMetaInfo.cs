﻿using DBMS.Core.Secure;


namespace DBMS.Core
{




    public enum NodeType
    {


    //File System Const
        ROOT_DIRECTORY = 0,
        SYSTEM_DIRECTORY = 1,
        DATABASE_DIRECTORY = 2,

        SECURITY_DIRECTORY = 3,

        //DataBase Object Const
        DATABASE = 20,

        VIEW = 21,
        PROCEDURE = 22,
        TABLE = 23,
        USER = 24,
        ROOT = 25,
        LOGIN = 26,
        BACKUP = 27,
        TRIGGER = 28,

        UNSET = -1,

        LOG_REPORT = 29,

        DELETE_TRIGGER = 30,
        INSERT_TRIGGER = 31,
        UPDATE_TRIGGER = 32,

    }


    public interface IMetaInfo
    {

        System.String NodeName { set; get; }
        System.String ChildrenList { set; get; }

        Password NodeAccessPassword { set; get; }
        /// <summary>
        /// Contains Type of node
        /// </summary>
        NodeType Type { set; get; }

        System.String Parent { set; get; }
        System.String Owner { set; get; }





    }
}
