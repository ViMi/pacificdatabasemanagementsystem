﻿


namespace DBMS.Core.Secure
{


    public enum EncriptionType : System.Byte
    {
        /// <summary>
        /// Simple Readable text
        /// </summary>
        PlaneText = 0,

        ///ENcription By Vigineare alogorithm
        Type7 = 1,

        ///Ecription By MD5 algorithm
        Type5 = 2
    };



    public interface IEncriptor
    {
        EncriptionType type { get; }
        System.String Decript(System.String password);
        System.String Encript(System.String password);

    }
}
