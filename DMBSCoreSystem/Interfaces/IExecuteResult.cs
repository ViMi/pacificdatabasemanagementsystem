﻿

using System;
using DBMS.Core.InfoTypes;


namespace DBMS.Core.Interprater
{

    
    public interface IExecuteResult 
        : ICloneable
    {

        void Reset();
        
        /// <summary>
        /// This method appen new value in execute result
        /// </summary>
        /// <param name="value"></param>
        void Append(System.String value);

        /// <summary>
        /// This method retur string which present execute result
        /// </summary>
        /// <returns></returns>
        System.String GetExecuteResult();


        /// <summary>
        /// This method set excute result item in object
        /// </summary>
        /// <param name="executeResult"></param>
        void SetExecuteResult(System.String executeResult);


        /// <summary>
        /// This methdo add execute item in object 
        /// </summary>
        /// <param name="obj"></param>
        void Append(System.Object obj);


        /// <summary>
        /// This method get execute item in object
        /// </summary>
        /// <returns></returns>
        Object GetExecuteResultObject();

        ITransactionMethodResult _CurrentTI { set; get; }
        /// <summary>
        /// This method set execute result in object 
        /// </summary>
        /// <param name="executeResult"></param>
        void SetExecuteResult(Object executeResult);
    }
}
