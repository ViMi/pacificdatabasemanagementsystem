﻿using System;
using System.Collections.Generic;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Helpers;


namespace DBMS.Core
{

    public static class DBMSStandartEventNames
    {
        public static readonly String ClearConsole  = "ClearConsole";
    }




    public static class DBMSEventControlSystem
    {

        #region Documentation
#if DocTrue
        static DBMSEventControlSystem() => Documentation.AddHistoryToSection(
            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreEvent,
            "DBMSEventControlSystem",
            PQLDocGlobal.TypeStaticClass,
            PQLDocGlobal.DescriptionUndefined
            );

#endif
        #endregion



        public delegate void DBMSEvent();

        private static Dictionary<String, DBMSEvent> _EventDict = new Dictionary<string, DBMSEvent>();


        internal static void RegisterNewEvent(System.String eventName, DBMSEvent e) => DBMSEventControlSystem._EventDict.Add(eventName, e);
        internal static void ExecuteEvent(System.String eventName)
        {
            DBMSEvent ev;
            if (DBMSEventControlSystem._EventDict.ContainsKey(eventName))
            {
                DBMSEventControlSystem._EventDict.TryGetValue(eventName, out ev);
                ev();
            } else
                throw new EventUnregistredError();
           

        }

    }
}
