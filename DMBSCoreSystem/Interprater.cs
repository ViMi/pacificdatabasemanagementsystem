﻿using System;
using DBMS.Core.IO;
using DBMS.Core.Helpers;


namespace DBMS.Core.Interprater
{
   internal static class Interprater
    {

        #region 
#if DocTrue
        static Interprater() => Documentation.AddHistoryToSection(
            PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreFlow,
            "Interprater",
            PQLDocGlobal.TypeStaticClass,
            PQLDocGlobal.DescriptionUndefined
            
            );

#endif
        #endregion



        public static System.String Execute (ISecureStrategy secureStrategy = null)
        {
            try
            {
               return 
                    (Executer.GlobalExecuteStrategy.Execute
                    (Parser.GlobalParseStrategy.Execute())).GetExecuteResult();
            }

            catch(Exception e)
            {
               return (secureStrategy.Check(e) ?? "Error");
            }


        }


    }
}
