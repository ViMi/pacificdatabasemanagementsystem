﻿using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.ErrorControlSystem.Exeption;

namespace DBMS.PQL.ErrorControlSystem.Exception
{
    internal sealed class ViewStatementsCountExcetion
        : DBExeption
    {

        #region Documentation
#if DocTrue
        static ViewStatementsCountExcetion()
        {

        }
#endif
        #endregion

        public ViewStatementsCountExcetion()
            => this.ServerMessageError = PQL.ErrorControlSystem.ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.VIEW_STATEMENT_COUNT_ERROR];

    }
}
