﻿
namespace DBMS.PQL.ErrorControlSystem
{

    /// <summary>
    /// This enum present erroc codes
    /// </summary>
    public enum ErrorCodes
        : System.Byte
    {
        SUCCESS_EXECUTE = 0,
        FAILURE_EXECUTE = 2,

        EMPTY_NAME_ERROR = 3,
        UNCORRECT_STRING_FORMAT = 4,
        STRING_SEPARATOR_ERROR = 5,
        LOGIN_RIGHT_LIST_UNSETED = 6,
        SCHEMA_LIST_UNSETED = 7,
        USER_TOKEN_NOT_CREATED = 8,
        PASSWORD_UNSET = 9,

        VIEW_STATEMENT_COUNT_ERROR = 10,

    };



}
