﻿using System;
using System.Collections.Generic;
using DBMS.Core.Helpers;




namespace DBMS.PQL.ErrorControlSystem
{
    public static class ErrorMessagesCore
    {

        #region Documentation
#if DocTrue
        static ErrorMessagesCore()
        {
            Documentation.AddHistoryToSection(
                    PQLDocGlobal.CoreSystem,
                    PQLDocGlobal.CoreErrorControlSystem,
                    "ErrorMessageCore",
                    PQLDocGlobal.TypeStaticClass,
                    PQLDocGlobal.DescriptionUndefined

       
            );


            Documentation.AddHistoryToSection(

                                        PQLDocGlobal.CoreSystem,
            PQLDocGlobal.CoreErrorControlSystem,
            "ErrorCodes",
            PQLDocGlobal.TypeEnum,
            PQLDocGlobal.DescriptionUndefined

                );

        }
#endif
        #endregion

        public static Dictionary<System.Byte, String> MessageDictionary = new Dictionary<System.Byte, System.String>()
        {

            [(Byte)(ErrorCodes.SUCCESS_EXECUTE)] = "SUCCESS EXECUTE" + Environment.NewLine,
            [(Byte)(ErrorCodes.FAILURE_EXECUTE)] = "FAILURE EECUTE (UNCATCHED ERROR)" + Environment.NewLine,
            [(Byte)(ErrorCodes.EMPTY_NAME_ERROR)] = "EMPTY NAME ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.UNCORRECT_STRING_FORMAT)] = "UNCORRECT STRING FORMAT" + Environment.NewLine,
            [(Byte)(ErrorCodes.STRING_SEPARATOR_ERROR)] = "STRING SEPARATOR FORMAT ERROR" + Environment.NewLine,
            [(Byte)(ErrorCodes.LOGIN_RIGHT_LIST_UNSETED)] = "LOGIN RIGHT LIST UNSETED" + Environment.NewLine,
            [(Byte)(ErrorCodes.SCHEMA_LIST_UNSETED)] = "SCHEMA LIST UNSETTED" + Environment.NewLine,
            [(Byte)(ErrorCodes.USER_TOKEN_NOT_CREATED)] = "USER TOKEN NOT CREATED" + Environment.NewLine,
            [(Byte)(ErrorCodes.PASSWORD_UNSET)] = "PASSWORD_UNSET" + Environment.NewLine,
            [(Byte)(ErrorCodes.VIEW_STATEMENT_COUNT_ERROR)] = "VIEW CAN CONTAIN ONLY ONE SELECT STAMENT" + Environment.NewLine,
         


        };

      



    }
}
