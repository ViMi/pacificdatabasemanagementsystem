﻿using DBMS.Core.Interprater;
using System;
using DBMS.Core.Helpers;




namespace DBMS.PQL
{
    public class NoSQLSecureStrategy 
        : ISecureStrategy
    {

        #region Documentation
#if DocTrue
        static NoSQLSecureStrategy() { }
#endif
        #endregion


        public Object Clone() => new NoSQLSecureStrategy();
        public String Check(Exception e) => e.Message;
        public String Check(Int32 erorrCode, String Message) => Message;
    }
}
