﻿using DBMS.Core.IO;
using DBMS.Core.Interprater.SecureStrategy;
using System;
using System.Collections.Generic;
using DBMS.Core.Secure.UserRights.RightTable;
using DBMS.Core;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.MMIL;
using DBMS.Core.MMIL.ILConfiguration;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.PQL.Grammar;
using DMBS.Core.Secure.UserRights.RightTable;
using DBMS.Core.UsersFromRoles;
using DBMS.Core.Helpers;
using System.Linq;

namespace DBMS.PQL
{
    public class NoSQlExecuteSecureStrategy 
        : IDBNoSQLExecuteSecureStrategy
    {


        #region Documentation
#if DocTrue
        static NoSQlExecuteSecureStrategy() { }
#endif
        #endregion




        private Dictionary<System.String, System.String> _ContextNamespacesRbac = new Dictionary<String, String>(10)
        {
            [PQLGrammar.CREATE_COMMAND] = "DBMS.PQL.DDL",

            [PQLGrammar.DROP_COMMAND] = "DBMS.PQL.DDL",


            [PQLGrammar.DENY_COMMAND] = "DBMS.PQL.DCL",
            [PQLGrammar.GRANT_COMMAND] = "DBMS.PQL.DCL",
            [PQLGrammar.REVOKE_COMMAND] = "DBMS.PQL.DCL",


            [PQLGrammar.DELETE_COMMAND] = "DBMS.PQL.DML",
            [PQLGrammar.INSERT_COMMAND] = "DBMS.PQL.DML",
            [PQLGrammar.SELECT_COMMAND] = "DBMS.PQL.DML",
            [PQLGrammar.UPDATE_COMMAND] = "DBMS.PQL.DML",


            [PQLGrammar.USE_COMMAND] = "DBMS.PQL",
            [PQLGrammar.BACKUP_COMMAND] = "DBMS.PQL",
            [PQLGrammar.EXECUTE_COMMAND] = "DBMS.PQL",

            [PQLGrammar.MIGRATE_COMMAND] = "DBMS.PQL",

            [PQLGrammar.ADMINER_COMMAND] = "DBMS.PQL",
            [PQLGrammar.CONSOLE_COMMAND] = "DBMS.PQL",
            [PQLGrammar.HELP] = "DBMS.PQL",
            [PQLGrammar.CHANGE_PASSWORD] = "DBMS.PQL.DCL",
            [PQLGrammar.FOR_COMMAND] = "DBMS.PQL",
            [PQLGrammar.WHILE_COMMAND] = "DBMS.PQL"
        };


        #region Global Secure Flow
        Boolean IDBNoSQLExecuteSecureStrategy.CheckSecureRule
            (IMMILTranslationUnit IntermediateLanguageContent)
        {


            Dictionary<System.String, System.String> dict
                = IntermediateLanguageContent.GetILDictionary();

            System.Boolean userExists =
                dict.TryGetValue(MMILDataBaseConfig.UserDeclarationCommand, out _);

            if (!userExists)
                throw new ExecutorControlSecurityError();


            System.Boolean dataBaseCheck =
                dict.TryGetValue(MMILDataBaseConfig.DataBaseDeclarationCommand, out _); //&& user.GetDataBaseAccess(dbName);


            if (!dataBaseCheck)
                throw new ExecutorControlSecurityError();



            System.String commands = System.String.Empty;
            if (!dict.TryGetValue(MMILDataBaseConfig.CommandDeclarationCommand, out _))
                throw new ExecutorControlSecurityError();




            System.String parameters = System.String.Empty;
            if (!dict.TryGetValue(MMILDataBaseConfig.CommandParameterDeclaration, out _))
                throw new ExecutorControlSecurityError();



            if (!dict.TryGetValue(MMILDataBaseConfig.StartContextCommand, out _))
                throw new ExecutorControlSecurityError();

            return true;
        }





        bool IDBNoSQLExecuteSecureStrategy.CheckSecureRule
                (Dictionary<System.String, System.String> dict)
        {


            System.Boolean userExists =
                dict.TryGetValue(MMILDataBaseConfig.UserDeclarationCommand, out _);

            if (!userExists)
                throw new ExecutorControlSecurityError();


            System.Boolean dataBaseCheck =
                dict.TryGetValue(MMILDataBaseConfig.DataBaseDeclarationCommand, out _); //&& user.GetDataBaseAccess(dbName);


            if (!dataBaseCheck)
                throw new ExecutorControlSecurityError();



            System.String commands = System.String.Empty;
            if (!dict.TryGetValue(MMILDataBaseConfig.CommandDeclarationCommand, out _))
                throw new ExecutorControlSecurityError();

           


            System.String parameters = System.String.Empty;
            if (!dict.TryGetValue(MMILDataBaseConfig.CommandParameterDeclaration, out _))
                throw new ExecutorControlSecurityError();



            if (!dict.TryGetValue(MMILDataBaseConfig.StartContextCommand, out _))
                throw new ExecutorControlSecurityError();

            return true;
        }



        public Object Clone() => new NoSQlExecuteSecureStrategy();

        public Boolean IsContextExist(System.String context) => _ContextNamespacesRbac.ContainsKey(context);
        public Boolean IsCommandExist(String command, String context)
        {

            HashSet<String> set;
            if (PQLGrammar.Commands.TryGetValue(context, out set))
                return set.Contains(command);
            
            return false;

        }

        public String TranslateContextTo(String context)
        {
            String com = String.Empty;
            if (PQLGrammar.GetCommandPQLComfogromity(context) != Rbac.UNCLASSED_COMMAND)
                com = PQLGrammar.GetCommandPQLComfogromity(context);
            else
                com = context;


            return com;
        }


        /// <summary>
        /// This need for rflection to resolve namespace of type of context
        /// </summary>
        /// <returns></returns>
        public System.Boolean GetContextNamespace(System.String contextName, out System.String result) =>
            this._ContextNamespacesRbac.TryGetValue(contextName, out result);
        #endregion





        #region User Secure Flow
        public Boolean CkeckTableUserRight
            (IUserToken user, System.String tableList, System.String commandList, System.String DataBaseName)
        {
            System.String[] tables = tableList.Split(' ');
            System.String[] command = commandList.Split(' '); 


            Dictionary<System.String, IAllowedCommand> userRight = user.GetTableRightForEach(tables, DataBaseName);


            for (System.Int32 i = 0; i < userRight.Count; ++i)
            {
                IAllowedCommand rightsList = null;

                if(!userRight.ContainsKey(tables[i]))
                {
                    userRight.TryGetValue(tables[i], out rightsList);
                    for (System.Int32 j = 0; j < command.Length; ++j)
                    {

                        CommandRight commandRight = null;
                        if (!rightsList.AllowCommand.TryGetValue(command[i], out commandRight))
                        {
                            throw new RightsError();
                        }

                    }

                } else
                {
                    throw new RightsError();
                }
            }


            return true;
        }


        public Boolean UserCanExecuteCommand(System.String command, List<System.String> ObjectName, List<NodeType> ObjectType)
        {

            #region Base Commands
            if (PQLGrammar.GetCommandRbacComfirmity(command) == Rbac.USE_COMMAND)
            {
                Dictionary<String, NodeType> nt = DBMSIO.GetCurrentLogin().LoginRights;
                String key = (ObjectName[0]
                    .Replace(PQLGrammar.LeftParameterBracetStd.ToString(), String.Empty)
                    .Replace(PQLGrammar.RightParameterBracetStd.ToString(), String.Empty)
                    .Replace(PQLGrammar.ConstantLiteralStringBracet.ToString(), String.Empty));

                if (!DBMSIO.IsDataBaseExist(key))
                    throw new DataBaseNotExist();

                if (nt.ContainsKey(key))
                    return true;
                else return false;

            }

            if ((PQLGrammar.GetCommandRbacComfirmity(command) == Rbac.GRANT_COMMAND || PQLGrammar.GetCommandRbacComfirmity(command) == Rbac.GRANT_COMMAND) &&
                DBMSIO.GetCurrentLogin().NodeName == DBMSIO.DBMSConfig.SimpleSecureAdmin) return true;

            if (PQLGrammar.GetCommandRbacComfirmity(command) == Rbac.CREATE_COMMAND && ObjectType[0] == NodeType.DATABASE)
                if (DBMSIO.GetCurrentLogin().NodeName == DBMSIO.DBMSConfig.SimpleSysAdminName || DBMSIO.GetCurrentLogin().NodeName == DBMSIO.DBMSConfig.SimpleDBCreator)
                    return true;


            if (PQLGrammar.GetCommandRbacComfirmity(command) == Rbac.ADMINER_COMMAND)
                if (DBMSIO.GetCurrentLogin().NodeName == DBMSIO.DBMSConfig.SimpleSysAdminName)
                    return true;


            if (command == PQLGrammar.CONSOLE_COMMAND)
                return true;


            if (command == PQLGrammar.HELP)
                return true;
            #endregion



            #region User Read
            //Get User to that DataBase

            String rootKey = String.Empty;
            IUserToken user = null;

            if (DBMSIO.GetCurrentDataBase() == null)
                throw new DataBaseNotExist();

            if (!DBMSIO.GetUserToken(DBMSIO.GetCurrentDataBase().GetDataBaseName(), out user))
                throw new UserNotExist();

            rootKey = DBMSIO.GetCurrentDataBase().GetDataBaseName();
            System.Boolean flag = false;
            #endregion



            #region Work With Command Rights
            AllowedCommandWithDB userRights = user.UserRights;
            Dictionary<String, AllowedCommand> allowedCommands = userRights.AllowedCommandOn;

            //Check db existing 
            if (!allowedCommands.ContainsKey(rootKey))
                throw new RightsError();

            AllowedCommand allowedOnDB;
            if (!allowedCommands.TryGetValue(rootKey, out allowedOnDB))
                throw new TypeError();

            if (allowedOnDB.AllowCommand == null)
                return false;

            //Check all rights in table

            CommandRight cr;
            
            if (allowedOnDB.AllowCommand.ContainsKey(Rbac.ALL_COMPATIBILE_OPERATION_ALLOWED))
                if (allowedOnDB.AllowCommand.TryGetValue(Rbac.ALL_COMPATIBILE_OPERATION_ALLOWED, out cr))
                    if (cr.CommandRights.TryGetValue(Rbac.GLOBAL_ALL_OBJECT_RIGHT_TOKEN, out _))
                        return true;



            


            if (allowedOnDB.AllowCommand.TryGetValue(command, out cr))
            {


                if (allowedOnDB.AllowCommand.ContainsKey(command))
                {


                    if (cr.CommandRights.TryGetValue(Rbac.GLOBAL_ALL_OBJECT_RIGHT_TOKEN, out flag))
                        return true;

                    if (cr.CommandRights.TryGetValue(Rbac.GLOBAL_DATABASE_RIGHT_TOKEN, out flag))
                        return true;

                    if (cr.CommandRights.TryGetValue(Rbac.GLOBAL_PROCEDURE_RIGHT_TOKEN, out flag) && ObjectType.ElementAt(0) == NodeType.PROCEDURE)
                        return true;

                    if (cr.CommandRights.TryGetValue(Rbac.GLOBAL_TABLE_RIGHT_TOKEN, out flag) && ObjectType.ElementAt(0) == NodeType.TABLE)
                        return true;

                    if (cr.CommandRights.TryGetValue(Rbac.GLOBAL_TRIGGER_RIGHT_TOKEN, out flag) && ObjectType.ElementAt(0) == NodeType.TRIGGER)
                        return true;

                    if (cr.CommandRights.TryGetValue(Rbac.GLOBAL_USER_RIGHT_TOKEN, out flag) && ObjectType.ElementAt(0) == NodeType.USER)
                        return true;

                    if (cr.CommandRights.TryGetValue(Rbac.GLOBAL_VIEW_RIGHT_TOKEN, out flag) && ObjectType.ElementAt(0) == NodeType.VIEW)
                        return true;

                    if (   command == PQLGrammar.DROP_COMMAND 
                        || command == PQLGrammar.CREATE_COMMAND 
                        || command == PQLGrammar.DENY_COMMAND 
                        || command == PQLGrammar.GRANT_COMMAND
                        || command == PQLGrammar.REVOKE_COMMAND) 
                    {

                        if (cr.CommandRights.TryGetValue("[" + ObjectType.ElementAt(0) + "]", out _))
                            return true;


                    }



                }




                if (allowedOnDB.AllowCommand.TryGetValue(command, out cr))
                {

                    if (cr.CommandRights.TryGetValue(Rbac.GLOBAL_ALL_OBJECT_RIGHT_TOKEN, out flag))
                        return true;

                    for (Int32 i = 0; i < ObjectName.Count; ++i)
                        if (!cr.CommandRights.TryGetValue(ObjectName[i], out flag))
                            return false;


                }

            }



            else if (allowedOnDB.AllowCommand.TryGetValue(PQLGrammar.GetCommandRbacComfirmity(command), out cr))
            {


                if (allowedOnDB.AllowCommand.ContainsKey(PQLGrammar.GetCommandRbacComfirmity(command)))
                {

                    if (cr.CommandRights.TryGetValue(Rbac.GLOBAL_ALL_OBJECT_RIGHT_TOKEN, out flag))
                        return true;

                    if (cr.CommandRights.TryGetValue(Rbac.GLOBAL_DATABASE_RIGHT_TOKEN, out flag))
                        return true;

                    if (cr.CommandRights.TryGetValue(Rbac.GLOBAL_PROCEDURE_RIGHT_TOKEN, out flag) && ObjectType.ElementAt(0) == NodeType.PROCEDURE)
                        return true;

                    if (cr.CommandRights.TryGetValue(Rbac.GLOBAL_TABLE_RIGHT_TOKEN, out flag) && ObjectType.ElementAt(0) == NodeType.TABLE)
                        return true;

                    if (cr.CommandRights.TryGetValue(Rbac.GLOBAL_TRIGGER_RIGHT_TOKEN, out flag) && ObjectType.ElementAt(0) == NodeType.TRIGGER)
                        return true;

                    if (cr.CommandRights.TryGetValue(Rbac.GLOBAL_USER_RIGHT_TOKEN, out flag) && ObjectType.ElementAt(0) == NodeType.USER)
                        return true;

                    if (cr.CommandRights.TryGetValue(Rbac.GLOBAL_VIEW_RIGHT_TOKEN, out flag) && ObjectType.ElementAt(0) == NodeType.VIEW)
                        return true;



                    if (command == PQLGrammar.DROP_COMMAND || command == PQLGrammar.CREATE_COMMAND)
                    {

                        if (cr.CommandRights.TryGetValue("[" + ObjectType.ElementAt(0) + "]", out _))
                            return true;


                    }

 

                }


                if (allowedOnDB.AllowCommand.TryGetValue(PQLGrammar.GetCommandRbacComfirmity(command), out cr))
                {

                    if (cr.CommandRights.TryGetValue(Rbac.GLOBAL_ALL_OBJECT_RIGHT_TOKEN, out flag))
                        return true;


                    for (Int32 i = 0; i < ObjectName.Count; ++i)
                        if (!cr.CommandRights.TryGetValue(ObjectName[i], out flag))
                            return false;

                }

            }
            return flag;



            #endregion

        }


        #endregion



    }
}
