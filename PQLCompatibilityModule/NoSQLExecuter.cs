﻿using DBMS.Core.Interprater;
using System;
using DBMS.Core.Interprater.SecureStrategy;
using System.Collections.Generic;
using System.Linq;
using DBMS.PQL.Extension;
using DBMS.Core;
using DBMS.Core.MMIL;
using System.Reflection;
using DBMS.Core.ErrorControlSystem.Exeption;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.Core.Helpers;
using DBMS.Core.Helpers.Tests;
using DBMS.Core.IO;
using DBMS.PQL.Grammar;

namespace DBMS.PQL
{
   public class NoSQLExecuter
        : IExecuteStrategy
    {


        #region Documentation
#if DocTrue
        static NoSQLExecuter() { }
#endif
        #endregion

        private String _NamespaceSeparator = ".";

        private String _StartOperationMethodName = "StartTransaction";
        private String _EndOperationMethodName = "EndTransaction";

        private String _CommandName = "CommandName";
        private String _OperateObjectName = "OperateObjectName";
        private String _OperateObectType = "OperateObectType";


        private IDBNoSQLExecuteSecureStrategy _SecureStrategy;
        private IExecuteResult _ExecuteResult;

        public NoSQLExecuter(IDBNoSQLExecuteSecureStrategy secureStrategy, IExecuteResult executeResult)
        {
            this._SecureStrategy = secureStrategy;
            this._ExecuteResult = executeResult;
            
        }

        public object Clone() => new NoSQLExecuter
                (this._SecureStrategy.Clone() as IDBNoSQLExecuteSecureStrategy, _ExecuteResult.Clone() as IExecuteResult);


        public IExecuteResult Execute (IParseResult parseResult)
        {

            
            List<MMILDataBaseContext> units
                = parseResult.GetTranslationUnits();
            
            
            for (System.Int32 i = 0; i < units.Count; ++i)
            {

                if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
                    _SecureStrategy.CheckSecureRule(units.ElementAt(i));



                (Type, Object) res = this._Execute(units.ElementAt(i));
                if (res.Item1 == null)
                    throw new ReflectionTypeTransformError();


                try
                {

                    PropertyInfo commandName = res.Item1.GetProperty(_CommandName);
                    PropertyInfo OperateObjectName = res.Item1.GetProperty(_OperateObjectName);
                    PropertyInfo OperateObectType = res.Item1.GetProperty(_OperateObectType);

                    System.String command = commandName.GetValue(res.Item2) as System.String;
                    List<string> ObjectName = OperateObjectName.GetValue(res.Item2) as List<String>;
                    List<NodeType> ObjectType = OperateObectType.GetValue(res.Item2) as List<NodeType>;


                    CommandTracer.CommanDesc = new CommandDescription(command, PQLGrammar.GetCommandGroup(command), String.Empty);
                    CommandTracer.Line = i;

                    if(DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
                           if (!this._SecureStrategy.UserCanExecuteCommand(command, ObjectName, ObjectType))
                                throw new RightsError();
                    

                    System.Object appendValue
                        = this.InitExecutionMethod(_StartOperationMethodName, res.Item1, res.Item2);

                    if (appendValue == null)
                        throw new ValueError();

                    _ExecuteResult.Append(appendValue);
                } 
                catch(Exception ex)
                {
                    this.InitExecutionMethod(_EndOperationMethodName, res.Item1, res.Item2);


                    DBExeption dbEx = ex as DBExeption;
                    if (dbEx != null)
                        _ExecuteResult.Append(new TransactionInformation.TransactionMethodResult(false, dbEx.ServerMessageError));
                    else
                        _ExecuteResult.Append(new TransactionInformation.TransactionMethodResult(false, ex.Message));

                    return _ExecuteResult;
                }
            }

            DBMSIO.Logger?.Flush();
            DBMSIO.Logger?.Reset();

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
                DBMSIO.ServerJournal?.Journilize(new MMILCollection() { ILUnits = units });

            
            return this._ExecuteResult;
          
        }








        private (Type, Object) _Execute
            (IMMILTranslationUnit contextIL)
        {


            MMILDataBaseContext IntermediateLanguageContent = contextIL as MMILDataBaseContext;

            System.String[] commands = IntermediateLanguageContent.GetCommandArray();
            System.String[] parameters = IntermediateLanguageContent.GetCommandParametersArray();
            System.String context = IntermediateLanguageContent.GetCommandContext();
            System.String dataBase = IntermediateLanguageContent.GetDataBaseName();
            System.String userName = IntermediateLanguageContent.GetUserName();


            System.Object res = null;
            Type execContext = null;


            if (!this._SecureStrategy.IsContextExist(context))
                throw new TypeError();


                for (System.Int32 i = 0; i < commands.Length; ++i)
            {
                if (execContext == null)
                {

                    if (!this._SecureStrategy.IsCommandExist(commands[i], context))
                         throw new TypeError();




                    System.String names
                        = System.String.Empty;
                    this._SecureStrategy.GetContextNamespace(context, out names);

                    

                    names += _NamespaceSeparator + context;

                    execContext
                       = Type.GetType(names);

                    res ??= this.InitEmptyConstructor(execContext);
                    if (res == null)
                        throw new TypeError();


                }

                parameters[i] = parameters[i].Trim().Trim(new System.Char[2] { PQLGrammar.LeftParameterBracetStd, PQLGrammar.RightParameterBracetStd });
                parameters[i] = parameters[i].Substring(1, (parameters[i].Length - 2));

                TransactionInfo info
                    = new TransactionInfo(userName, dataBase, parameters[i]);

                
                this.InitMethod(commands[i], info, res, execContext);
            }

            return (execContext, res);
        }

        
    }


}
