﻿using DBMS.Core.Interprater;
using DBMS.Core.MMIL;
using DBMS.Core.Interprater.SecureStrategy;

using System;
using DBMS.Core.IO;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.UsersFromRoles;
using DBMS.Core.Helpers;


using DBMS.Core;

namespace DBMS.PQL
{
    /// <summary>
    /// This class present NoSQL parse module for Pacific DBMS
    /// </summary>
    public class NoSQLParseModule
        : IParseStrategy
    {

        #region Documentation
#if DocTrue
        static NoSQLParseModule() { }
#endif
        #endregion


        private ILanguageSecureStrategy _SecurityStrategy;
        private System.String _Listing;


        /// <summary>
        /// This method provide translation of code into IL commands
        /// </summary>
        /// <param name="textContent"></param>
        /// <returns></returns>
        public IParseResult Execute()
        {

            MMILCollection parseResultCollection = new MMILCollection();


            System.String[] content
                = _SecurityStrategy.PrepareListingToTranslation();

            System.Int32 commandCount = content.Length;

            for (System.Int32 i = 0; i < commandCount; ++i)
            {



                System.String[] command = _SecurityStrategy.PrepareContextToTranslation(content[i]);
                if (String.IsNullOrWhiteSpace(command[0]))
                    continue;


                if (DBMSIO.GetDBMSMode() == DBMSMode.STANDART_MODE)
                    if (!_SecurityStrategy.IsAbstractCommand(command[0]))
                       throw new SyntaxError();



                System.String commandList = System.String.Empty;
                System.String parameterList = System.String.Empty;

                MMILDataBaseContext CurrentContext = new MMILDataBaseContext();

 
       
                CurrentContext.InitExecutionContext(command[0]);

                CurrentContext.SetServer(DBMSIO.ServerMetaInformation.ServerType.ToString());

                String dbName = String.Empty;
                String abstCommand = String.Empty;

                if (Rbac.USE_COMMAND != (PQL.Grammar.PQLGrammar.GetCommandRbacComfirmity(command[0]))) {
                    dbName = DBMSIO.GetCurrentDataBase() != null ? DBMSIO.GetCurrentDataBase().GetDataBaseName() : Rbac.DATABASE_PLACEHOLDER;
                    CurrentContext.SetDatabase(dbName);
                } else
                    abstCommand = command[0];

                CurrentContext.SetUser(DBMSIO.GetCurrentLogin().NodeName);
                


                for (System.Int32 j = 1; j < command.Length; ++j)
                {

                    command[j] = _SecurityStrategy.PrepareCommandToTranslation(command[j]);

                    System.String[] concreteCommand = new System.String[2]
                    {
                        _SecurityStrategy.GetMethodNameFromString(command[j]),
                        _SecurityStrategy.GetParameterFromString(command[j])
                    };


                    if (DBMSIO.GetDBMSMode() == DBMSMode.STANDART_MODE)
                        if (!_SecurityStrategy.IsConcreteCommand(command[0], concreteCommand[0]))
                        throw new SyntaxError();
                    

                    commandList += concreteCommand[0] + DBMSIO.DBMSConfig.StandartSeparator;
                    parameterList += concreteCommand[1] + DBMSIO.DBMSConfig.StandartSeparator;

                }

                CurrentContext.SetCommandUnsafe(commandList);
                CurrentContext.SetParametersUnsafe(parameterList);
                if (Rbac.USE_COMMAND == (PQL.Grammar.PQLGrammar.GetCommandRbacComfirmity(abstCommand)))
                    CurrentContext.SetDatabase(parameterList);

                CurrentContext.EndExecuteContext();
                parseResultCollection.ILUnits.Add(CurrentContext);


            }


            return parseResultCollection;
        }

        public object Clone()=> new NoSQLParseModule(this._SecurityStrategy.Clone() as ILanguageSecureStrategy, this._Listing);

        public NoSQLParseModule(ILanguageSecureStrategy secureStrategy, System.String listing)
        {
            this._SecurityStrategy = secureStrategy;
            this._Listing = listing;
        }


        public void SetListing(System.String listing)=> this._Listing = listing;


    }
}
