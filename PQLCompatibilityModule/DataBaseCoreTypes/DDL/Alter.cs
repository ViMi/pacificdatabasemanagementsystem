﻿using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.IO;
using DBMS.PQL.Grammar;
using System.Collections.Generic;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.Helpers;

using System;

namespace DBMS.PQL.DDL
{
    public sealed class Alter 
        : IPacificTransaction
    {

        #region Documentation
#if DocTrue
        static Alter() { }
#endif
        #endregion


        #region Variable Declaration
        public List<System.String> OperateObjectName { set; get; } = new List<System.String>();
        public System.String CommandName { set; get; } = PQLGrammar.DROP_COMMAND;
        public List<NodeType> OperateObectType { set; get; } = new List<NodeType>();

        private ITransactionMethodResult _Result = null;
        private IAlterTransaction _CommandType;
        #endregion



        public void Table(ITransactionInfo ti)
        {
            #region RuleCheck
            _Result = PQLGrammar.PQLLanRulesManager.Invoke
                 ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
            if (_Result.IsSuccess) return;
            _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
            if (!_Result.IsSuccess) return;
            #endregion

            OperateObectType.Add(NodeType.TABLE);
            _CommandType = new AlterTable(ti.UserName, ti.DataBaseName, ti.Parameters);
        }

        public void DataBase(ITransactionInfo ti)
        {
            #region RuleCheck
            _Result = PQLGrammar.PQLLanRulesManager.Invoke
               ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
            if (_Result.IsSuccess) return;
            _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
            if (!_Result.IsSuccess) return;
            #endregion

            OperateObectType.Add(NodeType.DATABASE);
            _CommandType = new AlterDataBase(ti.UserName, ti.DataBaseName);
        }

        public void Procedure(ITransactionInfo ti)
        {
            #region RuleCheck
            _Result = PQLGrammar.PQLLanRulesManager.Invoke
               ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
            if (_Result.IsSuccess) return;
            _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
            if (!_Result.IsSuccess) return;
            #endregion

            OperateObectType.Add(NodeType.PROCEDURE);
            _CommandType = new AlterProcedure(ti.UserName, ti.DataBaseName, ti.Parameters);
        }

        public void TriggerOnDelete(ITransactionInfo ti)
        {
            #region RuleCheck
            _Result = PQLGrammar.PQLLanRulesManager.Invoke
                 ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
            if (_Result.IsSuccess) return;
            _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
            if (!_Result.IsSuccess) return;
            #endregion


            OperateObectType.Add(NodeType.TRIGGER);
            _CommandType = new AlterTrigger(ti.UserName, ti.DataBaseName, ti.Parameters, Core.Triggers.TriggerType.ON_DELETE);
        }

        public void TriggerOnInsert(ITransactionInfo ti)
        {

            #region RuleCheck
            _Result = PQLGrammar.PQLLanRulesManager.Invoke
                 ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
            if (_Result.IsSuccess) return;
            _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
            if (!_Result.IsSuccess) return;
            #endregion

            OperateObectType.Add(NodeType.TRIGGER);
            _CommandType = new AlterTrigger(ti.UserName, ti.DataBaseName, ti.Parameters, Core.Triggers.TriggerType.ON_INSERT);
        }

        public void TriggerOnUpdate(ITransactionInfo ti)
        {

            #region RuleCheck
            _Result = PQLGrammar.PQLLanRulesManager.Invoke
                ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
            if (_Result.IsSuccess) return;
            _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
            if (!_Result.IsSuccess) return;
            #endregion


            OperateObectType.Add(NodeType.TRIGGER);
            _CommandType = new AlterTrigger(ti.UserName, ti.DataBaseName, ti.Parameters, Core.Triggers.TriggerType.ON_UPDATE);
        }

        public void User(ITransactionInfo ti)
        {

            #region RuleCheck
            _Result = PQLGrammar.PQLLanRulesManager.Invoke
                  ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
            if (_Result.IsSuccess) return;
            _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
            if (!_Result.IsSuccess) return;
            #endregion


            OperateObectType.Add(NodeType.USER);
            _CommandType = new AlterUser(ti.UserName, ti.DataBaseName, ti.Parameters);

        }

        public void Login(ITransactionInfo ti)
        {

            #region RuleCheck
            _Result = PQLGrammar.PQLLanRulesManager.Invoke
                 ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
            if (_Result.IsSuccess) return;
            _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
            if (!_Result.IsSuccess) return;
            #endregion

            OperateObectType.Add(NodeType.LOGIN);
            _CommandType = new AlterLogin(ti.Parameters, ti.UserName);
        }

        public void View(ITransactionInfo ti)
        {

            #region RuleCheck
            _Result = PQLGrammar.PQLLanRulesManager.Invoke
                  ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
            if (_Result.IsSuccess) return;

            _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters});
            if (!_Result.IsSuccess) return;
            #endregion

            OperateObectType.Add(NodeType.VIEW);
            _CommandType = new AlterView(ti.UserName, ti.DataBaseName, ti.Parameters);
        }

        public void As(ITransactionInfo ti)
        {

            #region RuleCheck
            _Result = PQLGrammar.PQLLanRulesManager.Invoke
                ((Byte)PQLLangRuleID.AS_PARAMETER_MULTIPLY_SETTED, new TransactionInfo() { Parameters = _CommandType.AsInfo });
            if (_Result.IsSuccess) return;
            #endregion

            _CommandType.AsInfo = ti.Parameters;

        }

        public ITable EndTransaction() => null;
        public ITransactionMethodResult StartTransaction()
        {

            #region RuleCheck
            if (_Result != null && _Result.IsSuccess)  return _Result;
            #endregion

            _CommandType.As();
            return _CommandType.Alter();

        }

    }
}
