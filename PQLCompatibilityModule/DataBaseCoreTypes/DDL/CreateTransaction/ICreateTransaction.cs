﻿using DBMS.Core.InfoTypes;

namespace DBMS.PQL.DDL
{
    internal interface ICreateTransaction
    {
        ITransactionMethodResult Create();
        ITransactionMethodResult As();

        System.String AsInfo { set; get; }


    }
}
