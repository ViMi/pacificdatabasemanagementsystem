﻿using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.IO;
using System;
using System.IO;
using DBMS.Core.Interprater;
using DBMS.Core.Secure.UserRights;
using System.Collections.Generic;
using System.Linq;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.Helpers;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.PQL.Grammar;


namespace DBMS.PQL.DDL
{
    internal sealed class CreateTable 
        : ICreateTransaction
    {

        #region Documentation
#if DocTrue
        static CreateTable() { }
#endif
        #endregion


        #region Variable Declaration

        private ITableInfo _Table = null;
        public System.String AsInfo { set; get; } = String.Empty;

        private ITransactionInfo _ti;
        private String _Separator = PQLGrammar.ChildrenPacketSeparator.ToString();
        private ITransactionMethodResult _Result = null;
        
        internal static String TableName { set; get; }
        #endregion

        

        public CreateTable(System.String ownerName, System.String dataBaseName, System.String tableName)
        {
            _ti = new TransactionInfo();

            _ti.UserName = ownerName;
            _ti.DataBaseName = dataBaseName;
            _ti.Parameters = tableName;

            TableName = tableName;
        }

        public ITransactionMethodResult Create()
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                if (_Result != null && _Result.IsSuccess) return _Result;
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                            ((Byte)PQLLangRuleID.TABLE_CREATED, new TransactionInfo() { AddedInfo = _Table });
                if (!_Result.IsSuccess) return _Result;
                #endregion
            }


            _ti.AddedInfo = _Table;
            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] CREATE TABLE {_ti.Parameters}");
            return DBMSIO.ExecuteCommand(DBCommand.CREATE_TABLE, _ti);

        }


        public ITransactionMethodResult As()
        {
            try
            {
                IParseStrategy parser
                    = new TableContentParser(
                        new TableContentLanguageSecureStrategy(AsInfo),
                        AsInfo
                        );

                IExecuteStrategy executer
                    = new TableContentExecuter(
                        new TableContentExecuterSecureStrategy(),
                        new TableRecordMetaInfo()

                        );


                IExecuteResult result = executer.Execute(parser.Execute());
                _Table = result.GetExecuteResultObject() as ITableInfo;
                _Table.TableName = _ti.Parameters;
                _Table.DataBaseName = DBMSIO.GetCurrentDataBase().GetDataBaseName() ?? String.Empty;
                _Table.Parent = DBMSIO.GetCurrentDataBase().GetDataBaseName() ?? String.Empty;
                _Table.Owner = DBMSIO.GetCurrentLogin().NodeName;


                _Table.ChildrenList = String.Empty;

                foreach (var item in _Table.ColumnConstraint.Keys)
                {

                    Dictionary<String, String> dict;
                    _Table.ColumnConstraint.TryGetValue(item, out dict);
                    foreach (var item2 in dict)
                        _Table.ChildrenList += item + _Separator + item2 + _Separator;
                    

                    _Table.ChildrenList += DBMSIO.DBMSConfig.StandartSeparator;

                }

                _Table.ServerName = DBMSIO.ServerMetaInformation.ServerType.ToString();


                return new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE]);
            }
             catch (Exception ex)
            {
                DBExeption dbEx = ex as DBExeption;
                if(dbEx != null)
                 return (_Result = new TransactionMethodResult(false, dbEx.ServerMessageError));
                else 
                return (_Result = new TransactionMethodResult(false, ex.Message));
            }
        }
    }
}
