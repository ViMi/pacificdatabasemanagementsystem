﻿using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.IO;
using System;
using System.IO;
using DBMS.Core.Interprater;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.MMIL;

using DBMS.Core.Helpers;
using DBMS.PQL.Grammar;

namespace DBMS.PQL.DDL
{



    internal sealed class CreateProcedure 
        : ICreateTransaction
    {


        #region Documentation
#if DocTrue
        static CreateProcedure() { }
#endif
        #endregion



        #region Variable Declaration
        private ITransactionInfo _ti;
        public System.String AsInfo { set; get; } = String.Empty;
        private ProcedureType _Type { set; get; }
        private StoredProcedure _Procedure { set; get; }
        private ITransactionMethodResult _Result = null;
        #endregion


        public CreateProcedure
            (System.String ownerName, 
            System.String dataBaseName,
            System.String procName, 
            ProcedureType type = ProcedureType.NO_PARAMES)
        {
            _ti = new TransactionInfo();
            _ti.UserName = ownerName;
            _ti.DataBaseName = dataBaseName;
            _ti.Parameters = procName;
        }



        public ITransactionMethodResult Create()
        {

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                if (_Result != null) return _Result;
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                ((Byte)PQLLangRuleID.AS_PARAMETER_UNSETED, new TransactionInfo() { Parameters = AsInfo });
                if (_Result.IsSuccess) return _Result;
                #endregion
            }



            _Procedure.ProcType = _Type;
            _ti.AddedInfo = _Procedure;

            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] CREATE PROCEDURE {_ti.Parameters}");
            return DBMSIO.IOIntegrationModule.ExecuteCommand(DBCommand.CREATE_PROCEDURE, _ti);
        }




        public ITransactionMethodResult As()
        {

            try
            {
                AsInfo = AsInfo.Trim()
                    .Replace(PQLGrammar.LeftParameterBracetInner.ToString(), PQLGrammar.LeftParameterBracetStd.ToString())
                    .Replace(PQLGrammar.RightParameterBracetInner.ToString(), PQLGrammar.RightParameterBracetStd.ToString())
                    .Replace(PQLGrammar.TerminalSymboInner.ToString(), PQLGrammar.TerminalSymbol.ToString())
                    .Replace(PQLGrammar.CommandsSeparatorInner.ToString(), PQLGrammar.CommandsSeparatorStd.ToString());

                IParseStrategy ps = new ProcedureParser(AsInfo, new NoSQLLanguageSecureStrategy(AsInfo));
                ps.SetListing(AsInfo);

                IParseResult res = ps.Execute();

                StoredProcedure sp = new StoredProcedure();
                sp.PrecompiledILCommand = (res as MMILCollection).ILUnits;

                _Procedure = sp;

                return (new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE]));
            }
            catch (Exception ex)
            {
                DBExeption dbEx = ex as DBExeption;
                if (dbEx != null)
                    return (_Result = new TransactionMethodResult(false, dbEx.ServerMessageError));
                else
                    return (_Result = new TransactionMethodResult(false, ex.Message));
            }
        }
    }
}
