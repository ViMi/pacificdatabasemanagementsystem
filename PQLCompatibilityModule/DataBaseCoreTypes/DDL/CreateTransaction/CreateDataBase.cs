﻿using System;
using DBMS.Core.InfoTypes;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.IO;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.Helpers;
using DBMS.PQL.Grammar;

namespace DBMS.PQL.DDL
{
    public class CreateDataBase 
        : ICreateTransaction
    {


#if DocTrue
        static CreateDataBase() { }
#endif


        private ITransactionInfo _ti;
        public System.String AsInfo { set; get; } = String.Empty;



        public CreateDataBase(System.String ownerName, System.String dataBaseName)
        {
            _ti = new TransactionInfo();
            _ti.Parameters = dataBaseName;
            _ti.UserName = ownerName;
        }

        public ITransactionMethodResult Create()
        {

            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] CREATE DATABASE {_ti.Parameters}");
            return DBMSIO.IOIntegrationModule.ExecuteCommand(DBCommand.CREATE_DATABASE, _ti);
        }
        public ITransactionMethodResult As()
            => new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE]);
    }
}
