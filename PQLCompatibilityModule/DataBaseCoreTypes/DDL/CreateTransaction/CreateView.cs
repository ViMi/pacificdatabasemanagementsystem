﻿using DBMS.Core.IO;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using System.IO;
using System;
using DBMS.Core.Interprater;
using DBMS.Core;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.MMIL;
using DBMS.Core.Helpers;

using DBMS.PQL.Grammar;

namespace DBMS.PQL.DDL
{
    internal sealed class CreateView
        : ICreateTransaction
    {

        #region Documenation
#if DocTrue
        static CreateView() { }
#endif
        #endregion



        #region Variable Declaration
        public System.String AsInfo { set; get; } = String.Empty;

        private ITransactionInfo _ti;
        private View _View {set;get;}
        private ITransactionMethodResult _Result = null;
        #endregion
        public CreateView(System.String ownerName, System.String dataBaseName, System.String viewName)
        {
            _ti = new TransactionInfo();
            _ti.UserName = ownerName;
            _ti.DataBaseName = dataBaseName;
            _ti.Parameters = viewName;

        }

        public ITransactionMethodResult Create()
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                if (_Result != null) return _Result;
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                ((Byte)PQLLangRuleID.AS_PARAMETER_UNSETED, new TransactionInfo() { Parameters = AsInfo });
                if (_Result.IsSuccess) return _Result;
                #endregion
            }



            _ti.AddedInfo = _View;
            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] CREATE VIEW {_ti.Parameters}");
            return DBMSIO.ExecuteCommand(DBCommand.CREATE_VIEW, _ti);

        }



        public ITransactionMethodResult As()
        {
            try
            {
                
                AsInfo = AsInfo.Trim()
                    .Replace(PQLGrammar.LeftParameterBracetInner.ToString(), PQLGrammar.LeftParameterBracetStd.ToString())
                    .Replace(PQLGrammar.RightParameterBracetInner.ToString(), PQLGrammar.RightParameterBracetStd.ToString())
                    .Replace(PQLGrammar.TerminalSymboInner.ToString(), PQLGrammar.TerminalSymbol.ToString())
                    .Replace(PQLGrammar.CommandsSeparatorInner.ToString(), PQLGrammar.CommandsSeparatorStd.ToString());

                IParseStrategy ps = new ViewParser(AsInfo, new NoSQLLanguageSecureStrategy(AsInfo));

                IParseResult res = ps.Execute();

                IView view = new View();
                view.PrecompiledILCommand = (res as MMILCollection).ILUnits;
                _View = view as View;
                return  new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE]);

            }
            catch (Exception ex)
            {
                DBExeption dbEx = ex as DBExeption;
                if (dbEx != null)
                    return (_Result = new TransactionMethodResult(false, dbEx.ServerMessageError));
                else
                    return (_Result = new TransactionMethodResult(false, ex.Message));
            }
        }



    }
}
