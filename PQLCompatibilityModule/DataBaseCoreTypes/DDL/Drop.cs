﻿using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.ErrorControlSystem;
using DBMS.PQL.Grammar;
using System.Collections.Generic;
using DBMS.Core.Helpers;
using System;

namespace DBMS.PQL.DDL
{
    public sealed class Drop
        : IPacificTransaction
    {


        #region Documentation
#if DocTrue
        static Drop() { }
#endif
        #endregion


        #region Variable Declaration
        public List<System.String> OperateObjectName { set; get; } = new List<System.String>();
        public System.String CommandName { set; get; } = PQLGrammar.DROP_COMMAND;
        public List<NodeType> OperateObectType { set; get; } = new List<NodeType>();
        private ITransactionMethodResult _Result = null;
        #endregion


        public void Table(ITransactionInfo ti)
        {


            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
                if (!_Result.IsSuccess) return;
                #endregion
            }

            OperateObjectName.Add(NodeType.TABLE.ToString());
            this.OperateObectType.Add(NodeType.TABLE);
            
                _Result = DBMSIO.ExecuteCommand(DBCommand.DROP_TABLE, ti);
            
        }
       
        public void Procedure(ITransactionInfo ti)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
                if (!_Result.IsSuccess) return;
                #endregion
            }


            this.OperateObectType.Add(NodeType.PROCEDURE);
                _Result = DBMSIO.ExecuteCommand(DBCommand.DROP_PROCEDURE, ti);
            OperateObjectName.Add(NodeType.PROCEDURE.ToString());

        }
      
        public void View(ITransactionInfo ti)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
                if (!_Result.IsSuccess) return;
                #endregion
            }

            this.OperateObectType.Add(NodeType.VIEW);
                _Result = DBMSIO.ExecuteCommand(DBCommand.DROP_VIEW, ti);
            OperateObjectName.Add(NodeType.VIEW.ToString());

        }
      
        public void Login(ITransactionInfo ti)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
                if (!_Result.IsSuccess) return;
                #endregion
            }

            OperateObjectName.Add(NodeType.LOGIN.ToString());
            this.OperateObectType.Add(NodeType.LOGIN);
                _Result = DBMSIO.ExecuteCommand(DBCommand.DROP_LOGIN, ti);
            
        }
     
        public void User(ITransactionInfo ti)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCkech
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
             ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
                if (!_Result.IsSuccess) return;
                #endregion
            }

            this.OperateObectType.Add(NodeType.USER);
                _Result = DBMSIO.ExecuteCommand(DBCommand.DROP_USER, ti);
            
        }
     
        public void DataBase(ITransactionInfo ti)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
               ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
                if (!_Result.IsSuccess) return;
                #endregion
            }

            OperateObjectName.Add(NodeType.DATABASE.ToString());
            this.OperateObectType.Add(NodeType.DATABASE);
                _Result = DBMSIO.ExecuteCommand(DBCommand.DROP_DATABASE, ti);
            
        }
      
        public void Trigger(ITransactionInfo ti)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
               ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
                if (!_Result.IsSuccess) return;
                #endregion
            }

            OperateObjectName.Add(NodeType.TRIGGER.ToString());
            this.OperateObectType.Add(NodeType.TRIGGER);
            _Result = DBMSIO.ExecuteCommand(DBCommand.DROP_TRIGGER, ti);
        }
        public ITransactionMethodResult StartTransaction()
        {
            DBMSIO.Logger?.AppendInfoToLog($"[DROP] {_Result.ResultString}");

            _Result.ResultString += CommandTracer.GetTracePath();
            return _Result;
        }


        public ITable EndTransaction() => null;


    }
}
