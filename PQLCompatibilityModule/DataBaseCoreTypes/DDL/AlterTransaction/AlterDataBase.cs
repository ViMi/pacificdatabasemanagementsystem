﻿using System;
using DBMS.Core.InfoTypes;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.IO;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.Core.ErrorControlSystem;

using DBMS.PQL.Grammar;

using DBMS.Core.Helpers;


namespace DBMS.PQL.DDL
{
    internal sealed class AlterDataBase
        : IAlterTransaction
    {


        #region Documentaiton
#if DocTrue
        static AlterDataBase() { }
#endif
        #endregion



        private ITransactionInfo _ti;
        public System.String AsInfo { set; get; } = String.Empty;

        public AlterDataBase(System.String ownerName, System.String dataBaseName)
        {
            _ti = new TransactionInfo();
            _ti.DataBaseName = dataBaseName;
            _ti.UserName = ownerName; 
        }

        public ITransactionMethodResult Alter()
        {


            #region RuleCheck
            ITransactionMethodResult _Result = PQLGrammar.PQLLanRulesManager.Invoke
            ((Byte)PQLLangRuleID.AS_PARAMETER_UNSETED, new TransactionInfo() { Parameters = AsInfo});
            if (_Result.IsSuccess) return _Result;
            #endregion


            _ti.AddedInfo = AsInfo;
            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] ALTER DATABASE {_ti.Parameters}");
            return DBMSIO.IOIntegrationModule.ExecuteCommand(DBCommand.ALTER_DATABASE, _ti);

        }
        public ITransactionMethodResult As()
            => new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE]);
    }
}
