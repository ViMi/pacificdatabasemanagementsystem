﻿using System;
using DBMS.Core.InfoTypes;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.IO;
using DBMS.Core.Interprater;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Helpers;

using DBMS.Core;
using DBMS.Core.MMIL;
using DBMS.PQL.Grammar;

namespace DBMS.PQL.DDL
{
    internal sealed class AlterProcedure
        : IAlterTransaction
    {

        #region Documentation
#if DocTrue
        static AlterProcedure() { }
#endif
        #endregion



        #region Variable Declartion
        private ITransactionInfo _ti;
        public System.String AsInfo { set; get; } = String.Empty;
        private ProcedureType _Type { set; get; }
        private StoredProcedure _Procedure { set; get; }
        private ITransactionMethodResult _Result = null;
        #endregion


        public AlterProcedure(System.String ownerName, System.String dataBaseName, System.String procName)
        {

            _ti = new TransactionInfo();
            _ti.UserName = ownerName;
            _ti.DataBaseName = dataBaseName;
            _ti.Parameters = procName;
        }



        public ITransactionMethodResult Alter()
        {

            #region RuleCheck
            if (_Result != null) return _Result;
            _Result = PQLGrammar.PQLLanRulesManager.Invoke
            ((Byte)PQLLangRuleID.AS_PARAMETER_UNSETED, new TransactionInfo() { Parameters = AsInfo });
            if (_Result.IsSuccess) return _Result;
            #endregion



          _ti.AddedInfo = AsInfo;
          DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] ALTER PROCEDURE {_ti.Parameters}");
         return DBMSIO.IOIntegrationModule.ExecuteCommand(DBCommand.ALTER_PROCEDURE, _ti);
            
        }


        public ITransactionMethodResult As()
        {
            try
            {
                
                AsInfo = AsInfo
                    .Trim()
                    .Replace(PQLGrammar.LeftParameterBracetInner.ToString(), PQLGrammar.LeftParameterBracetStd.ToString())
                    .Replace(PQLGrammar.RightParameterBracetInner.ToString(), PQLGrammar.RightParameterBracetStd.ToString())
                    .Replace(PQLGrammar.TerminalSymboInner.ToString(), PQLGrammar.TerminalSymbol.ToString())
                    .Replace(PQLGrammar.CommandsSeparatorInner.ToString(), PQLGrammar.CommandsSeparatorStd.ToString());

                IParseStrategy ps = new ProcedureParser(AsInfo, new NoSQLLanguageSecureStrategy(AsInfo));
                ps.SetListing(AsInfo);

                IParseResult res = ps.Execute();

                StoredProcedure sp = new StoredProcedure();
                sp.PrecompiledILCommand = (res as MMILCollection).ILUnits;

                _Procedure = sp;

                return (new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE]));
            }
            catch (Exception ex)
            {
                DBExeption dbEx = ex as DBExeption;
                if (dbEx != null)
                    return (_Result = new TransactionMethodResult(false, dbEx.ServerMessageError));
                else
                    return (_Result = new TransactionMethodResult(false, ex.Message));
            }
        }
    }

}
