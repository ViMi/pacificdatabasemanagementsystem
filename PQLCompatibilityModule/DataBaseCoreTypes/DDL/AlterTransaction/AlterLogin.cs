﻿using System;
using DBMS.Core.InfoTypes;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.IO;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.Core.ErrorControlSystem;

using DBMS.Core.Helpers;
using System.Collections.Generic;

using DBMS.Core;
using DBMS.Core.User;
using DBMS.Core.Secure;

using DBMS.PQL.Grammar;
using DBMS.PQL.ErrorControlSystem;


namespace DBMS.PQL.DDL
{
    internal sealed class AlterLogin
        : IAlterTransaction
    {

        #region Documentation
#if DocTrue
        static AlterLogin() { }
#endif
        #endregion



        #region Varialbe Definition
        private ITransactionInfo _ti;
        private String _SeparateContext = PQLGrammar.RightPackectSeparator.ToString();
        private String _Separator = PQLGrammar.RightSeparator.ToString();
        private ITransactionMethodResult _Result = null;
        public String AsInfo { get; set; } = String.Empty;

        #endregion


        public AlterLogin(System.String login, System.String userName)
        {

            _ti = new TransactionInfo();
            _ti.UserName = userName;
            _ti.Parameters = login;
        }

        
        public ITransactionMethodResult As()
        {
            String[] schemasList = AsInfo.Split(_SeparateContext[0]);
            Dictionary<System.String, NodeType> LoginRights = new Dictionary<String, NodeType>();

            System.String keyItem1 = DBMSIO
               .DBMSConfig
               .ServerName
               .Replace(
                   DBMSIO.DBMSConfig.StandartSeparator.ToString(),
                   String.Empty);


            #region RuleCheck
            _Result = PQLGrammar.PQLLanRulesManager.Invoke
                        ((Byte)PQLLangRuleID.STRING_SEPARATOR_LOGIN_CORRECT, new TransactionInfo() {Parameters = _ti.Parameters });
            if (!_Result.IsSuccess) return _Result;
            #endregion


            
            List<String> schemas = DBMSIO.GetSchemasList();

            for (Int32 i = 0; i < schemasList.Length; ++i)
            {
                String[] items = schemasList[i].Split(_Separator[0]);

                Boolean isSchemaExist = schemas != null;
                Boolean isCorrectLength = items.Length == 2;
                Boolean isContainSchems = schemas.Contains(items[0]);


                if (isCorrectLength && isSchemaExist && isContainSchems)
                {
                    NodeType type = NodeType.DATABASE;
                    type = type.NodeTypeResolverByName(items[1]);

                    LoginRights.Add(items[0], type);


                }
                else
                    return PQLGrammar.PQLLanRulesManager.Invoke
                         ((Byte)PQLLangRuleID.LOGIN_SHCME_LIST_CREATED, new TransactionInfo() { AddedInfo = _ti.AddedInfo });




            }



            if (!LoginRights.ContainsKey(keyItem1))
                LoginRights.Add(keyItem1, NodeType.ROOT_DIRECTORY);



            ILogin login = new Login();
            login.LoginRights = LoginRights;
            login.NodeName = _ti.Parameters;
            login.Owner = DBMSIO.DBMSConfig.ServerName;
            login.ChildrenList = String.Empty;
            login.NodeAccessPassword = new Password(EncriptionType.PlaneText, String.Empty);
            login.Parent = DBMSIO.DBMSConfig.BaseLoginDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty);
            login.Type = NodeType.LOGIN;



            _ti.AddedInfo = login;


            return new TransactionMethodResult(true, Core.ErrorControlSystem.ErrorMessagesCore.MessageDictionary[(Byte)Core.ErrorControlSystem.ErrorCodes.SUCCESS_EXECUTE]);



        }

        public ITransactionMethodResult Alter()
        {

            #region RuleCheck
            if (_Result != null)  return _Result;
            _Result = PQLGrammar.PQLLanRulesManager.Invoke
            ((Byte)PQLLangRuleID.LOGIN_SHCME_LIST_CREATED, new TransactionInfo() { AddedInfo = _ti.AddedInfo });
            if (!_Result.IsSuccess) return _Result;
            #endregion


            _ti.AddedInfo = AsInfo;
            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] ALTER LOGIN {_ti.Parameters}");
            return DBMSIO.ExecuteCommand(DBCommand.ALTER_LOGIN, _ti);
        }
    }
}
