﻿using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.IO;
using System;
using System.IO;
using DBMS.Core.Interprater;
using DBMS.Core.Secure.UserRights;
using System.Collections.Generic;
using System.Linq;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Helpers;

using DBMS.PQL.Grammar;

namespace DBMS.PQL.DDL
{
    internal sealed class AlterTable
        : IAlterTransaction
    {


        #region Documentation
#if DocTrue
        static AlterTable() { }
#endif
        #endregion




        #region Variable Definition
        public string AsInfo { get; set; } = String.Empty;

        private ITransactionInfo _ti;
        private ITableInfo _Table = null;
        private String _Separator = PQLGrammar.ChildrenPacketSeparator.ToString();
        private ITransactionMethodResult _Result = null;
        #endregion


        public AlterTable(System.String ownerName, System.String dataBaseName, System.String tableName)
        {
            _ti = new TransactionInfo();
            _ti.UserName = ownerName;
            _ti.DataBaseName = dataBaseName;
            _ti.Parameters = tableName;
        }

        public ITransactionMethodResult Alter()
        {

            #region RuleCheck
            if (_Result != null) return _Result;
            _Result = PQLGrammar.PQLLanRulesManager.Invoke
            ((Byte)PQLLangRuleID.AS_PARAMETER_UNSETED, new TransactionInfo() { Parameters = AsInfo });
            if (_Result.IsSuccess) return _Result;
            #endregion



                _ti.AddedInfo = _Table;
                DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] ALTER TABLE {_ti.Parameters}");
                return DBMSIO.ExecuteCommand(DBCommand.ALTER_TABLE, _ti);
            
        }


        public ITransactionMethodResult As()
        {

            try
            {
                IParseStrategy parser
                    = new TableContentParser(
                        new TableContentLanguageSecureStrategy(AsInfo),
                        AsInfo
                        );

                IExecuteStrategy executer
                    = new TableContentExecuter(
                        new TableContentExecuterSecureStrategy(),
                        new TableRecordMetaInfo()

                        );

                _Table = executer.Execute(parser.Execute()).GetExecuteResultObject() as ITableInfo;
                _Table.TableName = _ti.Parameters;
                _Table.DataBaseName = DBMSIO.GetCurrentDataBase().GetDataBaseName() ?? String.Empty;
                _Table.Parent = DBMSIO.GetCurrentDataBase().GetDataBaseName() ?? String.Empty;
                _Table.Owner = DBMSIO.GetCurrentLogin().NodeName;
                _Table.NodeName = "TABLE_META";

                _Table.ChildrenList = String.Empty;

                foreach (var item in _Table.ColumnConstraint.Keys)
                {

                    Dictionary<String, String> dict;
                    _Table.ColumnConstraint.TryGetValue(item, out dict);
                    foreach (var item2 in dict)
                    {
                        _Table.ChildrenList += item + _Separator + item2 + _Separator;
                    }

                    _Table.ChildrenList += DBMSIO.DBMSConfig.StandartSeparator;

                }

                _Table.ServerName = DBMSIO.ServerMetaInformation.ServerType.ToString();


                return new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE]);
            }
            catch (Exception ex)
            {
                DBExeption dbEx = ex as DBExeption;
                if (dbEx != null)
                    return (_Result = new TransactionMethodResult(false, dbEx.ServerMessageError));
                else
                    return (_Result = new TransactionMethodResult(false, ex.Message));
            }
        }
    }
}
