﻿using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.IO;
using System;
using System.IO;
using DBMS.Core.Interprater;
using DBMS.Core.Secure.UserRights;
using System.Collections.Generic;
using System.Linq;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Triggers;
using DBMS.Core.MMIL;
using DBMS.Core.Helpers;

using DBMS.PQL.Grammar;


namespace DBMS.PQL.DDL
{
    internal sealed class AlterTrigger
        : IAlterTransaction
    {

        #region Documentation
#if DocTrue
        static AlterTrigger() { }
#endif
        #endregion



        #region Variable Declaration
        private ITransactionInfo _ti;
        public String AsInfo { get; set; }
        private TriggerType _Type { set; get; }
        private ITrigger _Trigger { set; get; }
        private ITransactionMethodResult _Result = null;
        #endregion


        public AlterTrigger(
      System.String ownerName,
      System.String triggerName,
      System.String dataBaseName,
      TriggerType type = TriggerType.ON_DELETE)
        {
            _ti = new TransactionInfo();
            _ti.UserName = ownerName;
            _ti.DataBaseName = dataBaseName;
            _ti.Parameters = triggerName;
        }




        public ITransactionMethodResult As()
        {

            try
            {
                AsInfo = AsInfo.Trim()
                    .Replace(PQLGrammar.LeftParameterBracetInner.ToString(), PQLGrammar.LeftParameterBracetStd.ToString())
                    .Replace(PQLGrammar.RightParameterBracetInner.ToString(), PQLGrammar.RightParameterBracetStd.ToString())
                    .Replace(PQLGrammar.TerminalSymboInner.ToString(), PQLGrammar.TerminalSymbol.ToString())
                    .Replace(PQLGrammar.CommandsSeparatorInner.ToString(), PQLGrammar.CommandsSeparatorStd.ToString());


                IParseStrategy ps = new TriggerParser(AsInfo, new NoSQLLanguageSecureStrategy(AsInfo));
                ps.SetListing(AsInfo);

                IParseResult res = ps.Execute();

                Trigger trig = new Trigger();
                trig.PrecompiledILCommand = (res as MMILCollection).ILUnits;

                _Trigger = trig;


                return new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE]);

            }
            catch (Exception ex)
            {
                DBExeption dbEx = ex as DBExeption;
                if (dbEx != null)
                    return (_Result = new TransactionMethodResult(false, dbEx.ServerMessageError));
                else
                    return (_Result = new TransactionMethodResult(false, ex.Message));
            }


        }




        public ITransactionMethodResult Alter()
        {
            #region RuleCheck
            if (_Result != null) return _Result;
            _Result = PQLGrammar.PQLLanRulesManager.Invoke
            ((Byte)PQLLangRuleID.AS_PARAMETER_UNSETED, new TransactionInfo() { Parameters = AsInfo });
            if (_Result.IsSuccess) return _Result;
            #endregion



            _Trigger.TriggerType = _Type;

            _ti.AddedInfo = _Trigger;

            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] CREATE TRIGGER {_ti.Parameters}");
            return DBMSIO.IOIntegrationModule.ExecuteCommand(DBCommand.CREATE_PROCEDURE, _ti);
        }

    }
}
