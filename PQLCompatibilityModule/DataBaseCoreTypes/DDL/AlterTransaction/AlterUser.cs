﻿using System;
using DBMS.Core.InfoTypes;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.IO;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.Helpers;
using DBMS.Core;
using DBMS.Core.User;
using DMBS.Core.Secure.UserRights.RightTable;
using DBMS.PQL.Grammar;

namespace DBMS.PQL.DDL
{
    internal sealed class AlterUser
        : IAlterTransaction
    {


        #region Documentaiton
#if DocTrue
        static AlterUser() { }
#endif
        #endregion


        public System.String AsInfo { set; get; } = String.Empty;
        private ITransactionInfo _ti;


        public AlterUser(System.String userName, System.String dataBaseName, System.String ownerName)
        {

            _ti = new TransactionInfo();
            _ti.UserName = ownerName;
            _ti.DataBaseName = dataBaseName;
            _ti.Parameters = userName;
        }



        public ITransactionMethodResult Alter()
        {
            #region RuleCheck
            ITransactionMethodResult _Result = PQLGrammar.PQLLanRulesManager.Invoke
            ((Byte)PQLLangRuleID.AS_PARAMETER_UNSETED, new TransactionInfo() { Parameters = AsInfo });
            if (_Result.IsSuccess) return _Result;
            #endregion



            _ti.AddedInfo = AsInfo;

            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] ALTER USER {_ti.Parameters}");
            return DBMSIO.IOIntegrationModule.ExecuteCommand(DBCommand.ALTER_USER, _ti);

        }


        public ITransactionMethodResult As()
        {

            #region RuleCheck
            ITransactionMethodResult _Result = PQLGrammar.PQLLanRulesManager.Invoke
                        ((Byte)PQLLangRuleID.STRING_SEPARATOR_LOGIN_CORRECT, new TransactionInfo() { Parameters = _ti.Parameters });
            if (!_Result.IsSuccess) return _Result;
            #endregion


            IUserToken user = new User();

            AllowedCommandWithDB allowed = new AllowedCommandWithDB();
            allowed.AllowedCommandOn = new System.Collections.Generic.Dictionary<String, Core.Secure.UserRights.AllowedCommand>();
            allowed.AllowedCommandOn.Add(DBMSIO.GetCurrentDataBase().GetDataBaseName(), new Core.Secure.UserRights.AllowedCommand());



            user.ChildrenList = String.Empty;
            user.NodeAccessPassword = null;
            user.Type = NodeType.USER;
            user.Parent = DBMSIO.DBMSConfig.DataBasesUsersDir.Replace(DBMSIO.DBMSConfig.OSDirectoryWaySeparator, String.Empty);
            user.NodeName = _ti.Parameters;
            user.Owner = AsInfo;

            user.UserRights = new AllowedCommandWithDB();
            user.UserRights.AllowedCommandOn = new System.Collections.Generic.Dictionary<string, Core.Secure.UserRights.AllowedCommand>();
            user.UserRights.AllowedCommandOn.Add(DBMSIO.GetCurrentDataBase().GetDataBaseName(), new Core.Secure.UserRights.AllowedCommand());


            if (DBMSIO.IsLoginExist(AsInfo))
                _ti.AddedInfo = user;



            return new TransactionMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE]);
        }
    }
}
