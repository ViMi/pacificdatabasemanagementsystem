﻿using DBMS.Core.InfoTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBMS.PQL.DDL
{
    internal interface IAlterTransaction
    {

        ITransactionMethodResult Alter();
        ITransactionMethodResult As();

        System.String AsInfo { set; get; }

    }
}
