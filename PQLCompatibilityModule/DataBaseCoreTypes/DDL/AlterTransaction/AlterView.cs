﻿using System;
using DBMS.Core.InfoTypes;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.IO;
using DBMS.Core.Interprater;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core;
using DBMS.Core.MMIL;
using DBMS.PQL.Grammar;


namespace DBMS.PQL.DDL
{
    internal sealed class AlterView
       : IAlterTransaction
    {


        #region Documentation
#if DocTrue
        static AlterView() { }
#endif
        #endregion


        #region Variable Declartion
        public System.String AsInfo { set; get; } = String.Empty;

        private ITransactionMethodResult _Result = null;
        private ITransactionInfo _ti;
        private View _View { set; get; }
        #endregion


        public AlterView(System.String ownerName, System.String dataBaseName, System.String viewName)
        {

            _ti = new TransactionInfo();
            _ti.UserName = ownerName;
            _ti.DataBaseName = dataBaseName;
            _ti.Parameters = viewName;

        }

        public ITransactionMethodResult Alter()
        {

            #region RuleCheck
            if (_Result != null) return _Result;
            _Result = PQLGrammar.PQLLanRulesManager.Invoke
            ((Byte)PQLLangRuleID.AS_PARAMETER_UNSETED, new TransactionInfo() { Parameters = AsInfo });
            if (_Result.IsSuccess) return _Result;
            #endregion



                _ti.AddedInfo = AsInfo;
                DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] ALTER VIEW {_ti.Parameters}");
                return DBMSIO.ExecuteCommand(DBCommand.ALTER_VIEW, _ti);
            
        }


        public ITransactionMethodResult As()
        {
            try
            {
                //Добавить стратегию защиты не поддерживающую ничего кроме Select
                AsInfo = AsInfo
                    .Trim()
                    .Replace(PQLGrammar.LeftParameterBracetInner.ToString(), PQLGrammar.LeftParameterBracetStd.ToString())
                    .Replace(PQLGrammar.RightParameterBracetInner.ToString(), PQLGrammar.RightParameterBracetStd.ToString())
                    .Replace(PQLGrammar.TerminalSymboInner.ToString(), PQLGrammar.TerminalSymbol.ToString())
                    .Replace(PQLGrammar.CommandsSeparatorInner.ToString(), PQLGrammar.CommandsSeparatorStd.ToString());


                IParseStrategy ps = new ViewParser(AsInfo, new NoSQLLanguageSecureStrategy(AsInfo));

                IParseResult res = ps.Execute();

                IView view = new View();
                view.PrecompiledILCommand = (res as MMILCollection).ILUnits;
                _View = view as View;
                return new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE]);

            }
            catch (Exception ex)
            {
                DBExeption dbEx = ex as DBExeption;
                if (dbEx != null)
                    return (_Result = new TransactionMethodResult(false, dbEx.ServerMessageError));
                else
                    return (_Result = new TransactionMethodResult(false, ex.Message));
            }
        }


    }
}
