﻿using System;
using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.IO;
using System.Linq;
using DBMS.PQL.Grammar;
using System.Collections.Generic;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Helpers;



namespace DBMS.PQL.DDL
{


    public sealed class Create
        : IPacificTransaction
    {


        #region Documentation
#if DocTrue
        static Create() { }
#endif
        #endregion



        #region Variable Declaration
        public List<System.String> OperateObjectName { set; get; } = new List<System.String>();
        public System.String CommandName { set; get; } = PQLGrammar.CREATE_COMMAND;
        public List<NodeType> OperateObectType { set; get; } = new List<NodeType>();

        private ITransactionMethodResult _Result = null;
        private ICreateTransaction _CommandType;
        #endregion

        public void DataBase(ITransactionInfo ti) 
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RUleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                   ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
                if (!_Result.IsSuccess) return;
                #endregion
            }

            OperateObectType.Add(NodeType.DATABASE);
            OperateObjectName.Add(NodeType.DATABASE.ToString());
            _CommandType = new CreateDataBase(ti.UserName, ti.Parameters);
                 
        }

        public void Table(ITransactionInfo ti)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                   ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
                if (!_Result.IsSuccess) return;
                #endregion
            }

            OperateObectType.Add(NodeType.TABLE);
            OperateObjectName.Add(NodeType.TABLE.ToString());
            _CommandType = new CreateTable(ti.UserName, ti.DataBaseName, ti.Parameters);
        }

        public void TriggerOnUpdate(ITransactionInfo ti)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleChekc
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                   ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
                if (!_Result.IsSuccess) return;
                #endregion
            }

            OperateObectType.Add(NodeType.UPDATE_TRIGGER);
            OperateObjectName.Add(NodeType.TRIGGER.ToString());
            _CommandType = new CreateTrigger(ti.UserName, ti.Parameters, ti.DataBaseName, Core.Triggers.TriggerType.ON_UPDATE);

        }

        public void TriggerOnInsert(ITransactionInfo ti)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleChekc
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
               ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
                if (!_Result.IsSuccess) return;
                #endregion

            }


            OperateObectType.Add(NodeType.INSERT_TRIGGER);
            OperateObjectName.Add(NodeType.TRIGGER.ToString());
            _CommandType = new CreateTrigger(ti.UserName, ti.Parameters, ti.DataBaseName, Core.Triggers.TriggerType.ON_INSERT);

        }

        public void TriggerOnDelete(ITransactionInfo ti)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                 ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
                if (!_Result.IsSuccess) return;
                #endregion
            }

            OperateObectType.Add(NodeType.DELETE_TRIGGER);
            OperateObjectName.Add(NodeType.TRIGGER.ToString());
            _CommandType = new CreateTrigger(ti.UserName, ti.Parameters, ti.DataBaseName, Core.Triggers.TriggerType.ON_DELETE);

        }

        public void Procedure(ITransactionInfo ti)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCHeck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
               ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
                if (!_Result.IsSuccess) return;
                #endregion
            }


            OperateObectType.Add(NodeType.PROCEDURE);
            OperateObjectName.Add(NodeType.PROCEDURE.ToString());
            _CommandType = new CreateProcedure(ti.UserName, ti.DataBaseName, ti.Parameters);
        }

        public void View(ITransactionInfo ti)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCehck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                 ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
                if (!_Result.IsSuccess) return;
                #endregion
            }


            OperateObectType.Add(NodeType.VIEW);
            OperateObjectName.Add(NodeType.VIEW.ToString());
            _CommandType = new CreateView(ti.UserName, ti.DataBaseName, ti.Parameters);
        }

        public void User(ITransactionInfo ti)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                  ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
                if (!_Result.IsSuccess) return;
                #endregion
            }

            OperateObectType.Add(NodeType.USER);
            OperateObjectName.Add(NodeType.USER.ToString());
            _CommandType = new CreateUser(ti.Parameters, ti.DataBaseName, ti.UserName);
        }
     
        public void Login(ITransactionInfo ti)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                 ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                _Result = PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
                if (!_Result.IsSuccess) return;
                #endregion
            }

            OperateObectType.Add(NodeType.LOGIN);
            OperateObjectName.Add(NodeType.LOGIN.ToString());
            _CommandType = new CreateLogin(ti.Parameters, ti.UserName);
        }

        public void As(ITransactionInfo ti)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                 ((Byte)PQLLangRuleID.AS_PARAMETER_MULTIPLY_SETTED, new TransactionInfo() { Parameters = _CommandType.AsInfo });
                if (_Result.IsSuccess) return;
                #endregion
            }

            _CommandType.AsInfo = ti.Parameters;

        }

        public void To(ITransactionInfo ti)
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                if (this._CommandType == null) throw new DBExeption("Error Not Setted Create Type");
                if ((this._CommandType as CreateTrigger) == null) throw new DBExeption("Error that option available only to Triggers");
                if (!DBMSIO.GetCurrentDataBase().IsTableExist(ti.Parameters)) return;
                #endregion
            }

            CreateTrigger trig = this._CommandType as CreateTrigger;
            trig.ToTable = ti.Parameters;

        }

        public ITransactionMethodResult StartTransaction()
        {


            _CommandType.As();
            ITransactionMethodResult res = _CommandType.Create();
            res.ResultString += CommandTracer.GetTracePath();
            return res;

        }




        public ITable EndTransaction() => null;

    }


}
