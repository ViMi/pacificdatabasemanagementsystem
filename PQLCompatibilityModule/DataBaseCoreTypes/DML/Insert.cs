﻿using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using System;
using DBMS.Core.MMIL;
using System.Collections.Generic;
using System.Text;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.PQL.Grammar;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.Helpers;
using DBMS.Core.Helpers.Tests;

namespace DBMS.PQL.DML
{
    public sealed class Insert 
        : IPacificTransaction
    {

        #region Documentaiton
#if DocTrue
        static Insert() { }
#endif
        #endregion


        #region Variable Declaration
        public List<System.String> OperateObjectName { set; get; } = new List<System.String>();
        public System.String CommandName { set; get; } = PQLGrammar.INSERT_COMMAND;
        public List<NodeType> OperateObectType { set; get; } = new List<NodeType>() { NodeType.TABLE};

        private ITransactionMethodResult _Result = null;

        private ITransactionInfo _ti = new TransactionInfo();
        private MMILDataManipulationContext _Context;

        private List<String> _ValuesEntrie;
        #endregion

        public Insert()
        {
            _ValuesEntrie = new List<System.String>();
            _Context = new MMILDataManipulationContext();
            _Context.InitExecutionContext(CommandName);
        }


        public void Into(ITransactionInfo ti)
        {
            _ti.UserName = ti.UserName;
            _ti.DataBaseName = ti.DataBaseName;
            _ti.Parameters = ti.Parameters;

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
               ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                #endregion
            }

            OperateObjectName.Add(ti.Parameters);
            _Context.SetTo(ti.Parameters);
        }

        public void Values(ITransactionInfo ti) => _ValuesEntrie.Add(ti.Parameters);

        public ITransactionMethodResult StartTransaction()
        {

            _Context.EndExecuteContext();

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                if (_Result != null && _Result.IsSuccess) return _Result;
                #endregion
            }

            _ti.AddedInfo = _Context;
            _ti.AddedInfoFragmentTwo = _ValuesEntrie;
            _ti.DataBaseName = DBMSIO.GetCurrentDataBase().GetDataBaseName();

            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] INSERT {_ti.Parameters}");

            ITransactionMethodResult res = DBMSIO.ExecuteCommand(DBCommand.INSERT, _ti);
            res.ResultString += CommandTracer.GetTracePath();
       
            return res;

        }



        public void IntoGroup(ITransactionInfo ti) { }

        public ITable EndTransaction() => null;
    }

}
