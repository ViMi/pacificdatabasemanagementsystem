﻿using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.MMIL;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.PQL.Grammar;
using System.Collections.Generic;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.Helpers;
using System;
using DBMS.Core.Helpers.Tests;

namespace DBMS.PQL.DML
{
    public sealed class Update 
        : IPacificTransaction
    {


        #region Documentation
#if DocTrue
        static Update() { }
#endif
        #endregion



        #region Variable Declaration
        public List<System.String> OperateObjectName { set; get; } = new List<System.String>();
        public System.String CommandName { set; get; } = PQLGrammar.UPDATE_COMMAND;
        public List<NodeType> OperateObectType { set; get; } = new List<NodeType>();

        private ITransactionMethodResult _Result = null;
        private ITransactionInfo _ti = new TransactionInfo();
        private MMILDataManipulationContext _Context;
        #endregion


        public Update() => (_Context = new MMILDataManipulationContext()).InitExecutionContext(CommandName);

        public void From(ITransactionInfo ti)
        {
            _Context.SetFrom(ti.Parameters);

            this.OperateObjectName.Add(ti.Parameters);

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
               ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                #endregion
            }


        }
        public void Where(ITransactionInfo ti) =>_Context.SetWhere(ti.Parameters);
        public void Rows(ITransactionInfo ti) => _Context.SetRows((_ti.UserName = ti.Parameters));
        public void Set(ITransactionInfo ti)  => _Context.SetValues(ti.Parameters);

        public void FromGroup(ITransactionInfo ti) { }

        public ITable EndTransaction() => null;

        public ITransactionMethodResult StartTransaction()
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                if (_Result != null && _Result.IsSuccess) return _Result;
                #endregion
            }

            _Context.EndExecuteContext();
            _ti.AddedInfo = _Context;

            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] UPDATE {_ti.Parameters}");

            TestTimer.InitNewTimer();
            ITransactionMethodResult res = DBMSIO.ExecuteCommand(DBCommand.UPDATE, _ti);
            TestTimer.StopTimer();
            res.ResultString += CommandTracer.GetTracePath();
            TimeSpan resSpan = TestTimer.GetResultTime();
            res.ResultString += $"UPDATE {Environment.NewLine}Time Passed -->  ml:{resSpan.Milliseconds}  sec:{resSpan.Seconds}  min:{resSpan.Minutes} hr:{resSpan.Hours} {Environment.NewLine}";


            return res;


        }
    }
}
