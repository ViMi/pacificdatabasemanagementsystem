﻿using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.MMIL;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.PQL.Grammar;
using System.Collections.Generic;
using DBMS.Core.Helpers;
using DBMS.Core.Helpers.Tests;
using System;

namespace DBMS.PQL.DML
{ 
    public sealed class Delete 
        : IPacificTransaction
    {


        #region Documentation
#if DocTrue
        static Delete() { }
#endif
        #endregion


        #region Variable Definition
        public List<System.String> OperateObjectName { set; get; } = new List<System.String>();
        public System.String CommandName { set; get; } = PQLGrammar.DELETE_COMMAND;
        public List<NodeType> OperateObectType { set; get; } = new List<NodeType>() { NodeType.TABLE };

        private MMILDataManipulationContext _Context;

        private ITransactionInfo _ti = new TransactionInfo();

        #endregion

        public Delete() => (_Context = new MMILDataManipulationContext()).InitExecutionContext(CommandName);

        public void Rows(ITransactionInfo ti)
        {
            _ti.UserName = ti.UserName;
            _ti.DataBaseName = ti.DataBaseName;
            _Context.SetRows(ti.Parameters);
        }


        public void From(ITransactionInfo ti)
        {
            this.OperateObjectName.Add(ti.Parameters);
            _Context.SetFrom(ti.Parameters);

        }


        public void FromGroup(ITransactionInfo ti) { }

        public void Where(ITransactionInfo ti) => _Context.SetWhere(ti.Parameters);

        public ITransactionMethodResult StartTransaction()
        {

            _Context.EndExecuteContext();
            _ti.AddedInfo = _Context;

            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] DELETE {_ti.Parameters}");

            TestTimer.InitNewTimer();
            ITransactionMethodResult res = DBMSIO.ExecuteCommand(DBCommand.DELETE, _ti);
            TestTimer.StopTimer();
            res.ResultString += CommandTracer.GetTracePath();
            TimeSpan resSpan = TestTimer.GetResultTime();
            res.ResultString += $"DELETE {Environment.NewLine}Time Passed -->  ml:{resSpan.Milliseconds}  sec:{resSpan.Seconds}  min:{resSpan.Minutes} hr:{resSpan.Hours} {Environment.NewLine}";

            return res;

        }

        public ITable EndTransaction() => null;




    }
}
