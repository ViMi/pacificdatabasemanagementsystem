﻿using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.IO;
using System;
using System.Linq;
using System.Collections.Generic;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.MMIL;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.PQL.Grammar;
using DBMS.Core.Helpers;

using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Helpers.Tests;


namespace DBMS.PQL.DML
{
    public sealed class Select 
        : IPacificTransaction
    {

        #region Documentation
#if DocTrue
        static Select() { }
#endif
        #endregion


        #region Variable Definition
        public List<System.String> OperateObjectName { set; get; } = new List<System.String>();
        public System.String CommandName { set; get; } = PQLGrammar.SELECT_COMMAND;
        public List<NodeType> OperateObectType { set; get; } =  new List<NodeType>();

        private ITransactionInfo _ti = new TransactionInfo();
        private MMILDataManipulationContext _Context;
        #endregion


        
        public Select() => (_Context = new MMILDataManipulationContext()).InitExecutionContext(CommandName);




        public void From(ITransactionInfo ti)
        { 
            _Context.SetFrom(ti.Parameters);

            if (DBMSIO.GetCurrentDataBase() == null)
                throw new DataBaseNotExist();

            String[] from = 
                ti.Parameters.Split(DBMSIO.DBMSConfig.StandartSeparator);

            foreach(String item in from) {


                if (DBMSIO.GetCurrentDataBase().IsViewExist(item))
                    OperateObectType.Add(NodeType.VIEW);
                else if (DBMSIO.GetCurrentDataBase().IsTableExist(item))
                    OperateObectType.Add(NodeType.TABLE);
                  
                

                OperateObjectName.Add(item);
            }

         
        }


        #region OneLine Methods
        public void Distinct(ITransactionInfo ti) => _Context.SetDistinct(ti.Parameters);
        public void Where(ITransactionInfo ti)  => _Context.SetWhere(ti.Parameters);
        public void GroupBy(ITransactionInfo ti) => _Context.SetGroupBy(ti.Parameters);
        public void Join(ITransactionInfo ti) => _Context.SetJoin(ti.Parameters);
        public void Having(ITransactionInfo ti) => _Context.SetHaving(ti.Parameters);
        public void OrderBy(ITransactionInfo ti) => _Context.SetOrderBy(ti.Parameters);
        public void Rows(ITransactionInfo ti) => _Context.SetRows(ti.Parameters);
        public void Max(ITransactionInfo ti) => _Context.SetMax(ti.Parameters);
        public void Min(ITransactionInfo ti) => _Context.SetMin(ti.Parameters);
        public void Count(ITransactionInfo ti) => _Context.SetCount(ti.Parameters);
        public void Avg(ITransactionInfo ti) => _Context.SetAvg(ti.Parameters);
        public void Sum(ITransactionInfo ti) => _Context.SetSum(ti.Parameters);
        public void GetTypes(ITransactionInfo ti) => _Context.SetGetType(ti.Parameters);
        public void GetDomains(ITransactionInfo ti) => _Context.SetgetDomain(ti.Parameters);
        public void CrossJoin(ITransactionInfo ti) => _Context.SetJoin(ti.Parameters);
        public void Union(ITransactionInfo ti) => _Context.SetUnion(ti.Parameters);
        public void GetBounded(ITransactionInfo ti) => _Context.SetBounded(ti.Parameters);
        public void Reverse(ITransactionInfo ti) => _Context.SetReverse(ti.Parameters);
        public void OrderByDesc(ITransactionInfo ti) => _Context.SetOrderByDesc(ti.Parameters);
        public void OrderByAsc(ITransactionInfo ti) => _Context.SetOrderByAsc(ti.Parameters);
        #endregion


        #region Limitation Methods
        public void Skip(ITransactionInfo ti)
        {
            Int32 resInt;
            if (Int32.TryParse(ti.Parameters, out resInt)) 
                _Context.SetSkip(ti.Parameters); 
            else 
                throw new Exception();
            

        }

        public void Limit(ITransactionInfo ti)
        {
            Int32 resInt;
            if (Int32.TryParse(ti.Parameters, out resInt))
                _Context.SetLimit(ti.Parameters);
            
            else
                throw new Exception();
            
        }
   
        public void Top(ITransactionInfo ti)
        {

            Int32 resInt;
            if (Int32.TryParse(ti.Parameters, out resInt))
                _Context.SetTop(ti.Parameters);
            else
                throw new Exception();
            

        }
        #endregion


        public ITable EndTransaction() => null;

        public ITransactionMethodResult StartTransaction()
        {

            _Context.EndExecuteContext();
            _ti.AddedInfo = _Context;

            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] SELECT {_ti.Parameters}");

            TestTimer.InitNewTimer();
            ITransactionMethodResult res = DBMSIO.ExecuteCommand(DBCommand.SELECT, _ti);
            TestTimer.StopTimer();
            res.ResultString += CommandTracer.GetTracePath();
            TimeSpan resSpan = TestTimer.GetResultTime();
            res.ResultString += $"SELECT {Environment.NewLine}Time Passed -->  ml:{resSpan.Milliseconds}  sec:{resSpan.Seconds}  min:{resSpan.Minutes} hr:{resSpan.Hours} {Environment.NewLine}";

            return res;


        }
    }

}
