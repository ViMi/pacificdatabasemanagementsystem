﻿using DBMS.Core;
using DBMS.Core.Interprater;
using DBMS.Core.Interprater.SecureStrategy;
using DBMS.Core.IO;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.MMIL;
using DBMS.Core.MMIL.ILConfiguration;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Helpers;

using DBMS.PQL.Grammar;

namespace DBMS.PQL
{
   internal sealed class RightExecuter
        : IExecuteStrategy
    {

        #region Documentation
#if DocTrue
        static RightExecuter() { }
#endif
        #endregion



        private IDBNoSQLExecuteSecureStrategy _SecureStrategy;
        private IExecuteResult _ExecuteResult;

        public RightExecuter(IDBNoSQLExecuteSecureStrategy secureStrategy, IExecuteResult executeResult)
        {
            _SecureStrategy = secureStrategy;
            _ExecuteResult = executeResult;

        }



        public Object Clone()=> new TableContentExecuter(null, null);



        public IExecuteResult Execute(IParseResult parseResult)
        {
            List<MMILDataBaseContext> units
             = parseResult.GetTranslationUnits();


            AllowedCommand allowed = new AllowedCommand(new Dictionary<String, CommandRight>(1));

            for (System.Int32 i = 0; i < units.Count; ++i)
                _Execute(units.ElementAt(i), allowed);

            

            return (_ExecuteResult = allowed);

        }



        private void _Execute(IMMILTranslationUnit unit,  in AllowedCommand allowed)
        {

            Dictionary<System.String, System.String> dic = unit.GetILDictionary();
            System.String command;
            System.String on;

            dic.TryGetValue(MMILDataControlMapConfiguration.OnObjectDeclarationCommand, out on);
            dic.TryGetValue(MMILDataControlMapConfiguration.AllowedCommandDeclarationCommand, out command);



             CommandRight rights;

            if(allowed.AllowCommand.TryGetValue(command, out rights))
                rights.CommandRights.Add(on, true);
            else
            {
                allowed.AllowCommand.Add(command,
                       new CommandRight(
                                new Dictionary<String, Boolean>(1)
                                {
                                    [on] = true
                                }
                            )
                       );
            }

        }



    }
}
