﻿using DBMS.Core;
using DBMS.Core.MMIL;
using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.Helpers;

using DBMS.PQL.Grammar;

namespace DBMS.PQL
{
    internal sealed class RightSecureStrategy
         : IParserSecureStrategy
    {

        #region Documentaiton
#if DocTrue
        static RightSecureStrategy() { }
#endif
        #endregion


        public Boolean Check(System.String param) => PQLGrammar.AllCommands.Contains(param);
        public Boolean Check(List<System.String> param) { throw new NotImplementedException(); }
        public Boolean Check(Dictionary<System.String, System.String> param) { throw new NotImplementedException(); }
        public Boolean Check(IMMILTranslationUnit param) { throw new NotImplementedException(); }
        public Boolean Check(List<IMMILTranslationUnit> param) { throw new NotImplementedException(); }
    }
}
