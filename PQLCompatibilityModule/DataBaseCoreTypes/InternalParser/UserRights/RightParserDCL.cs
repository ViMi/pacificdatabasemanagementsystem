﻿using DBMS.Core;
using DBMS.Core.Interprater;
using DBMS.Core.Interprater.SecureStrategy;
using DBMS.Core.IO;
using DBMS.Core.MMIL;
using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Helpers;

using DBMS.PQL.Grammar;

namespace DBMS.PQL
{
    internal sealed class RightParserDCL
            : IParseStrategy
    {

        #region Documentaiton
#if DocTrue
        static RightParserDCL() { }
#endif
        #endregion



        #region Value Definition
        private System.String _Listing = System.String.Empty;
        private IParserSecureStrategy _SecureStrategy = new RightSecureStrategy();
        private ILanguageSecureStrategy _LanguageSecureStrategy = null;

        NoSQlExecuteSecureStrategy _ExStrategy = new NoSQlExecuteSecureStrategy();

        private Char _RightSeparator = PQLGrammar.RightSeparator;
        private Char _RightPacketSeparator = PQLGrammar.RightPackectSeparator;
#endregion


        public RightParserDCL
            (System.String listing, 
            ILanguageSecureStrategy languageSecStrategy,
            IParserSecureStrategy secureStrategy)
        {
            this._Listing = listing;
            this._LanguageSecureStrategy = languageSecStrategy;

        }



        public IParseResult Execute()
        {

            MMILCollection parseResultCollection = new MMILCollection();


            System.String[] content = _Listing.Split(_RightPacketSeparator, (Char)StringSplitOptions.RemoveEmptyEntries);
            System.Int32 commandCount = content.Length;


            for (System.Int16 i = 0; i < commandCount; ++i)
            {
                if (content[i] == String.Empty)
                    continue;

                MMILDataControlMap controlMap = new MMILDataControlMap();

                controlMap.InitExecutionContext(controlMap.StandartContextName);
                System.String[] rightContext = content[i].Split(_RightSeparator);


                if (rightContext.Length > 3)
                    throw new ValueError();

                if (!PQLGrammar.hashSet.Contains(rightContext[2]))
                    throw new SyntaxError();

                if (!_SecureStrategy.Check(rightContext[0]))
                    throw new TypeError();

                Resolve(rightContext[2], rightContext[0], rightContext[1]);


                controlMap.SetOnObject(rightContext[1]);
                controlMap.SetRights(rightContext[0]);

                controlMap.EndExecuteContext();

                parseResultCollection.ILUnits.Add(controlMap);



            }


            return parseResultCollection;
        }





        public void SetListing(String listing) => _Listing = listing;
        public Object Clone() => new ViewParser(_Listing, _LanguageSecureStrategy);




      
          
        private void Resolve(String type, String command, String context)
        {
         
            
            NodeType nt = NodeType.BACKUP;
               nt = PQLGrammar.ResolveType(type);

            if (!_ExStrategy.UserCanExecuteCommand(command, new List<String>() { context }, new List<NodeType>() { nt }) 
                && !_ExStrategy.UserCanExecuteCommand(PQL.Grammar.PQLGrammar.GetCommandRbacComfirmity(command), new List<String>() { context }, new List<NodeType>() { nt }))
                throw new RightsError();

            if ((command == PQLGrammar.SELECT_COMMAND 
                || command == PQLGrammar.UPDATE_COMMAND 
                || command == PQLGrammar.DELETE_COMMAND
                || command == PQLGrammar.INSERT_COMMAND)
                && (type == PQLGrammar.TABLE_TYPE || type == PQLGrammar.VIEW_TYPE))
            {
                if (DBMSIO.GetCurrentDataBase().IsTableExist(context))
                    return;

                if (DBMSIO.GetCurrentDataBase().IsViewExist(context))
                    return;

                throw new SyntaxError();
            }

            if((command == PQLGrammar.BACKUP_COMMAND
                || command == PQLGrammar.MIGRATE_COMMAND) && type == PQLGrammar.DATABASE_TYPE)
            {
                if (DBMSIO.IsDataBaseExist(context))
                    return;

                throw new SyntaxError();
            }

            if ((command == PQLGrammar.EXECUTE_COMMAND) && type == PQLGrammar.PROCEDURE_TYPE)
            {
                if (DBMSIO.GetCurrentDataBase().IsProcedureExist(context))
                    return;

                throw new SyntaxError();
            }

            if ((command == PQLGrammar.CREATE_COMMAND
                || command == PQLGrammar.DROP_COMMAND))
            {
                if(type == PQLGrammar.OBJ_TYPE)
                return;


                if ((type == PQLGrammar.TABLE_TYPE 
                    || type == PQLGrammar.USER_TYPE 
                    || type == PQLGrammar.VIEW_TYPE 
                    || type == PQLGrammar.PROCEDURE_TYPE
                    || type == PQLGrammar.TRIGGER_TYPE 
                    || type == PQLGrammar.DATABASE_TYPE 
                    || type == PQLGrammar.LOGIN_TYPE))
                    return;


            }

            if ((command == PQLGrammar.GRANT_COMMAND
                || command == PQLGrammar.DENY_COMMAND) && (type == PQLGrammar.USER_TYPE || type == PQLGrammar.LOGIN_TYPE))
            {
                return;

                throw new SyntaxError();
            }

            throw new SyntaxError();
        }


    }





}
