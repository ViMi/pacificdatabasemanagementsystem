﻿using DBMS.Core.Interprater;
using DBMS.Core.Table;
using System;
using System.Collections.Generic;
using DBMS.Core.Helpers;

using DBMS.Core.InfoTypes;


namespace DBMS.PQL
{
    internal sealed class TableCollection
        : IExecuteResult
    {

        #region Documentation
#if DocTrue
        static TableCollection() { }
#endif
        #endregion


        private List<ITable> _Tables { set; get; }
        public ITransactionMethodResult _CurrentTI { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public void Reset() => _Tables = null;
        public void Append(System.String value) { throw new NotImplementedException(); }

        public void Append(System.Object obj) => _Tables.Add(obj as ITable);

        public Object Clone()=> new TableCollection();

        public System.String GetExecuteResult() { throw new NotImplementedException(); }


        public Object GetExecuteResultObject() => _Tables;

        public void SetExecuteResult(System.String executeResult) {throw new NotImplementedException();}

        public void SetExecuteResult(Object executeResult)=>_Tables = executeResult as List<ITable>;
        
    }
}
