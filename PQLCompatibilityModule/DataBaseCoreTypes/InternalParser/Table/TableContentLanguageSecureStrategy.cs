﻿using DBMS.Core.Interprater.SecureStrategy;
using DBMS.Core.IO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using DBMS.Core.Helpers;

using DBMS.PQL.Grammar;

namespace DBMS.PQL
{
    internal sealed class TableContentLanguageSecureStrategy
        : ILanguageSecureStrategy
    {

        #region Documentaiton
#if DocTrue
        static TableContentLanguageSecureStrategy() { }
#endif
        #endregion



        
        private System.String _Listing { set; get; }

        public TableContentLanguageSecureStrategy(System.String listing) => _Listing = listing;

        public Object Clone(){ throw new NotImplementedException(); }


        public System.String GetClassNameFromString(System.String commandStirng) => String.Empty;

        public Dictionary<System.String, HashSet<System.String>> GetCommandDictionary(){ throw new NotImplementedException();}

        public System.String GetMethodNameFromString(System.String commandString) {throw new NotImplementedException();}

        public System.String GetParameterFromString(System.String commandString){throw new NotImplementedException();}

        public Boolean IsAbstractCommand(System.String command) => PQLGrammar._AbstractAttribute.Contains(command);

        public Boolean IsConcreteCommand(System.String command, System.String iddleCommand) => PQLGrammar._ConcreteAttributes.Contains(command);
        public System.String PrepareCommandToTranslation(System.String commandString) {  throw new NotImplementedException();  }

        public System.String[] PrepareContextToTranslation(System.String context) 
            => context.Split(DBMSIO.DBMSConfig.StandartSeparator, (Char)StringSplitOptions.RemoveEmptyEntries);
        public System.String[] PrepareListingToTranslation()
        {
            _Listing = Regex.Replace(_Listing, @"\s+", " ");
            return _Listing.Trim().Split(PQLGrammar.TableDefinitionColumnSeparator, (Char)StringSplitOptions.RemoveEmptyEntries);
            
        }
        public void RemoveOneLineComment(System.String startToken) { }

        public System.Boolean IsType(System.String type) => PQLGrammar._Types.Contains(type);
        public System.Boolean IsAttribute(System.String attribute) => attribute.Contains(attribute);


    }
}
