﻿using DBMS.Core.Interprater;
using DBMS.Core.Interprater.SecureStrategy;
using DBMS.Core.IO;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.MMIL;
using DBMS.Core.Secure.UserRights;
using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Helpers;
using DBMS.Core.UsersFromRoles;
using DBMS.PQL.Grammar;

using DBMS.PQL.DDL;

namespace DBMS.PQL
{
    internal sealed class TableContentParser
        : IParseStrategy
    {

        #region Documentation
#if DocTrue
        static TableContentParser() {}
#endif
        #endregion


        private ILanguageSecureStrategy _LanguageSecureStr = null;
        private System.String _Listing = System.String.Empty;

        public TableContentParser(ILanguageSecureStrategy secureStr, System.String listing)
        {
            this._LanguageSecureStr = secureStr;
            this._Listing = listing;
        }


            
        public Object Clone() => new TableContentParser(_LanguageSecureStr.Clone() as ILanguageSecureStrategy, this._Listing);

        public IParseResult Execute()
        {


            MMILCollection collection = new MMILCollection();



            System.String[] records = 
                _LanguageSecureStr.PrepareListingToTranslation();
            List<System.String> aliasList = new List<System.String>();

            for(System.Int32 i = 0; i < records.Length; ++i)
            {
                MMILDataBaseTableMap map = new MMILDataBaseTableMap();


                map.InitExecutionContext(map.StandartContextName);
                map.SetServer(DBMSIO.ServerMetaInformation.ServerType.ToString());


                if (records[i] == String.Empty)
                    continue;


                System.String[] context
                    = _LanguageSecureStr.PrepareContextToTranslation(records[i].Trim());

                if (_LanguageSecureStr.IsConcreteCommand(context[0], String.Empty) || _LanguageSecureStr.IsAbstractCommand(context[0]))
                    throw new DataMappingError();

                if (context[0].Replace(PQLGrammar.TableContentSeparator.ToString(), String.Empty).Length == 0)
                    throw new DataMappingError();

                if (CreateTable.TableName == null)
                    throw new TableNotExist();

                if (aliasList.Contains(CreateTable.TableName + PQLGrammar.ShemasSeparator + context[0]))
                    throw new MultiplyUpdateTableError();

                if (!PQLGrammar.PQLLanRulesManager.Invoke((Int32)PQLLangRuleID.RIGHT_NAME, new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = context[0] }).IsSuccess)
                    throw new SyntaxError();

                map.SetAlias(CreateTable.TableName + PQLGrammar.ShemasSeparator + context[0]);

                if (!_LanguageSecureStr.IsType(context[1]))
                    throw new DataMappingError();

                map.SetDataType(context[1]);

                System.String attributesNames = System.String.Empty;
                System.String attributesValues = System.String.Empty;

                for (System.Int32 j = 2; j < context.Length; ++j)
                {
                    if (_LanguageSecureStr.IsConcreteCommand(context[j], String.Empty))
                    {
                        attributesNames += context[j] + DBMSIO.DBMSConfig.StandartSeparator;
                        attributesValues += context[j + 1] + DBMSIO.DBMSConfig.StandartSeparator;
                        j += 1;
                    } else if(_LanguageSecureStr.IsAbstractCommand(context[j]))
                    {  
                        attributesNames += context[j] + DBMSIO.DBMSConfig.StandartSeparator;
                        attributesValues += Rbac.DATA_DEFAULT + DBMSIO.DBMSConfig.StandartSeparator;
                    } else
                        throw new DataMappingError();
                    
                }

                map.SetAttribute(attributesNames);
                map.SetAttributeValue(attributesValues);

                map.EndExecuteContext();

                collection.ILUnits.Add(map);

            }


            return collection;

        }

        public void SetListing(System.String listing) => this._Listing = listing;
    }
}
