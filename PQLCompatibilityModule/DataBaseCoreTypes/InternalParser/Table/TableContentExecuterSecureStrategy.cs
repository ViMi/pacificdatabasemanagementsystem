﻿using DBMS.Core;
using DBMS.Core.Interprater.SecureStrategy;
using DBMS.Core.MMIL;
using DBMS.Core.MMIL.ILConfiguration;
using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Helpers;



namespace DBMS.PQL
{
    internal sealed class TableContentExecuterSecureStrategy
        : IDBNoSQLExecuteSecureStrategy
    {


        #region Documentation
#if DocTrue
        static TableContentExecuterSecureStrategy() { }
#endif
        #endregion


        public Boolean CheckExecuteMainRule() { throw new NotImplementedException(); }

        public Boolean CheckSecureRule(Dictionary<System.String, System.String> IntermediateLanguageContent)
        {
            if (!IntermediateLanguageContent.TryGetValue(MMILDataBaseTableMappConfig.AliasDeclarationCommand, out _))
                throw new ExecutorControlSecurityError();

            if (!IntermediateLanguageContent.TryGetValue(MMILDataBaseTableMappConfig.AttributesDecalarationCommand, out _))
                throw new ExecutorControlSecurityError();

            if (!IntermediateLanguageContent.TryGetValue(MMILDataBaseTableMappConfig.AttributeValueDeclarationCommand, out _))
                throw new ExecutorControlSecurityError();

            if (!IntermediateLanguageContent.TryGetValue(MMILDataBaseTableMappConfig.DataTypeDeclarationCommand, out _))
                throw new ExecutorControlSecurityError();

            return true;

        }

        public Boolean CheckSecureRule(IMMILTranslationUnit translationUnit)
        {
            Dictionary<System.String, System.String> commandDic 
                = translationUnit.GetILDictionary();

            if (!commandDic.TryGetValue(MMILDataBaseTableMappConfig.AliasDeclarationCommand, out _))
                throw new ExecutorControlSecurityError();

            if (!commandDic.TryGetValue(MMILDataBaseTableMappConfig.AttributesDecalarationCommand, out _))
                throw new ExecutorControlSecurityError();

            if (!commandDic.TryGetValue(MMILDataBaseTableMappConfig.AttributeValueDeclarationCommand, out _))
                throw new ExecutorControlSecurityError();

            if (!commandDic.TryGetValue(MMILDataBaseTableMappConfig.DataTypeDeclarationCommand, out _))
                throw new ExecutorControlSecurityError();

            return true;

        }

        public Boolean CkeckTableUserRight(IUserToken user, System.String tables, System.String commands, System.String dataBaseName) {  throw new NotImplementedException(); }

        public Object Clone() => new TableContentExecuterSecureStrategy();

        public Boolean GetContextNamespace(System.String contextName, out System.String result) {throw new NotImplementedException(); }

        public Boolean IsCommandExist(System.String command, System.String context)  { throw new NotImplementedException();}

        public Boolean IsContextExist(System.String context) { throw new NotImplementedException(); }

        public string TranslateContextTo(string context){   throw new NotImplementedException(); }

        public Boolean UserCanExecuteCommand(System.String command, List<System.String> ObjectName, List<NodeType> ObjectType) { throw new NotImplementedException(); }
    }
}
