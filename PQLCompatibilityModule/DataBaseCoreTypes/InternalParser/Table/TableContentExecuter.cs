﻿using DBMS.Core;
using DBMS.Core.Interprater;
using DBMS.Core.Interprater.SecureStrategy;
using DBMS.Core.IO;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.MMIL;
using DBMS.Core.MMIL.ILConfiguration;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Helpers;
using System.Text.RegularExpressions;

using DBMS.PQL.Grammar;

namespace DBMS.PQL
{
    internal sealed class TableContentExecuter
        : IExecuteStrategy
    {


        #region Documentation
#if DocTrue
        static TableContentExecuter() { }
#endif
        #endregion




        private IDBNoSQLExecuteSecureStrategy _SecureStrategy;
        private IExecuteResult _ExecuteResult;


    


        public TableContentExecuter(IDBNoSQLExecuteSecureStrategy secureStrategy, IExecuteResult executeResult)
        {
            _SecureStrategy = secureStrategy;
            _ExecuteResult = executeResult;

        }

        public Object Clone() => new TableContentExecuter
                (_SecureStrategy.Clone() as IDBNoSQLExecuteSecureStrategy,
                _ExecuteResult.Clone() as IExecuteResult);

        public IExecuteResult Execute(IParseResult parseResult)
        {
            List<MMILDataBaseContext> units
             = parseResult.GetTranslationUnits();


            
            TableRecordMetaInfo info = new TableRecordMetaInfo();
            info.ColumnConstraint = new Dictionary<System.String, Dictionary<System.String, System.String>>(1);
            info.ColumnTypes = new Dictionary<System.String, PacificDataBaseDataType>(1);
            info.FK = new List<System.String>(1);

            for (System.Int32 i = 0; i < units.Count; ++i)
            {
                _SecureStrategy.CheckSecureRule(units.ElementAt(i));
               _Execute(units.ElementAt(i), info);
                
            }

            info.RowCount = 0;

            System.String tableName;
            units.ElementAt(0)
                .GetILDictionary()
                .TryGetValue(MMILDataBaseTableMappConfig.TableDeclarationCommand, out tableName);

            info.TableName = tableName;


            return (_ExecuteResult = info);

        }



        private void  _Execute(IMMILTranslationUnit unit,
            in TableRecordMetaInfo rows)
        {
            Dictionary<System.String, System.String> dic = unit.GetILDictionary();

            System.String attrValue = System.String.Empty;
            System.String attr = System.String.Empty;
            System.String DataType = System.String.Empty;
            System.String Alias = System.String.Empty;


                dic.TryGetValue(MMILDataBaseTableMappConfig.AliasDeclarationCommand, out Alias);
                dic.TryGetValue(MMILDataBaseTableMappConfig.AttributesDecalarationCommand, out attr);
                dic.TryGetValue(MMILDataBaseTableMappConfig.AttributeValueDeclarationCommand, out attrValue);
                dic.TryGetValue(MMILDataBaseTableMappConfig.DataTypeDeclarationCommand, out DataType);


            Dictionary<System.String,System.String> attributes = new Dictionary<System.String, System.String>(10);
            System.String[] attrNames = attr.Split(DBMSIO.DBMSConfig.StandartSeparator);
            System.String[] attrValues = attrValue.Split(DBMSIO.DBMSConfig.StandartSeparator);


            PacificDataBaseDataType dataType;
            if (!PQLGrammar.AllowedTypes.TryGetValue(DataType, out dataType))
                throw new ExecutorRuntimeError();


            AttributeType type;

            for (System.Int32 i = 0; i < attrNames.Length; ++i)
            {

                if (PQLGrammar.AllowedAttributes.TryGetValue(attrNames[i], out type) && type != AttributeType.UNSET)
                {

                    if(type == AttributeType.LIKE && dataType == PacificDataBaseDataType.NUMBER)
                    {
                        String likeParam = attrValues.ElementAt(i).Trim().Trim(PQLGrammar.ConstantLiteralStringBracetChar);
                        Regex reg = new Regex
                            (@"^((?:(?:(?:(?:\+|\-)?(?:\[\d\-\d\]\+?))|(?:(?:\+|\-)?\d+))\|?)+)*(?:(?:(?:\+|\-)?(?:\[\d\-\d\]\+?))|(?:(?:\+|\-)?\d+))$",
                            RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline);

                        if (!reg.IsMatch(likeParam))
                            throw new DataMappingError();
                    } 



                    if (type == AttributeType.PRIMARY_KEY)
                    {
                        if (attrNames.Contains(PQLGrammar.DEFAULT) 
                            || attrNames.Contains(PQLGrammar.LIKE) 
                            || attrNames.Contains(PQLGrammar.FOREIGN_KEY_REFERENCES)
                            || attrNames.Contains(PQLGrammar.NOT_NULL))
                            throw new DataMappingError();


                        if(attributes.ContainsKey(PQLGrammar.AUTO_INCREMENT))
                            throw new DataMappingError();


                        rows.IsIndexed = true;
                        rows.PK = Alias;
                    } else if(type==AttributeType.PRIMARY_KEY && rows.PK != String.Empty)
                        throw new ValueError();



                    if (type == AttributeType.IDENTYTI)
                        if (attrNames.Contains(PQLGrammar.DEFAULT) 
                            || attrNames.Contains(PQLGrammar.LIKE) 
                            || attrNames.Contains(PQLGrammar.NOT_NULL))
                            throw new DataMappingError();

                    if(type == AttributeType.NOT_NULL)
                        if (attrNames.Contains(PQLGrammar.AUTO_INCREMENT))
                            throw new DataMappingError();



                    if(type == AttributeType.UNIQUE)
                        if(attrNames.Contains(PQLGrammar.DEFAULT))
                            throw new DataMappingError();

                    if(type == AttributeType.DEFAULT)
                    {
                        if (dataType == PacificDataBaseDataType.NUMBER)
                            if (!Int32.TryParse(attrValues[i].Trim(), out _))
                                throw new DataMappingError();
                    }


                    if (type==AttributeType.FOREIGN_KEY || type == AttributeType.INCLUDED_TABLE)
                    {


                        String[] sepAtt = attrValues[i].Split(PQLGrammar.TableAttrLeftBracet);


                        if (DBMSIO.GetCurrentDataBase().IsTableExist(sepAtt[0].Trim()))
                        {
                            String columnConstraint =
                                sepAtt[1].Replace(PQLGrammar.TableAttrRightBracet.ToString(), String.Empty).Replace(PQLGrammar.ConstantLiteralStringBracet, String.Empty).Trim();

                            if (DBMSIO.GetCurrentDataBase().IsColumnWithNameExsitInTableWithName(columnConstraint, sepAtt[0].Trim()))
                            {
                                ITableInfo tableInfo = DBMSIO.GetCurrentDataBase().GetTableMetaByName(sepAtt[0].Trim());
                                Dictionary<String, String> constDict;
                                tableInfo.ColumnConstraint.TryGetValue
                                    (sepAtt[0].Trim() + PQLGrammar.ShemasSeparator + columnConstraint, out constDict);

                                PacificDataBaseDataType foreignColumnType;
                                tableInfo.ColumnTypes.TryGetValue
                                    (sepAtt[0].Trim() + PQLGrammar.ShemasSeparator + columnConstraint, out foreignColumnType);

                                
                                if (constDict.ContainsKey(PQLGrammar.PRIMARY_KEY) || constDict.ContainsKey(PQLGrammar.UNIQUE))
                                {
                                    if (foreignColumnType == dataType)
                                    {
                                        if (!rows.FK.Contains(Alias) && type == AttributeType.INCLUDED_TABLE)
                                            rows.FK.Add(Alias);
                                    } else
                                    {
                                        throw new DataMappingError();
                                    }
                                }
                                else throw new DataMappingError();
                                
                            }
                            else
                                throw new ValueError();
                            

                        } else
                            throw new TableNotExist();
                        
                        
                    }

                    attributes.Add(attrNames[i], attrValues[i]);
                }
                else if (type == AttributeType.UNSET)
                    continue;
                else
                    throw new ExecutorRuntimeError();
                
                

            }

            rows.ColumnConstraint.Add(Alias, attributes);
            rows.ColumnTypes.Add(Alias, dataType);

        }







    }
}
