﻿using DBMS.Core;
using DBMS.Core.MMIL;
using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.Helpers;

using DBMS.PQL.Grammar;

namespace DBMS.PQL
{
    internal sealed class ViewParserSecureStrategy
         : IParserSecureStrategy
    {


        #region Documentation
#if DocTrue
        static ViewParserSecureStrategy() { }
#endif
        #endregion


        public bool Check(System.String param) => param == PQLGrammar.SELECT_COMMAND;
        public bool Check(List<System.String> param)  { throw new NotImplementedException(); }
        public bool Check(Dictionary<System.String, System.String> param) {  throw new NotImplementedException();}
        public bool Check(IMMILTranslationUnit param){ throw new NotImplementedException();  }
        public bool Check(List<IMMILTranslationUnit> param)  { throw new NotImplementedException(); }
    }
}
