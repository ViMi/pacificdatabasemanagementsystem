﻿using DBMS.Core;
using DBMS.Core.Interprater;
using DBMS.Core.Interprater.SecureStrategy;
using DBMS.Core.IO;
using DBMS.Core.MMIL;
using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Helpers;

using DBMS.Core.UsersFromRoles;
using DBMS.PQL.Grammar;


namespace DBMS.PQL
{
    internal sealed class ViewParser
        : IParseStrategy
    {

        #region Documentation
#if DocTrue
        static ViewParser() { }
#endif
        #endregion



        private System.String _Listing = System.String.Empty;
        private IParserSecureStrategy _SecureStrategy = new ViewParserSecureStrategy();
        private ILanguageSecureStrategy _LanguageSecureStrategy = null;
        
        public ViewParser(System.String listing, ILanguageSecureStrategy languageSecStrategy)
        {
            this._Listing = listing;
            this._LanguageSecureStrategy = languageSecStrategy;

        }


        public IParseResult Execute()
        {
            MMILCollection parseResultCollection = new MMILCollection();
           

            System.String[] content = _LanguageSecureStrategy.PrepareListingToTranslation();

            System.Int32 commandCount = content.Length;

            if (commandCount > 1)
                throw new PQL.ErrorControlSystem.Exception.ViewStatementsCountExcetion();

            for (System.Int16 i = 0; i < commandCount; ++i)
            {



                System.String[] command = _LanguageSecureStrategy.PrepareContextToTranslation(content[i]);
                if (String.IsNullOrWhiteSpace(command[0]))
                    continue;

                if (!_LanguageSecureStrategy.IsAbstractCommand(command[0]))
                    throw new SyntaxError();



                System.String commandList = System.String.Empty;
                System.String parameterList = System.String.Empty;
                
                MMILDataBaseContext CurrentContext = new MMILDataBaseContext();

                if (DBMSIO.GetDBMSMode() != DBMSMode.WITHOUT_LNG_CONTROL)
                    if (!_SecureStrategy.Check(command[0]))
                    throw new ParseSecureError();

                CurrentContext.InitExecutionContext(command[0]);

              
                CurrentContext.SetServer(Rbac.DEFAULT_SERVER_TOKEN);
                CurrentContext.SetDatabase(DBMSIO.GetCurrentDataBase().GetDataBaseName());

                CurrentContext.SetUser(DBMSIO.GetCurrentLogin().NodeName);



                for (System.Int16 j = 1; j < command.Length; ++j)
                {

                    command[j] = _LanguageSecureStrategy.PrepareCommandToTranslation(command[j]);

                    System.String[] concreteCommand = new System.String[2]
                    {
                        _LanguageSecureStrategy.GetMethodNameFromString(command[j]),
                        _LanguageSecureStrategy.GetParameterFromString(command[j])
                    };


                    if (DBMSIO.GetDBMSMode() != DBMSMode.WITHOUT_LNG_CONTROL)
                    {
                        if (!_LanguageSecureStrategy.IsConcreteCommand(command[0], concreteCommand[0]))
                            throw new SyntaxError();

                        if (PQLGrammar.AggregateCommands.Contains(concreteCommand[0]))
                            throw new SyntaxError();
                    }



                    commandList += concreteCommand[0] + DBMSIO.DBMSConfig.StandartSeparator;
                    parameterList += concreteCommand[1] + DBMSIO.DBMSConfig.StandartSeparator;

                }

                CurrentContext.SetCommandUnsafe(commandList);
                CurrentContext.SetParametersUnsafe(parameterList);

                CurrentContext.EndExecuteContext();
                parseResultCollection.ILUnits.Add(CurrentContext);


            }


            return parseResultCollection;
        }


        public void SetListing(System.String listing) => _Listing = listing;
        public Object Clone() => new ViewParser(_Listing, _LanguageSecureStrategy);

    }
}
