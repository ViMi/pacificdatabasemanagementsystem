﻿using DBMS.Core;
using DBMS.Core.Interprater;
using DBMS.Core.Interprater.SecureStrategy;
using DBMS.Core.IO;
using DBMS.Core.MMIL;
using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Helpers;
using DBMS.PQL.Grammar;

using DBMS.Core.UsersFromRoles;

namespace DBMS.PQL
{
    internal sealed class SchemasParser
            : IParseStrategy
    {

        #region Documentaiton
#if DocTrue
        static SchemasParser() { }
#endif
        #endregion



        #region Value Defiition
        private System.String _Listing = System.String.Empty;
        private IParserSecureStrategy _SecureStrategy = new SchemasLanguageSecureStrategy();
        private ILanguageSecureStrategy _LanguageSecureStrategy = null;

        private Char _RightSeparator = PQLGrammar.RightSeparator;
        private Char _RightPacketSeparator = PQLGrammar.RightPackectSeparator;


        #endregion



        public SchemasParser(System.String listing, ILanguageSecureStrategy languageSecStrategy, IParserSecureStrategy secureStrategy)
        {
            this._Listing = listing;
            this._LanguageSecureStrategy = languageSecStrategy;

        }



        public IParseResult Execute()
        {
            MMILCollection parseResultCollection = new MMILCollection();


            System.String[] content = _Listing.Split(_RightPacketSeparator, (Char)StringSplitOptions.RemoveEmptyEntries);


            System.Int32 commandCount = content.Length;


            for (System.Int16 i = 0; i < commandCount; ++i)
            {
                if (content[i] == String.Empty)
                    continue;

                MMILDataControlMap controlMap = new MMILDataControlMap();

                controlMap.InitExecutionContext(controlMap.StandartContextName);

                System.String[] rightContext = content[i].Split(_RightSeparator);

                if (DBMSIO.GetDBMSMode() != DBMSMode.WITHOUT_LNG_CONTROL)
                {
                    if (rightContext.Length > 2)
                        throw new ValueError();

                    if (!DBMSIO.IsDataBaseExist(rightContext[0]))
                        if (!PQLGrammar._AllowedSchmas.Contains(rightContext[0]))
                            throw new SyntaxError();

                    if (!_SecureStrategy.Check(rightContext[1]))
                        throw new TypeError();
                }
                


                controlMap.SetOnObject(rightContext[1]);
                controlMap.SetRights(rightContext[0]);

                

                controlMap.EndExecuteContext();

                parseResultCollection.ILUnits.Add(controlMap);



            }


            return parseResultCollection;
        }


        public void SetListing(String listing) => _Listing = listing;
        public Object Clone() => new ViewParser(_Listing, _LanguageSecureStrategy);


    }


}
