﻿using DBMS.Core;
using DBMS.Core.Interprater;
using DBMS.Core.Interprater.SecureStrategy;
using DBMS.Core.IO;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.MMIL;
using DBMS.Core.MMIL.ILConfiguration;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.InfoTypes;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.Helpers;


namespace DBMS.PQL
{
    internal sealed class SchemasExecuter
         : IExecuteStrategy
    {

        #region Documentation
#if DocTrue
        static SchemasExecuter() { }
#endif
        #endregion



        private IDBNoSQLExecuteSecureStrategy _SecureStrategy;
        private IExecuteResult _ExecuteResult;

        public SchemasExecuter(IDBNoSQLExecuteSecureStrategy secureStrategy, IExecuteResult executeResult)
        {
            _SecureStrategy = secureStrategy;
            _ExecuteResult = executeResult;
        }



        public Object Clone() => new SchemasExecuter (null, null);



        public IExecuteResult Execute(IParseResult parseResult)
        {
            List<MMILDataBaseContext> units
             = parseResult.GetTranslationUnits();


            Dictionary<String, NodeType> allowed = new Dictionary<String, NodeType>();

            for (System.Int32 i = 0; i < units.Count; ++i)
                _Execute(units.ElementAt(i), allowed);


            TransactionMethodResult res = new TransactionMethodResult();
            res.AddedInfo = allowed;

            return (_ExecuteResult = res);

        }



        private void _Execute(IMMILTranslationUnit unit,   in Dictionary<String, NodeType> allowed)
        {

            Dictionary<System.String, System.String> dic = unit.GetILDictionary();
            System.String schema;
            System.String on;

            dic.TryGetValue(MMILDataControlMapConfiguration.OnObjectDeclarationCommand, out on);
            dic.TryGetValue(MMILDataControlMapConfiguration.AllowedCommandDeclarationCommand, out schema);
            
            NodeType nodes = NodeType.DATABASE;
            allowed.Add(schema, nodes.NodeTypeResolverByName(on));

        }



    }
}
