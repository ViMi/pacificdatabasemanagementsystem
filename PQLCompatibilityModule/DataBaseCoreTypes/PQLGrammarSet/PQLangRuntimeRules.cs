﻿using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core;
using System.Linq;
using System.Threading.Tasks;
using DBMS.Core.InfoTypes;
using DBMS.Core.IO;

using DBMS.Core.MetaInfoTables;
using System.IO;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.UsersFromRoles;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.Helpers;
using DBMS.Core.Helpers.RulesControl;
using DBMS.Core.Triggers;

using DBMS.PQL.TransactionInformation;

namespace DBMS.PQL
{

    public enum PQLLangRuleID 
        : System.Byte
    {
        MULTIPLY_ENTRY = 0,
        ENCRIPTO_UNSET = 1,
        STRING_FORMAT_RIGHT = 2,
        USER_RIGHT_LIST_CREATED = 3,
        LOGIN_SHCME_LIST_CREATED = 4,
        STRING_SEPARATOR_LOGIN_CORRECT = 5,
        TABLE_CREATED = 6,
        THIS_STRING_EMPTY = 7,
        PASSWORD_UNSET = 8,
        ENCRIPTO_CREATED = 9,
        AS_PARAMETER_UNSETED = 10,
        AS_PARAMETER_MULTIPLY_SETTED = 11,
        USER_TOKEN_CREATED = 12,
        RIGHT_NAME = 13
    }


    public sealed class PQLangRuntimeRules
        : IRulesControlObject
    {
        #region Documentation
#if DocTrue
        static PQLangRuntimeRules()
        {

        }
#endif
        #endregion




        #region Rules Initiate
        public List<IStatement> Statements { get; set; } = new List<IStatement>()
        {

            new PQLRuleStatement(PQLLangRuleID.MULTIPLY_ENTRY, PQLLangRuleID.MULTIPLY_ENTRY.ToString())
            {
                StatementDefinition = (ITransactionInfo ti) =>
                {
                   PQLCommandInfo pql = ti.AddedInfo as PQLCommandInfo;
               
                   if (pql.OperateObectType.Count > 1 || pql.OperateObjectName.Count > 1)
                             return new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.MULTIPLY_CONFI_VALUE_ERROR]);
               
                           
                   return new TransactionMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE]);

                }
            },




            new PQLRuleStatement(PQLLangRuleID.RIGHT_NAME, PQLLangRuleID.RIGHT_NAME.ToString())
            {
                StatementDefinition = (ITransactionInfo ti) =>
                {
                    if(ti.Parameters.Trim().Length == 0)
                            return new TransactionMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.PARSE_ERROR]);

                    foreach(Char token in ti.Parameters)
                        if(!Char.IsLetter(token))
                            return new TransactionMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.PARSE_ERROR]);

                    return new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE]);

                }
            },
         



              new PQLRuleStatement(PQLLangRuleID.ENCRIPTO_CREATED, PQLLangRuleID.ENCRIPTO_CREATED.ToString())
            {
                StatementDefinition = (ITransactionInfo ti) =>
                {


                   if (ti.AddedInfo != null)
                             return new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.CRIPTER_UNSET]);


                   return new TransactionMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.FAILURE_EXECUTE]);


                }
            },



                  new PQLRuleStatement(PQLLangRuleID.ENCRIPTO_UNSET, PQLLangRuleID.ENCRIPTO_UNSET.ToString())
            {
                StatementDefinition = (ITransactionInfo ti) =>
                {


                   if (String.IsNullOrEmpty(ti.Parameters))
                             return new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.CRIPTER_UNSET]);


                   return new TransactionMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.FAILURE_EXECUTE]);


                }
            },




                  new PQLRuleStatement(PQLLangRuleID.PASSWORD_UNSET, PQLLangRuleID.PASSWORD_UNSET.ToString())
            {
                StatementDefinition = (ITransactionInfo ti) =>
                {


                   if (String.IsNullOrEmpty(ti.Parameters))
                             return new TransactionMethodResult(true,PQL.ErrorControlSystem.ErrorMessagesCore.MessageDictionary[(Byte)PQL.ErrorControlSystem.ErrorCodes.PASSWORD_UNSET]);


                   return new TransactionMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.FAILURE_EXECUTE]);


                }
            },



            new PQLRuleStatement(PQLLangRuleID.LOGIN_SHCME_LIST_CREATED, PQLLangRuleID.LOGIN_SHCME_LIST_CREATED.ToString())
            {
                StatementDefinition = (ITransactionInfo ti) =>
                {
                         if (ti.AddedInfo != null)
                             return new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE]);


                   return new TransactionMethodResult(false, PQL.ErrorControlSystem.ErrorMessagesCore.MessageDictionary[(Byte)PQL.ErrorControlSystem.ErrorCodes.SCHEMA_LIST_UNSETED]);
                }
            },




              new PQLRuleStatement(PQLLangRuleID.STRING_FORMAT_RIGHT, PQLLangRuleID.STRING_FORMAT_RIGHT.ToString())
            {
                StatementDefinition = (ITransactionInfo ti) =>
                {
                   PQLCommandInfo pql = ti.AddedInfo as PQLCommandInfo;

                   if (pql.OperateObectType.Count > 1 || pql.OperateObjectName.Count > 1)
                             return new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.MULTIPLY_CONFI_VALUE_ERROR]);


                   return new TransactionMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE]);

                }
            },




               new PQLRuleStatement(PQLLangRuleID.STRING_SEPARATOR_LOGIN_CORRECT, PQLLangRuleID.STRING_SEPARATOR_LOGIN_CORRECT.ToString())
            {
                StatementDefinition = (ITransactionInfo ti) =>
                {
                    if (ti.Parameters.Split(DBMSIO.DBMSConfig.StandartSeparator).Length > 1)
                return new TransactionMethodResult(false, PQL.ErrorControlSystem.ErrorMessagesCore.MessageDictionary[(Byte)ErrorControlSystem.ErrorCodes.STRING_SEPARATOR_ERROR]);


                   return new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE]);

                }
            },





             new PQLRuleStatement(PQLLangRuleID.TABLE_CREATED, PQLLangRuleID.TABLE_CREATED.ToString())
            {
                StatementDefinition = (ITransactionInfo ti) =>
                {
                  

                   if (ti.AddedInfo != null)
                             return new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE]);


                   return new TransactionMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.AS_PARAMETER_UNSETED]);

                }
            },




              new PQLRuleStatement(PQLLangRuleID.USER_TOKEN_CREATED, PQLLangRuleID.USER_TOKEN_CREATED.ToString())
            {
                StatementDefinition = (ITransactionInfo ti) =>
                {


                   if (ti.AddedInfo != null)
                             return new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.SUCCESS_EXECUTE]);


                   return new TransactionMethodResult(false, PQL.ErrorControlSystem.ErrorMessagesCore.MessageDictionary[(Byte)PQL.ErrorControlSystem.ErrorCodes.USER_TOKEN_NOT_CREATED]);

                }
            },



              new PQLRuleStatement(PQLLangRuleID.THIS_STRING_EMPTY, PQLLangRuleID.THIS_STRING_EMPTY.ToString())
            {
                StatementDefinition = (ITransactionInfo ti) =>

                {
                   if (String.IsNullOrEmpty(ti.Parameters))
                             return new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.AS_PARAMETER_UNSETED]);


                   return new TransactionMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.FAILURE_EXECUTE]);

                }
            },



            new PQLRuleStatement(PQLLangRuleID.AS_PARAMETER_UNSETED, PQLLangRuleID.PASSWORD_UNSET.ToString())
            {
                StatementDefinition = (ITransactionInfo ti) =>
                {
                   if (String.IsNullOrEmpty(ti.Parameters) || ti.Parameters.Replace(" ", String.Empty) == String.Empty)
                             return new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.AS_PARAMETER_UNSETED]);


                   return new TransactionMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.FAILURE_EXECUTE]);

                }
            },




            new PQLRuleStatement(PQLLangRuleID.AS_PARAMETER_MULTIPLY_SETTED, PQLLangRuleID.AS_PARAMETER_MULTIPLY_SETTED.ToString())
            {
                StatementDefinition = (ITransactionInfo ti) =>

                {
                   if (!String.IsNullOrEmpty(ti.Parameters))
                             return new TransactionMethodResult(true, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.MULTIPLY_CONFI_VALUE_ERROR]);


                   return new TransactionMethodResult(false, ErrorMessagesCore.MessageDictionary[(Byte)ErrorCodes.FAILURE_EXECUTE]);

                }
            },


        };

        #endregion


        public IStatement FindStatement(System.String statmentName) => this.Statements.Find(x => x.StatementName == statmentName);
        public IStatement FindStatement(System.Int32 statementID) => this.Statements.Find(x => x.StatementID == statementID);
        public ITransactionMethodResult Invoke(System.Int32 statementID, ITransactionInfo ti) => FindStatement(statementID).Invoke(ti);
        public ITransactionMethodResult Invoke(System.String statementName, ITransactionInfo ti) => FindStatement(statementName).Invoke(ti);
    }
}
