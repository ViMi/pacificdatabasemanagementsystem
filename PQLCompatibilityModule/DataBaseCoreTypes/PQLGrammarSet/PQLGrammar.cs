﻿using DBMS.Core.Table;
using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.UsersFromRoles;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.IO;
using DBMS.Core.Helpers;

using DBMS.PQL;

using DBMS.Core;
using DBMS.Core.Interprater;
using DBMS.Core.Interprater.SecureStrategy;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.MMIL;
using DBMS.Core.MMIL.ILConfiguration;
using DBMS.Core.Secure.UserRights;
using System.Linq;
using System.Runtime.InteropServices;


namespace DBMS.PQL.Grammar
{
   public static class PQLGrammar
    {

        #region Documenttion
#if DocTrue
        static PQLGrammar() { }
#endif
        #endregion



        public static PQLangRuntimeRules PQLLanRulesManager { set; get; } = new PQLangRuntimeRules();

        public static readonly String SELECT_COMMAND = "Select";
        public static readonly String CREATE_COMMAND = "Create";
        public static readonly String DROP_COMMAND = "Drop";
        public static readonly String DELETE_COMMAND = "Delete";
        public static readonly String INSERT_COMMAND = "Insert";
        public static readonly String UPDATE_COMMAND = "Update";
        public static readonly String GRANT_COMMAND = "Grant";
        public static readonly String DENY_COMMAND = "Deny";
        public static readonly String REVOKE_COMMAND = "Revoke";
        public static readonly String MIGRATE_COMMAND = "Migrate";
        public static readonly String ADMINER_COMMAND = "Adminer";
        public static readonly String EXECUTE_COMMAND = "Execute";
        public static readonly String USE_COMMAND = "Use";
        public static readonly String BACKUP_COMMAND = "BackUp";
        public static readonly String CHANGE_PASSWORD = "ChangePassword";
        public static readonly String HELP = "Help";
        public static readonly String CONSOLE_COMMAND = "Console";
        public static readonly String WHILE_COMMAND = "While";
        public static readonly String FOR_COMMAND ="For";

            
        public static readonly Char RightPackectSeparator = '|';
        public static readonly Char RightSeparator = '-';
        public static readonly Char ChildrenPacketSeparator = ':';

        public static readonly Char TerminalSymbol = ';';
        public static readonly Char OneLineComment = '#';
      
        public static readonly Char LeftParameterBracetStd = '(';
        public static readonly Char RightParameterBracetStd = ')';
        public static readonly Char CommandsSeparatorStd = '.';

        public static readonly Char LeftParameterBracetInner = '[';
        public static readonly Char RightParameterBracetInner = ']';
        public static readonly Char CommandsSeparatorInner = '-';
        public static readonly Char TerminalSymboInner = '|';

        public static readonly String ConstantLiteralStringBracet = "\"";
        public static readonly Char ConstantLiteralStringBracetChar = '"';


        public static readonly Char TableDefinitionColumnSeparator = ',';
        public static readonly Char TableAttrLeftBracet = '[';
        public static readonly Char TableAttrRightBracet = ']';
        public static readonly Char TableContentSeparator = ' ';

        public static readonly Char ShemasSeparator = '*';


        public static readonly String AUTO_INCREMENT = "AUTHO_INCREMENT";
        public static readonly String UNIQUE = "UNIQUE";
        public static readonly String DEFAULT = "DEFAULT";
        public static readonly String LIKE = "LIKE";
        public static readonly String PRIMARY_KEY = "PRIMARY_KEY";
        public static readonly String NOT_NULL = "NOT_NULL";
        public static readonly String FOREIGN_KEY_REFERENCES = "FOREIGN_KEY_REFERENCES";
        public static readonly String INCLUDE_FOREIGN_TABLE_BY_KEY = "INCLUDE_FOREIGN_TABLE_BY_KEY";


        public static readonly String NUMBER = "NUMBER";
        public static readonly String STRING = "STRING";
        public static readonly String INCLUDED_TABLE = "INCLUDED_TABLE";


        public static HashSet<String> AggregateCommands = new HashSet<string>()
        {
            "Count", "Max", "Min", "Avg", "Sum", "GetTypes", "GetDomains"
        };


        public static Dictionary<System.String, HashSet<System.String>> Commands = new Dictionary<String, HashSet<String>>()
        {
            [PQLGrammar.CREATE_COMMAND] = new HashSet<String>() { "DataBase", "Table", "Procedure", "View", "User", "Login", "As", "TriggerOnUpdate", "TriggerOnInsert", "To","TriggerOnDelete" },
            [PQLGrammar.USE_COMMAND] = new HashSet<String>() { "DataBase" },
            [PQLGrammar.INSERT_COMMAND] = new HashSet<String>() { "Into", "Values" },
            [PQLGrammar.UPDATE_COMMAND] = new HashSet<String>() { "Rows", "Set", "Where", "From" },
            [PQLGrammar.DROP_COMMAND] = new HashSet<String>() { "Procedure", "Table", "DataBase", "View", "Login", "User", "Trigger" },
            [PQLGrammar.DELETE_COMMAND] = new HashSet<String>() { "From", "Where", "Rows" },
            [PQLGrammar.SELECT_COMMAND] = new HashSet<String>() { "Rows", "Reverse", "OrderByAsc", "OrderByDesc" ,"CrossJoin", "Union" ,"From", "Where", "GroupBy", "Limit" ,"Skip","Distinct", "GetTypes", "GetDomains", "Count", "Max", "Min", "Sum", "Avg" , "Top"},
            [PQLGrammar.MIGRATE_COMMAND] = new HashSet<String>() { "DataBase", "ToDiskStorage" },
            [PQLGrammar.EXECUTE_COMMAND] = new HashSet<String>() { "Procedure" },
            [PQLGrammar.BACKUP_COMMAND] = new HashSet<String>() { "CreateOn", "CreateOnCurrent", "WithLog", "WithOutLog", "ToDiskStorage" },
            [PQLGrammar.ADMINER_COMMAND] = new HashSet<String>() {
                "RestoreServer",
            "GetCountOfProceduresOnServer",
            "GetCountOfTableOnServer",
            "GetCountOfViewOnServer",
            "GetCountOfUserOnServer",
            "GetCountOfLoginOnServer",
            "GetCurrentLoginInfo",
            "GetCurrentDataBaseInfo",
            "GetServerName",
            "GetLogFileContent",
            "GetPreviousStartDate",
            "GetLastStartDate",
            "GetServerType",
            "GetCountOfProceduresOnCurrentDataBase",
            "GetCountOfViewOnCurrentDataBase",
            "GetCountOfUserOnCurrentDataBase",
            "GetCountOfTablesOnCurrentDataBase",
            "GetAllProceduresNameOnCurrentDataBase",
            "GetAllTableNameOnCurrentDataBase",
            "GetAllViewNameOnCurrenDataBase",
            "GetAllProceduresNameOnServer",
            "GetAllTableNameOnServer",
            "GetAllViewNameOnServer",
            "GetTriggerText",
            "GetViewText",
            "GetStoredProcedureText",
            "GetTableColumnData",
            "GetTableConstraint",
            "GetServerSettings",
            "SetServerName",
            "SetServerType",
            "SetServerPassword",
            "WithCriptStrategy",
            "UnsafeMode",
            "StdMode",
            "WithoutLngSafeMode"
            },
            [PQLGrammar.REVOKE_COMMAND] = new HashSet<String>() { "ToUser", "ToLogin", "Rights" },
            [PQLGrammar.GRANT_COMMAND] = new HashSet<String>() { "ToUser", "ToLogin", "Rights" },
            [PQLGrammar.DENY_COMMAND] = new HashSet<String>() { "ToUser", "ToLogin", "Rights" },
            [PQLGrammar.CHANGE_PASSWORD] = new HashSet<String>() { "ToLogin", "WithCriptStrategy", "SetPassword" },
            [PQLGrammar.HELP] = new HashSet<String>() { "DDL", "DML", "DCL", "Admin", "Extend", "All" },
            [PQLGrammar.CONSOLE_COMMAND] = new HashSet<String>() { "Print", "NewLine", "Clear" },
            [PQLGrammar.WHILE_COMMAND] = new HashSet<String>() {
                "WithStep", "CounterNotEqu", "Content"},
            [PQLGrammar.FOR_COMMAND] = new HashSet<String>() {
            "Content", "WithStep", "To", "From"
            },
            
        };





        public static HashSet<System.String> _AllowedSchmas = new HashSet<string>()
        {
            "[DATABASE]",
            "[TABLE]",
            "[SECURE]",
            "[TABLE]",
            "[PROCEDURE]",
            "[VIEW]",
            "[USER]",
            "[TRIGGER]",
            "[LOGIN]",
            "[BACKUP]",
            "[LOG_REPORT]",
            "[SECURITY]",
             "[SERVER]"
        };


        public static  Dictionary<System.String, HashSet<System.String>> _Commands = new Dictionary<String, HashSet<String>>(20)
        {
            [PQLGrammar.CREATE_COMMAND] = PQLGrammar.Commands[PQLGrammar.CREATE_COMMAND],
            [PQLGrammar.USE_COMMAND] = PQLGrammar.Commands[PQLGrammar.USE_COMMAND],
            [PQLGrammar.INSERT_COMMAND] = PQLGrammar.Commands[PQLGrammar.INSERT_COMMAND],
            [PQLGrammar.UPDATE_COMMAND] = PQLGrammar.Commands[PQLGrammar.UPDATE_COMMAND],
            [PQLGrammar.DROP_COMMAND] = PQLGrammar.Commands[PQLGrammar.DROP_COMMAND],
            [PQLGrammar.DELETE_COMMAND] = PQLGrammar.Commands[PQLGrammar.DELETE_COMMAND],
            [PQLGrammar.SELECT_COMMAND] = PQLGrammar.Commands[PQLGrammar.SELECT_COMMAND],
            [PQLGrammar.GRANT_COMMAND] = PQLGrammar.Commands[PQLGrammar.GRANT_COMMAND],
            [PQLGrammar.DENY_COMMAND] = PQLGrammar.Commands[PQLGrammar.DENY_COMMAND],
            [PQLGrammar.REVOKE_COMMAND] = PQLGrammar.Commands[PQLGrammar.REVOKE_COMMAND],
            [PQLGrammar.CHANGE_PASSWORD] = PQLGrammar.Commands[PQLGrammar.CHANGE_PASSWORD],
            [PQLGrammar.ADMINER_COMMAND] = PQLGrammar.Commands[PQLGrammar.ADMINER_COMMAND],
            [PQLGrammar.HELP] = PQLGrammar.Commands[PQLGrammar.HELP],
            [PQLGrammar.CONSOLE_COMMAND] = PQLGrammar.Commands[PQLGrammar.CONSOLE_COMMAND],
            [PQLGrammar.BACKUP_COMMAND] = PQLGrammar.Commands[PQLGrammar.MIGRATE_COMMAND],
            [PQLGrammar.MIGRATE_COMMAND] = PQLGrammar.Commands[PQLGrammar.BACKUP_COMMAND],
            [PQLGrammar.EXECUTE_COMMAND] = PQLGrammar.Commands[PQLGrammar.EXECUTE_COMMAND],
            [PQLGrammar.WHILE_COMMAND] = PQLGrammar.Commands[PQLGrammar.WHILE_COMMAND],
            [PQLGrammar.FOR_COMMAND] = PQLGrammar.Commands[PQLGrammar.FOR_COMMAND],

        
        };



        public static HashSet<String> hashSet = new HashSet<System.String>()
        {

            "[DATABASE]",
            "[OBJ]",
            "[TABLE]",
            "[SECURE]",
            "[PROCEDURE]",
            "[VIEW]",
            "[USER]",
            "[TRIGGER]",
            "[LOGIN]",
            "[BACKUP]",
            "[LOG_REPORT]",
            "[SECURITY]",
            "[SERVER]"
        };

        public static readonly String DATABASE_TYPE = "[DATABASE]";
        public static readonly String OBJ_TYPE = "[OBJ]";
        public static readonly String TABLE_TYPE = "[TABLE]";
        public static readonly String SECURE_TYPE = "[SECURE]";
        public static readonly String PROCEDURE_TYPE = "[PROCEDURE]";
        public static readonly String VIEW_TYPE = "[VIEW]";
        public static readonly String USER_TYPE = "[USER]";
        public static readonly String TRIGGER_TYPE = "[TRIGGER]";
        public static readonly String LOGIN_TYPE = "[LOGIN]";
        public static readonly String BACKUP_TYPE = "[BACKUP]";
        public static readonly String LOG_REPORT_TYPE = "[LOG_REPORT]";
        public static readonly String SECURITY_TYPE = "[SECURITY]";
        public static readonly String SERVER_TYPE = "[SERVER]";

        public static readonly String ALL_ROWS_CONST = "*";





        public static List<String> compareOperators = new List<System.String>() { "==", "<", ">", "<=", ">=", "!=" };
        public const String EQU = "==";
        public const String LSS = "<";
        public const String GRT = ">";
        public const String NOT = "!";
        public const String NOT_EQU = "!=";
        public const String LSS_EQU = "<=";
        public const String GRT_EQU = ">=";
 

        public const String WhereSeparator = " ";
        public const String AND = "and";
        public const String OR = "or";

        public const String InsertValueSeparator = ",";

        public const String PackegSeparator = "|";
        public const String SetSimbol = "=";


        public const String MAX = "MAX";
        public const String MIN = "MIN";
        public const String AVG = "AVG";
        public const String SUM = "SUM";
        public const String COUNT = "COUNT";

        public const String DESC = "DESC";
        public const String ASC = "ASC";

        public static NodeType ResolveType(String typeName) => typeName switch
        {
            "[TABLE]" => NodeType.TABLE,
            "[VIEW]" => NodeType.VIEW,
            "[PROCEDURE]" => NodeType.PROCEDURE,
            "[USER]" => NodeType.USER,
            "[LOGIN]" => NodeType.LOGIN,
            "[DATABASE]" => NodeType.DATABASE,
            "[TRIGGER]" => NodeType.TRIGGER,
            "[OBJ]" => NodeType.ROOT,

            _ => throw new SyntaxError()
        };



        public static List<String> AllowedSchemas = new List<String>()
        {
            "DATA_BASE",
            "SECURE_ROOT"
        };



        public static Dictionary<System.String, PacificDataBaseDataType> AllowedTypes
        = new Dictionary<System.String, PacificDataBaseDataType>(10)
        {
            [PQLGrammar.NUMBER] = PacificDataBaseDataType.NUMBER,
            [PQLGrammar.STRING] = PacificDataBaseDataType.STRING,
            [PQLGrammar.INCLUDED_TABLE] = PacificDataBaseDataType.INCLUDED_TABLE

        };

        public static Dictionary<System.String, AttributeType> AllowedAttributes
            = new Dictionary<System.String, AttributeType>(10)
            {
                [PQLGrammar.PRIMARY_KEY] = AttributeType.PRIMARY_KEY,
                [PQLGrammar.FOREIGN_KEY_REFERENCES] = AttributeType.FOREIGN_KEY,
                [PQLGrammar.AUTO_INCREMENT] = AttributeType.IDENTYTI,
                [PQLGrammar.NOT_NULL] = AttributeType.NOT_NULL,
                [PQLGrammar.DEFAULT] = AttributeType.DEFAULT,
                [PQLGrammar.UNIQUE] = AttributeType.UNIQUE,
                [PQLGrammar.LIKE] = AttributeType.LIKE,
                [PQLGrammar.INCLUDE_FOREIGN_TABLE_BY_KEY] = AttributeType.INCLUDED_TABLE,
                [String.Empty] = AttributeType.UNSET,

            };



        public static HashSet<System.String> _Types = new HashSet<System.String>()
        {
            PQLGrammar.STRING, PQLGrammar.NUMBER, PQLGrammar.INCLUDED_TABLE
        };


        public static HashSet<System.String> _AbstractAttribute = new HashSet<System.String>()
        {
           PQLGrammar.PRIMARY_KEY,
           PQLGrammar.AUTO_INCREMENT,
           PQLGrammar.UNIQUE,
           PQLGrammar.NOT_NULL
        };


        public static HashSet<System.String> _ConcreteAttributes = new HashSet<System.String>()
        {
           PQLGrammar.FOREIGN_KEY_REFERENCES,
           PQLGrammar.DEFAULT,
           PQLGrammar.LIKE,
           PQLGrammar.INCLUDE_FOREIGN_TABLE_BY_KEY
        };



        public static readonly List<String> AllCommands = new List<System.String>()
        {
            PQLGrammar.SELECT_COMMAND,
            PQLGrammar.CREATE_COMMAND,
            PQLGrammar.DROP_COMMAND,
            PQLGrammar.DELETE_COMMAND,
            PQLGrammar.INSERT_COMMAND,
            PQLGrammar.UPDATE_COMMAND,
            PQLGrammar.GRANT_COMMAND,
            PQLGrammar.DENY_COMMAND,
            PQLGrammar.REVOKE_COMMAND,
            PQLGrammar.MIGRATE_COMMAND,
            PQLGrammar.ADMINER_COMMAND,
            PQLGrammar.EXECUTE_COMMAND,
            PQLGrammar.USE_COMMAND,
            PQLGrammar.BACKUP_COMMAND,
            PQLGrammar.CHANGE_PASSWORD,
            PQLGrammar.HELP,
            PQLGrammar.CONSOLE_COMMAND,
            PQLGrammar.WHILE_COMMAND,
            PQLGrammar.FOR_COMMAND
        };


        public static Dictionary<System.String, PacificDataBaseDataType> DataTypes = new Dictionary<System.String, PacificDataBaseDataType>()
        {
            [PQLGrammar.NUMBER] = PacificDataBaseDataType.NUMBER,
            [PQLGrammar.STRING] = PacificDataBaseDataType.STRING,
            [PQLGrammar.INCLUDED_TABLE] = PacificDataBaseDataType.INCLUDED_TABLE
        };





        public static Dictionary<String, Dictionary<String,String>> RbacCompatibilityMap = new Dictionary<System.String, Dictionary<System.String, System.String>>()
        {
            [Rbac.DDL_TOKEN] = new Dictionary<String, String>()
            {
                [Rbac.CREATE_COMMAND] = PQLGrammar.CREATE_COMMAND,
                [Rbac.DROP_COMMAND] = PQLGrammar.DROP_COMMAND
            },

            [Rbac.DML_TOKEN] = new Dictionary<System.String, System.String>()
            {
                [Rbac.INSERT_COMMAND] = PQLGrammar.INSERT_COMMAND,
                [Rbac.UPDATE_COMMAND] = PQLGrammar.UPDATE_COMMAND,
                [Rbac.DELETE_COMMAND] = PQLGrammar.DELETE_COMMAND,
                [Rbac.SELECT_COMMAND] = PQLGrammar.SELECT_COMMAND
            },

            [Rbac.DCL_TOKEN] = new Dictionary<System.String, System.String>()
            {
                [Rbac.GRANT_COMMAND] = PQLGrammar.GRANT_COMMAND,
                [Rbac.DENY_COMMAND] = PQLGrammar.DENY_COMMAND,
                [Rbac.REVOKE_COMMAND] = PQLGrammar.REVOKE_COMMAND
            },

            [Rbac.ADMIN_TOKEN] = new Dictionary<System.String, System.String>()
            {
                [Rbac.ADMINER_COMMAND] = PQLGrammar.ADMINER_COMMAND,
            },

            [Rbac.EXTEND_TOKEN] = new Dictionary<System.String, System.String>()
            {
                [Rbac.USE_COMMAND] = PQLGrammar.USE_COMMAND,
                [Rbac.BACKUP_COMMAND] = PQLGrammar.BACKUP_COMMAND,
                [Rbac.MIGRATE_COMMAND] = PQLGrammar.MIGRATE_COMMAND,
                [Rbac.CHANGE_PASSWORD] = PQLGrammar.CHANGE_PASSWORD,
                [Rbac.EXECUTE_COMMAND] = PQLGrammar.EXECUTE_COMMAND,
            },

            [Rbac.UNCLASSED_TOKEN] = new Dictionary<System.String, System.String>()
            {
                [Rbac.UNCLASSED_COMMAND] = PQLGrammar.CONSOLE_COMMAND,
                [Rbac.UNCLASSED_COMMAND] = PQLGrammar.HELP,
                [Rbac.UNCLASSED_COMMAND] = PQLGrammar.WHILE_COMMAND,
                [Rbac.UNCLASSED_COMMAND] = PQLGrammar.FOR_COMMAND,
            }
        };






        public static Dictionary<String, String> CommandGroups = new Dictionary<System.String, String>()
        {
            [PQLGrammar.SELECT_COMMAND] = Rbac.DML_TOKEN,
            [PQLGrammar.DELETE_COMMAND] = Rbac.DML_TOKEN,
            [PQLGrammar.UPDATE_COMMAND] = Rbac.DML_TOKEN,
            [PQLGrammar.INSERT_COMMAND] = Rbac.DML_TOKEN,

            [PQLGrammar.CREATE_COMMAND] = Rbac.DDL_TOKEN,
            [PQLGrammar.DROP_COMMAND]   = Rbac.DDL_TOKEN,

            [PQLGrammar.GRANT_COMMAND]  = Rbac.DCL_TOKEN,
            [PQLGrammar.REVOKE_COMMAND] = Rbac.DCL_TOKEN,
            [PQLGrammar.DENY_COMMAND]   = Rbac.DCL_TOKEN,

            [PQLGrammar.ADMINER_COMMAND] = Rbac.ADMIN_TOKEN,

            [PQLGrammar.USE_COMMAND]     = Rbac.EXTEND_TOKEN,
            [PQLGrammar.MIGRATE_COMMAND] = Rbac.EXTEND_TOKEN,
            [PQLGrammar.BACKUP_COMMAND]  = Rbac.EXTEND_TOKEN,
            [PQLGrammar.CHANGE_PASSWORD] = Rbac.EXTEND_TOKEN,
            [PQLGrammar.EXECUTE_COMMAND] = Rbac.EXTEND_TOKEN,
            [PQLGrammar.WHILE_COMMAND] = Rbac.EXTEND_TOKEN,
            [PQLGrammar.FOR_COMMAND] = Rbac.EXTEND_TOKEN,
            [PQLGrammar.HELP] = Rbac.EXTEND_TOKEN,
            [PQLGrammar.CONSOLE_COMMAND] = Rbac.EXTEND_TOKEN,
        };




        public static Dictionary<String, String> CommandComformity = new Dictionary<System.String, String>()
        {
            [PQLGrammar.SELECT_COMMAND] = Rbac.SELECT_COMMAND,
            [PQLGrammar.DELETE_COMMAND] = Rbac.DELETE_COMMAND,
            [PQLGrammar.UPDATE_COMMAND] = Rbac.UPDATE_COMMAND,
            [PQLGrammar.INSERT_COMMAND] = Rbac.INSERT_COMMAND,

            [PQLGrammar.CREATE_COMMAND] = Rbac.CREATE_COMMAND,
            [PQLGrammar.DROP_COMMAND] = Rbac.DROP_COMMAND,

            [PQLGrammar.GRANT_COMMAND] = Rbac.GRANT_COMMAND,
            [PQLGrammar.REVOKE_COMMAND] = Rbac.REVOKE_COMMAND,
            [PQLGrammar.DENY_COMMAND] = Rbac.DENY_COMMAND,

            [PQLGrammar.ADMINER_COMMAND] = Rbac.ADMINER_COMMAND,

            [PQLGrammar.USE_COMMAND] = Rbac.USE_COMMAND,
            [PQLGrammar.MIGRATE_COMMAND] = Rbac.MIGRATE_COMMAND,
            [PQLGrammar.BACKUP_COMMAND] = Rbac.BACKUP_COMMAND,
            [PQLGrammar.CHANGE_PASSWORD] = Rbac.CHANGE_PASSWORD,
            [PQLGrammar.EXECUTE_COMMAND] = Rbac.EXECUTE_COMMAND,

            [PQLGrammar.HELP] = Rbac.UNCLASSED_COMMAND,
            [PQLGrammar.CONSOLE_COMMAND] = Rbac.UNCLASSED_COMMAND,
            [PQLGrammar.WHILE_COMMAND] = Rbac.UNCLASSED_COMMAND,
            [PQLGrammar.FOR_COMMAND] = Rbac.UNCLASSED_COMMAND,
           
        };


        public static Dictionary<String, String> CommandComformityRbac = new Dictionary<System.String, String>()
        {
            [Rbac.SELECT_COMMAND] = SELECT_COMMAND,
            [Rbac.DELETE_COMMAND] = DELETE_COMMAND,
            [Rbac.UPDATE_COMMAND] = UPDATE_COMMAND,
            [Rbac.INSERT_COMMAND] = INSERT_COMMAND,

            [Rbac.CREATE_COMMAND] = CREATE_COMMAND,
            [Rbac.DROP_COMMAND] = DROP_COMMAND,

            [Rbac.GRANT_COMMAND] = GRANT_COMMAND,
            [Rbac.REVOKE_COMMAND] = REVOKE_COMMAND,
            [Rbac.DENY_COMMAND] = DENY_COMMAND,

            [Rbac.ADMINER_COMMAND] = ADMINER_COMMAND,

            [Rbac.USE_COMMAND] = USE_COMMAND,
            [Rbac.MIGRATE_COMMAND] = MIGRATE_COMMAND,
            [Rbac.BACKUP_COMMAND] = BACKUP_COMMAND,
            [Rbac.CHANGE_PASSWORD] = CHANGE_PASSWORD,
            [Rbac.EXECUTE_COMMAND] = EXECUTE_COMMAND,


            [PQLGrammar.HELP] = Rbac.UNCLASSED_COMMAND,
            [PQLGrammar.CONSOLE_COMMAND] = Rbac.UNCLASSED_COMMAND,
            [PQLGrammar.WHILE_COMMAND] = Rbac.UNCLASSED_COMMAND,
            [PQLGrammar.FOR_COMMAND] = Rbac.UNCLASSED_COMMAND,

        };




        public static System.String GetCommandPQLComfogromity(System.String command)
        {
            System.String res;
            if (PQLGrammar.CommandComformityRbac.TryGetValue(command, out res))
                return res;

            if (command == String.Empty)
                return command;


            throw new ValueError();
        }

        public static System.String GetCommandRbacComfirmity(System.String command)
        {
            System.String res;
            if (PQLGrammar.CommandComformity.TryGetValue(command, out res))
                return res;

            if (command == String.Empty)
                return command;


            throw new ValueError();
        }



        public static System.String GetCommandGroup(System.String command)
        {
            System.String res;
            if(PQLGrammar.CommandGroups.TryGetValue(command, out res))
            {
                return res;
            }


            throw new ValueError();
            
        }



        public static String GetHelp(String commandTypeToken)
        {
            String help = String.Empty;
           if(PQLGrammar.Help.ContainsKey(commandTypeToken))
            {
                PQLGrammar.Help.TryGetValue(commandTypeToken, out help);
            } 

            return help;
        }





        public static String GetHelpWarning(String commandType)
        {

            String res = String.Empty;
            if(HelpWarnings.ContainsKey(commandType))
            {
                HelpWarnings.TryGetValue(commandType, out res);
                
            }

            return res;
        }


        public static Dictionary<String, String> HelpWarnings = new Dictionary<string, string>()
        {   

            [Rbac.DDL_TOKEN] = 
            "{WARNING} ! ---- Таблиці як і  мова дуже утливі до синтаксису (навіть до кількості розділових знаків)" + Environment.NewLine
            + "---------Усі вкладені виклики які відбуваються у процедурах та представленнях мають виконуватися у форматі Select[\"\"]-Rows[\"\"]-From[\"\"]| " + Environment.NewLine
            + "Це пов'язано з особливостями парсеру" + Environment.NewLine, 


        };




        public static Dictionary<String, String> Help = new Dictionary<string, string>()
        {
            [Rbac.ADMIN_TOKEN] =
             GetHelpWarning(Rbac.ADMIN_TOKEN) +
            "[ADMINER COMMAND SET]" + Environment.NewLine
            + "----Base Command declaration - " + PQLGrammar.ADMINER_COMMAND + Environment.NewLine
            + "------{Conrete Commands} " + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetCountOfProceduresOnServer(\"\"); ---> Return a count of all procedures which contain server" + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetCountOfTableOnServer(\"\"); ---> Return a count of all tables which contain server" + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetCountOfViewOnServer(\"\"); ---> Return a count of all views which contain server" + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetCountOfUserOnServer(\"\"); ---> Return a count of all users which contain server" + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetCountOfLoginOnServer(\"\"); ---> Return a count of all login which contain server" + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetCurrentLoginInfo(\"\"); ---> return info about current login" + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetCurrentDatabaseInfo(\"\"); ---> Return info about current used DataBase" + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetServerName(\"\"); ---> Return a current server name" + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetLogFileContent(\"\"); ---> Return a contant of Log file" + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetPreviousStartDate(\"\"); ---> Return a previous start date" + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetServerType(\"\"); ---> Return a ctype of server" + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetLastStartDate(\"\"); ---> Return last start date" + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetCountOfProceduresOnCurrentDatabase(\"\"); ---> Return a count of procedure in current DataBase" + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetCountOfViewOnCurrentDatabase(\"\"); ---> Return a count of views in current DataBase" + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetCountOfUserOnCurrentDatabase(\"\"); ---> Return a count of users in current DataBase" + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetCountOfTablesOnCurrentDatabase(\"\"); ---> Return a count of tables in current DataBase" + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetAllProceduresNameOnCurrentDatabase(\"\"); ---> Return a procedure names in current DataBase" + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetAllTableNameOnCurrentDatabase(\"\"); ---> Return a table names in current DataBase" + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetAllViewNameOnCurrenDatabase(\"\"); ---> Return a view names in current DataBase" + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetAllProceduredNameOnServer(\"\"); ---> Return a procedures names on Server " + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetAllTableNameOnServer(\"\"); ---> Return a tables names on Server " + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".GetAllViewNameOnServer(\"\"); ---> Return a views names on Server " + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".SetServerName(\"serverName\"); ---> Set a new server name " + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".SetServerType(\"serverType\");---> Set a new server type (LOCAL or CLUSTERED) " + Environment.NewLine
            + PQLGrammar.ADMINER_COMMAND + ".SetServerPassword(\"passwordStr\"); ---> Set a new server password  " + Environment.NewLine
            + "--------------.WithCriptStrategy(\"Type5 | PlaneText\"); ---> Set a new server password  with encript type (Type5(MD5) or PlaneText)" + Environment.NewLine
            ,




            [Rbac.DML_TOKEN] =
            GetHelpWarning(Rbac.DML_TOKEN) +
            "[DATA MANIPULATION SET (DML)]" + Environment.NewLine
            + "----- Base Command declarations -" +
            PQLGrammar.SELECT_COMMAND + " " + PQLGrammar.DELETE_COMMAND + " " + PQLGrammar.INSERT_COMMAND + " " + PQLGrammar.UPDATE_COMMAND + Environment.NewLine
            + "--------{Concrete Commands} " + Environment.NewLine
            + PQLGrammar.SELECT_COMMAND + ".Rows(\"rowsName1 rowsname2 rowsNameN\").From(\"tableName1 tabneName2 tableNameN\")" + Environment.NewLine
            + "--------------Where(\"(table.columnName | columnName | [0-9]+ | \"[a-bA-Z]+\"|) == | != | < | <= | >= ((table.columnName | columnName | [0-9]+ | \"[a-bA-Z]+\"| [or|and])+ \")" + Environment.NewLine
            + "--------------------Skip(\"[0-9]+\")" + Environment.NewLine
            + "------------------------Limit(\"[0-9]+\")" + Environment.NewLine
            + PQLGrammar.DELETE_COMMAND + ".Rows(\"rowsName1...rowsNameN\").From(\"tableName1\")" + Environment.NewLine
            + "--------------Where(\"(table.columnName | columnName | [0-9]+ | \"[a-bA-Z]+\"|) == | != | < | <= | >= ((table.columnName | columnName | [0-9]+ | \"[a-bA-Z]+\"| [or|and])+ \")" + Environment.NewLine
            + PQLGrammar.INSERT_COMMAND + ".Into(\"tableName\").values(\"value+\")" + Environment.NewLine
            + PQLGrammar.UPDATE_COMMAND + ".Rows(\"rowName\").From(\"fromTable\").Set(\"columnName = [a-zA-Z]+ | [0-9]+\")" + Environment.NewLine
            + "--------------Where(\"(table.columnName | columnName | [0-9]+ | \"[a-bA-Z]+\"|) == | != | < | <= | >= ((table.columnName | columnName | [0-9]+ | \"[a-bA-Z]+\"| [or|and])+ \")" + Environment.NewLine
            ,



            [Rbac.DDL_TOKEN] = 
            GetHelpWarning(Rbac.DDL_TOKEN) +
            "[DATA DECLARATION SET (DDL)]" + Environment.NewLine
            + "------Base Command declaration - " + PQLGrammar.CREATE_COMMAND + " "  + " " + PQLGrammar.DROP_COMMAND + Environment.NewLine
            + PQLGrammar.CREATE_COMMAND + ".(Table(\"tableName\")|Procedure(\"procName\") | View(\"viewName\") | DataBase(\"dataBaseName\") | User(\"userName\") | Login(\"\")).As(\"declaration\");" + Environment.NewLine
            + PQLGrammar.DROP_COMMAND + ".(Table(\"tableName\")|Procedure(\"procName\") | View(\"viewName\") | DataBase(\"dataBaseName\") | User(\"userName\") | Login(\"\"));" + Environment.NewLine
            + "-------------------Table Declaration -" + Environment.NewLine
            + "------------------------{Allowed Attribute} " + Environment.NewLine
            + "   ---- PRIMARY_KEY --> main key must be unique in table" + Environment.NewLine
            + "   ---- AUTHO_INCREMENT --> auto icrement value (value must be NUMBER)" + Environment.NewLine
            + "   ---- FOREIGN_KEY_REFERECES foreigTableName[\"columnName\"] --> attribtes connect field in table with another field in other table" + Environment.NewLine
            + "   ---- UNIQUE --> field must be unique" + Environment.NewLine
            + "   ---- NOT_NULL --> field must not be empty" + Environment.NewLine
            + "   ---- DEFAULT \"defaultValue\" --> field has a default value whict will be setted if value no transmitted by insert statement" + Environment.NewLine
            + "   ---- ------{Common Row declare}" + Environment.NewLine
            + "    -------------- RowName RowType RowAttribute*,   (last declaration without ,)" + Environment.NewLine
            + "-----------------User Declaration -" + Environment.NewLine
            + "-----------------Procedure or View declaration" + Environment.NewLine
            + " --------------------  In procedure declaration you can use all statement except Create" + Environment.NewLine
            ,


            [Rbac.DCL_TOKEN] =
            GetHelpWarning(Rbac.DCL_TOKEN) +
            "[DATA CONTROL SET (DCL)]" + Environment.NewLine
            + "------------Base Command declaration - " + PQLGrammar.GRANT_COMMAND + " " + PQLGrammar.DENY_COMMAND + " " + PQLGrammar.REVOKE_COMMAND + Environment.NewLine
            + "-----Concrete Command -------" + Environment.NewLine
            + PQLGrammar.GRANT_COMMAND + ".ToUser(\"userName\").Rights(\"Rights\");" + Environment.NewLine
            + PQLGrammar.GRANT_COMMAND + ".ToLogin(\"userName\").Rights(\"Rights\");" + Environment.NewLine
            + PQLGrammar.DENY_COMMAND + ".ToUser(\"userName\").Rights(\"Rights\");" + Environment.NewLine
            + PQLGrammar.DENY_COMMAND + ".ToLogin(\"userName\").Rights(\"Rights\");" + Environment.NewLine
            + PQLGrammar.REVOKE_COMMAND + "ToUser(\"userName\").Rights(\"Rights\");" + Environment.NewLine
            + PQLGrammar.REVOKE_COMMAND + ".Login(\"userName\").Rights(\"Rights\");" + Environment.NewLine

            ,

            [Rbac.EXTEND_TOKEN] = 
            GetHelpWarning(Rbac.EXTEND_TOKEN) +
            "[EXTEND COMMAND]" + Environment.NewLine
            + "-------Base Command Declaration -" +
            PQLGrammar.USE_COMMAND + " " + PQLGrammar.EXECUTE_COMMAND + " " + PQLGrammar.CHANGE_PASSWORD + " " + PQLGrammar.HELP + " " + PQLGrammar.BACKUP_COMMAND + " " + PQLGrammar.MIGRATE_COMMAND + Environment.NewLine
            + "------------{Concrete Commamd} ----" + Environment.NewLine
            + PQLGrammar.MIGRATE_COMMAND + ".DataBase(\"dataBaseName\").ToDiskStorage(\"AbsoluteWay\");" + Environment.NewLine
            + PQLGrammar.BACKUP_COMMAND + "CreateOnCurrent(\"\").ToDiskStorage(\"AbsoluteWay\");" + Environment.NewLine
            + "---------------WithLog(\"\") | WithoutLog(\"\") " + Environment.NewLine
            + PQLGrammar.USE_COMMAND + ".DataBase(\"DataBaseName\")" + Environment.NewLine
            + PQLGrammar.HELP + ".DDL(\"\") | DML(\"\") | DCL(\"\") | Admin (\"\") | Extend (\"\") | All(\"\")" + Environment.NewLine
            + PQLGrammar.EXECUTE_COMMAND + ".Procudure(\"procedureName\");" + Environment.NewLine
            + PQLGrammar.CHANGE_PASSWORD + ".(ToUser(\"userName\") | ToLogin(\"loginName\") | ToCurrentLogin(\"\")).SetPassword(\"password\").WithCriptStrategy(\"(Type5 | PlaneText\");" + Environment.NewLine
            + PQLGrammar.CONSOLE_COMMAND + "Print(\"\").NewLine(\"\");" + Environment.NewLine

        };







    }
}
