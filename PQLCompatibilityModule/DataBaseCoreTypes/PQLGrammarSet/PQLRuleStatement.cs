﻿using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core;
using System.Linq;
using System.Threading.Tasks;
using DBMS.Core.InfoTypes;
using DBMS.Core.IO;

using DBMS.Core.MetaInfoTables;
using System.IO;
using DBMS.Core.ErrorControlSystem.Exeption;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.UsersFromRoles;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.Helpers;
using DBMS.Core.Helpers.RulesControl;
using DBMS.Core.Triggers;


namespace DBMS.PQL
{
    public sealed class PQLRuleStatement
           : IStatement
    {
        public System.Int32 StatementID { get; set; }
        public System.String StatementName { get; set; }
        public Statement StatementDefinition { get; set; }


        public PQLRuleStatement(PQLLangRuleID ruleID, System.String ruleName)
        {
            this.StatementID = (Byte)ruleID;
            this.StatementName = ruleName;
        }

        public ITransactionMethodResult Invoke(ITransactionInfo ti) => StatementDefinition(ti);
    }
}
