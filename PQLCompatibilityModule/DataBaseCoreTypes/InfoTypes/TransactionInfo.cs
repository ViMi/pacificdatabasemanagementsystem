﻿using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.Helpers;




namespace DBMS.PQL.TransactionInformation
{

    public sealed class TransactionInfo 
        : ITransactionInfo
    {

        #region Documentation
#if DocTrue
        static TransactionInfo() { }
#endif
        #endregion


        public TransactionInfo() { }

        public TransactionInfo(
            System.String userName, 
            System.String dataBaseName,
            System.String parameters = "", 
            System.Object addedInfo = null)
        {
            this.UserName = userName;
            this.DataBaseName = dataBaseName;
            this.Parameters = parameters;
            this.AddedInfo = addedInfo;
        }



        
        public System.Object AddedInfo { set; get; }
        public System.Object AddedInfoFragmentTwo { set; get; }
        public System.String UserName { set; get; }
        public System.String DataBaseName { set; get; }
        public System.String Parameters { set; get; }
    }

}
