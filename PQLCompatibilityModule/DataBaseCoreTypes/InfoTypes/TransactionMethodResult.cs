﻿using DBMS.PQL.TransactionInformation;
using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core.InfoTypes;
using DBMS.Core.Helpers;

using DBMS.Core.Interprater;

namespace DBMS.PQL.TransactionInformation
{
    public sealed class TransactionMethodResult :
         ITransactionMethodResult
        ,IExecuteResult
    {


        #region Documentation
#if DocTrue
        static TransactionMethodResult() { }
#endif
        #endregion


        public TransactionMethodResult() {  }



        public TransactionMethodResult(System.Boolean status, System.String result)
        {
            this.IsSuccess = status;
            this.ResultString = result;
        } 


        public Object AddedInfo { set; get; }

        public System.Boolean IsSuccess {  get; }
        public System.String ResultString { set; get; }
        public ITransactionMethodResult _CurrentTI { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public void Append(string value) {  throw new NotImplementedException(); }
        public void Append(object obj) {  throw new NotImplementedException();  }
        public object Clone() { throw new NotImplementedException(); }
        public string GetExecuteResult(){ throw new NotImplementedException();}
        public object GetExecuteResultObject() { throw new NotImplementedException();}
        public void Reset(){ throw new NotImplementedException();  }
        public void SetExecuteResult(string executeResult)  {  throw new NotImplementedException(); }
        public void SetExecuteResult(object executeResult) { throw new NotImplementedException(); }
    }
}
