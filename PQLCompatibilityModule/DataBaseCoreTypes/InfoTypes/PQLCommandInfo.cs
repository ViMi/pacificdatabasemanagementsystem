﻿using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core;

namespace DBMS.PQL
{
    internal sealed class PQLCommandInfo
    {
        public List<System.String> OperateObjectName { set; get; }
        public System.String CommandName { set; get; }
        public List<NodeType> OperateObectType { set; get; }


        public PQLCommandInfo(
            List<String> operateObjName = default, 
            String commandName = default,
            List<NodeType> nodesType = default)
        {
            this.OperateObjectName = operateObjName;
            this.CommandName = commandName;
            this.OperateObectType = nodesType;
        }

    }
}
