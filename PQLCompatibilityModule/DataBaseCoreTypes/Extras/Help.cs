﻿using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.IO;
using DBMS.Formatter;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;

using DBMS.PQL.Grammar;

using DBMS.Core.UsersFromRoles;
using DBMS.Core.ErrorControlSystem;
using DBMS.Core.ErrorControlSystem.Exeption;

using DBMS.Core.Helpers;



namespace DBMS.PQL
{
    public sealed class Help
         : IPacificTransaction
    {


        #region Documentation
#if DocTrue
        static Help() { }
#endif
        #endregion



        #region Variable Declaration
        public List<String> OperateObjectName { get; set; } = new List<String>() { PQLGrammar.HELP };
        public String CommandName { get; set; } = PQLGrammar.HELP;
        public List<NodeType> OperateObectType { get; set; } = new List<NodeType>() { NodeType.ROOT };


        private String resultHelp = String.Empty;
        #endregion


        public void DDL(ITransactionInfo ti) => resultHelp = PQLGrammar.GetHelp(Rbac.DDL_TOKEN);
        public void DML(ITransactionInfo ti) => resultHelp += PQLGrammar.GetHelp(Rbac.DML_TOKEN);
        public void DCL(ITransactionInfo ti) => resultHelp = PQLGrammar.GetHelp(Rbac.DCL_TOKEN);
        public void Extend(ITransactionInfo ti) => resultHelp += PQLGrammar.GetHelp(Rbac.EXTEND_TOKEN);
        public void Admin(ITransactionInfo ti) => resultHelp += PQLGrammar.GetHelp(Rbac.ADMIN_TOKEN);


        public void All(ITransactionInfo ti) 
        {
            resultHelp = String.Empty;

            resultHelp += PQLGrammar.GetHelp(Rbac.DDL_TOKEN);
            resultHelp += PQLGrammar.GetHelp(Rbac.DML_TOKEN);
            resultHelp += PQLGrammar.GetHelp(Rbac.DCL_TOKEN);
            resultHelp += PQLGrammar.GetHelp(Rbac.EXTEND_TOKEN);
            resultHelp += PQLGrammar.GetHelp(Rbac.ADMIN_TOKEN);


        }


        public ITable EndTransaction()
        {
            resultHelp = String.Empty;
            return null;
        }

        public ITransactionMethodResult StartTransaction()
        {
          DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName} GET A HELP INFO");
         return   new TransactionMethodResult(true, resultHelp);

        }
    }
}
