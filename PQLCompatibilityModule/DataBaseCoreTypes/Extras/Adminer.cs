﻿using DBMS.Core;
using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.Core.Secure;
using System;
using System.Collections.Generic;
using System.Linq;
using DBMS.Core.ErrorControlSystem;
using DBMS.PQL.Grammar;
using DBMS.Core.Helpers;
using DBMS.PQL.ErrorControlSystem;

namespace DBMS.PQL
{
   public sealed class Adminer
         : IPacificTransaction
    {


        #region Documentation
#if DocTrue
        static Adminer() { }
#endif
        #endregion



        #region Variable Declaration
        public List<System.String> OperateObjectName { set; get; } = new List<System.String>() { DBMSIO.DBMSConfig.ServerName };
        public System.String CommandName { set; get; } = PQLGrammar.ADMINER_COMMAND;
        public List<NodeType> OperateObectType { set; get; } = new List<NodeType>() { NodeType.ROOT };

        private TransactionMethodResult _Result = null;

        private DBCommand _Command = DBCommand.GET_COUNT_OF_PROCEDURES_ON_SERVER;
        private TransactionInfo _ti = new TransactionInfo();
        private System.String _Cripter = String.Empty;
        private HashSet<String> _AllowedTypes = new HashSet<System.String>()
        {
            "LOCAL", "CLUSTERED"
        };
        #endregion


        #region OneLine Methods
        public void GetCountOfProceduresOnServer(ITransactionInfo ti)=> _Command = DBCommand.GET_COUNT_OF_PROCEDURES_ON_SERVER;
        public void GetCountOfTableOnServer(ITransactionInfo ti)=> _Command = DBCommand.GET_COUNT_OF_TABLE_ON_SERVER;
        public void GetCountOfViewOnServer(ITransactionInfo ti)=> _Command = DBCommand.GET_COUNT_OF_VIEW_ON_SERVER;
        public void GetCountOfUserOnServer(ITransactionInfo ti)=> _Command = DBCommand.GET_COUNT_OF_USER_ON_SERVER;
        public void GetCountOfLoginOnServer(ITransactionInfo ti)=> _Command = DBCommand.GET_COUNT_OF_LOGIN_ON_SERVER;
        public void GetCurrentLoginInfo(ITransactionInfo ti)=> _Command = DBCommand.GET_CURRENT_LOGIN_INFO;
        public void GetCurrentDataBaseInfo(ITransactionInfo ti)=> _Command = DBCommand.GET_CURRENT_DATABASE_INFO;
        public void GetServerName(ITransactionInfo ti)=> _Command = DBCommand.GET_SERVER_NAME;
        public void GetLogFileContent(ITransactionInfo ti)=> _Command = DBCommand.GET_LOG_FILE_CONTENT;
        public void GetPreviousStartDate(ITransactionInfo ti)=> _Command = DBCommand.GET_PREVIOUS_START_DATE;
        public void GetLastStartDate(ITransactionInfo ti)=> _Command = DBCommand.GET_LAST_START_DATE;
        public void GetServerType(ITransactionInfo ti)=> _Command = DBCommand.GET_SERVER_TYPE;
        public void GetCountOfProceduresOnCurrentDataBase(ITransactionInfo ti)=> _Command = DBCommand.GET_COUNT_OF_PROCEDURES_ON_CURRENT_DATABASE;
        public void GetCountOfViewOnCurrentDataBase(ITransactionInfo ti)=> _Command = DBCommand.GET_COUNT_OF_VIEW_ON_CURRENT_DATABASE;
        public void GetCountOfUserOnCurrentDataBase(ITransactionInfo ti)=> _Command = DBCommand.GET_COUNT_OF_USER_ON_CURRENT_DATABASE;
        public void GetCountOfTablesOnCurrentDataBase(ITransactionInfo ti)=> _Command = DBCommand.GET_COUNT_OF_TABLES_ON_CURRENT_DATABASE;
        public void GetAllProceduresNameOnCurrentDataBase(ITransactionInfo ti)=> _Command = DBCommand.GET_ALL_PROCEDURES_NAME_ON_CURRENT_DATABASE;
        public void GetAllTableNameOnCurrentDataBase(ITransactionInfo ti)=> _Command = DBCommand.GET_ALL_TABLE_NAME_ON_CURRENT_DATABASE;
        public void GetAllViewNameOnCurrenDataBase(ITransactionInfo ti)=> _Command = DBCommand.GET_ALL_VIEW_NAME_ON_CURRENT_DATABASE;
        public void GetAllProceduresNameOnServer(ITransactionInfo ti)=> _Command = DBCommand.GET_ALL_PROCEDURES_NAME_ON_SERVER;
        public void GetAllTableNameOnServer(ITransactionInfo ti)=> _Command = DBCommand.GET_ALL_TABLE_NAME_ON_SERVER;
        public void GetAllViewNameOnServer(ITransactionInfo ti)=> _Command = DBCommand.GET_ALL_VIEW_NAME_ON_SERVER;
        public void RestoreServer(ITransactionInfo ti)=> _Command = DBCommand.RESTORE_SERVER;
        public void GetTableColumnData(ITransactionInfo ti) => _Command = DBCommand.GET_TABLE_COLUMNS_DATAS;
        public void GetTableConstraint(ITransactionInfo ti) => _Command = DBCommand.GET_TABLE_COLUMNS_CONSTRAINTS;
        public void WithCriptStrategy(ITransactionInfo ti) => _Cripter = ti.Parameters;
        public void GetServerSettings(ITransactionInfo ti) => _Command = DBCommand.GET_SERVER_CONFIG;



        public void UnsafeMode(ITransactionInfo ti) => _Command = DBCommand.SET_UNSAFE_MODE;
        public void StdMode(ITransactionInfo ti) => _Command = DBCommand.SET_STANDART_MODE;
        public void WithoutLngSafeMode(ITransactionInfo ti) => _Command = DBCommand.SET_LNG_UNSAFE_MODE;


        #endregion



        public void GetTriggerText(ITransactionInfo ti) 
        {

            _ti.Parameters = ti.Parameters;
            _Command = DBCommand.GET_TRIGGER_TEXT;
        
        }
        public void GetViewText(ITransactionInfo ti)
        {
            _ti.Parameters = ti.Parameters;
            _Command = DBCommand.GET_VIEW_TEXT;
        }
        public void GetStoredProcedureText(ITransactionInfo ti)
        {

            _ti.Parameters = ti.Parameters;
            _Command = DBCommand.GET_STORED_PROCEDURE_TEXT;
        }

        public void SetServerName(ITransactionInfo ti) {

            _ti.Parameters = ti.Parameters;

            if (_ti.Parameters.Trim() == String.Empty || _ti.Parameters.Contains(' '))
                _Result = new TransactionMethodResult(
                    false, 
                    PQL.ErrorControlSystem.ErrorMessagesCore.MessageDictionary[(Byte)PQL.ErrorControlSystem.ErrorCodes.UNCORRECT_STRING_FORMAT]);

            _Command = DBCommand.SET_SERVER_NAME; 
        
        }



        public void SetServerType(ITransactionInfo ti) {

            _ti.Parameters = ti.Parameters;


            if (_ti.Parameters.Trim() == String.Empty)
                _Result = new TransactionMethodResult(
                    false, 
                    PQL.ErrorControlSystem.ErrorMessagesCore.MessageDictionary[(Byte)PQL.ErrorControlSystem.ErrorCodes.EMPTY_NAME_ERROR]);

            _Command = DBCommand.SET_SERVER_TYPE; 
        
        
        }


        public void SetServerPassword(ITransactionInfo ti) 
        {
            _Command = DBCommand.SET_SERVER_PASSWORD;
            _ti.Parameters = ti.Parameters;

        }



        public ITransactionMethodResult StartTransaction()
        {
            if (_Command == DBCommand.SET_SERVER_PASSWORD)
            {
                if (_Cripter == string.Empty)
                    return new TransactionMethodResult(
                        false,
                        Core.ErrorControlSystem.ErrorMessagesCore.MessageDictionary[(Byte)Core.ErrorControlSystem.ErrorCodes.CRIPTER_UNSET]);

                IEncriptor enc = Cript.ResolveEncriptorByName(_Cripter);
                if (enc == null)
                    return new TransactionMethodResult(
                        false,
                        Core.ErrorControlSystem.ErrorMessagesCore.MessageDictionary[(Byte)Core.ErrorControlSystem.ErrorCodes.CRIPTER_UNDEFINED]);

                _ti.AddedInfo = enc;

            }

            if (this._Command == DBCommand.SET_SERVER_TYPE && !_AllowedTypes.Contains(_ti.Parameters))
                return new TransactionMethodResult(
                    false,
                    Core.ErrorControlSystem.ErrorMessagesCore.MessageDictionary[(Byte)Core.ErrorControlSystem.ErrorCodes.SERVER_TYPE_UNDEFINED]);

            else if (_Command == DBCommand.SET_SERVER_TYPE && _AllowedTypes.Contains(_ti.Parameters)) 
                _ti.AddedInfo = _ti.Parameters == "LOCAL" ? ServerType.LOCAL : ServerType.CLUSTERED;
            

            if (_Command == DBCommand.SET_SERVER_NAME && _Result != null)
                return _Result;


            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] EXECUTE ADMINER COMMAND [{_Command}]");

            ITransactionMethodResult res = DBMSIO.ExecuteCommand(_Command, _ti);
            res.ResultString += CommandTracer.GetTracePath();
            return res;

        }




        public ITable EndTransaction() => null;


    }



   
}
