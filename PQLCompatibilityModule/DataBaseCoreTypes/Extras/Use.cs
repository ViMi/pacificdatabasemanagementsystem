﻿using DBMS.Core;
using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.PQL.Grammar;
using System.Collections.Generic;
using DBMS.Core.Helpers;


namespace DBMS.PQL
{
    public sealed class Use 
        : IPacificTransaction
    {


        #region Documentaion
#if DocTrue
        static Use() { }
#endif
        #endregion


        public List<System.String> OperateObjectName { set; get; } = new List<System.String>();
        public System.String CommandName { set; get; } = PQLGrammar.USE_COMMAND;
        public List<NodeType> OperateObectType { set; get; } = new List<NodeType>() { NodeType.DATABASE };


        ITransactionInfo _ti = new TransactionInfo();

        public void DataBase(ITransactionInfo ti) 
        {
            _ti.Parameters = ti.Parameters;
            _ti.UserName = ti.UserName;
            _ti.DataBaseName = ti.DataBaseName;

            OperateObjectName.Add(ti.DataBaseName);

        }

        public ITransactionMethodResult StartTransaction()
        {

            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] USE {_ti.Parameters} ");
            ITransactionMethodResult res = DBMSIO.ExecuteCommand(DBCommand.USE, _ti);
            res.ResultString += CommandTracer.GetTracePath();
            return res;
            
        }


        public ITable EndTransaction() => null;

    }
}
