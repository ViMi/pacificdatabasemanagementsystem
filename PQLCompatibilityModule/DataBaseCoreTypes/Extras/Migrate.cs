﻿using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.PQL.Grammar;
using System.Collections.Generic;
using DBMS.Core.Helpers;



namespace DBMS.PQL
{
    public sealed class Migrate 
        : IPacificTransaction
    {

        #region Documentation
#if DocTrue
        static Migrate() { }
#endif
        #endregion



        #region Variable Declaration
        public List<System.String> OperateObjectName { set; get; } =new List<System.String>() { DBMSIO.GetCurrentDataBase().GetDataBaseName() };
        public System.String CommandName { set; get; } = PQLGrammar.MIGRATE_COMMAND;
        public List<NodeType> OperateObectType { set; get; } = new List<NodeType>() { NodeType.DATABASE };


        private ITransactionInfo _ti = new TransactionInfo();
        private IMigrate _Migrate = new MigrateStatement();
        #endregion


        public void DataBase(ITransactionInfo ti) => _Migrate.SourcePoint = ti.Parameters;
        public void ToDiskStorage(ITransactionInfo ti)=> _Migrate.DestinationPoint = ti.Parameters;



        public ITransactionMethodResult StartTransaction()
        {

            _ti.AddedInfo = _Migrate;
            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] MIGRATE {_ti.Parameters}");
           
            ITransactionMethodResult res = DBMSIO.ExecuteCommand(DBCommand.MIGRATE, _ti);
            res.ResultString += CommandTracer.GetTracePath();
            return res;

        }

        public ITable EndTransaction() => null;

    }
}
