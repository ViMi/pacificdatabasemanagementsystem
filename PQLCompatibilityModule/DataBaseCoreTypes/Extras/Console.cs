﻿using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using DBMS.PQL.Grammar;
using System.Collections.Generic;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using System;
using DBMS.Core.Helpers;



namespace DBMS.PQL
{
    public sealed class Console
        : IPacificTransaction
    {


        #region Documentation
#if DocTrue
        static Console() { }
#endif
        #endregion


        #region Variable Declartion
        public List<String> OperateObjectName { get; set; } = new List<String>() { "TEXT" };
        public String CommandName { get; set; } = PQLGrammar.CONSOLE_COMMAND;
        public List<NodeType> OperateObectType { get; set; } = new List<NodeType>() { NodeType.UNSET };
            
        ITransactionInfo _ti = new TransactionInfo();
        #endregion


        #region One Line Methods
        public void Print(ITransactionInfo ti) => _ti.Parameters = ti.Parameters;
        public void NewLine(ITransactionInfo ti) => _ti.Parameters += Environment.NewLine;
        public void Clear(ITransactionInfo ti) => DBMSIO.ExecuteEvent(DBMSStandartEventNames.ClearConsole);
        #endregion

        public ITable EndTransaction() => null;

        public ITransactionMethodResult StartTransaction()
        {

            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName} SET A CONSOLE STRING ");
            return   new TransactionMethodResult(true, _ti.Parameters);

        }
    }
}
