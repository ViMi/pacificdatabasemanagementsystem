﻿using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core;
using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using DBMS.Core.Table;
using DBMS.PQL;
using DBMS.PQL.Grammar;
using DBMS.Core.Interprater;
using DBMS.Formatter;
using DBMS.PQL.TransactionInformation;

namespace DBMS.PQL
{
    public sealed class For
        : IPacificTransaction
    {
        public List<System.String> OperateObjectName { get; set; } = new List<System.String>() { PQLGrammar.OBJ_TYPE };
        public System.String CommandName { get; set; } = PQLGrammar.FOR_COMMAND;
        public List<NodeType> OperateObectType { get; set; } = new List<NodeType>() { NodeType.UNSET };


        private Int32 _Step = 1;
        private Int32 _From = 0;
        private Int32 _To = 0;
        private System.String _Content = System.String.Empty;

        public void From(ITransactionInfo ti)
        {
            if (Int32.TryParse(ti.Parameters, out _))
                _From = Int32.Parse(ti.Parameters);
        
        }
        public void To(ITransactionInfo ti) 
        {

            if (Int32.TryParse(ti.Parameters, out _))
                _To = Int32.Parse(ti.Parameters);
        

        }

        public void WithStep(ITransactionInfo ti) 
        {
            if (Int32.TryParse(ti.Parameters, out _))
                _Step = Int32.Parse(ti.Parameters);
        
        
        }
        public void Content(ITransactionInfo ti)
        {


            _Content = ti.Parameters.Trim()
            .Replace(PQLGrammar.LeftParameterBracetInner.ToString(), PQLGrammar.LeftParameterBracetStd.ToString())
            .Replace(PQLGrammar.RightParameterBracetInner.ToString(), PQLGrammar.RightParameterBracetStd.ToString())
            .Replace(PQLGrammar.TerminalSymboInner.ToString(), PQLGrammar.TerminalSymbol.ToString())
            .Replace(PQLGrammar.CommandsSeparatorInner.ToString(), PQLGrammar.CommandsSeparatorStd.ToString());


        }

        public ITable EndTransaction() => null;

        public ITransactionMethodResult StartTransaction()
        {
            String res = String.Empty;

            for (; _From < _To; _From += _Step)
                res += _Content + Environment.NewLine;

            IExecuteStrategy executer = new NoSQLExecuter(new NoSQlExecuteSecureStrategy(), new DBMS.Formatter.ExecuteResultNotFormat());
            IParseStrategy parser = new NoSQLParseModule(new NoSQLLanguageSecureStrategy(res), res);


            String resultJson = executer.Execute(parser.Execute()).GetExecuteResult();

            return new TransactionMethodResult(true, resultJson);

        }
    }
}
