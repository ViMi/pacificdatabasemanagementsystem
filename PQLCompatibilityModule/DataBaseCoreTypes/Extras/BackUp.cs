﻿using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using DBMS.PQL.Grammar;
using System.Collections.Generic;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.Core.Helpers;



namespace DBMS.PQL
{

    public sealed class BackUp
        : IPacificTransaction
    {

        #region Documentation
#if DocTrue
        static BackUp() { }
#endif
        #endregion


        #region Variable Declaration
        public List<System.String> OperateObjectName { set; get; } =new List<System.String>() { DBMSIO.GetCurrentDataBase().GetDataBaseName() };
        public System.String CommandName { set; get; } = PQLGrammar.BACKUP_COMMAND;
        public List<NodeType> OperateObectType { set; get; } = new List<NodeType>(){NodeType.DATABASE };

        private ITransactionInfo _ti = new TransactionInfo();

        private IBackUpInfo _BackUp = new BackUpInfo();
        #endregion




        public BackUp() => _BackUp.LogConfig = new LoggerInfo();


        public void CreateOn(ITransactionInfo ti)
        {
            _ti.UserName = DBMSIO.GetCurrentLogin().NodeName;
            _ti.DataBaseName = ti.Parameters;
            _BackUp.SourcePoint = ti.Parameters;

            OperateObjectName.Add(ti.Parameters);
        }
        public void CreateOnCurrent(ITransactionInfo ti)
        {
            _ti.UserName = DBMSIO.GetCurrentLogin().NodeName;
            _ti.DataBaseName = DBMSIO.GetCurrentDataBase().GetDataBaseName();
           _BackUp.SourcePoint = DBMSIO.GetCurrentDataBase().GetDataBaseName();

            OperateObjectName.Add(DBMSIO.GetCurrentDataBase().GetDataBaseName());
        }



        #region One Line Methods
        public void WithLog(ITransactionInfo ti) => _BackUp.LogConfig.Log = true;
        public void WithOutLog(ITransactionInfo ti) => _BackUp.LogConfig.Log = false;
        public void ToDiskStorage(ITransactionInfo ti) => _BackUp.DestinationPoint = ti.Parameters;
        #endregion


        public ITransactionMethodResult StartTransaction()
        {

            _ti.AddedInfo = _BackUp;
            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] CREATE BACKUP [{_ti.Parameters}]");

            ITransactionMethodResult res = DBMSIO.ExecuteCommand(DBCommand.BACKUP, _ti);
            res.ResultString += CommandTracer.GetTracePath();
            return res;

        }


        public ITable EndTransaction() => null;

    }

}
