﻿using System;
using System.Collections.Generic;
using System.Text;
using DBMS.Core;
using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using DBMS.Core.Table;
using DBMS.PQL;
using DBMS.PQL.Grammar;


using DBMS.Core.Interprater;
using DBMS.Formatter;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.Helpers.Tests;

namespace DBMS.PQL
{
    public sealed class While
        : IPacificTransaction
    {
        public List<System.String> OperateObjectName { get; set; } = new List<System.String>() { PQLGrammar.OBJ_TYPE };
        public System.String CommandName { get; set; } = PQLGrammar.WHILE_COMMAND;
        public List<NodeType> OperateObectType { get; set; } = new List<NodeType>() { NodeType.UNSET };

        private System.Int32 _Step = 1;
        private System.Int32 _Counter = 0;
        private System.Int32 _EndPoint = 0;
        private System.String _Content = System.String.Empty;

        public void CounterNotEqu(ITransactionInfo ti) 
        {

            if (Int32.TryParse(ti.Parameters, out _))
                _EndPoint = Int32.Parse(ti.Parameters);
            
        
        }
        public void WithStep(ITransactionInfo ti)
        {
            if (Int32.TryParse(ti.Parameters, out _))
                _Step = Int32.Parse(ti.Parameters);
        }
        public void Content(ITransactionInfo ti) {
        
            _Content = ti.Parameters.Trim()
                    .Replace(PQLGrammar.LeftParameterBracetInner.ToString(), PQLGrammar.LeftParameterBracetStd.ToString())
                    .Replace(PQLGrammar.RightParameterBracetInner.ToString(), PQLGrammar.RightParameterBracetStd.ToString())
                    .Replace(PQLGrammar.TerminalSymboInner.ToString(), PQLGrammar.TerminalSymbol.ToString())
                    .Replace(PQLGrammar.CommandsSeparatorInner.ToString(), PQLGrammar.CommandsSeparatorStd.ToString());

        }

        public ITable EndTransaction() => null;

        public ITransactionMethodResult StartTransaction()
        {
            String res = System.String.Empty;

            String values = _Content.Split('.')[2].Replace(";", "");
            res += _Content.Replace(";","");
            while(_Counter != _EndPoint)
            {
                res += "." + values;
                _Counter++;
            }
            res += ";";

            IExecuteStrategy executer = new NoSQLExecuter(new NoSQlExecuteSecureStrategy(),new DBMS.Formatter.ExecuteResultNotFormat());
            IParseStrategy parser = new NoSQLParseModule(new NoSQLLanguageSecureStrategy(res), res);

            TestTimer.InitNewTimer();

            String resultJson = executer.Execute(parser.Execute()).GetExecuteResult();

            TestTimer.StopTimer();
            TimeSpan resSpan = TestTimer.GetResultTime();
            resultJson += $"{Environment.NewLine}Time Passed -->  ml:{resSpan.Milliseconds}  sec:{resSpan.Seconds}  min:{resSpan.Minutes} hr:{resSpan.Hours} {Environment.NewLine}";


            return new TransactionMethodResult(true, resultJson);
        }
    }
}
