﻿using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using DBMS.Core.IO;
using DBMS.Formatter;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;
using DBMS.Core.ErrorControlSystem;

using DBMS.PQL.Grammar;
using System.Collections.Generic;

using DBMS.Core.Helpers;


namespace DBMS.PQL
{
    public sealed class Execute 
        : IPacificTransaction
    {


        #region Documentatio
#if DocTrue
        static Execute() { }
#endif
        #endregion


        #region Varialbe Declaration
        public List<System.String> OperateObjectName { set; get; } = new List<System.String>();
        public System.String CommandName { set; get; } = PQLGrammar.EXECUTE_COMMAND;
        public List<NodeType> OperateObectType { set; get; } = new List<NodeType>() { NodeType.PROCEDURE };

        ITransactionMethodResult _Result;
        ITransactionInfo _ti = new TransactionInfo();
        #endregion




        public void Procedure(ITransactionInfo ti) 
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RueCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke(
                (System.Int32)PQLLangRuleID.RIGHT_NAME,
                new DBMS.PQL.TransactionInformation.TransactionInfo() { Parameters = ti.Parameters });
                if (!_Result.IsSuccess) return;
                #endregion
            }

            _ti.UserName = ti.UserName;
            _ti.DataBaseName = ti.DataBaseName;
            _ti.Parameters = ti.Parameters;

            OperateObjectName.Add(ti.Parameters);

        }


        public ITransactionMethodResult StartTransaction()
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
                if (!_Result.IsSuccess) return _Result;

            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}]  EXECUTE PROCEDURE {_ti.Parameters}");

            ITransactionMethodResult res = DBMSIO.ExecuteCommand(DBCommand.EXECUTE, _ti);
            res.ResultString += CommandTracer.GetTracePath();
            return res;
  
        }

        public ITable EndTransaction() => null;

    }
}
