﻿using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using DBMS.Core.MetaInfoTables;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;

using System;
using System.Collections.Generic;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.ErrorControlSystem;
using DBMS.PQL.Grammar;

using DBMS.Core.Helpers;
using DBMS.Core.ErrorControlSystem.Exeption;

namespace DBMS.PQL.DCL
{
    public sealed class Revoke 
        :IPacificTransaction
    {

        #region Documentation
#if DocTrue
        static Revoke() { }
#endif
        #endregion 

        #region Variable Definition
        public List<System.String> OperateObjectName { set; get; } = new List<System.String>();
        public System.String CommandName { set; get; } = PQLGrammar.REVOKE_COMMAND;
        public List<NodeType> OperateObectType { set; get; } = new List<NodeType>();


        private ITransactionMethodResult _Result = null;
        private ITransactionInfo _ti = new TransactionInfo();

        private DBCommand _Command = DBCommand.REVOKE_USER;
        private System.String _RightsList = System.String.Empty;
       
        private String _RightPacketSeparator = PQLGrammar.RightPackectSeparator.ToString();

        #endregion


        public void ToUser(ITransactionInfo ti)
        {
            _Command = DBCommand.REVOKE_USER;
            _ti.UserName = ti.UserName;
            _ti.DataBaseName = ti.DataBaseName;
            _ti.Parameters = ti.Parameters;

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                #endregion
            }

            OperateObectType.Add(NodeType.USER);
            OperateObjectName.Add(ti.Parameters);
        }

        public void ToLogin(ITransactionInfo ti)
        {
            _Command = DBCommand.REVOKE_LOGIN;
            _ti.UserName = ti.UserName;
            _ti.DataBaseName = ti.DataBaseName;
            _ti.Parameters = ti.Parameters;

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
               ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                #endregion
            }

            OperateObectType.Add(NodeType.LOGIN);
            OperateObjectName.Add(ti.Parameters);
        }


        public void Rights(ITransactionInfo ti) => _RightsList += ti.Parameters + _RightPacketSeparator;



        public ITransactionMethodResult StartTransaction()
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                if (_Result != null && _Result.IsSuccess) return _Result;
                #endregion
            }

            try
            {
                if (_Command == DBCommand.REVOKE_LOGIN)
                    _ti.AddedInfo = (new SchemasExecuter(null, null).Execute(new SchemasParser(_RightsList, null, null).Execute()) as ITransactionMethodResult).AddedInfo;
                else if (_Command == DBCommand.REVOKE_USER)
                    _ti.AddedInfo = (new RightExecuter(null, null).Execute(new RightParserDCL(_RightsList, null, null).Execute()));
            } catch (DBExeption ex)
            {
                return new TransactionMethodResult(false, ex.ServerMessageError);
            }


            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] REVOKE FOR {_Command} ON {_ti.Parameters}");


            ITransactionMethodResult res = DBMSIO.ExecuteCommand(_Command, _ti);
            res.ResultString += CommandTracer.GetTracePath();
            return res;

        }


        public ITable EndTransaction() => null;


    }
}
