﻿using DBMS.Core;
using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;

using DBMS.Core.Secure;


using System;
using System.Collections.Generic;
using DBMS.Core.ErrorControlSystem;
using DBMS.PQL.Grammar;

using DBMS.Core.Helpers;




namespace DBMS.PQL.DCL
{
    public sealed class ChangePassword
                : IPacificTransaction
    {

        #region Documentation
#if DocTrue
        static ChangePassword() { }
#endif
        #endregion


        #region Variable Declaration

        public List<System.String> OperateObjectName { set; get; } = new List<System.String>();
        public System.String CommandName { set; get; } = PQLGrammar.CHANGE_PASSWORD;
        public List<NodeType> OperateObectType { set; get; } = new List<NodeType>();

        private ITransactionMethodResult _Result = null;
        private String _PasswordStr = String.Empty;
        private ITransactionInfo _ti = new TransactionInfo();
        private DBCommand _Command = DBCommand.CHANGE_PASSWORD_TO_USER;
        private String _Cripter = String.Empty;
        #endregion

        public void ToUser(ITransactionInfo ti)
        {
            _Command = DBCommand.CHANGE_PASSWORD_TO_USER;
            _ti.UserName = ti.Parameters;
            _ti.DataBaseName = ti.DataBaseName;

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                #endregion
            }

            OperateObectType.Add(NodeType.USER);
            OperateObjectName.Add(ti.Parameters);
        }
        public void ToLogin(ITransactionInfo ti)
        {
            _Command = DBCommand.CHANGE_PASSWORD_TO_LOGIN;
            _ti.UserName = ti.Parameters;
            _ti.DataBaseName = ti.DataBaseName;

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                #endregion
            }

            OperateObectType.Add(NodeType.LOGIN);
            OperateObjectName.Add(ti.Parameters);

        }
        public void ToCurrentLogin(ITransactionInfo ti)
        {
            _Command = DBCommand.CHANGE_PASSWORD_TO_LOGIN;
            _ti.UserName = DBMSIO.GetCurrentLogin().NodeName;
            _ti.DataBaseName = ti.DataBaseName;

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                #endregion
            }


            OperateObectType.Add(NodeType.LOGIN);
            OperateObjectName.Add(ti.Parameters);
        }


        public void WithCriptStrategy(ITransactionInfo ti)=> _Cripter = ti.Parameters;
        public void SetPassword(ITransactionInfo ti) => _PasswordStr = ti.Parameters;


        public ITable EndTransaction() => null;

        public ITransactionMethodResult StartTransaction()
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                if (_Result != null && _Result.IsSuccess) return _Result;

                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                 ((Byte)PQLLangRuleID.ENCRIPTO_UNSET, new TransactionInfo() { Parameters = _Cripter });
                if (_Result.IsSuccess) return _Result;

                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                    ((Byte)PQLLangRuleID.PASSWORD_UNSET, new TransactionInfo() { Parameters = (_ti.Parameters = _PasswordStr) });
                if (_Result.IsSuccess) return _Result;

                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                  ((Byte)PQLLangRuleID.ENCRIPTO_CREATED, new TransactionInfo() { AddedInfo = (_ti.AddedInfo = Cript.ResolveEncriptorByName(_Cripter)) });
                if (!_Result.IsSuccess) return _Result;
                #endregion
            }

            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] CHANGE PASSWORD {_Command}");


            ITransactionMethodResult res = DBMSIO.ExecuteCommand(_Command, _ti);
            res.ResultString += CommandTracer.GetTracePath();
            return res;

           
        }


  

    }
}
