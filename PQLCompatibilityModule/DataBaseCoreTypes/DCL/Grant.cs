﻿using DBMS.Core;
using DBMS.Core.FileSystem.DataBaseCoreTypes.InfoTypes;
using DBMS.Core.InfoTypes;
using DBMS.Core.IO;
using DBMS.Core.Table;
using DBMS.PQL.TransactionInformation;
using TransactionInfo = DBMS.PQL.TransactionInformation.TransactionInfo;

using System;
using System.Collections.Generic;
using DBMS.Core.Secure.UserRights;
using DBMS.Core.ErrorControlSystem;
using DBMS.PQL.Grammar;

using DBMS.Core.Helpers;

using System.Linq;

using DBMS.Core.ErrorControlSystem.Exeption;

using DBMS.Core.UsersFromRoles;

namespace DBMS.PQL.DCL
{
    public sealed class Grant 
        :IPacificTransaction
    {


        #region Documentation
#if DocTrue
        static Grant() { }
#endif
        #endregion



        #region Variable Definition
        public List<System.String> OperateObjectName { set; get; } = new List<System.String>();
        public System.String CommandName { set; get; } = PQLGrammar.GRANT_COMMAND;
        public List<NodeType> OperateObectType { set; get; } = new List<NodeType>();


        private ITransactionMethodResult _Result = null;

        private ITransactionInfo _ti = new TransactionInfo();
        private DBCommand _Command = DBCommand.DENY_USER;
        private System.String _RightsList = System.String.Empty;

        private String _RightPacketSeparator = PQLGrammar.RightPackectSeparator.ToString();
        #endregion

        public void ToUser(ITransactionInfo ti)
        {
            _Command = DBCommand.GRANT_USER;
            _ti.UserName = ti.UserName;
            _ti.DataBaseName = ti.DataBaseName;
            _ti.Parameters = ti.Parameters;

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
            ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                #endregion
            }

            OperateObectType.Add(NodeType.USER);
            OperateObjectName.Add(ti.Parameters);
        }

        public void ToLogin(ITransactionInfo ti)
        {

            _Command = DBCommand.GRANT_LOGIN;
            _ti.UserName = ti.UserName;
            _ti.DataBaseName = ti.DataBaseName;
            _ti.Parameters = ti.Parameters;

            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RuleCheck
                _Result = PQLGrammar.PQLLanRulesManager.Invoke
                        ((Byte)PQLLangRuleID.MULTIPLY_ENTRY, new TransactionInfo() { AddedInfo = new PQLCommandInfo(OperateObjectName, CommandName, OperateObectType) });
                if (_Result.IsSuccess) return;
                #endregion
            }

            OperateObectType.Add(NodeType.LOGIN);
            OperateObjectName.Add(ti.Parameters);
        }

        public void Rights(ITransactionInfo ti) => _RightsList += ti.Parameters + _RightPacketSeparator;

        public ITransactionMethodResult StartTransaction()
        {
            if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
            {
                #region RucleCheck
                if (_Result != null && _Result.IsSuccess) return _Result;
                #endregion
            }

            try
            {
                if (_Command == DBCommand.GRANT_LOGIN)
                    _ti.AddedInfo = (new SchemasExecuter(null, null).Execute(new SchemasParser(_RightsList, null, null).Execute()) as ITransactionMethodResult).AddedInfo;
                else if (_Command == DBCommand.GRANT_USER)
                {
                    _ti.AddedInfo = (new RightExecuter(null, null).Execute(new RightParserDCL(_RightsList, null, null).Execute()));
                    if (DBMSIO.GetDBMSMode() != DBMSMode.UNSAFE_MODE)
                        this.AddShemasToLogin();
                }



            }
            catch (DBExeption ex)
            {
                return new TransactionMethodResult(false, ex.ServerMessageError);
            }

            DBMSIO.Logger?.AppendInfoToLog($"USER [{DBMSIO.GetCurrentLogin().NodeName}] GRANT FOR {_Command} ON {_ti.Parameters}");

            ITransactionMethodResult res = DBMSIO.ExecuteCommand(_Command, _ti);
            res.ResultString += CommandTracer.GetTracePath();
            return res;

      
        }

        public ITable EndTransaction() => null;



        private void AddShemasToLogin()
        {


            #region Schemas Login Grant                
            AllowedCommand allowed = _ti.AddedInfo as AllowedCommand;
            IUserToken user = DBMSIO.GetCurrentDataBase().GetUserByName(_ti.Parameters);
            ILogin loginToken = DBMSIO.GetLogin(user.Owner);

            if (allowed.AllowCommand.ContainsKey(PQLGrammar.DELETE_COMMAND)
                || allowed.AllowCommand.ContainsKey(PQLGrammar.UPDATE_COMMAND)
                || allowed.AllowCommand.ContainsKey(PQLGrammar.INSERT_COMMAND))
            {
                CommandRight cr;
                if (allowed.AllowCommand.ContainsKey(PQLGrammar.DELETE_COMMAND))
                    if (allowed.AllowCommand.TryGetValue(PQLGrammar.DELETE_COMMAND, out cr))
                    {
                        for (System.Int32 i = 0; i < cr.CommandRights.Count; ++i)
                        {
                            String ObjName = cr.CommandRights.ElementAt(i).Key;
                            Boolean status = cr.CommandRights.ElementAt(i).Value;
                            if (status)
                                if (!DBMSIO.GetCurrentDataBase().IsTableExist(ObjName))
                                    throw new Exception();
                        }
                    }


                if (allowed.AllowCommand.ContainsKey(PQLGrammar.UPDATE_COMMAND))
                {
                    if (allowed.AllowCommand.TryGetValue(PQLGrammar.UPDATE_COMMAND, out cr))
                    {
                        for (System.Int32 i = 0; i < cr.CommandRights.Count; ++i)
                        {
                            String ObjName = cr.CommandRights.ElementAt(i).Key;
                            Boolean status = cr.CommandRights.ElementAt(i).Value;
                            if (status)
                                if (!DBMSIO.GetCurrentDataBase().IsTableExist(ObjName))
                                    throw new Exception();
                        }
                    }
                }


                if (allowed.AllowCommand.ContainsKey(PQLGrammar.INSERT_COMMAND))
                {
                    if (allowed.AllowCommand.TryGetValue(PQLGrammar.INSERT_COMMAND, out cr))
                    {
                        for (System.Int32 i = 0; i < cr.CommandRights.Count; ++i)
                        {
                            String ObjName = cr.CommandRights.ElementAt(i).Key;
                            Boolean status = cr.CommandRights.ElementAt(i).Value;
                            if (status)
                                if (!DBMSIO.GetCurrentDataBase().IsTableExist(ObjName))
                                    throw new Exception();
                        }
                    }
                }

            }


            if (allowed.AllowCommand.ContainsKey(PQLGrammar.SELECT_COMMAND))
            {
                CommandRight cr;
                if (allowed.AllowCommand.TryGetValue(PQLGrammar.SELECT_COMMAND, out cr))
                {
                    for (System.Int32 i = 0; i < cr.CommandRights.Count; ++i)
                    {
                        String objName = cr.CommandRights.ElementAt(i).Key;
                        Boolean status = cr.CommandRights.ElementAt(i).Value;

                        if (status)
                            if (!DBMSIO.GetCurrentDataBase().IsTableExist(objName) && !DBMSIO.GetCurrentDataBase().IsViewExist(objName))
                                throw new Exception();
                          
                        
                    }
                }

            }


            if (allowed.AllowCommand.ContainsKey(PQLGrammar.DROP_COMMAND)

                || allowed.AllowCommand.ContainsKey(PQLGrammar.CREATE_COMMAND))
            {

                CommandRight cr;
              



                if (allowed.AllowCommand.ContainsKey(PQLGrammar.CREATE_COMMAND))
                    if (allowed.AllowCommand.TryGetValue(PQLGrammar.CREATE_COMMAND, out cr))
                    {
                        for (System.Int32 i = 0; i < cr.CommandRights.Count; ++i)
                        {
                            String ObjName = cr.CommandRights.ElementAt(i).Key;
                            Boolean status = cr.CommandRights.ElementAt(i).Value;
                            if (status)
                            {
                                if (ObjName != "[TABLE]"
                                    && ObjName != "[VIEW]"
                                    && ObjName != "[PROCEDURE]"
                                    && ObjName != "[TRIGGER]"
                                    && ObjName != "[LOGIN]"
                                    && ObjName != "[USER]"
                                    && ObjName != "[DATABASE]")
                                {
                                    throw new Exception();
                                }
                            }
                        }
                    }

         


            }




            if (allowed.AllowCommand.ContainsKey(PQLGrammar.EXECUTE_COMMAND))
            {
                CommandRight cr;
                if (allowed.AllowCommand.TryGetValue(PQLGrammar.EXECUTE_COMMAND, out cr))
                {
                    for (System.Int32 i = 0; i < cr.CommandRights.Count; ++i)
                    {
                        String objName = cr.CommandRights.ElementAt(i).Key;
                        Boolean status = cr.CommandRights.ElementAt(i).Value;

                        if (status)
                            if (!DBMSIO.GetCurrentDataBase().IsProcedureExist(objName))
                                throw new Exception();

                        
                    }
                }
            }


            if (allowed.AllowCommand.ContainsKey(PQLGrammar.MIGRATE_COMMAND)
                || allowed.AllowCommand.ContainsKey(PQLGrammar.BACKUP_COMMAND))
            {
                CommandRight cr;
                if (allowed.AllowCommand.TryGetValue(PQLGrammar.BACKUP_COMMAND, out cr))
                {
                    for (System.Int32 i = 0; i < cr.CommandRights.Count; ++i)
                    {
                        String objName = cr.CommandRights.ElementAt(i).Key;
                        Boolean status = cr.CommandRights.ElementAt(i).Value;

                        if (status)
                            if (!DBMSIO.IsDataBaseExist(objName))
                                throw new Exception();
                        
                    }
                }


                if (allowed.AllowCommand.TryGetValue(PQLGrammar.MIGRATE_COMMAND, out cr))
                {
                    for (System.Int32 i = 0; i < cr.CommandRights.Count; ++i)
                    {
                        String objName = cr.CommandRights.ElementAt(i).Key;
                        Boolean status = cr.CommandRights.ElementAt(i).Value;

                        if (status)
                            if (!DBMSIO.IsDataBaseExist(objName))
                                throw new Exception();
                        
                    }
                }

                #endregion
            }

        }

    }
}
