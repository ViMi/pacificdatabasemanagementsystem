﻿using DBMS.Core.Interprater;
using DBMS.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Runtime.CompilerServices;
using DBMS.Core.InfoTypes;
using DBMS.Core.Helpers;


namespace DBMS.PQL.Extension
{
    public static class TypeReflectionExtension

    {

#if DocTrue
        static TypeReflectionExtension() { }
#endif

        public static T GetQueryNode<T>(this IParseStrategy parser, System.String typeName)
        {
           
            Type type = Type.GetType(typeName);
            ConstructorInfo constructor = type.GetConstructor(null);
            T queryObj = (T)constructor.Invoke(null);
            return queryObj;
        }




        /// <summary>
        /// This Method execute method by it`s name,
        /// type and parameters (WANRNING: This method execute constructor without parameters)
        /// </summary>
        /// <param name="strategy"></param>
        /// <param name="methodName"></param>
        /// <param name="parameters"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static System.Object InitMethod(
            this IExecuteStrategy strategy,
            System.String methodName, 
            ITransactionInfo info, 
            in Object obj,
            in Type context)
        {


            Object[] array = new object[1] { info };
            MethodInfo method = context.GetMethod(methodName);
            method.Invoke(obj, array);

            return null; 


        }




        public static System.Object InitEmptyConstructor(
            this IExecuteStrategy strategy,
            in Type context
            )
        {
            Object obj = null;

            ConstructorInfo[] constructors
                = context.GetConstructors();

            foreach (var item in constructors)
            {
                if (item.GetParameters().Length == 0)
                {
                    obj = item.Invoke(null);
                }
            }

            return obj;

        }


        public static System.Object InitExecutionMethod(
            this IExecuteStrategy strategy,
            System.String methodName, 
            in Type context,
            in Object obj)
        {

            MethodInfo method 
                = context.GetMethod(methodName);

            return method.Invoke(obj, null);

        }






        /// <summary>
        /// This Method execute method by it`s name, type and parameters
        /// </summary>
        /// <param name="strategy"></param>
        /// <param name="methodName"></param>
        /// <param name="parameters"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static System.Object InitMethod<T>
            (this IParseStrategy strategy, 
            System.String methodName,
            System.String parameters, 
            in T context)
        {

            Type contextType = context.GetType();
            Object[] array = new object[1] { parameters };
            return (contextType.GetMethod(methodName).Invoke(contextType, array));


        }




        /// <summary>
        /// This Method execute method by it`s name, type and parameters
        /// </summary>
        /// <param name="strategy"></param>
        /// <param name="methodName"></param>
        /// <param name="parameters"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static System.Object InitMethod
            (this IParseStrategy strategy, 
            System.String methodName, 
            System.String parameters, 
            System.String contextName)
        {

            Type contextType = Type.GetType(contextName);
            Object[] array = new object[1] { parameters };
            return (contextType.GetMethod(methodName).Invoke(contextType, array));


        }

    }
}
