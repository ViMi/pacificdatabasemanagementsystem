﻿using DBMS.Core.Interprater.SecureStrategy;
using System;
using System.Collections.Generic;
using DBMS.PQL.Grammar;
using DBMS.Core.Helpers;




namespace DBMS.PQL
{
    public sealed class NoSQLLanguageSecureStrategy
        : ILanguageSecureStrategy
    {


        #region Documentation
#if DocTrue
        static NoSQLLanguageSecureStrategy()
        {

        }

#endif
        #endregion



        private System.String _Listing;


        public NoSQLLanguageSecureStrategy(System.String listing) => this._Listing = listing;



        public Dictionary<System.String, HashSet<System.String>> GetCommandDictionary()=> PQLGrammar._Commands;
        public bool IsAbstractCommand(System.String command)=> PQLGrammar._Commands.ContainsKey(command);
        public bool IsConcreteCommand(System.String abstractCommand, System.String concreteCommand)
        {
            if (PQLGrammar._Commands.ContainsKey(abstractCommand))
            {
                HashSet<String> allCommands;
                PQLGrammar.Commands.TryGetValue(abstractCommand, out allCommands);
                if (allCommands.Contains(concreteCommand))
                    return true;

            }


            return false;
            

        }
        public System.String[] PrepareContextToTranslation(System.String context) => context.Split(PQLGrammar.CommandsSeparatorStd, (Char)StringSplitOptions.RemoveEmptyEntries);
        public System.String PrepareCommandToTranslation(System.String commandString) => commandString.Trim(' ').Replace(PQLGrammar.LeftParameterBracetStd.ToString(), " (");
        public System.String GetMethodNameFromString(System.String commandString) 
            => commandString.Split(' ', (Char)StringSplitOptions.RemoveEmptyEntries)[0];
        public System.String GetParameterFromString(System.String commandString) => commandString.Substring(commandString.IndexOf(' ')).Trim();

        public void RemoveOneLineComment(System.String startToken)
        {
            System.String[] str = _Listing.Replace(Environment.NewLine, "!").Split('!');
            System.Int32 length = str.Length;
            System.String newListing = System.String.Empty;

            for(System.Int32 i = 0; i < length; ++i)
            {
                if (str[i].StartsWith(PQLGrammar.OneLineComment.ToString()))
                    continue;

                newListing += str[i]; 
            }

            this._Listing = newListing;
        }

        public System.String[] PrepareListingToTranslation()
        {
            
           System.String[] str= _Listing.Replace(Environment.NewLine, System.String.Empty)
            .Replace("\t", String.Empty)
            .Replace("\n", String.Empty)
            .Trim()
            .Split(PQLGrammar.TerminalSymbol, (Char)StringSplitOptions.RemoveEmptyEntries);

            List<String> strCol = new List<String>(2);

            for (Int32 i = 0; i < str.Length; ++i)
                if (str[i].Length != 0)
                    strCol.Add(str[i]);
    


            System.String[] strRes = new System.String[strCol.Count];

            for(Int32 i =0; i < strCol.Count; ++i)  
                strRes[i]   = strCol[i];
            


            return strRes;
        }



        public Object Clone()=> new NoSQLLanguageSecureStrategy(this._Listing);


        /// <summary>
        /// It is not emplemented here always return false
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public Boolean IsType(System.String type) => false;


        /// <summary>
        /// It is not emplemented here always return false
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public Boolean IsAttribute(System.String attribute) => false;

        public System.String GetClassNameFromString(System.String commandStirng) { throw new NotImplementedException(); }
    }
}
